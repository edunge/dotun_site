/*
 *  Sugar Library v1.4.1
 *
 *  Freely distributable and licensed under the MIT-style license.
 *  Copyright (c) 2014 Andrew Plummer
 *  http://sugarjs.com/
 *
 * ---------------------------- */
(function(){
  /***
   * @package Core
   * @description Internal utility and common methods.
   ***/


  // A few optimizations for Google Closure Compiler will save us a couple kb in the release script.
  var object = Object, array = Array, regexp = RegExp, date = Date, string = String, number = Number, math = Math, Undefined;

  // The global context
  var globalContext = typeof global !== 'undefined' ? global : this;

  // Internal toString
  var internalToString = object.prototype.toString;

  // Internal hasOwnProperty
  var internalHasOwnProperty = object.prototype.hasOwnProperty;

  // defineProperty exists in IE8 but will error when trying to define a property on
  // native objects. IE8 does not have defineProperies, however, so this check saves a try/catch block.
  var definePropertySupport = object.defineProperty && object.defineProperties;

  // Are regexes type function?
  var regexIsFunction = typeof regexp() === 'function';

  // Do strings have no keys?
  var noKeysInStringObjects = !('0' in new string('a'));

  // Type check methods need a way to be accessed dynamically.
  var typeChecks = {};

  // Classes that can be matched by value
  var matchedByValueReg = /^\[object Date|Array|String|Number|RegExp|Boolean|Arguments\]$/;

  // Class initializers and class helpers
  var ClassNames = 'Boolean,Number,String,Array,Date,RegExp,Function'.split(',');

  var isBoolean  = buildPrimitiveClassCheck('boolean', ClassNames[0]);
  var isNumber   = buildPrimitiveClassCheck('number',  ClassNames[1]);
  var isString   = buildPrimitiveClassCheck('string',  ClassNames[2]);

  var isArray    = buildClassCheck(ClassNames[3]);
  var isDate     = buildClassCheck(ClassNames[4]);
  var isRegExp   = buildClassCheck(ClassNames[5]);


  // Wanted to enhance performance here by using simply "typeof"
  // but Firefox has two major issues that make this impossible,
  // one fixed, the other not. Despite being typeof "function"
  // the objects below still report in as [object Function], so
  // we need to perform a full class check here.
  //
  // 1. Regexes can be typeof "function" in FF < 3
  //    https://bugzilla.mozilla.org/show_bug.cgi?id=61911 (fixed)
  //
  // 2. HTMLEmbedElement and HTMLObjectElement are be typeof "function"
  //    https://bugzilla.mozilla.org/show_bug.cgi?id=268945 (won't fix)
  //
  var isFunction = buildClassCheck(ClassNames[6]);

  function isClass(obj, klass, cached) {
    var k = cached || className(obj);
    return k === '[object '+klass+']';
  }

  function buildClassCheck(klass) {
    var fn = (klass === 'Array' && array.isArray) || function(obj, cached) {
      return isClass(obj, klass, cached);
    };
    typeChecks[klass] = fn;
    return fn;
  }

  function buildPrimitiveClassCheck(type, klass) {
    var fn = function(obj) {
      if(isObjectType(obj)) {
        return isClass(obj, klass);
      }
      return typeof obj === type;
    }
    typeChecks[klass] = fn;
    return fn;
  }

  function className(obj) {
    return internalToString.call(obj);
  }

  function initializeClasses() {
    initializeClass(object);
    iterateOverObject(ClassNames, function(i,name) {
      initializeClass(globalContext[name]);
    });
  }

  function initializeClass(klass) {
    if(klass['SugarMethods']) return;
    defineProperty(klass, 'SugarMethods', {});
    extend(klass, false, true, {
      'extend': function(methods, override, instance) {
        extend(klass, instance !== false, override, methods);
      },
      'sugarRestore': function() {
        return batchMethodExecute(this, klass, arguments, function(target, name, m) {
          defineProperty(target, name, m.method);
        });
      },
      'sugarRevert': function() {
        return batchMethodExecute(this, klass, arguments, function(target, name, m) {
          if(m['existed']) {
            defineProperty(target, name, m['original']);
          } else {
            delete target[name];
          }
        });
      }
    });
  }

  // Class extending methods

  function extend(klass, instance, override, methods) {
    var extendee = instance ? klass.prototype : klass;
    initializeClass(klass);
    iterateOverObject(methods, function(name, extendedFn) {
      var nativeFn = extendee[name],
          existed  = hasOwnProperty(extendee, name);
      if(isFunction(override) && nativeFn) {
        extendedFn = wrapNative(nativeFn, extendedFn, override);
      }
      if(override !== false || !nativeFn) {
        defineProperty(extendee, name, extendedFn);
      }
      // If the method is internal to Sugar, then
      // store a reference so it can be restored later.
      klass['SugarMethods'][name] = {
        'method':   extendedFn,
        'existed':  existed,
        'original': nativeFn,
        'instance': instance
      };
    });
  }

  function extendSimilar(klass, instance, override, set, fn) {
    var methods = {};
    set = isString(set) ? set.split(',') : set;
    set.forEach(function(name, i) {
      fn(methods, name, i);
    });
    extend(klass, instance, override, methods);
  }

  function batchMethodExecute(target, klass, args, fn) {
    var all = args.length === 0, methods = multiArgs(args), changed = false;
    iterateOverObject(klass['SugarMethods'], function(name, m) {
      if(all || methods.indexOf(name) !== -1) {
        changed = true;
        fn(m['instance'] ? target.prototype : target, name, m);
      }
    });
    return changed;
  }

  function wrapNative(nativeFn, extendedFn, condition) {
    return function(a) {
      return condition.apply(this, arguments) ?
             extendedFn.apply(this, arguments) :
             nativeFn.apply(this, arguments);
    }
  }

  function defineProperty(target, name, method) {
    if(definePropertySupport) {
      object.defineProperty(target, name, {
        'value': method,
        'configurable': true,
        'enumerable': false,
        'writable': true
      });
    } else {
      target[name] = method;
    }
  }


  // Argument helpers

  function multiArgs(args, fn, from) {
    var result = [], i = from || 0, len;
    for(len = args.length; i < len; i++) {
      result.push(args[i]);
      if(fn) fn.call(args, args[i], i);
    }
    return result;
  }

  function flattenedArgs(args, fn, from) {
    var arg = args[from || 0];
    if(isArray(arg)) {
      args = arg;
      from = 0;
    }
    return multiArgs(args, fn, from);
  }

  function checkCallback(fn) {
    if(!fn || !fn.call) {
      throw new TypeError('Callback is not callable');
    }
  }


  // General helpers

  function isDefined(o) {
    return o !== Undefined;
  }

  function isUndefined(o) {
    return o === Undefined;
  }


  // Object helpers

  function hasProperty(obj, prop) {
    return !isPrimitiveType(obj) && prop in obj;
  }

  function hasOwnProperty(obj, prop) {
    return !!obj && internalHasOwnProperty.call(obj, prop);
  }

  function isObjectType(obj) {
    // 1. Check for null
    // 2. Check for regexes in environments where they are "functions".
    return !!obj && (typeof obj === 'object' || (regexIsFunction && isRegExp(obj)));
  }

  function isPrimitiveType(obj) {
    var type = typeof obj;
    return obj == null || type === 'string' || type === 'number' || type === 'boolean';
  }

  function isPlainObject(obj, klass) {
    klass = klass || className(obj);
    try {
      // Not own constructor property must be Object
      // This code was borrowed from jQuery.isPlainObject
      if (obj && obj.constructor &&
            !hasOwnProperty(obj, 'constructor') &&
            !hasOwnProperty(obj.constructor.prototype, 'isPrototypeOf')) {
        return false;
      }
    } catch (e) {
      // IE8,9 Will throw exceptions on certain host objects.
      return false;
    }
    // === on the constructor is not safe across iframes
    // 'hasOwnProperty' ensures that the object also inherits
    // from Object, which is false for DOMElements in IE.
    return !!obj && klass === '[object Object]' && 'hasOwnProperty' in obj;
  }

  function iterateOverObject(obj, fn) {
    var key;
    for(key in obj) {
      if(!hasOwnProperty(obj, key)) continue;
      if(fn.call(obj, key, obj[key], obj) === false) break;
    }
  }

  function simpleRepeat(n, fn) {
    for(var i = 0; i < n; i++) {
      fn(i);
    }
  }

  function simpleMerge(target, source) {
    iterateOverObject(source, function(key) {
      target[key] = source[key];
    });
    return target;
  }

   // Make primtives types like strings into objects.
   function coercePrimitiveToObject(obj) {
     if(isPrimitiveType(obj)) {
       obj = object(obj);
     }
     if(noKeysInStringObjects && isString(obj)) {
       forceStringCoercion(obj);
     }
     return obj;
   }

   // Force strings to have their indexes set in
   // environments that don't do this automatically.
   function forceStringCoercion(obj) {
     var i = 0, chr;
     while(chr = obj.charAt(i)) {
       obj[i++] = chr;
     }
   }

  // Hash definition

  function Hash(obj) {
    simpleMerge(this, coercePrimitiveToObject(obj));
  };

  Hash.prototype.constructor = object;

  // Math helpers

  var abs   = math.abs;
  var pow   = math.pow;
  var ceil  = math.ceil;
  var floor = math.floor;
  var round = math.round;
  var min   = math.min;
  var max   = math.max;

  function withPrecision(val, precision, fn) {
    var multiplier = pow(10, abs(precision || 0));
    fn = fn || round;
    if(precision < 0) multiplier = 1 / multiplier;
    return fn(val * multiplier) / multiplier;
  }

  // Full width number helpers

  var HalfWidthZeroCode = 0x30;
  var HalfWidthNineCode = 0x39;
  var FullWidthZeroCode = 0xff10;
  var FullWidthNineCode = 0xff19;

  var HalfWidthPeriod = '.';
  var FullWidthPeriod = '．';
  var HalfWidthComma  = ',';

  // Used here and later in the Date package.
  var FullWidthDigits   = '';

  var NumberNormalizeMap = {};
  var NumberNormalizeReg;

  function codeIsNumeral(code) {
    return (code >= HalfWidthZeroCode && code <= HalfWidthNineCode) ||
           (code >= FullWidthZeroCode && code <= FullWidthNineCode);
  }

  function buildNumberHelpers() {
    var digit, i;
    for(i = 0; i <= 9; i++) {
      digit = chr(i + FullWidthZeroCode);
      FullWidthDigits += digit;
      NumberNormalizeMap[digit] = chr(i + HalfWidthZeroCode);
    }
    NumberNormalizeMap[HalfWidthComma] = '';
    NumberNormalizeMap[FullWidthPeriod] = HalfWidthPeriod;
    // Mapping this to itself to easily be able to easily
    // capture it in stringToNumber to detect decimals later.
    NumberNormalizeMap[HalfWidthPeriod] = HalfWidthPeriod;
    NumberNormalizeReg = regexp('[' + FullWidthDigits + FullWidthPeriod + HalfWidthComma + HalfWidthPeriod + ']', 'g');
  }

  // String helpers

  function chr(num) {
    return string.fromCharCode(num);
  }

  // WhiteSpace/LineTerminator as defined in ES5.1 plus Unicode characters in the Space, Separator category.
  function getTrimmableCharacters() {
    return '\u0009\u000A\u000B\u000C\u000D\u0020\u00A0\u1680\u180E\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u2028\u2029\u3000\uFEFF';
  }

  function repeatString(str, num) {
    var result = '', str = str.toString();
    while (num > 0) {
      if (num & 1) {
        result += str;
      }
      if (num >>= 1) {
        str += str;
      }
    }
    return result;
  }

  // Returns taking into account full-width characters, commas, and decimals.
  function stringToNumber(str, base) {
    var sanitized, isDecimal;
    sanitized = str.replace(NumberNormalizeReg, function(chr) {
      var replacement = NumberNormalizeMap[chr];
      if(replacement === HalfWidthPeriod) {
        isDecimal = true;
      }
      return replacement;
    });
    return isDecimal ? parseFloat(sanitized) : parseInt(sanitized, base || 10);
  }


  // Used by Number and Date

  function padNumber(num, place, sign, base) {
    var str = abs(num).toString(base || 10);
    str = repeatString('0', place - str.replace(/\.\d+/, '').length) + str;
    if(sign || num < 0) {
      str = (num < 0 ? '-' : '+') + str;
    }
    return str;
  }

  function getOrdinalizedSuffix(num) {
    if(num >= 11 && num <= 13) {
      return 'th';
    } else {
      switch(num % 10) {
        case 1:  return 'st';
        case 2:  return 'nd';
        case 3:  return 'rd';
        default: return 'th';
      }
    }
  }


  // RegExp helpers

  function getRegExpFlags(reg, add) {
    var flags = '';
    add = add || '';
    function checkFlag(prop, flag) {
      if(prop || add.indexOf(flag) > -1) {
        flags += flag;
      }
    }
    checkFlag(reg.multiline, 'm');
    checkFlag(reg.ignoreCase, 'i');
    checkFlag(reg.global, 'g');
    checkFlag(reg.sticky, 'y');
    return flags;
  }

  function escapeRegExp(str) {
    if(!isString(str)) str = string(str);
    return str.replace(/([\\/\'*+?|()\[\]{}.^$])/g,'\\$1');
  }


  // Date helpers

  function callDateGet(d, method) {
    return d['get' + (d._utc ? 'UTC' : '') + method]();
  }

  function callDateSet(d, method, value) {
    return d['set' + (d._utc && method != 'ISOWeek' ? 'UTC' : '') + method](value);
  }

  // Used by Array#unique and Object.equal

  function stringify(thing, stack) {
    var type = typeof thing,
        thingIsObject,
        thingIsArray,
        klass, value,
        arr, key, i, len;

    // Return quickly if string to save cycles
    if(type === 'string') return thing;

    klass         = internalToString.call(thing)
    thingIsObject = isPlainObject(thing, klass);
    thingIsArray  = isArray(thing, klass);

    if(thing != null && thingIsObject || thingIsArray) {
      // This method for checking for cyclic structures was egregiously stolen from
      // the ingenious method by @kitcambridge from the Underscore script:
      // https://github.com/documentcloud/underscore/issues/240
      if(!stack) stack = [];
      // Allowing a step into the structure before triggering this
      // script to save cycles on standard JSON structures and also to
      // try as hard as possible to catch basic properties that may have
      // been modified.
      if(stack.length > 1) {
        i = stack.length;
        while (i--) {
          if (stack[i] === thing) {
            return 'CYC';
          }
        }
      }
      stack.push(thing);
      value = thing.valueOf() + string(thing.constructor);
      arr = thingIsArray ? thing : object.keys(thing).sort();
      for(i = 0, len = arr.length; i < len; i++) {
        key = thingIsArray ? i : arr[i];
        value += key + stringify(thing[key], stack);
      }
      stack.pop();
    } else if(1 / thing === -Infinity) {
      value = '-0';
    } else {
      value = string(thing && thing.valueOf ? thing.valueOf() : thing);
    }
    return type + klass + value;
  }

  function isEqual(a, b) {
    if(a === b) {
      // Return quickly up front when matching by reference,
      // but be careful about 0 !== -0.
      return a !== 0 || 1 / a === 1 / b;
    } else if(objectIsMatchedByValue(a) && objectIsMatchedByValue(b)) {
      return stringify(a) === stringify(b);
    }
    return false;
  }

  function objectIsMatchedByValue(obj) {
    // Only known objects are matched by value. This is notably excluding functions, DOM Elements, and instances of
    // user-created classes. The latter can arguably be matched by value, but distinguishing between these and
    // host objects -- which should never be compared by value -- is very tricky so not dealing with it here.
    var klass = className(obj);
    return matchedByValueReg.test(klass) || isPlainObject(obj, klass);
  }


  // Used by Array#at and String#at

  function getEntriesForIndexes(obj, args, isString) {
    var result,
        length    = obj.length,
        argsLen   = args.length,
        overshoot = args[argsLen - 1] !== false,
        multiple  = argsLen > (overshoot ? 1 : 2);
    if(!multiple) {
      return entryAtIndex(obj, length, args[0], overshoot, isString);
    }
    result = [];
    multiArgs(args, function(index) {
      if(isBoolean(index)) return false;
      result.push(entryAtIndex(obj, length, index, overshoot, isString));
    });
    return result;
  }

  function entryAtIndex(obj, length, index, overshoot, isString) {
    if(overshoot) {
      index = index % length;
      if(index < 0) index = length + index;
    }
    return isString ? obj.charAt(index) : obj[index];
  }


  // Object class methods implemented as instance methods

  function buildObjectInstanceMethods(set, target) {
    extendSimilar(target, true, false, set, function(methods, name) {
      methods[name + (name === 'equal' ? 's' : '')] = function() {
        return object[name].apply(null, [this].concat(multiArgs(arguments)));
      }
    });
  }

  initializeClasses();
  buildNumberHelpers();


  /***
   * @package Array
   * @dependency core
   * @description Array manipulation and traversal, "fuzzy matching" against elements, alphanumeric sorting and collation, enumerable methods on Object.
   *
   ***/


  function regexMatcher(reg) {
    reg = regexp(reg);
    return function (el) {
      return reg.test(el);
    }
  }

  function dateMatcher(d) {
    var ms = d.getTime();
    return function (el) {
      return !!(el && el.getTime) && el.getTime() === ms;
    }
  }

  function functionMatcher(fn) {
    return function (el, i, arr) {
      // Return true up front if match by reference
      return el === fn || fn.call(this, el, i, arr);
    }
  }

  function invertedArgsFunctionMatcher(fn) {
    return function (value, key, obj) {
      // Return true up front if match by reference
      return value === fn || fn.call(obj, key, value, obj);
    }
  }

  function fuzzyMatcher(obj, isObject) {
    var matchers = {};
    return function (el, i, arr) {
      var key;
      if(!isObjectType(el)) {
        return false;
      }
      for(key in obj) {
        matchers[key] = matchers[key] || getMatcher(obj[key], isObject);
        if(matchers[key].call(arr, el[key], i, arr) === false) {
          return false;
        }
      }
      return true;
    }
  }

  function defaultMatcher(f) {
    return function (el) {
      return el === f || isEqual(el, f);
    }
  }

  function getMatcher(f, isObject) {
    if(isPrimitiveType(f)) {
      // Do nothing and fall through to the
      // default matcher below.
    } else if(isRegExp(f)) {
      // Match against a regexp
      return regexMatcher(f);
    } else if(isDate(f)) {
      // Match against a date. isEqual below should also
      // catch this but matching directly up front for speed.
      return dateMatcher(f);
    } else if(isFunction(f)) {
      // Match against a filtering function
      if(isObject) {
        return invertedArgsFunctionMatcher(f);
      } else {
        return functionMatcher(f);
      }
    } else if(isPlainObject(f)) {
      // Match against a fuzzy hash or array.
      return fuzzyMatcher(f, isObject);
    }
    // Default is standard isEqual
    return defaultMatcher(f);
  }

  function transformArgument(el, map, context, mapArgs) {
    if(!map) {
      return el;
    } else if(map.apply) {
      return map.apply(context, mapArgs || []);
    } else if(isFunction(el[map])) {
      return el[map].call(el);
    } else {
      return el[map];
    }
  }

  // Basic array internal methods

  function arrayEach(arr, fn, startIndex, loop) {
    var index, i, length = +arr.length;
    if(startIndex < 0) startIndex = arr.length + startIndex;
    i = isNaN(startIndex) ? 0 : startIndex;
    if(loop === true) {
      length += i;
    }
    while(i < length) {
      index = i % arr.length;
      if(!(index in arr)) {
        return iterateOverSparseArray(arr, fn, i, loop);
      } else if(fn.call(arr, arr[index], index, arr) === false) {
        break;
      }
      i++;
    }
  }

  function iterateOverSparseArray(arr, fn, fromIndex, loop) {
    var indexes = [], i;
    for(i in arr) {
      if(isArrayIndex(arr, i) && i >= fromIndex) {
        indexes.push(parseInt(i));
      }
    }
    indexes.sort().each(function(index) {
      return fn.call(arr, arr[index], index, arr);
    });
    return arr;
  }

  function isArrayIndex(arr, i) {
    return i in arr && toUInt32(i) == i && i != 0xffffffff;
  }

  function toUInt32(i) {
    return i >>> 0;
  }

  function arrayFind(arr, f, startIndex, loop, returnIndex, context) {
    var result, index, matcher;
    if(arr.length > 0) {
      matcher = getMatcher(f);
      arrayEach(arr, function(el, i) {
        if(matcher.call(context, el, i, arr)) {
          result = el;
          index = i;
          return false;
        }
      }, startIndex, loop);
    }
    return returnIndex ? index : result;
  }

  function arrayUnique(arr, map) {
    var result = [], o = {}, transformed;
    arrayEach(arr, function(el, i) {
      transformed = map ? transformArgument(el, map, arr, [el, i, arr]) : el;
      if(!checkForElementInHashAndSet(o, transformed)) {
        result.push(el);
      }
    })
    return result;
  }

  function arrayIntersect(arr1, arr2, subtract) {
    var result = [], o = {};
    arr2.each(function(el) {
      checkForElementInHashAndSet(o, el);
    });
    arr1.each(function(el) {
      var stringified = stringify(el),
          isReference = !objectIsMatchedByValue(el);
      // Add the result to the array if:
      // 1. We're subtracting intersections or it doesn't already exist in the result and
      // 2. It exists in the compared array and we're adding, or it doesn't exist and we're removing.
      if(elementExistsInHash(o, stringified, el, isReference) !== subtract) {
        discardElementFromHash(o, stringified, el, isReference);
        result.push(el);
      }
    });
    return result;
  }

  function arrayFlatten(arr, level, current) {
    level = level || Infinity;
    current = current || 0;
    var result = [];
    arrayEach(arr, function(el) {
      if(isArray(el) && current < level) {
        result = result.concat(arrayFlatten(el, level, current + 1));
      } else {
        result.push(el);
      }
    });
    return result;
  }

  function isArrayLike(obj) {
    return hasProperty(obj, 'length') && !isString(obj) && !isPlainObject(obj);
  }

  function isArgumentsObject(obj) {
    // .callee exists on Arguments objects in < IE8
    return hasProperty(obj, 'length') && (className(obj) === '[object Arguments]' || !!obj.callee);
  }

  function flatArguments(args) {
    var result = [];
    multiArgs(args, function(arg) {
      result = result.concat(arg);
    });
    return result;
  }

  function elementExistsInHash(hash, key, element, isReference) {
    var exists = key in hash;
    if(isReference) {
      if(!hash[key]) {
        hash[key] = [];
      }
      exists = hash[key].indexOf(element) !== -1;
    }
    return exists;
  }

  function checkForElementInHashAndSet(hash, element) {
    var stringified = stringify(element),
        isReference = !objectIsMatchedByValue(element),
        exists      = elementExistsInHash(hash, stringified, element, isReference);
    if(isReference) {
      hash[stringified].push(element);
    } else {
      hash[stringified] = element;
    }
    return exists;
  }

  function discardElementFromHash(hash, key, element, isReference) {
    var arr, i = 0;
    if(isReference) {
      arr = hash[key];
      while(i < arr.length) {
        if(arr[i] === element) {
          arr.splice(i, 1);
        } else {
          i += 1;
        }
      }
    } else {
      delete hash[key];
    }
  }

  // Support methods

  function getMinOrMax(obj, map, which, all) {
    var el,
        key,
        edge,
        test,
        result = [],
        max = which === 'max',
        min = which === 'min',
        isArray = array.isArray(obj);
    for(key in obj) {
      if(!obj.hasOwnProperty(key)) continue;
      el   = obj[key];
      test = transformArgument(el, map, obj, isArray ? [el, parseInt(key), obj] : []);
      if(isUndefined(test)) {
        throw new TypeError('Cannot compare with undefined');
      }
      if(test === edge) {
        result.push(el);
      } else if(isUndefined(edge) || (max && test > edge) || (min && test < edge)) {
        result = [el];
        edge = test;
      }
    }
    if(!isArray) result = arrayFlatten(result, 1);
    return all ? result : result[0];
  }


  // Alphanumeric collation helpers

  function collateStrings(a, b) {
    var aValue, bValue, aChar, bChar, aEquiv, bEquiv, index = 0, tiebreaker = 0;

    var sortIgnore      = array[AlphanumericSortIgnore];
    var sortIgnoreCase  = array[AlphanumericSortIgnoreCase];
    var sortEquivalents = array[AlphanumericSortEquivalents];
    var sortOrder       = array[AlphanumericSortOrder];
    var naturalSort     = array[AlphanumericSortNatural];

    a = getCollationReadyString(a, sortIgnore, sortIgnoreCase);
    b = getCollationReadyString(b, sortIgnore, sortIgnoreCase);

    do {

      aChar  = getCollationCharacter(a, index, sortEquivalents);
      bChar  = getCollationCharacter(b, index, sortEquivalents);
      aValue = getSortOrderIndex(aChar, sortOrder);
      bValue = getSortOrderIndex(bChar, sortOrder);

      if(aValue === -1 || bValue === -1) {
        aValue = a.charCodeAt(index) || null;
        bValue = b.charCodeAt(index) || null;
        if(naturalSort && codeIsNumeral(aValue) && codeIsNumeral(bValue)) {
          aValue = stringToNumber(a.slice(index));
          bValue = stringToNumber(b.slice(index));
        }
      } else {
        aEquiv = aChar !== a.charAt(index);
        bEquiv = bChar !== b.charAt(index);
        if(aEquiv !== bEquiv && tiebreaker === 0) {
          tiebreaker = aEquiv - bEquiv;
        }
      }
      index += 1;
    } while(aValue != null && bValue != null && aValue === bValue);
    if(aValue === bValue) return tiebreaker;
    return aValue - bValue;
  }

  function getCollationReadyString(str, sortIgnore, sortIgnoreCase) {
    if(!isString(str)) str = string(str);
    if(sortIgnoreCase) {
      str = str.toLowerCase();
    }
    if(sortIgnore) {
      str = str.replace(sortIgnore, '');
    }
    return str;
  }

  function getCollationCharacter(str, index, sortEquivalents) {
    var chr = str.charAt(index);
    return sortEquivalents[chr] || chr;
  }

  function getSortOrderIndex(chr, sortOrder) {
    if(!chr) {
      return null;
    } else {
      return sortOrder.indexOf(chr);
    }
  }

  var AlphanumericSort            = 'AlphanumericSort';
  var AlphanumericSortOrder       = 'AlphanumericSortOrder';
  var AlphanumericSortIgnore      = 'AlphanumericSortIgnore';
  var AlphanumericSortIgnoreCase  = 'AlphanumericSortIgnoreCase';
  var AlphanumericSortEquivalents = 'AlphanumericSortEquivalents';
  var AlphanumericSortNatural     = 'AlphanumericSortNatural';



  function buildEnhancements() {
    var nativeMap = array.prototype.map;
    var callbackCheck = function() {
      var args = arguments;
      return args.length > 0 && !isFunction(args[0]);
    };
    extendSimilar(array, true, callbackCheck, 'every,all,some,filter,any,none,find,findIndex', function(methods, name) {
      var nativeFn = array.prototype[name]
      methods[name] = function(f) {
        var matcher = getMatcher(f);
        return nativeFn.call(this, function(el, index) {
          return matcher(el, index, this);
        });
      }
    });
    extend(array, true, callbackCheck, {
      'map': function(f) {
        return nativeMap.call(this, function(el, index) {
          return transformArgument(el, f, this, [el, index, this]);
        });
      }
    });
  }

  function buildAlphanumericSort() {
    var order = 'AÁÀÂÃĄBCĆČÇDĎÐEÉÈĚÊËĘFGĞHıIÍÌİÎÏJKLŁMNŃŇÑOÓÒÔPQRŘSŚŠŞTŤUÚÙŮÛÜVWXYÝZŹŻŽÞÆŒØÕÅÄÖ';
    var equiv = 'AÁÀÂÃÄ,CÇ,EÉÈÊË,IÍÌİÎÏ,OÓÒÔÕÖ,Sß,UÚÙÛÜ';
    array[AlphanumericSortOrder] = order.split('').map(function(str) {
      return str + str.toLowerCase();
    }).join('');
    var equivalents = {};
    arrayEach(equiv.split(','), function(set) {
      var equivalent = set.charAt(0);
      arrayEach(set.slice(1).split(''), function(chr) {
        equivalents[chr] = equivalent;
        equivalents[chr.toLowerCase()] = equivalent.toLowerCase();
      });
    });
    array[AlphanumericSortNatural] = true;
    array[AlphanumericSortIgnoreCase] = true;
    array[AlphanumericSortEquivalents] = equivalents;
  }

  extend(array, false, true, {

    /***
     *
     * @method Array.create(<obj1>, <obj2>, ...)
     * @returns Array
     * @short Alternate array constructor.
     * @extra This method will create a single array by calling %concat% on all arguments passed. In addition to ensuring that an unknown variable is in a single, flat array (the standard constructor will create nested arrays, this one will not), it is also a useful shorthand to convert a function's arguments object into a standard array.
     * @example
     *
     *   Array.create('one', true, 3)   -> ['one', true, 3]
     *   Array.create(['one', true, 3]) -> ['one', true, 3]
     +   Array.create(function(n) {
     *     return arguments;
     *   }('howdy', 'doody'));
     *
     ***/
    'create': function() {
      var result = [];
      multiArgs(arguments, function(a) {
        if(isArgumentsObject(a) || isArrayLike(a)) {
          a = array.prototype.slice.call(a, 0);
        }
        result = result.concat(a);
      });
      return result;
    }

  });

  extend(array, true, false, {

    /***
     * @method find(<f>, [context] = undefined)
     * @returns Mixed
     * @short Returns the first element that matches <f>.
     * @extra [context] is the %this% object if passed. When <f> is a function, will use native implementation if it exists. <f> will also match a string, number, array, object, or alternately test against a function or regex. This method implements @array_matching.
     * @example
     *
     +   [{a:1,b:2},{a:1,b:3},{a:1,b:4}].find(function(n) {
     *     return n['a'] == 1;
     *   });                                  -> {a:1,b:3}
     *   ['cuba','japan','canada'].find(/^c/) -> 'cuba'
     *
     ***/
    'find': function(f, context) {
      checkCallback(f);
      return arrayFind(this, f, 0, false, false, context);
    },

    /***
     * @method findIndex(<f>, [context] = undefined)
     * @returns Number
     * @short Returns the index of the first element that matches <f> or -1 if not found.
     * @extra [context] is the %this% object if passed. When <f> is a function, will use native implementation if it exists. <f> will also match a string, number, array, object, or alternately test against a function or regex. This method implements @array_matching.
     *
     * @example
     *
     +   [1,2,3,4].findIndex(function(n) {
     *     return n % 2 == 0;
     *   }); -> 1
     +   [1,2,3,4].findIndex(3);               -> 2
     +   ['one','two','three'].findIndex(/t/); -> 1
     *
     ***/
    'findIndex': function(f, context) {
      var index;
      checkCallback(f);
      index = arrayFind(this, f, 0, false, true, context);
      return isUndefined(index) ? -1 : index;
    }

  });

  extend(array, true, true, {

    /***
     * @method findFrom(<f>, [index] = 0, [loop] = false)
     * @returns Array
     * @short Returns any element that matches <f>, beginning from [index].
     * @extra <f> will match a string, number, array, object, or alternately test against a function or regex. Will continue from index = 0 if [loop] is true. This method implements @array_matching.
     * @example
     *
     *   ['cuba','japan','canada'].findFrom(/^c/, 2) -> 'canada'
     *
     ***/
    'findFrom': function(f, index, loop) {
      return arrayFind(this, f, index, loop);
    },

    /***
     * @method findIndexFrom(<f>, [index] = 0, [loop] = false)
     * @returns Array
     * @short Returns the index of any element that matches <f>, beginning from [index].
     * @extra <f> will match a string, number, array, object, or alternately test against a function or regex. Will continue from index = 0 if [loop] is true. This method implements @array_matching.
     * @example
     *
     *   ['cuba','japan','canada'].findIndexFrom(/^c/, 2) -> 2
     *
     ***/
    'findIndexFrom': function(f, index, loop) {
      var index = arrayFind(this, f, index, loop, true);
      return isUndefined(index) ? -1 : index;
    },

    /***
     * @method findAll(<f>, [index] = 0, [loop] = false)
     * @returns Array
     * @short Returns all elements that match <f>.
     * @extra <f> will match a string, number, array, object, or alternately test against a function or regex. Starts at [index], and will continue once from index = 0 if [loop] is true. This method implements @array_matching.
     * @example
     *
     +   [{a:1,b:2},{a:1,b:3},{a:2,b:4}].findAll(function(n) {
     *     return n['a'] == 1;
     *   });                                        -> [{a:1,b:3},{a:1,b:4}]
     *   ['cuba','japan','canada'].findAll(/^c/)    -> 'cuba','canada'
     *   ['cuba','japan','canada'].findAll(/^c/, 2) -> 'canada'
     *
     ***/
    'findAll': function(f, index, loop) {
      var result = [], matcher;
      if(this.length > 0) {
        matcher = getMatcher(f);
        arrayEach(this, function(el, i, arr) {
          if(matcher(el, i, arr)) {
            result.push(el);
          }
        }, index, loop);
      }
      return result;
    },

    /***
     * @method count(<f>)
     * @returns Number
     * @short Counts all elements in the array that match <f>.
     * @extra <f> will match a string, number, array, object, or alternately test against a function or regex. This method implements @array_matching.
     * @example
     *
     *   [1,2,3,1].count(1)       -> 2
     *   ['a','b','c'].count(/b/) -> 1
     +   [{a:1},{b:2}].count(function(n) {
     *     return n['a'] > 1;
     *   });                      -> 0
     *
     ***/
    'count': function(f) {
      if(isUndefined(f)) return this.length;
      return this.findAll(f).length;
    },

    /***
     * @method removeAt(<start>, [end])
     * @returns Array
     * @short Removes element at <start>. If [end] is specified, removes the range between <start> and [end]. This method will change the array! If you don't intend the array to be changed use %clone% first.
     * @example
     *
     *   ['a','b','c'].removeAt(0) -> ['b','c']
     *   [1,2,3,4].removeAt(1, 3)  -> [1]
     *
     ***/
    'removeAt': function(start, end) {
      if(isUndefined(start)) return this;
      if(isUndefined(end))   end = start;
      this.splice(start, end - start + 1);
      return this;
    },

    /***
     * @method include(<el>, [index])
     * @returns Array
     * @short Adds <el> to the array.
     * @extra This is a non-destructive alias for %add%. It will not change the original array.
     * @example
     *
     *   [1,2,3,4].include(5)       -> [1,2,3,4,5]
     *   [1,2,3,4].include(8, 1)    -> [1,8,2,3,4]
     *   [1,2,3,4].include([5,6,7]) -> [1,2,3,4,5,6,7]
     *
     ***/
    'include': function(el, index) {
      return this.clone().add(el, index);
    },

    /***
     * @method exclude([f1], [f2], ...)
     * @returns Array
     * @short Removes any element in the array that matches [f1], [f2], etc.
     * @extra This is a non-destructive alias for %remove%. It will not change the original array. This method implements @array_matching.
     * @example
     *
     *   [1,2,3].exclude(3)         -> [1,2]
     *   ['a','b','c'].exclude(/b/) -> ['a','c']
     +   [{a:1},{b:2}].exclude(function(n) {
     *     return n['a'] == 1;
     *   });                       -> [{b:2}]
     *
     ***/
    'exclude': function() {
      return array.prototype.remove.apply(this.clone(), arguments);
    },

    /***
     * @method clone()
     * @returns Array
     * @short Makes a shallow clone of the array.
     * @example
     *
     *   [1,2,3].clone() -> [1,2,3]
     *
     ***/
    'clone': function() {
      return simpleMerge([], this);
    },

    /***
     * @method unique([map] = null)
     * @returns Array
     * @short Removes all duplicate elements in the array.
     * @extra [map] may be a function mapping the value to be uniqued on or a string acting as a shortcut. This is most commonly used when you have a key that ensures the object's uniqueness, and don't need to check all fields. This method will also correctly operate on arrays of objects.
     * @example
     *
     *   [1,2,2,3].unique()                 -> [1,2,3]
     *   [{foo:'bar'},{foo:'bar'}].unique() -> [{foo:'bar'}]
     +   [{foo:'bar'},{foo:'bar'}].unique(function(obj){
     *     return obj.foo;
     *   }); -> [{foo:'bar'}]
     *   [{foo:'bar'},{foo:'bar'}].unique('foo') -> [{foo:'bar'}]
     *
     ***/
    'unique': function(map) {
      return arrayUnique(this, map);
    },

    /***
     * @method flatten([limit] = Infinity)
     * @returns Array
     * @short Returns a flattened, one-dimensional copy of the array.
     * @extra You can optionally specify a [limit], which will only flatten that depth.
     * @example
     *
     *   [[1], 2, [3]].flatten()      -> [1,2,3]
     *   [['a'],[],'b','c'].flatten() -> ['a','b','c']
     *
     ***/
    'flatten': function(limit) {
      return arrayFlatten(this, limit);
    },

    /***
     * @method union([a1], [a2], ...)
     * @returns Array
     * @short Returns an array containing all elements in all arrays with duplicates removed.
     * @extra This method will also correctly operate on arrays of objects.
     * @example
     *
     *   [1,3,5].union([5,7,9])     -> [1,3,5,7,9]
     *   ['a','b'].union(['b','c']) -> ['a','b','c']
     *
     ***/
    'union': function() {
      return arrayUnique(this.concat(flatArguments(arguments)));
    },

    /***
     * @method intersect([a1], [a2], ...)
     * @returns Array
     * @short Returns an array containing the elements all arrays have in common.
     * @extra This method will also correctly operate on arrays of objects.
     * @example
     *
     *   [1,3,5].intersect([5,7,9])   -> [5]
     *   ['a','b'].intersect('b','c') -> ['b']
     *
     ***/
    'intersect': function() {
      return arrayIntersect(this, flatArguments(arguments), false);
    },

    /***
     * @method subtract([a1], [a2], ...)
     * @returns Array
     * @short Subtracts from the array all elements in [a1], [a2], etc.
     * @extra This method will also correctly operate on arrays of objects.
     * @example
     *
     *   [1,3,5].subtract([5,7,9])   -> [1,3]
     *   [1,3,5].subtract([3],[5])   -> [1]
     *   ['a','b'].subtract('b','c') -> ['a']
     *
     ***/
    'subtract': function(a) {
      return arrayIntersect(this, flatArguments(arguments), true);
    },

    /***
     * @method at(<index>, [loop] = true)
     * @returns Mixed
     * @short Gets the element(s) at a given index.
     * @extra When [loop] is true, overshooting the end of the array (or the beginning) will begin counting from the other end. As an alternate syntax, passing multiple indexes will get the elements at those indexes.
     * @example
     *
     *   [1,2,3].at(0)        -> 1
     *   [1,2,3].at(2)        -> 3
     *   [1,2,3].at(4)        -> 2
     *   [1,2,3].at(4, false) -> null
     *   [1,2,3].at(-1)       -> 3
     *   [1,2,3].at(0,1)      -> [1,2]
     *
     ***/
    'at': function() {
      return getEntriesForIndexes(this, arguments);
    },

    /***
     * @method first([num] = 1)
     * @returns Mixed
     * @short Returns the first element(s) in the array.
     * @extra When <num> is passed, returns the first <num> elements in the array.
     * @example
     *
     *   [1,2,3].first()        -> 1
     *   [1,2,3].first(2)       -> [1,2]
     *
     ***/
    'first': function(num) {
      if(isUndefined(num)) return this[0];
      if(num < 0) num = 0;
      return this.slice(0, num);
    },

    /***
     * @method last([num] = 1)
     * @returns Mixed
     * @short Returns the last element(s) in the array.
     * @extra When <num> is passed, returns the last <num> elements in the array.
     * @example
     *
     *   [1,2,3].last()        -> 3
     *   [1,2,3].last(2)       -> [2,3]
     *
     ***/
    'last': function(num) {
      if(isUndefined(num)) return this[this.length - 1];
      var start = this.length - num < 0 ? 0 : this.length - num;
      return this.slice(start);
    },

    /***
     * @method from(<index>)
     * @returns Array
     * @short Returns a slice of the array from <index>.
     * @example
     *
     *   [1,2,3].from(1)  -> [2,3]
     *   [1,2,3].from(2)  -> [3]
     *
     ***/
    'from': function(num) {
      return this.slice(num);
    },

    /***
     * @method to(<index>)
     * @returns Array
     * @short Returns a slice of the array up to <index>.
     * @example
     *
     *   [1,2,3].to(1)  -> [1]
     *   [1,2,3].to(2)  -> [1,2]
     *
     ***/
    'to': function(num) {
      if(isUndefined(num)) num = this.length;
      return this.slice(0, num);
    },

    /***
     * @method min([map], [all] = false)
     * @returns Mixed
     * @short Returns the element in the array with the lowest value.
     * @extra [map] may be a function mapping the value to be checked or a string acting as a shortcut. If [all] is true, will return all min values in an array.
     * @example
     *
     *   [1,2,3].min()                          -> 1
     *   ['fee','fo','fum'].min('length')       -> 'fo'
     *   ['fee','fo','fum'].min('length', true) -> ['fo']
     +   ['fee','fo','fum'].min(function(n) {
     *     return n.length;
     *   });                              -> ['fo']
     +   [{a:3,a:2}].min(function(n) {
     *     return n['a'];
     *   });                              -> [{a:2}]
     *
     ***/
    'min': function(map, all) {
      return getMinOrMax(this, map, 'min', all);
    },

    /***
     * @method max([map], [all] = false)
     * @returns Mixed
     * @short Returns the element in the array with the greatest value.
     * @extra [map] may be a function mapping the value to be checked or a string acting as a shortcut. If [all] is true, will return all max values in an array.
     * @example
     *
     *   [1,2,3].max()                          -> 3
     *   ['fee','fo','fum'].max('length')       -> 'fee'
     *   ['fee','fo','fum'].max('length', true) -> ['fee']
     +   [{a:3,a:2}].max(function(n) {
     *     return n['a'];
     *   });                              -> {a:3}
     *
     ***/
    'max': function(map, all) {
      return getMinOrMax(this, map, 'max', all);
    },

    /***
     * @method least([map])
     * @returns Array
     * @short Returns the elements in the array with the least commonly occuring value.
     * @extra [map] may be a function mapping the value to be checked or a string acting as a shortcut.
     * @example
     *
     *   [3,2,2].least()                   -> [3]
     *   ['fe','fo','fum'].least('length') -> ['fum']
     +   [{age:35,name:'ken'},{age:12,name:'bob'},{age:12,name:'ted'}].least(function(n) {
     *     return n.age;
     *   });                               -> [{age:35,name:'ken'}]
     *
     ***/
    'least': function(map, all) {
      return getMinOrMax(this.groupBy.apply(this, [map]), 'length', 'min', all);
    },

    /***
     * @method most([map])
     * @returns Array
     * @short Returns the elements in the array with the most commonly occuring value.
     * @extra [map] may be a function mapping the value to be checked or a string acting as a shortcut.
     * @example
     *
     *   [3,2,2].most()                   -> [2]
     *   ['fe','fo','fum'].most('length') -> ['fe','fo']
     +   [{age:35,name:'ken'},{age:12,name:'bob'},{age:12,name:'ted'}].most(function(n) {
     *     return n.age;
     *   });                              -> [{age:12,name:'bob'},{age:12,name:'ted'}]
     *
     ***/
    'most': function(map, all) {
      return getMinOrMax(this.groupBy.apply(this, [map]), 'length', 'max', all);
    },

    /***
     * @method sum([map])
     * @returns Number
     * @short Sums all values in the array.
     * @extra [map] may be a function mapping the value to be summed or a string acting as a shortcut.
     * @example
     *
     *   [1,2,2].sum()                           -> 5
     +   [{age:35},{age:12},{age:12}].sum(function(n) {
     *     return n.age;
     *   });                                     -> 59
     *   [{age:35},{age:12},{age:12}].sum('age') -> 59
     *
     ***/
    'sum': function(map) {
      var arr = map ? this.map(map) : this;
      return arr.length > 0 ? arr.reduce(function(a,b) { return a + b; }) : 0;
    },

    /***
     * @method average([map])
     * @returns Number
     * @short Gets the mean average for all values in the array.
     * @extra [map] may be a function mapping the value to be averaged or a string acting as a shortcut.
     * @example
     *
     *   [1,2,3].average()                           -> 2
     +   [{age:35},{age:11},{age:11}].average(function(n) {
     *     return n.age;
     *   });                                         -> 19
     *   [{age:35},{age:11},{age:11}].average('age') -> 19
     *
     ***/
    'average': function(map) {
      var arr = map ? this.map(map) : this;
      return arr.length > 0 ? arr.sum() / arr.length : 0;
    },

    /***
     * @method inGroups(<num>, [padding])
     * @returns Array
     * @short Groups the array into <num> arrays.
     * @extra [padding] specifies a value with which to pad the last array so that they are all equal length.
     * @example
     *
     *   [1,2,3,4,5,6,7].inGroups(3)         -> [ [1,2,3], [4,5,6], [7] ]
     *   [1,2,3,4,5,6,7].inGroups(3, 'none') -> [ [1,2,3], [4,5,6], [7,'none','none'] ]
     *
     ***/
    'inGroups': function(num, padding) {
      var pad = arguments.length > 1;
      var arr = this;
      var result = [];
      var divisor = ceil(this.length / num);
      simpleRepeat(num, function(i) {
        var index = i * divisor;
        var group = arr.slice(index, index + divisor);
        if(pad && group.length < divisor) {
          simpleRepeat(divisor - group.length, function() {
            group = group.add(padding);
          });
        }
        result.push(group);
      });
      return result;
    },

    /***
     * @method inGroupsOf(<num>, [padding] = null)
     * @returns Array
     * @short Groups the array into arrays of <num> elements each.
     * @extra [padding] specifies a value with which to pad the last array so that they are all equal length.
     * @example
     *
     *   [1,2,3,4,5,6,7].inGroupsOf(4)         -> [ [1,2,3,4], [5,6,7] ]
     *   [1,2,3,4,5,6,7].inGroupsOf(4, 'none') -> [ [1,2,3,4], [5,6,7,'none'] ]
     *
     ***/
    'inGroupsOf': function(num, padding) {
      var result = [], len = this.length, arr = this, group;
      if(len === 0 || num === 0) return arr;
      if(isUndefined(num)) num = 1;
      if(isUndefined(padding)) padding = null;
      simpleRepeat(ceil(len / num), function(i) {
        group = arr.slice(num * i, num * i + num);
        while(group.length < num) {
          group.push(padding);
        }
        result.push(group);
      });
      return result;
    },

    /***
     * @method isEmpty()
     * @returns Boolean
     * @short Returns true if the array is empty.
     * @extra This is true if the array has a length of zero, or contains only %undefined%, %null%, or %NaN%.
     * @example
     *
     *   [].isEmpty()               -> true
     *   [null,undefined].isEmpty() -> true
     *
     ***/
    'isEmpty': function() {
      return this.compact().length == 0;
    },

    /***
     * @method sortBy(<map>, [desc] = false)
     * @returns Array
     * @short Sorts the array by <map>.
     * @extra <map> may be a function, a string acting as a shortcut, or blank (direct comparison of array values). [desc] will sort the array in descending order. When the field being sorted on is a string, the resulting order will be determined by an internal collation algorithm that is optimized for major Western languages, but can be customized. For more information see @array_sorting.
     * @example
     *
     *   ['world','a','new'].sortBy('length')       -> ['a','new','world']
     *   ['world','a','new'].sortBy('length', true) -> ['world','new','a']
     +   [{age:72},{age:13},{age:18}].sortBy(function(n) {
     *     return n.age;
     *   });                                        -> [{age:13},{age:18},{age:72}]
     *
     ***/
    'sortBy': function(map, desc) {
      var arr = this.clone();
      arr.sort(function(a, b) {
        var aProperty, bProperty, comp;
        aProperty = transformArgument(a, map, arr, [a]);
        bProperty = transformArgument(b, map, arr, [b]);
        if(isString(aProperty) && isString(bProperty)) {
          comp = collateStrings(aProperty, bProperty);
        } else if(aProperty < bProperty) {
          comp = -1;
        } else if(aProperty > bProperty) {
          comp = 1;
        } else {
          comp = 0;
        }
        return comp * (desc ? -1 : 1);
      });
      return arr;
    },

    /***
     * @method randomize()
     * @returns Array
     * @short Returns a copy of the array with the elements randomized.
     * @extra Uses Fisher-Yates algorithm.
     * @example
     *
     *   [1,2,3,4].randomize()  -> [?,?,?,?]
     *
     ***/
    'randomize': function() {
      var arr = this.concat(), i = arr.length, j, x;
      while(i) {
        j = (math.random() * i) | 0;
        x = arr[--i];
        arr[i] = arr[j];
        arr[j] = x;
      }
      return arr;
    },

    /***
     * @method zip([arr1], [arr2], ...)
     * @returns Array
     * @short Merges multiple arrays together.
     * @extra This method "zips up" smaller arrays into one large whose elements are "all elements at index 0", "all elements at index 1", etc. Useful when you have associated data that is split over separated arrays. If the arrays passed have more elements than the original array, they will be discarded. If they have fewer elements, the missing elements will filled with %null%.
     * @example
     *
     *   [1,2,3].zip([4,5,6])                                       -> [[1,2], [3,4], [5,6]]
     *   ['Martin','John'].zip(['Luther','F.'], ['King','Kennedy']) -> [['Martin','Luther','King'], ['John','F.','Kennedy']]
     *
     ***/
    'zip': function() {
      var args = multiArgs(arguments);
      return this.map(function(el, i) {
        return [el].concat(args.map(function(k) {
          return (i in k) ? k[i] : null;
        }));
      });
    },

    /***
     * @method sample([num])
     * @returns Mixed
     * @short Returns a random element from the array.
     * @extra If [num] is passed, will return [num] samples from the array.
     * @example
     *
     *   [1,2,3,4,5].sample()  -> // Random element
     *   [1,2,3,4,5].sample(3) -> // Array of 3 random elements
     *
     ***/
    'sample': function(num) {
      var arr = this.randomize();
      return arguments.length > 0 ? arr.slice(0, num) : arr[0];
    },

    /***
     * @method each(<fn>, [index] = 0, [loop] = false)
     * @returns Array
     * @short Runs <fn> against each element in the array. Enhanced version of %Array#forEach%.
     * @extra Parameters passed to <fn> are identical to %forEach%, ie. the first parameter is the current element, second parameter is the current index, and third parameter is the array itself. If <fn> returns %false% at any time it will break out of the loop. Once %each% finishes, it will return the array. If [index] is passed, <fn> will begin at that index and work its way to the end. If [loop] is true, it will then start over from the beginning of the array and continue until it reaches [index] - 1.
     * @example
     *
     *   [1,2,3,4].each(function(n) {
     *     // Called 4 times: 1, 2, 3, 4
     *   });
     *   [1,2,3,4].each(function(n) {
     *     // Called 4 times: 3, 4, 1, 2
     *   }, 2, true);
     *
     ***/
    'each': function(fn, index, loop) {
      arrayEach(this, fn, index, loop);
      return this;
    },

    /***
     * @method add(<el>, [index])
     * @returns Array
     * @short Adds <el> to the array.
     * @extra If [index] is specified, it will add at [index], otherwise adds to the end of the array. %add% behaves like %concat% in that if <el> is an array it will be joined, not inserted. This method will change the array! Use %include% for a non-destructive alias. Also, %insert% is provided as an alias that reads better when using an index.
     * @example
     *
     *   [1,2,3,4].add(5)       -> [1,2,3,4,5]
     *   [1,2,3,4].add([5,6,7]) -> [1,2,3,4,5,6,7]
     *   [1,2,3,4].insert(8, 1) -> [1,8,2,3,4]
     *
     ***/
    'add': function(el, index) {
      if(!isNumber(number(index)) || isNaN(index)) index = this.length;
      array.prototype.splice.apply(this, [index, 0].concat(el));
      return this;
    },

    /***
     * @method remove([f1], [f2], ...)
     * @returns Array
     * @short Removes any element in the array that matches [f1], [f2], etc.
     * @extra Will match a string, number, array, object, or alternately test against a function or regex. This method will change the array! Use %exclude% for a non-destructive alias. This method implements @array_matching.
     * @example
     *
     *   [1,2,3].remove(3)         -> [1,2]
     *   ['a','b','c'].remove(/b/) -> ['a','c']
     +   [{a:1},{b:2}].remove(function(n) {
     *     return n['a'] == 1;
     *   });                       -> [{b:2}]
     *
     ***/
    'remove': function() {
      var arr = this;
      multiArgs(arguments, function(f) {
        var i = 0, matcher = getMatcher(f);
        while(i < arr.length) {
          if(matcher(arr[i], i, arr)) {
            arr.splice(i, 1);
          } else {
            i++;
          }
        }
      });
      return arr;
    },

    /***
     * @method compact([all] = false)
     * @returns Array
     * @short Removes all instances of %undefined%, %null%, and %NaN% from the array.
     * @extra If [all] is %true%, all "falsy" elements will be removed. This includes empty strings, 0, and false.
     * @example
     *
     *   [1,null,2,undefined,3].compact() -> [1,2,3]
     *   [1,'',2,false,3].compact()       -> [1,'',2,false,3]
     *   [1,'',2,false,3].compact(true)   -> [1,2,3]
     *
     ***/
    'compact': function(all) {
      var result = [];
      arrayEach(this, function(el, i) {
        if(isArray(el)) {
          result.push(el.compact());
        } else if(all && el) {
          result.push(el);
        } else if(!all && el != null && el.valueOf() === el.valueOf()) {
          result.push(el);
        }
      });
      return result;
    },

    /***
     * @method groupBy(<map>, [fn])
     * @returns Object
     * @short Groups the array by <map>.
     * @extra Will return an object with keys equal to the grouped values. <map> may be a mapping function, or a string acting as a shortcut. Optionally calls [fn] for each group.
     * @example
     *
     *   ['fee','fi','fum'].groupBy('length') -> { 2: ['fi'], 3: ['fee','fum'] }
     +   [{age:35,name:'ken'},{age:15,name:'bob'}].groupBy(function(n) {
     *     return n.age;
     *   });                                  -> { 35: [{age:35,name:'ken'}], 15: [{age:15,name:'bob'}] }
     *
     ***/
    'groupBy': function(map, fn) {
      var arr = this, result = {}, key;
      arrayEach(arr, function(el, index) {
        key = transformArgument(el, map, arr, [el, index, arr]);
        if(!result[key]) result[key] = [];
        result[key].push(el);
      });
      if(fn) {
        iterateOverObject(result, fn);
      }
      return result;
    },

    /***
     * @method none(<f>)
     * @returns Boolean
     * @short Returns true if none of the elements in the array match <f>.
     * @extra <f> will match a string, number, array, object, or alternately test against a function or regex. This method implements @array_matching.
     * @example
     *
     *   [1,2,3].none(5)         -> true
     *   ['a','b','c'].none(/b/) -> false
     +   [{a:1},{b:2}].none(function(n) {
     *     return n['a'] > 1;
     *   });                     -> true
     *
     ***/
    'none': function() {
      return !this.any.apply(this, arguments);
    }


  });


  // Aliases

  extend(array, true, true, {

    /***
     * @method all()
     * @alias every
     *
     ***/
    'all': array.prototype.every,

    /*** @method any()
     * @alias some
     *
     ***/
    'any': array.prototype.some,

    /***
     * @method insert()
     * @alias add
     *
     ***/
    'insert': array.prototype.add

  });


  /***
   * Object module
   * Enumerable methods on objects
   *
   ***/

   function keysWithObjectCoercion(obj) {
     return object.keys(coercePrimitiveToObject(obj));
   }

  /***
   * @method [enumerable](<obj>)
   * @returns Boolean
   * @short Enumerable methods in the Array package are also available to the Object class. They will perform their normal operations for every property in <obj>.
   * @extra In cases where a callback is used, instead of %element, index%, the callback will instead be passed %key, value%. Enumerable methods are also available to extended objects as instance methods.
   *
   * @set
   *   each
   *   map
   *   any
   *   all
   *   none
   *   count
   *   find
   *   findAll
   *   reduce
   *   isEmpty
   *   sum
   *   average
   *   min
   *   max
   *   least
   *   most
   *
   * @example
   *
   *   Object.any({foo:'bar'}, 'bar')            -> true
   *   Object.extended({foo:'bar'}).any('bar')   -> true
   *   Object.isEmpty({})                        -> true
   +   Object.map({ fred: { age: 52 } }, 'age'); -> { fred: 52 }
   *
   ***/

  function buildEnumerableMethods(names, mapping) {
    extendSimilar(object, false, true, names, function(methods, name) {
      methods[name] = function(obj, arg1, arg2) {
        var result, coerced = keysWithObjectCoercion(obj), matcher;
        if(!mapping) {
          matcher = getMatcher(arg1, true);
        }
        result = array.prototype[name].call(coerced, function(key) {
          var value = obj[key];
          if(mapping) {
            return transformArgument(value, arg1, obj, [key, value, obj]);
          } else {
            return matcher(value, key, obj);
          }
        }, arg2);
        if(isArray(result)) {
          // The method has returned an array of keys so use this array
          // to build up the resulting object in the form we want it in.
          result = result.reduce(function(o, key, i) {
            o[key] = obj[key];
            return o;
          }, {});
        }
        return result;
      };
    });
    buildObjectInstanceMethods(names, Hash);
  }

  function exportSortAlgorithm() {
    array[AlphanumericSort] = collateStrings;
  }

  extend(object, false, true, {

    'map': function(obj, map) {
      var result = {}, key, value;
      for(key in obj) {
        if(!hasOwnProperty(obj, key)) continue;
        value = obj[key];
        result[key] = transformArgument(value, map, obj, [key, value, obj]);
      }
      return result;
    },

    'reduce': function(obj) {
      var values = keysWithObjectCoercion(obj).map(function(key) {
        return obj[key];
      });
      return values.reduce.apply(values, multiArgs(arguments, null, 1));
    },

    'each': function(obj, fn) {
      checkCallback(fn);
      iterateOverObject(obj, fn);
      return obj;
    },

    /***
     * @method size(<obj>)
     * @returns Number
     * @short Returns the number of properties in <obj>.
     * @extra %size% is available as an instance method on extended objects.
     * @example
     *
     *   Object.size({ foo: 'bar' }) -> 1
     *
     ***/
    'size': function (obj) {
      return keysWithObjectCoercion(obj).length;
    }

  });

  var EnumerableFindingMethods = 'any,all,none,count,find,findAll,isEmpty'.split(',');
  var EnumerableMappingMethods = 'sum,average,min,max,least,most'.split(',');
  var EnumerableOtherMethods   = 'map,reduce,size'.split(',');
  var EnumerableMethods        = EnumerableFindingMethods.concat(EnumerableMappingMethods).concat(EnumerableOtherMethods);

  buildEnhancements();
  buildAlphanumericSort();
  buildEnumerableMethods(EnumerableFindingMethods);
  buildEnumerableMethods(EnumerableMappingMethods, true);
  buildObjectInstanceMethods(EnumerableOtherMethods, Hash);
  exportSortAlgorithm();


  /***
   * @package Object
   * @dependency core
   * @description Object manipulation, type checking (isNumber, isString, ...), extended objects with hash-like methods available as instance methods.
   *
   * Much thanks to kangax for his informative aricle about how problems with instanceof and constructor
   * http://perfectionkills.com/instanceof-considered-harmful-or-how-to-write-a-robust-isarray/
   *
   ***/

  var ObjectTypeMethods = 'isObject,isNaN'.split(',');
  var ObjectHashMethods = 'keys,values,select,reject,each,merge,clone,equal,watch,tap,has,toQueryString'.split(',');

  function setParamsObject(obj, param, value, castBoolean) {
    var reg = /^(.+?)(\[.*\])$/, paramIsArray, match, allKeys, key;
    if(match = param.match(reg)) {
      key = match[1];
      allKeys = match[2].replace(/^\[|\]$/g, '').split('][');
      allKeys.forEach(function(k) {
        paramIsArray = !k || k.match(/^\d+$/);
        if(!key && isArray(obj)) key = obj.length;
        if(!hasOwnProperty(obj, key)) {
          obj[key] = paramIsArray ? [] : {};
        }
        obj = obj[key];
        key = k;
      });
      if(!key && paramIsArray) key = obj.length.toString();
      setParamsObject(obj, key, value, castBoolean);
    } else if(castBoolean && value === 'true') {
      obj[param] = true;
    } else if(castBoolean && value === 'false') {
      obj[param] = false;
    } else {
      obj[param] = value;
    }
  }

  function objectToQueryString(base, obj) {
    var tmp;
    // If a custom toString exists bail here and use that instead
    if(isArray(obj) || (isObjectType(obj) && obj.toString === internalToString)) {
      tmp = [];
      iterateOverObject(obj, function(key, value) {
        if(base) {
          key = base + '[' + key + ']';
        }
        tmp.push(objectToQueryString(key, value));
      });
      return tmp.join('&');
    } else {
      if(!base) return '';
      return sanitizeURIComponent(base) + '=' + (isDate(obj) ? obj.getTime() : sanitizeURIComponent(obj));
    }
  }

  function sanitizeURIComponent(obj) {
    // undefined, null, and NaN are represented as a blank string,
    // while false and 0 are stringified. "+" is allowed in query string
    return !obj && obj !== false && obj !== 0 ? '' : encodeURIComponent(obj).replace(/%20/g, '+');
  }

  function matchInObject(match, key, value) {
    if(isRegExp(match)) {
      return match.test(key);
    } else if(isObjectType(match)) {
      return match[key] === value;
    } else {
      return key === string(match);
    }
  }

  function selectFromObject(obj, args, select) {
    var match, result = obj instanceof Hash ? new Hash : {};
    iterateOverObject(obj, function(key, value) {
      match = false;
      flattenedArgs(args, function(arg) {
        if(matchInObject(arg, key, value)) {
          match = true;
        }
      }, 1);
      if(match === select) {
        result[key] = value;
      }
    });
    return result;
  }


  /***
   * @method Object.is[Type](<obj>)
   * @returns Boolean
   * @short Returns true if <obj> is an object of that type.
   * @extra %isObject% will return false on anything that is not an object literal, including instances of inherited classes. Note also that %isNaN% will ONLY return true if the object IS %NaN%. It does not mean the same as browser native %isNaN%, which returns true for anything that is "not a number".
   *
   * @set
   *   isArray
   *   isObject
   *   isBoolean
   *   isDate
   *   isFunction
   *   isNaN
   *   isNumber
   *   isString
   *   isRegExp
   *
   * @example
   *
   *   Object.isArray([1,2,3])            -> true
   *   Object.isDate(3)                   -> false
   *   Object.isRegExp(/wasabi/)          -> true
   *   Object.isObject({ broken:'wear' }) -> true
   *
   ***/
  function buildTypeMethods() {
    extendSimilar(object, false, true, ClassNames, function(methods, name) {
      var method = 'is' + name;
      ObjectTypeMethods.push(method);
      methods[method] = typeChecks[name];
    });
  }

  function buildObjectExtend() {
    extend(object, false, function(){ return arguments.length === 0; }, {
      'extend': function() {
        var methods = ObjectTypeMethods.concat(ObjectHashMethods)
        if(typeof EnumerableMethods !== 'undefined') {
          methods = methods.concat(EnumerableMethods);
        }
        buildObjectInstanceMethods(methods, object);
      }
    });
  }

  extend(object, false, true, {
      /***
       * @method watch(<obj>, <prop>, <fn>)
       * @returns Nothing
       * @short Watches a property of <obj> and runs <fn> when it changes.
       * @extra <fn> is passed three arguments: the property <prop>, the old value, and the new value. The return value of [fn] will be set as the new value. This method is useful for things such as validating or cleaning the value when it is set. Warning: this method WILL NOT work in browsers that don't support %Object.defineProperty% (IE 8 and below). This is the only method in Sugar that is not fully compatible with all browsers. %watch% is available as an instance method on extended objects.
       * @example
       *
       *   Object.watch({ foo: 'bar' }, 'foo', function(prop, oldVal, newVal) {
       *     // Will be run when the property 'foo' is set on the object.
       *   });
       *   Object.extended().watch({ foo: 'bar' }, 'foo', function(prop, oldVal, newVal) {
       *     // Will be run when the property 'foo' is set on the object.
       *   });
       *
       ***/
    'watch': function(obj, prop, fn) {
      if(!definePropertySupport) return;
      var value = obj[prop];
      object.defineProperty(obj, prop, {
        'enumerable'  : true,
        'configurable': true,
        'get': function() {
          return value;
        },
        'set': function(to) {
          value = fn.call(obj, prop, value, to);
        }
      });
    }
  });

  extend(object, false, function() { return arguments.length > 1; }, {

    /***
     * @method keys(<obj>, [fn])
     * @returns Array
     * @short Returns an array containing the keys in <obj>. Optionally calls [fn] for each key.
     * @extra This method is provided for browsers that don't support it natively, and additionally is enhanced to accept the callback [fn]. Returned keys are in no particular order. %keys% is available as an instance method on extended objects.
     * @example
     *
     *   Object.keys({ broken: 'wear' }) -> ['broken']
     *   Object.keys({ broken: 'wear' }, function(key, value) {
     *     // Called once for each key.
     *   });
     *   Object.extended({ broken: 'wear' }).keys() -> ['broken']
     *
     ***/
    'keys': function(obj, fn) {
      var keys = object.keys(obj);
      keys.forEach(function(key) {
        fn.call(obj, key, obj[key]);
      });
      return keys;
    }

  });

  extend(object, false, true, {

    'isObject': function(obj) {
      return isPlainObject(obj);
    },

    'isNaN': function(obj) {
      // This is only true of NaN
      return isNumber(obj) && obj.valueOf() !== obj.valueOf();
    },

    /***
     * @method equal(<a>, <b>)
     * @returns Boolean
     * @short Returns true if <a> and <b> are equal.
     * @extra %equal% in Sugar is "egal", meaning the values are equal if they are "not observably distinguishable". Note that on extended objects the name is %equals% for readability.
     * @example
     *
     *   Object.equal({a:2}, {a:2}) -> true
     *   Object.equal({a:2}, {a:3}) -> false
     *   Object.extended({a:2}).equals({a:3}) -> false
     *
     ***/
    'equal': function(a, b) {
      return isEqual(a, b);
    },

    /***
     * @method Object.extended(<obj> = {})
     * @returns Extended object
     * @short Creates a new object, equivalent to %new Object()% or %{}%, but with extended methods.
     * @extra See extended objects for more.
     * @example
     *
     *   Object.extended()
     *   Object.extended({ happy:true, pappy:false }).keys() -> ['happy','pappy']
     *   Object.extended({ happy:true, pappy:false }).values() -> [true, false]
     *
     ***/
    'extended': function(obj) {
      return new Hash(obj);
    },

    /***
     * @method merge(<target>, <source>, [deep] = false, [resolve] = true)
     * @returns Merged object
     * @short Merges all the properties of <source> into <target>.
     * @extra Merges are shallow unless [deep] is %true%. Properties of <source> will win in the case of conflicts, unless [resolve] is %false%. [resolve] can also be a function that resolves the conflict. In this case it will be passed 3 arguments, %key%, %targetVal%, and %sourceVal%, with the context set to <source>. This will allow you to solve conflict any way you want, ie. adding two numbers together, etc. %merge% is available as an instance method on extended objects.
     * @example
     *
     *   Object.merge({a:1},{b:2}) -> { a:1, b:2 }
     *   Object.merge({a:1},{a:2}, false, false) -> { a:1 }
     +   Object.merge({a:1},{a:2}, false, function(key, a, b) {
     *     return a + b;
     *   }); -> { a:3 }
     *   Object.extended({a:1}).merge({b:2}) -> { a:1, b:2 }
     *
     ***/
    'merge': function(target, source, deep, resolve) {
      var key, sourceIsObject, targetIsObject, sourceVal, targetVal, conflict, result;
      // Strings cannot be reliably merged thanks to
      // their properties not being enumerable in < IE8.
      if(target && typeof source !== 'string') {
        for(key in source) {
          if(!hasOwnProperty(source, key) || !target) continue;
          sourceVal      = source[key];
          targetVal      = target[key];
          conflict       = isDefined(targetVal);
          sourceIsObject = isObjectType(sourceVal);
          targetIsObject = isObjectType(targetVal);
          result         = conflict && resolve === false ? targetVal : sourceVal;

          if(conflict) {
            if(isFunction(resolve)) {
              // Use the result of the callback as the result.
              result = resolve.call(source, key, targetVal, sourceVal)
            }
          }

          // Going deep
          if(deep && (sourceIsObject || targetIsObject)) {
            if(isDate(sourceVal)) {
              result = new date(sourceVal.getTime());
            } else if(isRegExp(sourceVal)) {
              result = new regexp(sourceVal.source, getRegExpFlags(sourceVal));
            } else {
              if(!targetIsObject) target[key] = array.isArray(sourceVal) ? [] : {};
              object.merge(target[key], sourceVal, deep, resolve);
              continue;
            }
          }
          target[key] = result;
        }
      }
      return target;
    },

    /***
     * @method values(<obj>, [fn])
     * @returns Array
     * @short Returns an array containing the values in <obj>. Optionally calls [fn] for each value.
     * @extra Returned values are in no particular order. %values% is available as an instance method on extended objects.
     * @example
     *
     *   Object.values({ broken: 'wear' }) -> ['wear']
     *   Object.values({ broken: 'wear' }, function(value) {
     *     // Called once for each value.
     *   });
     *   Object.extended({ broken: 'wear' }).values() -> ['wear']
     *
     ***/
    'values': function(obj, fn) {
      var values = [];
      iterateOverObject(obj, function(k,v) {
        values.push(v);
        if(fn) fn.call(obj,v);
      });
      return values;
    },

    /***
     * @method clone(<obj> = {}, [deep] = false)
     * @returns Cloned object
     * @short Creates a clone (copy) of <obj>.
     * @extra Default is a shallow clone, unless [deep] is true. %clone% is available as an instance method on extended objects.
     * @example
     *
     *   Object.clone({foo:'bar'})            -> { foo: 'bar' }
     *   Object.clone()                       -> {}
     *   Object.extended({foo:'bar'}).clone() -> { foo: 'bar' }
     *
     ***/
    'clone': function(obj, deep) {
      var target, klass;
      if(!isObjectType(obj)) {
        return obj;
      }
      klass = className(obj);
      if(isDate(obj, klass) && obj.clone) {
        // Preserve internal UTC flag when applicable.
        return obj.clone();
      } else if(isDate(obj, klass) || isRegExp(obj, klass)) {
        return new obj.constructor(obj);
      } else if(obj instanceof Hash) {
        target = new Hash;
      } else if(isArray(obj, klass)) {
        target = [];
      } else if(isPlainObject(obj, klass)) {
        target = {};
      } else {
        throw new TypeError('Clone must be a basic data type.');
      }
      return object.merge(target, obj, deep);
    },

    /***
     * @method Object.fromQueryString(<str>, [booleans] = false)
     * @returns Object
     * @short Converts the query string of a URL into an object.
     * @extra If [booleans] is true, then %"true"% and %"false"% will be cast into booleans. All other values, including numbers will remain their string values.
     * @example
     *
     *   Object.fromQueryString('foo=bar&broken=wear') -> { foo: 'bar', broken: 'wear' }
     *   Object.fromQueryString('foo[]=1&foo[]=2')     -> { foo: ['1','2'] }
     *   Object.fromQueryString('foo=true', true)      -> { foo: true }
     *
     ***/
    'fromQueryString': function(str, castBoolean) {
      var result = object.extended(), split;
      str = str && str.toString ? str.toString() : '';
      str.replace(/^.*?\?/, '').split('&').forEach(function(p) {
        var split = p.split('=');
        if(split.length !== 2) return;
        setParamsObject(result, split[0], decodeURIComponent(split[1]), castBoolean);
      });
      return result;
    },

    /***
     * @method Object.toQueryString(<obj>, [namespace] = null)
     * @returns Object
     * @short Converts the object into a query string.
     * @extra Accepts deep nested objects and arrays. If [namespace] is passed, it will be prefixed to all param names.
     * @example
     *
     *   Object.toQueryString({foo:'bar'})          -> 'foo=bar'
     *   Object.toQueryString({foo:['a','b','c']})  -> 'foo[0]=a&foo[1]=b&foo[2]=c'
     *   Object.toQueryString({name:'Bob'}, 'user') -> 'user[name]=Bob'
     *
     ***/
    'toQueryString': function(obj, namespace) {
      return objectToQueryString(namespace, obj);
    },

    /***
     * @method tap(<obj>, <fn>)
     * @returns Object
     * @short Runs <fn> and returns <obj>.
     * @extra  A string can also be used as a shortcut to a method. This method is used to run an intermediary function in the middle of method chaining. As a standalone method on the Object class it doesn't have too much use. The power of %tap% comes when using extended objects or modifying the Object prototype with Object.extend().
     * @example
     *
     *   Object.extend();
     *   [2,4,6].map(Math.exp).tap(function(arr) {
     *     arr.pop()
     *   });
     *   [2,4,6].map(Math.exp).tap('pop').map(Math.round); ->  [7,55]
     *
     ***/
    'tap': function(obj, arg) {
      var fn = arg;
      if(!isFunction(arg)) {
        fn = function() {
          if(arg) obj[arg]();
        }
      }
      fn.call(obj, obj);
      return obj;
    },

    /***
     * @method has(<obj>, <key>)
     * @returns Boolean
     * @short Checks if <obj> has <key> using hasOwnProperty from Object.prototype.
     * @extra This method is considered safer than %Object#hasOwnProperty% when using objects as hashes. See http://www.devthought.com/2012/01/18/an-object-is-not-a-hash/ for more.
     * @example
     *
     *   Object.has({ foo: 'bar' }, 'foo') -> true
     *   Object.has({ foo: 'bar' }, 'baz') -> false
     *   Object.has({ hasOwnProperty: true }, 'foo') -> false
     *
     ***/
    'has': function (obj, key) {
      return hasOwnProperty(obj, key);
    },

    /***
     * @method select(<obj>, <find>, ...)
     * @returns Object
     * @short Builds a new object containing the values specified in <find>.
     * @extra When <find> is a string, that single key will be selected. It can also be a regex, selecting any key that matches, or an object which will match if the key also exists in that object, effectively doing an "intersect" operation on that object. Multiple selections may also be passed as an array or directly as enumerated arguments. %select% is available as an instance method on extended objects.
     * @example
     *
     *   Object.select({a:1,b:2}, 'a')        -> {a:1}
     *   Object.select({a:1,b:2}, /[a-z]/)    -> {a:1,ba:2}
     *   Object.select({a:1,b:2}, {a:1})      -> {a:1}
     *   Object.select({a:1,b:2}, 'a', 'b')   -> {a:1,b:2}
     *   Object.select({a:1,b:2}, ['a', 'b']) -> {a:1,b:2}
     *
     ***/
    'select': function (obj) {
      return selectFromObject(obj, arguments, true);
    },

    /***
     * @method reject(<obj>, <find>, ...)
     * @returns Object
     * @short Builds a new object containing all values except those specified in <find>.
     * @extra When <find> is a string, that single key will be rejected. It can also be a regex, rejecting any key that matches, or an object which will match if the key also exists in that object, effectively "subtracting" that object. Multiple selections may also be passed as an array or directly as enumerated arguments. %reject% is available as an instance method on extended objects.
     * @example
     *
     *   Object.reject({a:1,b:2}, 'a')        -> {b:2}
     *   Object.reject({a:1,b:2}, /[a-z]/)    -> {}
     *   Object.reject({a:1,b:2}, {a:1})      -> {b:2}
     *   Object.reject({a:1,b:2}, 'a', 'b')   -> {}
     *   Object.reject({a:1,b:2}, ['a', 'b']) -> {}
     *
     ***/
    'reject': function (obj) {
      return selectFromObject(obj, arguments, false);
    }

  });


  buildTypeMethods();
  buildObjectExtend();
  buildObjectInstanceMethods(ObjectHashMethods, Hash);
})();/****************
    Blast.js
****************/

/*! Blast.js (2.0.0): julian.com/research/blast (C) 2015 Julian Shapiro. MIT @license: en.wikipedia.org/wiki/MIT_License */

;(function ($, window, document, undefined) {

    /*********************
       Helper Functions
    *********************/

    /* IE detection. Gist: https://gist.github.com/julianshapiro/9098609 */
    var IE = (function () {
        if (document.documentMode) {
            return document.documentMode;
        } else {
            for (var i = 7; i > 0; i--) {
                var div = document.createElement("div");

                div.innerHTML = "<!--[if IE " + i + "]><span></span><![endif]-->";

                if (div.getElementsByTagName("span").length) {
                    div = null;

                    return i;
                }

                div = null;
            }
        }

        return undefined;
    })();

    /* Shim to prevent console.log() from throwing errors on IE<=7. */
    var console = window.console || { log: function () {}, time: function () {} };

    /*****************
        Constants
    *****************/

    var NAME = "blast",
        characterRanges = {
            latinPunctuation: "–—′’'“″„\"(«.…¡¿′’'”″“\")».…!?",
            latinLetters: "\\u0041-\\u005A\\u0061-\\u007A\\u00C0-\\u017F\\u0100-\\u01FF\\u0180-\\u027F"
        },
        Reg = {
            /* If the abbreviations RegEx is missing a title abbreviation that you find yourself needing to often escape manually, tweet me: @Shapiro. */
            abbreviations: new RegExp("[^" + characterRanges.latinLetters + "](e\\.g\\.)|(i\\.e\\.)|(mr\\.)|(mrs\\.)|(ms\\.)|(dr\\.)|(prof\\.)|(esq\\.)|(sr\\.)|(jr\\.)[^" + characterRanges.latinLetters + "]", "ig"),
            innerWordPeriod: new RegExp("[" + characterRanges.latinLetters + "]\.[" + characterRanges.latinLetters + "]", "ig"),
            onlyContainsPunctuation: new RegExp("[^" + characterRanges.latinPunctuation + "]"),
            adjoinedPunctuation: new RegExp("^[" + characterRanges.latinPunctuation + "]+|[" + characterRanges.latinPunctuation + "]+$", "g"),
            skippedElements: /(script|style|select|textarea)/i,
            hasPluginClass: new RegExp("(^| )" + NAME + "( |$)", "gi")
        };

    /****************
       $.fn.blast
    ****************/

    $.fn[NAME] = function (options) {

        /*************************
           Punctuation Escaping
        *************************/

        /* Escape likely false-positives of sentence-final periods. Escaping is performed by wrapping a character's ASCII equivalent in double curly brackets,
           which is then reversed (deencodcoded) after delimiting. */
        function encodePunctuation (text) {
            return text
                    /* Escape the following Latin abbreviations and English titles: e.g., i.e., Mr., Mrs., Ms., Dr., Prof., Esq., Sr., and Jr. */
                    .replace(Reg.abbreviations, function(match) {
                        return match.replace(/\./g, "{{46}}");
                    })
                    /* Escape inner-word (non-space-delimited) periods. For example, the period inside "Blast.js". */
                    .replace(Reg.innerWordPeriod, function(match) {
                        return match.replace(/\./g, "{{46}}");
                    });
        }

        /* Used to decode both the output of encodePunctuation() and punctuation that has been manually escaped by users. */
        function decodePunctuation (text) {
            return text.replace(/{{(\d{1,3})}}/g, function(fullMatch, subMatch) {
                return String.fromCharCode(subMatch);
            });
        }

        /******************
           DOM Traversal
        ******************/

        function wrapNode (node, opts) {
            var wrapper = document.createElement(opts.tag);

            /* Assign the element a class of "blast". */
            wrapper.className = NAME;

            /* If a custom class was provided, assign that too. */
            if (opts.customClass) {
                wrapper.className += " " + opts.customClass;

                /* If an opts.customClass is provided, generate an ID consisting of customClass and a number indicating the match's iteration. */
                if (opts.generateIndexID) {
                    wrapper.id = opts.customClass + "-" + Element.blastedIndex;
                }
            }

            /* For the "all" delimiter, prevent space characters from collapsing. */
            if (opts.delimiter === "all" && /\s/.test(node.data)) {
                wrapper.style.whiteSpace = "pre-line";
            }

            /* Assign the element a class equal to its escaped inner text. Only applicable to the character and word delimiters (since they do not contain spaces). */
            if (opts.generateValueClass === true && !opts.search && (opts.delimiter === "character" || opts.delimiter === "word")) {
                var valueClass,
                    text = node.data;

                /* For the word delimiter, remove adjoined punctuation, which is unlikely to be desired as part of the match -- unless the text
                   consists solely of punctuation (e.g. "!!!"), in which case we leave the text as-is. */
                if (opts.delimiter === "word" && Reg.onlyContainsPunctuation.test(text)) {
                    /* E: Remove punctuation that's adjoined to either side of the word match. */
                    text = text.replace(Reg.adjoinedPunctuation, "");
                }

                valueClass = NAME + "-" + opts.delimiter.toLowerCase() + "-" + text.toLowerCase();

                wrapper.className += " " + valueClass;
            }

            /* Hide the wrapper elements from screenreaders now that we've set the target's aria-label attribute. */
            if (opts.aria) {
                wrapper.setAttribute("aria-hidden", "true");
            }

            wrapper.appendChild(node.cloneNode(false));

            return wrapper;
        }

        function traverseDOM (node, opts) {
            var matchPosition = -1,
                skipNodeBit = 0;

            /* Only proceed if the node is a text node and isn't empty. */
            if (node.nodeType === 3 && node.data.length) {
                /* Perform punctuation encoding/decoding once per original whole text node (before it gets split up into bits). */
                if (Element.nodeBeginning) {
                    /* For the sentence delimiter, we first escape likely false-positive sentence-final punctuation. For all other delimiters,
                       we must decode the user's manually-escaped punctuation so that the RegEx can match correctly (without being thrown off by characters in {{ASCII}}). */
                    node.data = (!opts.search && opts.delimiter === "sentence") ? encodePunctuation(node.data) : decodePunctuation(node.data);

                    Element.nodeBeginning = false;
                }

                matchPosition = node.data.search(delimiterRegex);

                /* If there's a RegEx match in this text node, proceed with element wrapping. */
                if (matchPosition !== -1) {
                    var match = node.data.match(delimiterRegex),
                        matchText = match[0],
                        subMatchText = match[1] || false;

                    /* RegEx queries that can return empty strings (e.g ".*") produce an empty matchText which throws the entire traversal process into an infinite loop due to the position index not incrementing.
                       Thus, we bump up the position index manually, resulting in a zero-width split at this location followed by the continuation of the traversal process. */
                    if (matchText === "") {
                        matchPosition++;
                    /* If a RegEx submatch is produced that is not identical to the full string match, use the submatch's index position and text.
                       This technique allows us to avoid writing multi-part RegEx queries for submatch finding. */
                    } else if (subMatchText && subMatchText !== matchText) {
                        matchPosition += matchText.indexOf(subMatchText);
                        matchText = subMatchText;
                    }

                    /* Split this text node into two separate nodes at the position of the match, returning the node that begins after the match position. */
                    var middleBit = node.splitText(matchPosition);

                    /* Split the newly-produced text node at the end of the match's text so that middleBit is a text node that consists solely of the matched text. The other newly-created text node, which begins
                       at the end of the match's text, is what will be traversed in the subsequent loop (in order to find additional matches in the containing text node). */
                    middleBit.splitText(matchText.length);

                    /* Over-increment the loop counter (see below) so that we skip the extra node (middleBit) that we've just created (and already processed). */
                    skipNodeBit = 1;

                    if (!opts.search && opts.delimiter === "sentence") {
                        /* Now that we've forcefully escaped all likely false-positive sentence-final punctuation, we must decode the punctuation back from ASCII. */
                        middleBit.data = decodePunctuation(middleBit.data);
                    }

                    /* Create the wrapped node. */
                    var wrappedNode = wrapNode(middleBit, opts, Element.blastedIndex);
                    /* Then replace the middleBit text node with its wrapped version. */
                    middleBit.parentNode.replaceChild(wrappedNode, middleBit);

                    /* Push the wrapper onto the Element.wrappers array (for later use with stack manipulation). */
                    Element.wrappers.push(wrappedNode);

                    Element.blastedIndex++;

                    /* Note: We use this slow splice-then-iterate method because every match needs to be converted into an HTML element node. A text node's text cannot have HTML elements inserted into it. */
                    /* TODO: To improve performance, use documentFragments to delay node manipulation so that DOM queries and updates can be batched across elements. */
                }
            /* Traverse the DOM tree until we find text nodes. Skip script and style elements. Skip select and textarea elements since they contain special text nodes that users would not want wrapped.
               Additionally, check for the existence of our plugin's class to ensure that we do not retraverse elements that have already been blasted. */
            /* Note: This basic DOM traversal technique is copyright Johann Burkard <http://johannburkard.de>. Licensed under the MIT License: http://en.wikipedia.org/wiki/MIT_License */
            } else if (node.nodeType === 1 && node.hasChildNodes() && !Reg.skippedElements.test(node.tagName) && !Reg.hasPluginClass.test(node.className)) {
                /* Note: We don't cache childNodes' length since it's a live nodeList (which changes dynamically with the use of splitText() above). */
                for (var i = 0; i < node.childNodes.length; i++) {
                    Element.nodeBeginning = true;

                    i += traverseDOM(node.childNodes[i], opts);
                }
            }

            return skipNodeBit;
        }

        /*******************
           Call Variables
        *******************/

        var opts = $.extend({}, $.fn[NAME].defaults, options),
            delimiterRegex,
            /* Container for variables specific to each element targeted by the Blast call. */
            Element = {};

        /***********************
           Delimiter Creation
        ***********************/

        /* Ensure that the opts.delimiter search variable is a non-empty string. */
        if (opts.search.length && (typeof opts.search === "string" || /^\d/.test(parseFloat(opts.search)))) {
            /* Since the search is performed as a Regex (see below), we escape the string's Regex meta-characters. */
            opts.delimiter = opts.search.toString().replace(/[-[\]{,}(.)*+?|^$\\\/]/g, "\\$&");

            /* Note: This matches the apostrophe+s of the phrase's possessive form: {PHRASE's}. */
            /* Note: This will not match text that is part of a compound word (two words adjoined with a dash), e.g. "front" won't match inside "front-end". */
            /* Note: Based on the way the search algorithm is implemented, it is not possible to search for a string that consists solely of punctuation characters. */
            /* Note: By creating boundaries at Latin alphabet ranges instead of merely spaces, we effectively match phrases that are inlined alongside any type of non-Latin-letter,
               e.g. word|, word!, ♥word♥ will all match. */
            delimiterRegex = new RegExp("(?:^|[^-" + characterRanges.latinLetters + "])(" + opts.delimiter + "('s)?)(?![-" + characterRanges.latinLetters + "])", "i");
        } else {
            /* Normalize the string's case for the delimiter switch check below. */
            if (typeof opts.delimiter === "string") {
                opts.delimiter = opts.delimiter.toLowerCase();
            }

            switch (opts.delimiter) {
                case "all":
                    /* Matches every character then later sets spaces to "white-space: pre-line" so they don't collapse. */
                    delimiterRegex = /(.)/;
                    break;

                case "letter":
                case "char":
                case "character":
                    /* Matches every non-space character. */
                    /* Note: This is the slowest delimiter. However, its slowness is only noticeable when it's used on larger bodies of text (of over 500 characters) on <=IE8.
                       (Run Blast with opts.debug=true to monitor execution times.) */
                    delimiterRegex = /(\S)/;
                    break;

                case "word":
                    /* Matches strings in between space characters. */
                    /* Note: Matches will include any punctuation that's adjoined to the word, e.g. "Hey!" will be a full match. */
                    /* Note: Remember that, with Blast, every HTML element marks the start of a brand new string. Hence, "in<b>si</b>de" matches as three separate words. */
                    delimiterRegex = /\s*(\S+)\s*/;
                    break;

                case "sentence":
                    /* Matches phrases either ending in Latin alphabet punctuation or located at the end of the text. (Linebreaks are not considered punctuation.) */
                    /* Note: If you don't want punctuation to demarcate a sentence match, replace the punctuation character with {{ASCII_CODE_FOR_DESIRED_PUNCTUATION}}. ASCII codes: .={{46}}, ?={{63}}, !={{33}} */
                    delimiterRegex = /(?=\S)(([.]{2,})?[^!?]+?([.…!?]+|(?=\s+$)|$)(\s*[′’'”″“")»]+)*)/;
                    /* RegExp explanation (Tip: Use Regex101.com to play around with this expression and see which strings it matches):
                       - Expanded view: /(?=\S) ( ([.]{2,})? [^!?]+? ([.…!?]+|(?=\s+$)|$) (\s*[′’'”″“")»]+)* )
                       - (?=\S) --> Match must contain a non-space character.
                       - ([.]{2,})? --> Match may begin with a group of periods.
                       - [^!?]+? --> Grab everything that isn't an unequivocally-terminating punctuation character, but stop at the following condition...
                       - ([.…!?]+|(?=\s+$)|$) --> Match the last occurrence of sentence-final punctuation or the end of the text (optionally with left-side trailing spaces).
                       - (\s*[′’'”″“")»]+)* --> After the final punctuation, match any and all pairs of (optionally space-delimited) quotes and parentheses.
                    */
                    break;

                case "element":
                    /* Matches text between HTML tags. */
                    /* Note: Wrapping always occurs inside of elements, i.e. <b><span class="blast">Bolded text here</span></b>. */
                    delimiterRegex = /(?=\S)([\S\s]*\S)/;
                    break;

                /*****************
                   Custom Regex
                *****************/

                default:
                    /* You can pass in /your-own-regex/. */
                    if (opts.delimiter instanceof RegExp) {
                        delimiterRegex = opts.delimiter;
                    } else {
                        console.log(NAME + ": Unrecognized delimiter, empty search string, or invalid custom Regex. Aborting.");

                        /* Abort this Blast call. */
                        return true;
                    }
            }
        }

        /**********************
           Element Iteration
        **********************/

        this.each(function() {
            var $this = $(this),
                text = $this.text();

            /* When anything except false is passed in for the options object, Blast is initiated. */
            if (options !== false) {

                /**********************
                   Element Variables
                **********************/

                Element = {
                    /* The index of each wrapper element generated by blasting. */
                    blastedIndex: 0,
                    /* Whether we're just entering this node. */
                    nodeBeginning: false,
                    /* Keep track of the elements generated by Blast so that they can (optionally) be pushed onto the jQuery call stack. */
                    wrappers: Element.wrappers || []
                };

                /*****************
                   Housekeeping
                *****************/

                /* Unless a consecutive opts.search is being performed, an element's existing Blast call is reversed before proceeding. */
                if ($this.data(NAME) !== undefined && ($this.data(NAME) !== "search" || opts.search === false)) {
                    reverse($this, opts);

                    if (opts.debug) console.log(NAME + ": Removed element's existing Blast call.");
                }

                /* Store the current delimiter type so that it can be compared against on subsequent calls (see above). */
                $this.data(NAME, opts.search !== false ? "search" : opts.delimiter);

                if (opts.aria) {
                    $this.attr("aria-label", text);
                }

                /****************
                   Preparation
                ****************/

                /* Perform optional HTML tag stripping. */
                if (opts.stripHTMLTags) {
                    $this.html(text);
                }

                /* If the browser throws an error for the provided element type (browers whitelist the letters and types of the elements they accept), fall back to using "span". */
                try {
                    document.createElement(opts.tag);
                } catch (error) {
                    opts.tag = "span";

                    if (opts.debug) console.log(NAME + ": Invalid tag supplied. Defaulting to span.");
                }

                /* For reference purposes when reversing Blast, assign the target element a root class. */
                $this.addClass(NAME + "-root");

                /* Initiate the DOM traversal process. */
                if (opts.debug) console.time(NAME);
                traverseDOM(this, opts);
                if (opts.debug) console.timeEnd(NAME);

            /* If false is passed in as the first parameter, reverse Blast. */
            } else if (options === false && $this.data(NAME) !== undefined) {
                reverse($this, opts);
            }

            /**************
               Debugging
            **************/

            /* Output the full string of each wrapper element and color alternate the wrappers. This is in addition to the performance timing that has already been outputted. */
            if (opts.debug) {
                $.each(Element.wrappers, function(index, element) {
                    console.log(NAME + " [" + opts.delimiter + "] " + this.outerHTML);
                    this.style.backgroundColor = index % 2 ? "#f12185" : "#075d9a";
                });
            }
        });

        /************
           Reverse
        ************/

        function reverse ($this, opts) {
            if (opts.debug) console.time("blast reversal");

            var skippedDescendantRoot = false;

            $this
                .removeClass(NAME + "-root")
                .removeAttr("aria-label")
                .find("." + NAME)
                    .each(function () {
                        var $this = $(this);
                        /* Do not reverse Blast on descendant root elements. (Before you can reverse Blast on an element, you must reverse Blast on any parent elements that have been Blasted.) */
                        if (!$this.closest("." + NAME + "-root").length) {
                            var thisParentNode = this.parentNode;

                            /* This triggers some sort of node layout, thereby solving a node normalization bug in <=IE7 for reasons unknown. If you know the specific reason, tweet me: @Shapiro. */
                            if (IE <= 7) (thisParentNode.firstChild.nodeName);

                            /* Strip the HTML tags off of the wrapper elements by replacing the elements with their child node's text. */
                            thisParentNode.replaceChild(this.firstChild, this);

                            /* Normalize() parents to remove empty text nodes and concatenate sibling text nodes. (This cleans up the DOM after our manipulation.) */
                            thisParentNode.normalize();
                        } else {
                            skippedDescendantRoot = true;
                        }
                    });

            /* Zepto core doesn't include cache-based $.data(), so we mimic data-attr removal by setting it to undefined. */
            if (window.Zepto) {
                $this.data(NAME, undefined);
            } else {
                $this.removeData(NAME);
            }

            if (opts.debug) {
                console.log(NAME + ": Reversed Blast" + ($this.attr("id") ? " on #" + $this.attr("id") + "." : ".") + (skippedDescendantRoot ? " Skipped reversal on the children of one or more descendant root elements." : ""));
                console.timeEnd("blast reversal");
            }
        }

        /*************
            Chain
        *************/

        /* Either return a stack composed of our call's Element.wrappers or return the element(s) originally targeted by the Blast call. */
        /* Note: returnGenerated can only be disabled on a per-call basis (not a per-element basis). */
        if (options !== false && opts.returnGenerated === true) {
            /* A reimplementation of jQuery's $.pushStack() (since Zepto does not provide this function). */
            var newStack = $().add(Element.wrappers);
            newStack.prevObject = this;
            newStack.context = this.context;

            return newStack;
        } else {
            return this;
        }
    };

    /***************
        Defaults
    ***************/

    $.fn.blast.defaults = {
        returnGenerated: true,
        delimiter: "word",
        tag: "span",
        search: false,
        customClass: "",
        generateIndexID: false,
        generateValueClass: false,
        stripHTMLTags: false,
        aria: true,
        debug: false
    };
})(window.jQuery || window.Zepto, window, document);

/*****************
   Known Issues
*****************/

/* In <=IE7, when Blast is called on the same element more than once with opts.stripHTMLTags=false, calls after the first may not target the entirety of the element and/or may
   inject excess spacing between inner text parts due to <=IE7's faulty node normalization. *//*!
 * VERSION: 1.19.0
 * DATE: 2016-07-14
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * Includes all of the following: TweenLite, TweenMax, TimelineLite, TimelineMax, EasePack, CSSPlugin, RoundPropsPlugin, BezierPlugin, AttrPlugin, DirectionalRotationPlugin
 *
 * @license Copyright (c) 2008-2016, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 *
 * @author: Jack Doyle, jack@greensock.com
 **/
var _gsScope="undefined"!=typeof module&&module.exports&&"undefined"!=typeof global?global:this||window;(_gsScope._gsQueue||(_gsScope._gsQueue=[])).push(function(){"use strict";_gsScope._gsDefine("TweenMax",["core.Animation","core.SimpleTimeline","TweenLite"],function(a,b,c){var d=function(a){var b,c=[],d=a.length;for(b=0;b!==d;c.push(a[b++]));return c},e=function(a,b,c){var d,e,f=a.cycle;for(d in f)e=f[d],a[d]="function"==typeof e?e(c,b[c]):e[c%e.length];delete a.cycle},f=function(a,b,d){c.call(this,a,b,d),this._cycle=0,this._yoyo=this.vars.yoyo===!0,this._repeat=this.vars.repeat||0,this._repeatDelay=this.vars.repeatDelay||0,this._dirty=!0,this.render=f.prototype.render},g=1e-10,h=c._internals,i=h.isSelector,j=h.isArray,k=f.prototype=c.to({},.1,{}),l=[];f.version="1.19.0",k.constructor=f,k.kill()._gc=!1,f.killTweensOf=f.killDelayedCallsTo=c.killTweensOf,f.getTweensOf=c.getTweensOf,f.lagSmoothing=c.lagSmoothing,f.ticker=c.ticker,f.render=c.render,k.invalidate=function(){return this._yoyo=this.vars.yoyo===!0,this._repeat=this.vars.repeat||0,this._repeatDelay=this.vars.repeatDelay||0,this._uncache(!0),c.prototype.invalidate.call(this)},k.updateTo=function(a,b){var d,e=this.ratio,f=this.vars.immediateRender||a.immediateRender;b&&this._startTime<this._timeline._time&&(this._startTime=this._timeline._time,this._uncache(!1),this._gc?this._enabled(!0,!1):this._timeline.insert(this,this._startTime-this._delay));for(d in a)this.vars[d]=a[d];if(this._initted||f)if(b)this._initted=!1,f&&this.render(0,!0,!0);else if(this._gc&&this._enabled(!0,!1),this._notifyPluginsOfEnabled&&this._firstPT&&c._onPluginEvent("_onDisable",this),this._time/this._duration>.998){var g=this._totalTime;this.render(0,!0,!1),this._initted=!1,this.render(g,!0,!1)}else if(this._initted=!1,this._init(),this._time>0||f)for(var h,i=1/(1-e),j=this._firstPT;j;)h=j.s+j.c,j.c*=i,j.s=h-j.c,j=j._next;return this},k.render=function(a,b,c){this._initted||0===this._duration&&this.vars.repeat&&this.invalidate();var d,e,f,i,j,k,l,m,n=this._dirty?this.totalDuration():this._totalDuration,o=this._time,p=this._totalTime,q=this._cycle,r=this._duration,s=this._rawPrevTime;if(a>=n-1e-7?(this._totalTime=n,this._cycle=this._repeat,this._yoyo&&0!==(1&this._cycle)?(this._time=0,this.ratio=this._ease._calcEnd?this._ease.getRatio(0):0):(this._time=r,this.ratio=this._ease._calcEnd?this._ease.getRatio(1):1),this._reversed||(d=!0,e="onComplete",c=c||this._timeline.autoRemoveChildren),0===r&&(this._initted||!this.vars.lazy||c)&&(this._startTime===this._timeline._duration&&(a=0),(0>s||0>=a&&a>=-1e-7||s===g&&"isPause"!==this.data)&&s!==a&&(c=!0,s>g&&(e="onReverseComplete")),this._rawPrevTime=m=!b||a||s===a?a:g)):1e-7>a?(this._totalTime=this._time=this._cycle=0,this.ratio=this._ease._calcEnd?this._ease.getRatio(0):0,(0!==p||0===r&&s>0)&&(e="onReverseComplete",d=this._reversed),0>a&&(this._active=!1,0===r&&(this._initted||!this.vars.lazy||c)&&(s>=0&&(c=!0),this._rawPrevTime=m=!b||a||s===a?a:g)),this._initted||(c=!0)):(this._totalTime=this._time=a,0!==this._repeat&&(i=r+this._repeatDelay,this._cycle=this._totalTime/i>>0,0!==this._cycle&&this._cycle===this._totalTime/i&&a>=p&&this._cycle--,this._time=this._totalTime-this._cycle*i,this._yoyo&&0!==(1&this._cycle)&&(this._time=r-this._time),this._time>r?this._time=r:this._time<0&&(this._time=0)),this._easeType?(j=this._time/r,k=this._easeType,l=this._easePower,(1===k||3===k&&j>=.5)&&(j=1-j),3===k&&(j*=2),1===l?j*=j:2===l?j*=j*j:3===l?j*=j*j*j:4===l&&(j*=j*j*j*j),1===k?this.ratio=1-j:2===k?this.ratio=j:this._time/r<.5?this.ratio=j/2:this.ratio=1-j/2):this.ratio=this._ease.getRatio(this._time/r)),o===this._time&&!c&&q===this._cycle)return void(p!==this._totalTime&&this._onUpdate&&(b||this._callback("onUpdate")));if(!this._initted){if(this._init(),!this._initted||this._gc)return;if(!c&&this._firstPT&&(this.vars.lazy!==!1&&this._duration||this.vars.lazy&&!this._duration))return this._time=o,this._totalTime=p,this._rawPrevTime=s,this._cycle=q,h.lazyTweens.push(this),void(this._lazy=[a,b]);this._time&&!d?this.ratio=this._ease.getRatio(this._time/r):d&&this._ease._calcEnd&&(this.ratio=this._ease.getRatio(0===this._time?0:1))}for(this._lazy!==!1&&(this._lazy=!1),this._active||!this._paused&&this._time!==o&&a>=0&&(this._active=!0),0===p&&(2===this._initted&&a>0&&this._init(),this._startAt&&(a>=0?this._startAt.render(a,b,c):e||(e="_dummyGS")),this.vars.onStart&&(0!==this._totalTime||0===r)&&(b||this._callback("onStart"))),f=this._firstPT;f;)f.f?f.t[f.p](f.c*this.ratio+f.s):f.t[f.p]=f.c*this.ratio+f.s,f=f._next;this._onUpdate&&(0>a&&this._startAt&&this._startTime&&this._startAt.render(a,b,c),b||(this._totalTime!==p||e)&&this._callback("onUpdate")),this._cycle!==q&&(b||this._gc||this.vars.onRepeat&&this._callback("onRepeat")),e&&(!this._gc||c)&&(0>a&&this._startAt&&!this._onUpdate&&this._startTime&&this._startAt.render(a,b,c),d&&(this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!b&&this.vars[e]&&this._callback(e),0===r&&this._rawPrevTime===g&&m!==g&&(this._rawPrevTime=0))},f.to=function(a,b,c){return new f(a,b,c)},f.from=function(a,b,c){return c.runBackwards=!0,c.immediateRender=0!=c.immediateRender,new f(a,b,c)},f.fromTo=function(a,b,c,d){return d.startAt=c,d.immediateRender=0!=d.immediateRender&&0!=c.immediateRender,new f(a,b,d)},f.staggerTo=f.allTo=function(a,b,g,h,k,m,n){h=h||0;var o,p,q,r,s=0,t=[],u=function(){g.onComplete&&g.onComplete.apply(g.onCompleteScope||this,arguments),k.apply(n||g.callbackScope||this,m||l)},v=g.cycle,w=g.startAt&&g.startAt.cycle;for(j(a)||("string"==typeof a&&(a=c.selector(a)||a),i(a)&&(a=d(a))),a=a||[],0>h&&(a=d(a),a.reverse(),h*=-1),o=a.length-1,q=0;o>=q;q++){p={};for(r in g)p[r]=g[r];if(v&&(e(p,a,q),null!=p.duration&&(b=p.duration,delete p.duration)),w){w=p.startAt={};for(r in g.startAt)w[r]=g.startAt[r];e(p.startAt,a,q)}p.delay=s+(p.delay||0),q===o&&k&&(p.onComplete=u),t[q]=new f(a[q],b,p),s+=h}return t},f.staggerFrom=f.allFrom=function(a,b,c,d,e,g,h){return c.runBackwards=!0,c.immediateRender=0!=c.immediateRender,f.staggerTo(a,b,c,d,e,g,h)},f.staggerFromTo=f.allFromTo=function(a,b,c,d,e,g,h,i){return d.startAt=c,d.immediateRender=0!=d.immediateRender&&0!=c.immediateRender,f.staggerTo(a,b,d,e,g,h,i)},f.delayedCall=function(a,b,c,d,e){return new f(b,0,{delay:a,onComplete:b,onCompleteParams:c,callbackScope:d,onReverseComplete:b,onReverseCompleteParams:c,immediateRender:!1,useFrames:e,overwrite:0})},f.set=function(a,b){return new f(a,0,b)},f.isTweening=function(a){return c.getTweensOf(a,!0).length>0};var m=function(a,b){for(var d=[],e=0,f=a._first;f;)f instanceof c?d[e++]=f:(b&&(d[e++]=f),d=d.concat(m(f,b)),e=d.length),f=f._next;return d},n=f.getAllTweens=function(b){return m(a._rootTimeline,b).concat(m(a._rootFramesTimeline,b))};f.killAll=function(a,c,d,e){null==c&&(c=!0),null==d&&(d=!0);var f,g,h,i=n(0!=e),j=i.length,k=c&&d&&e;for(h=0;j>h;h++)g=i[h],(k||g instanceof b||(f=g.target===g.vars.onComplete)&&d||c&&!f)&&(a?g.totalTime(g._reversed?0:g.totalDuration()):g._enabled(!1,!1))},f.killChildTweensOf=function(a,b){if(null!=a){var e,g,k,l,m,n=h.tweenLookup;if("string"==typeof a&&(a=c.selector(a)||a),i(a)&&(a=d(a)),j(a))for(l=a.length;--l>-1;)f.killChildTweensOf(a[l],b);else{e=[];for(k in n)for(g=n[k].target.parentNode;g;)g===a&&(e=e.concat(n[k].tweens)),g=g.parentNode;for(m=e.length,l=0;m>l;l++)b&&e[l].totalTime(e[l].totalDuration()),e[l]._enabled(!1,!1)}}};var o=function(a,c,d,e){c=c!==!1,d=d!==!1,e=e!==!1;for(var f,g,h=n(e),i=c&&d&&e,j=h.length;--j>-1;)g=h[j],(i||g instanceof b||(f=g.target===g.vars.onComplete)&&d||c&&!f)&&g.paused(a)};return f.pauseAll=function(a,b,c){o(!0,a,b,c)},f.resumeAll=function(a,b,c){o(!1,a,b,c)},f.globalTimeScale=function(b){var d=a._rootTimeline,e=c.ticker.time;return arguments.length?(b=b||g,d._startTime=e-(e-d._startTime)*d._timeScale/b,d=a._rootFramesTimeline,e=c.ticker.frame,d._startTime=e-(e-d._startTime)*d._timeScale/b,d._timeScale=a._rootTimeline._timeScale=b,b):d._timeScale},k.progress=function(a,b){return arguments.length?this.totalTime(this.duration()*(this._yoyo&&0!==(1&this._cycle)?1-a:a)+this._cycle*(this._duration+this._repeatDelay),b):this._time/this.duration()},k.totalProgress=function(a,b){return arguments.length?this.totalTime(this.totalDuration()*a,b):this._totalTime/this.totalDuration()},k.time=function(a,b){return arguments.length?(this._dirty&&this.totalDuration(),a>this._duration&&(a=this._duration),this._yoyo&&0!==(1&this._cycle)?a=this._duration-a+this._cycle*(this._duration+this._repeatDelay):0!==this._repeat&&(a+=this._cycle*(this._duration+this._repeatDelay)),this.totalTime(a,b)):this._time},k.duration=function(b){return arguments.length?a.prototype.duration.call(this,b):this._duration},k.totalDuration=function(a){return arguments.length?-1===this._repeat?this:this.duration((a-this._repeat*this._repeatDelay)/(this._repeat+1)):(this._dirty&&(this._totalDuration=-1===this._repeat?999999999999:this._duration*(this._repeat+1)+this._repeatDelay*this._repeat,this._dirty=!1),this._totalDuration)},k.repeat=function(a){return arguments.length?(this._repeat=a,this._uncache(!0)):this._repeat},k.repeatDelay=function(a){return arguments.length?(this._repeatDelay=a,this._uncache(!0)):this._repeatDelay},k.yoyo=function(a){return arguments.length?(this._yoyo=a,this):this._yoyo},f},!0),_gsScope._gsDefine("TimelineLite",["core.Animation","core.SimpleTimeline","TweenLite"],function(a,b,c){var d=function(a){b.call(this,a),this._labels={},this.autoRemoveChildren=this.vars.autoRemoveChildren===!0,this.smoothChildTiming=this.vars.smoothChildTiming===!0,this._sortChildren=!0,this._onUpdate=this.vars.onUpdate;var c,d,e=this.vars;for(d in e)c=e[d],i(c)&&-1!==c.join("").indexOf("{self}")&&(e[d]=this._swapSelfInParams(c));i(e.tweens)&&this.add(e.tweens,0,e.align,e.stagger)},e=1e-10,f=c._internals,g=d._internals={},h=f.isSelector,i=f.isArray,j=f.lazyTweens,k=f.lazyRender,l=_gsScope._gsDefine.globals,m=function(a){var b,c={};for(b in a)c[b]=a[b];return c},n=function(a,b,c){var d,e,f=a.cycle;for(d in f)e=f[d],a[d]="function"==typeof e?e.call(b[c],c):e[c%e.length];delete a.cycle},o=g.pauseCallback=function(){},p=function(a){var b,c=[],d=a.length;for(b=0;b!==d;c.push(a[b++]));return c},q=d.prototype=new b;return d.version="1.19.0",q.constructor=d,q.kill()._gc=q._forcingPlayhead=q._hasPause=!1,q.to=function(a,b,d,e){var f=d.repeat&&l.TweenMax||c;return b?this.add(new f(a,b,d),e):this.set(a,d,e)},q.from=function(a,b,d,e){return this.add((d.repeat&&l.TweenMax||c).from(a,b,d),e)},q.fromTo=function(a,b,d,e,f){var g=e.repeat&&l.TweenMax||c;return b?this.add(g.fromTo(a,b,d,e),f):this.set(a,e,f)},q.staggerTo=function(a,b,e,f,g,i,j,k){var l,o,q=new d({onComplete:i,onCompleteParams:j,callbackScope:k,smoothChildTiming:this.smoothChildTiming}),r=e.cycle;for("string"==typeof a&&(a=c.selector(a)||a),a=a||[],h(a)&&(a=p(a)),f=f||0,0>f&&(a=p(a),a.reverse(),f*=-1),o=0;o<a.length;o++)l=m(e),l.startAt&&(l.startAt=m(l.startAt),l.startAt.cycle&&n(l.startAt,a,o)),r&&(n(l,a,o),null!=l.duration&&(b=l.duration,delete l.duration)),q.to(a[o],b,l,o*f);return this.add(q,g)},q.staggerFrom=function(a,b,c,d,e,f,g,h){return c.immediateRender=0!=c.immediateRender,c.runBackwards=!0,this.staggerTo(a,b,c,d,e,f,g,h)},q.staggerFromTo=function(a,b,c,d,e,f,g,h,i){return d.startAt=c,d.immediateRender=0!=d.immediateRender&&0!=c.immediateRender,this.staggerTo(a,b,d,e,f,g,h,i)},q.call=function(a,b,d,e){return this.add(c.delayedCall(0,a,b,d),e)},q.set=function(a,b,d){return d=this._parseTimeOrLabel(d,0,!0),null==b.immediateRender&&(b.immediateRender=d===this._time&&!this._paused),this.add(new c(a,0,b),d)},d.exportRoot=function(a,b){a=a||{},null==a.smoothChildTiming&&(a.smoothChildTiming=!0);var e,f,g=new d(a),h=g._timeline;for(null==b&&(b=!0),h._remove(g,!0),g._startTime=0,g._rawPrevTime=g._time=g._totalTime=h._time,e=h._first;e;)f=e._next,b&&e instanceof c&&e.target===e.vars.onComplete||g.add(e,e._startTime-e._delay),e=f;return h.add(g,0),g},q.add=function(e,f,g,h){var j,k,l,m,n,o;if("number"!=typeof f&&(f=this._parseTimeOrLabel(f,0,!0,e)),!(e instanceof a)){if(e instanceof Array||e&&e.push&&i(e)){for(g=g||"normal",h=h||0,j=f,k=e.length,l=0;k>l;l++)i(m=e[l])&&(m=new d({tweens:m})),this.add(m,j),"string"!=typeof m&&"function"!=typeof m&&("sequence"===g?j=m._startTime+m.totalDuration()/m._timeScale:"start"===g&&(m._startTime-=m.delay())),j+=h;return this._uncache(!0)}if("string"==typeof e)return this.addLabel(e,f);if("function"!=typeof e)throw"Cannot add "+e+" into the timeline; it is not a tween, timeline, function, or string.";e=c.delayedCall(0,e)}if(b.prototype.add.call(this,e,f),(this._gc||this._time===this._duration)&&!this._paused&&this._duration<this.duration())for(n=this,o=n.rawTime()>e._startTime;n._timeline;)o&&n._timeline.smoothChildTiming?n.totalTime(n._totalTime,!0):n._gc&&n._enabled(!0,!1),n=n._timeline;return this},q.remove=function(b){if(b instanceof a){this._remove(b,!1);var c=b._timeline=b.vars.useFrames?a._rootFramesTimeline:a._rootTimeline;return b._startTime=(b._paused?b._pauseTime:c._time)-(b._reversed?b.totalDuration()-b._totalTime:b._totalTime)/b._timeScale,this}if(b instanceof Array||b&&b.push&&i(b)){for(var d=b.length;--d>-1;)this.remove(b[d]);return this}return"string"==typeof b?this.removeLabel(b):this.kill(null,b)},q._remove=function(a,c){b.prototype._remove.call(this,a,c);var d=this._last;return d?this._time>d._startTime+d._totalDuration/d._timeScale&&(this._time=this.duration(),this._totalTime=this._totalDuration):this._time=this._totalTime=this._duration=this._totalDuration=0,this},q.append=function(a,b){return this.add(a,this._parseTimeOrLabel(null,b,!0,a))},q.insert=q.insertMultiple=function(a,b,c,d){return this.add(a,b||0,c,d)},q.appendMultiple=function(a,b,c,d){return this.add(a,this._parseTimeOrLabel(null,b,!0,a),c,d)},q.addLabel=function(a,b){return this._labels[a]=this._parseTimeOrLabel(b),this},q.addPause=function(a,b,d,e){var f=c.delayedCall(0,o,d,e||this);return f.vars.onComplete=f.vars.onReverseComplete=b,f.data="isPause",this._hasPause=!0,this.add(f,a)},q.removeLabel=function(a){return delete this._labels[a],this},q.getLabelTime=function(a){return null!=this._labels[a]?this._labels[a]:-1},q._parseTimeOrLabel=function(b,c,d,e){var f;if(e instanceof a&&e.timeline===this)this.remove(e);else if(e&&(e instanceof Array||e.push&&i(e)))for(f=e.length;--f>-1;)e[f]instanceof a&&e[f].timeline===this&&this.remove(e[f]);if("string"==typeof c)return this._parseTimeOrLabel(c,d&&"number"==typeof b&&null==this._labels[c]?b-this.duration():0,d);if(c=c||0,"string"!=typeof b||!isNaN(b)&&null==this._labels[b])null==b&&(b=this.duration());else{if(f=b.indexOf("="),-1===f)return null==this._labels[b]?d?this._labels[b]=this.duration()+c:c:this._labels[b]+c;c=parseInt(b.charAt(f-1)+"1",10)*Number(b.substr(f+1)),b=f>1?this._parseTimeOrLabel(b.substr(0,f-1),0,d):this.duration()}return Number(b)+c},q.seek=function(a,b){return this.totalTime("number"==typeof a?a:this._parseTimeOrLabel(a),b!==!1)},q.stop=function(){return this.paused(!0)},q.gotoAndPlay=function(a,b){return this.play(a,b)},q.gotoAndStop=function(a,b){return this.pause(a,b)},q.render=function(a,b,c){this._gc&&this._enabled(!0,!1);var d,f,g,h,i,l,m,n=this._dirty?this.totalDuration():this._totalDuration,o=this._time,p=this._startTime,q=this._timeScale,r=this._paused;if(a>=n-1e-7)this._totalTime=this._time=n,this._reversed||this._hasPausedChild()||(f=!0,h="onComplete",i=!!this._timeline.autoRemoveChildren,0===this._duration&&(0>=a&&a>=-1e-7||this._rawPrevTime<0||this._rawPrevTime===e)&&this._rawPrevTime!==a&&this._first&&(i=!0,this._rawPrevTime>e&&(h="onReverseComplete"))),this._rawPrevTime=this._duration||!b||a||this._rawPrevTime===a?a:e,a=n+1e-4;else if(1e-7>a)if(this._totalTime=this._time=0,(0!==o||0===this._duration&&this._rawPrevTime!==e&&(this._rawPrevTime>0||0>a&&this._rawPrevTime>=0))&&(h="onReverseComplete",f=this._reversed),0>a)this._active=!1,this._timeline.autoRemoveChildren&&this._reversed?(i=f=!0,h="onReverseComplete"):this._rawPrevTime>=0&&this._first&&(i=!0),this._rawPrevTime=a;else{if(this._rawPrevTime=this._duration||!b||a||this._rawPrevTime===a?a:e,0===a&&f)for(d=this._first;d&&0===d._startTime;)d._duration||(f=!1),d=d._next;a=0,this._initted||(i=!0)}else{if(this._hasPause&&!this._forcingPlayhead&&!b){if(a>=o)for(d=this._first;d&&d._startTime<=a&&!l;)d._duration||"isPause"!==d.data||d.ratio||0===d._startTime&&0===this._rawPrevTime||(l=d),d=d._next;else for(d=this._last;d&&d._startTime>=a&&!l;)d._duration||"isPause"===d.data&&d._rawPrevTime>0&&(l=d),d=d._prev;l&&(this._time=a=l._startTime,this._totalTime=a+this._cycle*(this._totalDuration+this._repeatDelay))}this._totalTime=this._time=this._rawPrevTime=a}if(this._time!==o&&this._first||c||i||l){if(this._initted||(this._initted=!0),this._active||!this._paused&&this._time!==o&&a>0&&(this._active=!0),0===o&&this.vars.onStart&&(0===this._time&&this._duration||b||this._callback("onStart")),m=this._time,m>=o)for(d=this._first;d&&(g=d._next,m===this._time&&(!this._paused||r));)(d._active||d._startTime<=m&&!d._paused&&!d._gc)&&(l===d&&this.pause(),d._reversed?d.render((d._dirty?d.totalDuration():d._totalDuration)-(a-d._startTime)*d._timeScale,b,c):d.render((a-d._startTime)*d._timeScale,b,c)),d=g;else for(d=this._last;d&&(g=d._prev,m===this._time&&(!this._paused||r));){if(d._active||d._startTime<=o&&!d._paused&&!d._gc){if(l===d){for(l=d._prev;l&&l.endTime()>this._time;)l.render(l._reversed?l.totalDuration()-(a-l._startTime)*l._timeScale:(a-l._startTime)*l._timeScale,b,c),l=l._prev;l=null,this.pause()}d._reversed?d.render((d._dirty?d.totalDuration():d._totalDuration)-(a-d._startTime)*d._timeScale,b,c):d.render((a-d._startTime)*d._timeScale,b,c)}d=g}this._onUpdate&&(b||(j.length&&k(),this._callback("onUpdate"))),h&&(this._gc||(p===this._startTime||q!==this._timeScale)&&(0===this._time||n>=this.totalDuration())&&(f&&(j.length&&k(),this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!b&&this.vars[h]&&this._callback(h)))}},q._hasPausedChild=function(){for(var a=this._first;a;){if(a._paused||a instanceof d&&a._hasPausedChild())return!0;a=a._next}return!1},q.getChildren=function(a,b,d,e){e=e||-9999999999;for(var f=[],g=this._first,h=0;g;)g._startTime<e||(g instanceof c?b!==!1&&(f[h++]=g):(d!==!1&&(f[h++]=g),a!==!1&&(f=f.concat(g.getChildren(!0,b,d)),h=f.length))),g=g._next;return f},q.getTweensOf=function(a,b){var d,e,f=this._gc,g=[],h=0;for(f&&this._enabled(!0,!0),d=c.getTweensOf(a),e=d.length;--e>-1;)(d[e].timeline===this||b&&this._contains(d[e]))&&(g[h++]=d[e]);return f&&this._enabled(!1,!0),g},q.recent=function(){return this._recent},q._contains=function(a){for(var b=a.timeline;b;){if(b===this)return!0;b=b.timeline}return!1},q.shiftChildren=function(a,b,c){c=c||0;for(var d,e=this._first,f=this._labels;e;)e._startTime>=c&&(e._startTime+=a),e=e._next;if(b)for(d in f)f[d]>=c&&(f[d]+=a);return this._uncache(!0)},q._kill=function(a,b){if(!a&&!b)return this._enabled(!1,!1);for(var c=b?this.getTweensOf(b):this.getChildren(!0,!0,!1),d=c.length,e=!1;--d>-1;)c[d]._kill(a,b)&&(e=!0);return e},q.clear=function(a){var b=this.getChildren(!1,!0,!0),c=b.length;for(this._time=this._totalTime=0;--c>-1;)b[c]._enabled(!1,!1);return a!==!1&&(this._labels={}),this._uncache(!0)},q.invalidate=function(){for(var b=this._first;b;)b.invalidate(),b=b._next;return a.prototype.invalidate.call(this)},q._enabled=function(a,c){if(a===this._gc)for(var d=this._first;d;)d._enabled(a,!0),d=d._next;return b.prototype._enabled.call(this,a,c)},q.totalTime=function(b,c,d){this._forcingPlayhead=!0;var e=a.prototype.totalTime.apply(this,arguments);return this._forcingPlayhead=!1,e},q.duration=function(a){return arguments.length?(0!==this.duration()&&0!==a&&this.timeScale(this._duration/a),this):(this._dirty&&this.totalDuration(),this._duration)},q.totalDuration=function(a){if(!arguments.length){if(this._dirty){for(var b,c,d=0,e=this._last,f=999999999999;e;)b=e._prev,e._dirty&&e.totalDuration(),e._startTime>f&&this._sortChildren&&!e._paused?this.add(e,e._startTime-e._delay):f=e._startTime,e._startTime<0&&!e._paused&&(d-=e._startTime,this._timeline.smoothChildTiming&&(this._startTime+=e._startTime/this._timeScale),this.shiftChildren(-e._startTime,!1,-9999999999),f=0),c=e._startTime+e._totalDuration/e._timeScale,c>d&&(d=c),e=b;this._duration=this._totalDuration=d,this._dirty=!1}return this._totalDuration}return a&&this.totalDuration()?this.timeScale(this._totalDuration/a):this},q.paused=function(b){if(!b)for(var c=this._first,d=this._time;c;)c._startTime===d&&"isPause"===c.data&&(c._rawPrevTime=0),c=c._next;return a.prototype.paused.apply(this,arguments)},q.usesFrames=function(){for(var b=this._timeline;b._timeline;)b=b._timeline;return b===a._rootFramesTimeline},q.rawTime=function(){return this._paused?this._totalTime:(this._timeline.rawTime()-this._startTime)*this._timeScale},d},!0),_gsScope._gsDefine("TimelineMax",["TimelineLite","TweenLite","easing.Ease"],function(a,b,c){var d=function(b){a.call(this,b),this._repeat=this.vars.repeat||0,this._repeatDelay=this.vars.repeatDelay||0,this._cycle=0,this._yoyo=this.vars.yoyo===!0,this._dirty=!0},e=1e-10,f=b._internals,g=f.lazyTweens,h=f.lazyRender,i=_gsScope._gsDefine.globals,j=new c(null,null,1,0),k=d.prototype=new a;return k.constructor=d,k.kill()._gc=!1,d.version="1.19.0",k.invalidate=function(){return this._yoyo=this.vars.yoyo===!0,this._repeat=this.vars.repeat||0,this._repeatDelay=this.vars.repeatDelay||0,this._uncache(!0),a.prototype.invalidate.call(this)},k.addCallback=function(a,c,d,e){return this.add(b.delayedCall(0,a,d,e),c)},k.removeCallback=function(a,b){if(a)if(null==b)this._kill(null,a);else for(var c=this.getTweensOf(a,!1),d=c.length,e=this._parseTimeOrLabel(b);--d>-1;)c[d]._startTime===e&&c[d]._enabled(!1,!1);return this},k.removePause=function(b){return this.removeCallback(a._internals.pauseCallback,b)},k.tweenTo=function(a,c){c=c||{};var d,e,f,g={ease:j,useFrames:this.usesFrames(),immediateRender:!1},h=c.repeat&&i.TweenMax||b;for(e in c)g[e]=c[e];return g.time=this._parseTimeOrLabel(a),d=Math.abs(Number(g.time)-this._time)/this._timeScale||.001,f=new h(this,d,g),g.onStart=function(){f.target.paused(!0),f.vars.time!==f.target.time()&&d===f.duration()&&f.duration(Math.abs(f.vars.time-f.target.time())/f.target._timeScale),c.onStart&&f._callback("onStart")},f},k.tweenFromTo=function(a,b,c){c=c||{},a=this._parseTimeOrLabel(a),c.startAt={onComplete:this.seek,onCompleteParams:[a],callbackScope:this},c.immediateRender=c.immediateRender!==!1;var d=this.tweenTo(b,c);return d.duration(Math.abs(d.vars.time-a)/this._timeScale||.001)},k.render=function(a,b,c){this._gc&&this._enabled(!0,!1);var d,f,i,j,k,l,m,n,o=this._dirty?this.totalDuration():this._totalDuration,p=this._duration,q=this._time,r=this._totalTime,s=this._startTime,t=this._timeScale,u=this._rawPrevTime,v=this._paused,w=this._cycle;if(a>=o-1e-7)this._locked||(this._totalTime=o,this._cycle=this._repeat),this._reversed||this._hasPausedChild()||(f=!0,j="onComplete",k=!!this._timeline.autoRemoveChildren,0===this._duration&&(0>=a&&a>=-1e-7||0>u||u===e)&&u!==a&&this._first&&(k=!0,u>e&&(j="onReverseComplete"))),this._rawPrevTime=this._duration||!b||a||this._rawPrevTime===a?a:e,this._yoyo&&0!==(1&this._cycle)?this._time=a=0:(this._time=p,a=p+1e-4);else if(1e-7>a)if(this._locked||(this._totalTime=this._cycle=0),this._time=0,(0!==q||0===p&&u!==e&&(u>0||0>a&&u>=0)&&!this._locked)&&(j="onReverseComplete",f=this._reversed),0>a)this._active=!1,this._timeline.autoRemoveChildren&&this._reversed?(k=f=!0,j="onReverseComplete"):u>=0&&this._first&&(k=!0),this._rawPrevTime=a;else{if(this._rawPrevTime=p||!b||a||this._rawPrevTime===a?a:e,0===a&&f)for(d=this._first;d&&0===d._startTime;)d._duration||(f=!1),d=d._next;a=0,this._initted||(k=!0)}else if(0===p&&0>u&&(k=!0),this._time=this._rawPrevTime=a,this._locked||(this._totalTime=a,0!==this._repeat&&(l=p+this._repeatDelay,this._cycle=this._totalTime/l>>0,0!==this._cycle&&this._cycle===this._totalTime/l&&a>=r&&this._cycle--,this._time=this._totalTime-this._cycle*l,this._yoyo&&0!==(1&this._cycle)&&(this._time=p-this._time),this._time>p?(this._time=p,a=p+1e-4):this._time<0?this._time=a=0:a=this._time)),this._hasPause&&!this._forcingPlayhead&&!b){if(a=this._time,a>=q)for(d=this._first;d&&d._startTime<=a&&!m;)d._duration||"isPause"!==d.data||d.ratio||0===d._startTime&&0===this._rawPrevTime||(m=d),d=d._next;else for(d=this._last;d&&d._startTime>=a&&!m;)d._duration||"isPause"===d.data&&d._rawPrevTime>0&&(m=d),d=d._prev;m&&(this._time=a=m._startTime,this._totalTime=a+this._cycle*(this._totalDuration+this._repeatDelay))}if(this._cycle!==w&&!this._locked){var x=this._yoyo&&0!==(1&w),y=x===(this._yoyo&&0!==(1&this._cycle)),z=this._totalTime,A=this._cycle,B=this._rawPrevTime,C=this._time;if(this._totalTime=w*p,this._cycle<w?x=!x:this._totalTime+=p,this._time=q,this._rawPrevTime=0===p?u-1e-4:u,this._cycle=w,this._locked=!0,q=x?0:p,this.render(q,b,0===p),b||this._gc||this.vars.onRepeat&&this._callback("onRepeat"),q!==this._time)return;if(y&&(q=x?p+1e-4:-1e-4,this.render(q,!0,!1)),this._locked=!1,this._paused&&!v)return;this._time=C,this._totalTime=z,this._cycle=A,this._rawPrevTime=B}if(!(this._time!==q&&this._first||c||k||m))return void(r!==this._totalTime&&this._onUpdate&&(b||this._callback("onUpdate")));if(this._initted||(this._initted=!0),this._active||!this._paused&&this._totalTime!==r&&a>0&&(this._active=!0),0===r&&this.vars.onStart&&(0===this._totalTime&&this._totalDuration||b||this._callback("onStart")),n=this._time,n>=q)for(d=this._first;d&&(i=d._next,n===this._time&&(!this._paused||v));)(d._active||d._startTime<=this._time&&!d._paused&&!d._gc)&&(m===d&&this.pause(),d._reversed?d.render((d._dirty?d.totalDuration():d._totalDuration)-(a-d._startTime)*d._timeScale,b,c):d.render((a-d._startTime)*d._timeScale,b,c)),d=i;else for(d=this._last;d&&(i=d._prev,n===this._time&&(!this._paused||v));){if(d._active||d._startTime<=q&&!d._paused&&!d._gc){if(m===d){for(m=d._prev;m&&m.endTime()>this._time;)m.render(m._reversed?m.totalDuration()-(a-m._startTime)*m._timeScale:(a-m._startTime)*m._timeScale,b,c),m=m._prev;m=null,this.pause()}d._reversed?d.render((d._dirty?d.totalDuration():d._totalDuration)-(a-d._startTime)*d._timeScale,b,c):d.render((a-d._startTime)*d._timeScale,b,c)}d=i}this._onUpdate&&(b||(g.length&&h(),this._callback("onUpdate"))),j&&(this._locked||this._gc||(s===this._startTime||t!==this._timeScale)&&(0===this._time||o>=this.totalDuration())&&(f&&(g.length&&h(),this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!b&&this.vars[j]&&this._callback(j)))},k.getActive=function(a,b,c){null==a&&(a=!0),null==b&&(b=!0),null==c&&(c=!1);var d,e,f=[],g=this.getChildren(a,b,c),h=0,i=g.length;for(d=0;i>d;d++)e=g[d],e.isActive()&&(f[h++]=e);return f},k.getLabelAfter=function(a){a||0!==a&&(a=this._time);var b,c=this.getLabelsArray(),d=c.length;for(b=0;d>b;b++)if(c[b].time>a)return c[b].name;return null},k.getLabelBefore=function(a){null==a&&(a=this._time);for(var b=this.getLabelsArray(),c=b.length;--c>-1;)if(b[c].time<a)return b[c].name;return null},k.getLabelsArray=function(){var a,b=[],c=0;for(a in this._labels)b[c++]={time:this._labels[a],name:a};return b.sort(function(a,b){return a.time-b.time}),b},k.progress=function(a,b){return arguments.length?this.totalTime(this.duration()*(this._yoyo&&0!==(1&this._cycle)?1-a:a)+this._cycle*(this._duration+this._repeatDelay),b):this._time/this.duration()},k.totalProgress=function(a,b){return arguments.length?this.totalTime(this.totalDuration()*a,b):this._totalTime/this.totalDuration()},k.totalDuration=function(b){return arguments.length?-1!==this._repeat&&b?this.timeScale(this.totalDuration()/b):this:(this._dirty&&(a.prototype.totalDuration.call(this),this._totalDuration=-1===this._repeat?999999999999:this._duration*(this._repeat+1)+this._repeatDelay*this._repeat),this._totalDuration)},k.time=function(a,b){return arguments.length?(this._dirty&&this.totalDuration(),a>this._duration&&(a=this._duration),this._yoyo&&0!==(1&this._cycle)?a=this._duration-a+this._cycle*(this._duration+this._repeatDelay):0!==this._repeat&&(a+=this._cycle*(this._duration+this._repeatDelay)),this.totalTime(a,b)):this._time},k.repeat=function(a){return arguments.length?(this._repeat=a,this._uncache(!0)):this._repeat},k.repeatDelay=function(a){return arguments.length?(this._repeatDelay=a,this._uncache(!0)):this._repeatDelay},k.yoyo=function(a){return arguments.length?(this._yoyo=a,this):this._yoyo},k.currentLabel=function(a){return arguments.length?this.seek(a,!0):this.getLabelBefore(this._time+1e-8)},d},!0),function(){var a=180/Math.PI,b=[],c=[],d=[],e={},f=_gsScope._gsDefine.globals,g=function(a,b,c,d){c===d&&(c=d-(d-b)/1e6),a===b&&(b=a+(c-a)/1e6),this.a=a,this.b=b,this.c=c,this.d=d,this.da=d-a,this.ca=c-a,this.ba=b-a},h=",x,y,z,left,top,right,bottom,marginTop,marginLeft,marginRight,marginBottom,paddingLeft,paddingTop,paddingRight,paddingBottom,backgroundPosition,backgroundPosition_y,",i=function(a,b,c,d){var e={a:a},f={},g={},h={c:d},i=(a+b)/2,j=(b+c)/2,k=(c+d)/2,l=(i+j)/2,m=(j+k)/2,n=(m-l)/8;return e.b=i+(a-i)/4,f.b=l+n,e.c=f.a=(e.b+f.b)/2,f.c=g.a=(l+m)/2,g.b=m-n,h.b=k+(d-k)/4,g.c=h.a=(g.b+h.b)/2,[e,f,g,h]},j=function(a,e,f,g,h){var j,k,l,m,n,o,p,q,r,s,t,u,v,w=a.length-1,x=0,y=a[0].a;for(j=0;w>j;j++)n=a[x],k=n.a,l=n.d,m=a[x+1].d,h?(t=b[j],u=c[j],v=(u+t)*e*.25/(g?.5:d[j]||.5),o=l-(l-k)*(g?.5*e:0!==t?v/t:0),p=l+(m-l)*(g?.5*e:0!==u?v/u:0),q=l-(o+((p-o)*(3*t/(t+u)+.5)/4||0))):(o=l-(l-k)*e*.5,p=l+(m-l)*e*.5,q=l-(o+p)/2),o+=q,p+=q,n.c=r=o,0!==j?n.b=y:n.b=y=n.a+.6*(n.c-n.a),n.da=l-k,n.ca=r-k,n.ba=y-k,f?(s=i(k,y,r,l),a.splice(x,1,s[0],s[1],s[2],s[3]),x+=4):x++,y=p;n=a[x],n.b=y,n.c=y+.4*(n.d-y),n.da=n.d-n.a,n.ca=n.c-n.a,n.ba=y-n.a,f&&(s=i(n.a,y,n.c,n.d),a.splice(x,1,s[0],s[1],s[2],s[3]))},k=function(a,d,e,f){var h,i,j,k,l,m,n=[];if(f)for(a=[f].concat(a),i=a.length;--i>-1;)"string"==typeof(m=a[i][d])&&"="===m.charAt(1)&&(a[i][d]=f[d]+Number(m.charAt(0)+m.substr(2)));if(h=a.length-2,0>h)return n[0]=new g(a[0][d],0,0,a[-1>h?0:1][d]),n;for(i=0;h>i;i++)j=a[i][d],k=a[i+1][d],n[i]=new g(j,0,0,k),e&&(l=a[i+2][d],b[i]=(b[i]||0)+(k-j)*(k-j),c[i]=(c[i]||0)+(l-k)*(l-k));return n[i]=new g(a[i][d],0,0,a[i+1][d]),n},l=function(a,f,g,i,l,m){var n,o,p,q,r,s,t,u,v={},w=[],x=m||a[0];l="string"==typeof l?","+l+",":h,null==f&&(f=1);for(o in a[0])w.push(o);if(a.length>1){for(u=a[a.length-1],t=!0,n=w.length;--n>-1;)if(o=w[n],Math.abs(x[o]-u[o])>.05){t=!1;break}t&&(a=a.concat(),m&&a.unshift(m),a.push(a[1]),m=a[a.length-3])}for(b.length=c.length=d.length=0,n=w.length;--n>-1;)o=w[n],e[o]=-1!==l.indexOf(","+o+","),v[o]=k(a,o,e[o],m);for(n=b.length;--n>-1;)b[n]=Math.sqrt(b[n]),c[n]=Math.sqrt(c[n]);if(!i){for(n=w.length;--n>-1;)if(e[o])for(p=v[w[n]],s=p.length-1,q=0;s>q;q++)r=p[q+1].da/c[q]+p[q].da/b[q]||0,d[q]=(d[q]||0)+r*r;for(n=d.length;--n>-1;)d[n]=Math.sqrt(d[n])}for(n=w.length,q=g?4:1;--n>-1;)o=w[n],p=v[o],j(p,f,g,i,e[o]),t&&(p.splice(0,q),p.splice(p.length-q,q));return v},m=function(a,b,c){b=b||"soft";var d,e,f,h,i,j,k,l,m,n,o,p={},q="cubic"===b?3:2,r="soft"===b,s=[];if(r&&c&&(a=[c].concat(a)),null==a||a.length<q+1)throw"invalid Bezier data";for(m in a[0])s.push(m);for(j=s.length;--j>-1;){for(m=s[j],p[m]=i=[],n=0,l=a.length,k=0;l>k;k++)d=null==c?a[k][m]:"string"==typeof(o=a[k][m])&&"="===o.charAt(1)?c[m]+Number(o.charAt(0)+o.substr(2)):Number(o),r&&k>1&&l-1>k&&(i[n++]=(d+i[n-2])/2),i[n++]=d;for(l=n-q+1,n=0,k=0;l>k;k+=q)d=i[k],e=i[k+1],f=i[k+2],h=2===q?0:i[k+3],i[n++]=o=3===q?new g(d,e,f,h):new g(d,(2*e+d)/3,(2*e+f)/3,f);i.length=n}return p},n=function(a,b,c){for(var d,e,f,g,h,i,j,k,l,m,n,o=1/c,p=a.length;--p>-1;)for(m=a[p],f=m.a,g=m.d-f,h=m.c-f,i=m.b-f,d=e=0,k=1;c>=k;k++)j=o*k,l=1-j,d=e-(e=(j*j*g+3*l*(j*h+l*i))*j),n=p*c+k-1,b[n]=(b[n]||0)+d*d},o=function(a,b){b=b>>0||6;var c,d,e,f,g=[],h=[],i=0,j=0,k=b-1,l=[],m=[];for(c in a)n(a[c],g,b);for(e=g.length,d=0;e>d;d++)i+=Math.sqrt(g[d]),f=d%b,m[f]=i,f===k&&(j+=i,f=d/b>>0,l[f]=m,h[f]=j,i=0,m=[]);return{length:j,lengths:h,
segments:l}},p=_gsScope._gsDefine.plugin({propName:"bezier",priority:-1,version:"1.3.7",API:2,global:!0,init:function(a,b,c){this._target=a,b instanceof Array&&(b={values:b}),this._func={},this._mod={},this._props=[],this._timeRes=null==b.timeResolution?6:parseInt(b.timeResolution,10);var d,e,f,g,h,i=b.values||[],j={},k=i[0],n=b.autoRotate||c.vars.orientToBezier;this._autoRotate=n?n instanceof Array?n:[["x","y","rotation",n===!0?0:Number(n)||0]]:null;for(d in k)this._props.push(d);for(f=this._props.length;--f>-1;)d=this._props[f],this._overwriteProps.push(d),e=this._func[d]="function"==typeof a[d],j[d]=e?a[d.indexOf("set")||"function"!=typeof a["get"+d.substr(3)]?d:"get"+d.substr(3)]():parseFloat(a[d]),h||j[d]!==i[0][d]&&(h=j);if(this._beziers="cubic"!==b.type&&"quadratic"!==b.type&&"soft"!==b.type?l(i,isNaN(b.curviness)?1:b.curviness,!1,"thruBasic"===b.type,b.correlate,h):m(i,b.type,j),this._segCount=this._beziers[d].length,this._timeRes){var p=o(this._beziers,this._timeRes);this._length=p.length,this._lengths=p.lengths,this._segments=p.segments,this._l1=this._li=this._s1=this._si=0,this._l2=this._lengths[0],this._curSeg=this._segments[0],this._s2=this._curSeg[0],this._prec=1/this._curSeg.length}if(n=this._autoRotate)for(this._initialRotations=[],n[0]instanceof Array||(this._autoRotate=n=[n]),f=n.length;--f>-1;){for(g=0;3>g;g++)d=n[f][g],this._func[d]="function"==typeof a[d]?a[d.indexOf("set")||"function"!=typeof a["get"+d.substr(3)]?d:"get"+d.substr(3)]:!1;d=n[f][2],this._initialRotations[f]=(this._func[d]?this._func[d].call(this._target):this._target[d])||0,this._overwriteProps.push(d)}return this._startRatio=c.vars.runBackwards?1:0,!0},set:function(b){var c,d,e,f,g,h,i,j,k,l,m=this._segCount,n=this._func,o=this._target,p=b!==this._startRatio;if(this._timeRes){if(k=this._lengths,l=this._curSeg,b*=this._length,e=this._li,b>this._l2&&m-1>e){for(j=m-1;j>e&&(this._l2=k[++e])<=b;);this._l1=k[e-1],this._li=e,this._curSeg=l=this._segments[e],this._s2=l[this._s1=this._si=0]}else if(b<this._l1&&e>0){for(;e>0&&(this._l1=k[--e])>=b;);0===e&&b<this._l1?this._l1=0:e++,this._l2=k[e],this._li=e,this._curSeg=l=this._segments[e],this._s1=l[(this._si=l.length-1)-1]||0,this._s2=l[this._si]}if(c=e,b-=this._l1,e=this._si,b>this._s2&&e<l.length-1){for(j=l.length-1;j>e&&(this._s2=l[++e])<=b;);this._s1=l[e-1],this._si=e}else if(b<this._s1&&e>0){for(;e>0&&(this._s1=l[--e])>=b;);0===e&&b<this._s1?this._s1=0:e++,this._s2=l[e],this._si=e}h=(e+(b-this._s1)/(this._s2-this._s1))*this._prec||0}else c=0>b?0:b>=1?m-1:m*b>>0,h=(b-c*(1/m))*m;for(d=1-h,e=this._props.length;--e>-1;)f=this._props[e],g=this._beziers[f][c],i=(h*h*g.da+3*d*(h*g.ca+d*g.ba))*h+g.a,this._mod[f]&&(i=this._mod[f](i,o)),n[f]?o[f](i):o[f]=i;if(this._autoRotate){var q,r,s,t,u,v,w,x=this._autoRotate;for(e=x.length;--e>-1;)f=x[e][2],v=x[e][3]||0,w=x[e][4]===!0?1:a,g=this._beziers[x[e][0]],q=this._beziers[x[e][1]],g&&q&&(g=g[c],q=q[c],r=g.a+(g.b-g.a)*h,t=g.b+(g.c-g.b)*h,r+=(t-r)*h,t+=(g.c+(g.d-g.c)*h-t)*h,s=q.a+(q.b-q.a)*h,u=q.b+(q.c-q.b)*h,s+=(u-s)*h,u+=(q.c+(q.d-q.c)*h-u)*h,i=p?Math.atan2(u-s,t-r)*w+v:this._initialRotations[e],this._mod[f]&&(i=this._mod[f](i,o)),n[f]?o[f](i):o[f]=i)}}}),q=p.prototype;p.bezierThrough=l,p.cubicToQuadratic=i,p._autoCSS=!0,p.quadraticToCubic=function(a,b,c){return new g(a,(2*b+a)/3,(2*b+c)/3,c)},p._cssRegister=function(){var a=f.CSSPlugin;if(a){var b=a._internals,c=b._parseToProxy,d=b._setPluginRatio,e=b.CSSPropTween;b._registerComplexSpecialProp("bezier",{parser:function(a,b,f,g,h,i){b instanceof Array&&(b={values:b}),i=new p;var j,k,l,m=b.values,n=m.length-1,o=[],q={};if(0>n)return h;for(j=0;n>=j;j++)l=c(a,m[j],g,h,i,n!==j),o[j]=l.end;for(k in b)q[k]=b[k];return q.values=o,h=new e(a,"bezier",0,0,l.pt,2),h.data=l,h.plugin=i,h.setRatio=d,0===q.autoRotate&&(q.autoRotate=!0),!q.autoRotate||q.autoRotate instanceof Array||(j=q.autoRotate===!0?0:Number(q.autoRotate),q.autoRotate=null!=l.end.left?[["left","top","rotation",j,!1]]:null!=l.end.x?[["x","y","rotation",j,!1]]:!1),q.autoRotate&&(g._transform||g._enableTransforms(!1),l.autoRotate=g._target._gsTransform,l.proxy.rotation=l.autoRotate.rotation||0,g._overwriteProps.push("rotation")),i._onInitTween(l.proxy,q,g._tween),h}})}},q._mod=function(a){for(var b,c=this._overwriteProps,d=c.length;--d>-1;)b=a[c[d]],b&&"function"==typeof b&&(this._mod[c[d]]=b)},q._kill=function(a){var b,c,d=this._props;for(b in this._beziers)if(b in a)for(delete this._beziers[b],delete this._func[b],c=d.length;--c>-1;)d[c]===b&&d.splice(c,1);if(d=this._autoRotate)for(c=d.length;--c>-1;)a[d[c][2]]&&d.splice(c,1);return this._super._kill.call(this,a)}}(),_gsScope._gsDefine("plugins.CSSPlugin",["plugins.TweenPlugin","TweenLite"],function(a,b){var c,d,e,f,g=function(){a.call(this,"css"),this._overwriteProps.length=0,this.setRatio=g.prototype.setRatio},h=_gsScope._gsDefine.globals,i={},j=g.prototype=new a("css");j.constructor=g,g.version="1.19.0",g.API=2,g.defaultTransformPerspective=0,g.defaultSkewType="compensated",g.defaultSmoothOrigin=!0,j="px",g.suffixMap={top:j,right:j,bottom:j,left:j,width:j,height:j,fontSize:j,padding:j,margin:j,perspective:j,lineHeight:""};var k,l,m,n,o,p,q,r,s=/(?:\-|\.|\b)(\d|\.|e\-)+/g,t=/(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g,u=/(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi,v=/(?![+-]?\d*\.?\d+|[+-]|e[+-]\d+)[^0-9]/g,w=/(?:\d|\-|\+|=|#|\.)*/g,x=/opacity *= *([^)]*)/i,y=/opacity:([^;]*)/i,z=/alpha\(opacity *=.+?\)/i,A=/^(rgb|hsl)/,B=/([A-Z])/g,C=/-([a-z])/gi,D=/(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi,E=function(a,b){return b.toUpperCase()},F=/(?:Left|Right|Width)/i,G=/(M11|M12|M21|M22)=[\d\-\.e]+/gi,H=/progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i,I=/,(?=[^\)]*(?:\(|$))/gi,J=/[\s,\(]/i,K=Math.PI/180,L=180/Math.PI,M={},N=document,O=function(a){return N.createElementNS?N.createElementNS("http://www.w3.org/1999/xhtml",a):N.createElement(a)},P=O("div"),Q=O("img"),R=g._internals={_specialProps:i},S=navigator.userAgent,T=function(){var a=S.indexOf("Android"),b=O("a");return m=-1!==S.indexOf("Safari")&&-1===S.indexOf("Chrome")&&(-1===a||Number(S.substr(a+8,1))>3),o=m&&Number(S.substr(S.indexOf("Version/")+8,1))<6,n=-1!==S.indexOf("Firefox"),(/MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(S)||/Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/.exec(S))&&(p=parseFloat(RegExp.$1)),b?(b.style.cssText="top:1px;opacity:.55;",/^0.55/.test(b.style.opacity)):!1}(),U=function(a){return x.test("string"==typeof a?a:(a.currentStyle?a.currentStyle.filter:a.style.filter)||"")?parseFloat(RegExp.$1)/100:1},V=function(a){window.console&&console.log(a)},W="",X="",Y=function(a,b){b=b||P;var c,d,e=b.style;if(void 0!==e[a])return a;for(a=a.charAt(0).toUpperCase()+a.substr(1),c=["O","Moz","ms","Ms","Webkit"],d=5;--d>-1&&void 0===e[c[d]+a];);return d>=0?(X=3===d?"ms":c[d],W="-"+X.toLowerCase()+"-",X+a):null},Z=N.defaultView?N.defaultView.getComputedStyle:function(){},$=g.getStyle=function(a,b,c,d,e){var f;return T||"opacity"!==b?(!d&&a.style[b]?f=a.style[b]:(c=c||Z(a))?f=c[b]||c.getPropertyValue(b)||c.getPropertyValue(b.replace(B,"-$1").toLowerCase()):a.currentStyle&&(f=a.currentStyle[b]),null==e||f&&"none"!==f&&"auto"!==f&&"auto auto"!==f?f:e):U(a)},_=R.convertToPixels=function(a,c,d,e,f){if("px"===e||!e)return d;if("auto"===e||!d)return 0;var h,i,j,k=F.test(c),l=a,m=P.style,n=0>d,o=1===d;if(n&&(d=-d),o&&(d*=100),"%"===e&&-1!==c.indexOf("border"))h=d/100*(k?a.clientWidth:a.clientHeight);else{if(m.cssText="border:0 solid red;position:"+$(a,"position")+";line-height:0;","%"!==e&&l.appendChild&&"v"!==e.charAt(0)&&"rem"!==e)m[k?"borderLeftWidth":"borderTopWidth"]=d+e;else{if(l=a.parentNode||N.body,i=l._gsCache,j=b.ticker.frame,i&&k&&i.time===j)return i.width*d/100;m[k?"width":"height"]=d+e}l.appendChild(P),h=parseFloat(P[k?"offsetWidth":"offsetHeight"]),l.removeChild(P),k&&"%"===e&&g.cacheWidths!==!1&&(i=l._gsCache=l._gsCache||{},i.time=j,i.width=h/d*100),0!==h||f||(h=_(a,c,d,e,!0))}return o&&(h/=100),n?-h:h},aa=R.calculateOffset=function(a,b,c){if("absolute"!==$(a,"position",c))return 0;var d="left"===b?"Left":"Top",e=$(a,"margin"+d,c);return a["offset"+d]-(_(a,b,parseFloat(e),e.replace(w,""))||0)},ba=function(a,b){var c,d,e,f={};if(b=b||Z(a,null))if(c=b.length)for(;--c>-1;)e=b[c],(-1===e.indexOf("-transform")||Ca===e)&&(f[e.replace(C,E)]=b.getPropertyValue(e));else for(c in b)(-1===c.indexOf("Transform")||Ba===c)&&(f[c]=b[c]);else if(b=a.currentStyle||a.style)for(c in b)"string"==typeof c&&void 0===f[c]&&(f[c.replace(C,E)]=b[c]);return T||(f.opacity=U(a)),d=Pa(a,b,!1),f.rotation=d.rotation,f.skewX=d.skewX,f.scaleX=d.scaleX,f.scaleY=d.scaleY,f.x=d.x,f.y=d.y,Ea&&(f.z=d.z,f.rotationX=d.rotationX,f.rotationY=d.rotationY,f.scaleZ=d.scaleZ),f.filters&&delete f.filters,f},ca=function(a,b,c,d,e){var f,g,h,i={},j=a.style;for(g in c)"cssText"!==g&&"length"!==g&&isNaN(g)&&(b[g]!==(f=c[g])||e&&e[g])&&-1===g.indexOf("Origin")&&("number"==typeof f||"string"==typeof f)&&(i[g]="auto"!==f||"left"!==g&&"top"!==g?""!==f&&"auto"!==f&&"none"!==f||"string"!=typeof b[g]||""===b[g].replace(v,"")?f:0:aa(a,g),void 0!==j[g]&&(h=new ra(j,g,j[g],h)));if(d)for(g in d)"className"!==g&&(i[g]=d[g]);return{difs:i,firstMPT:h}},da={width:["Left","Right"],height:["Top","Bottom"]},ea=["marginLeft","marginRight","marginTop","marginBottom"],fa=function(a,b,c){if("svg"===(a.nodeName+"").toLowerCase())return(c||Z(a))[b]||0;if(a.getBBox&&Ma(a))return a.getBBox()[b]||0;var d=parseFloat("width"===b?a.offsetWidth:a.offsetHeight),e=da[b],f=e.length;for(c=c||Z(a,null);--f>-1;)d-=parseFloat($(a,"padding"+e[f],c,!0))||0,d-=parseFloat($(a,"border"+e[f]+"Width",c,!0))||0;return d},ga=function(a,b){if("contain"===a||"auto"===a||"auto auto"===a)return a+" ";(null==a||""===a)&&(a="0 0");var c,d=a.split(" "),e=-1!==a.indexOf("left")?"0%":-1!==a.indexOf("right")?"100%":d[0],f=-1!==a.indexOf("top")?"0%":-1!==a.indexOf("bottom")?"100%":d[1];if(d.length>3&&!b){for(d=a.split(", ").join(",").split(","),a=[],c=0;c<d.length;c++)a.push(ga(d[c]));return a.join(",")}return null==f?f="center"===e?"50%":"0":"center"===f&&(f="50%"),("center"===e||isNaN(parseFloat(e))&&-1===(e+"").indexOf("="))&&(e="50%"),a=e+" "+f+(d.length>2?" "+d[2]:""),b&&(b.oxp=-1!==e.indexOf("%"),b.oyp=-1!==f.indexOf("%"),b.oxr="="===e.charAt(1),b.oyr="="===f.charAt(1),b.ox=parseFloat(e.replace(v,"")),b.oy=parseFloat(f.replace(v,"")),b.v=a),b||a},ha=function(a,b){return"function"==typeof a&&(a=a(r,q)),"string"==typeof a&&"="===a.charAt(1)?parseInt(a.charAt(0)+"1",10)*parseFloat(a.substr(2)):parseFloat(a)-parseFloat(b)||0},ia=function(a,b){return"function"==typeof a&&(a=a(r,q)),null==a?b:"string"==typeof a&&"="===a.charAt(1)?parseInt(a.charAt(0)+"1",10)*parseFloat(a.substr(2))+b:parseFloat(a)||0},ja=function(a,b,c,d){var e,f,g,h,i,j=1e-6;return"function"==typeof a&&(a=a(r,q)),null==a?h=b:"number"==typeof a?h=a:(e=360,f=a.split("_"),i="="===a.charAt(1),g=(i?parseInt(a.charAt(0)+"1",10)*parseFloat(f[0].substr(2)):parseFloat(f[0]))*(-1===a.indexOf("rad")?1:L)-(i?0:b),f.length&&(d&&(d[c]=b+g),-1!==a.indexOf("short")&&(g%=e,g!==g%(e/2)&&(g=0>g?g+e:g-e)),-1!==a.indexOf("_cw")&&0>g?g=(g+9999999999*e)%e-(g/e|0)*e:-1!==a.indexOf("ccw")&&g>0&&(g=(g-9999999999*e)%e-(g/e|0)*e)),h=b+g),j>h&&h>-j&&(h=0),h},ka={aqua:[0,255,255],lime:[0,255,0],silver:[192,192,192],black:[0,0,0],maroon:[128,0,0],teal:[0,128,128],blue:[0,0,255],navy:[0,0,128],white:[255,255,255],fuchsia:[255,0,255],olive:[128,128,0],yellow:[255,255,0],orange:[255,165,0],gray:[128,128,128],purple:[128,0,128],green:[0,128,0],red:[255,0,0],pink:[255,192,203],cyan:[0,255,255],transparent:[255,255,255,0]},la=function(a,b,c){return a=0>a?a+1:a>1?a-1:a,255*(1>6*a?b+(c-b)*a*6:.5>a?c:2>3*a?b+(c-b)*(2/3-a)*6:b)+.5|0},ma=g.parseColor=function(a,b){var c,d,e,f,g,h,i,j,k,l,m;if(a)if("number"==typeof a)c=[a>>16,a>>8&255,255&a];else{if(","===a.charAt(a.length-1)&&(a=a.substr(0,a.length-1)),ka[a])c=ka[a];else if("#"===a.charAt(0))4===a.length&&(d=a.charAt(1),e=a.charAt(2),f=a.charAt(3),a="#"+d+d+e+e+f+f),a=parseInt(a.substr(1),16),c=[a>>16,a>>8&255,255&a];else if("hsl"===a.substr(0,3))if(c=m=a.match(s),b){if(-1!==a.indexOf("="))return a.match(t)}else g=Number(c[0])%360/360,h=Number(c[1])/100,i=Number(c[2])/100,e=.5>=i?i*(h+1):i+h-i*h,d=2*i-e,c.length>3&&(c[3]=Number(a[3])),c[0]=la(g+1/3,d,e),c[1]=la(g,d,e),c[2]=la(g-1/3,d,e);else c=a.match(s)||ka.transparent;c[0]=Number(c[0]),c[1]=Number(c[1]),c[2]=Number(c[2]),c.length>3&&(c[3]=Number(c[3]))}else c=ka.black;return b&&!m&&(d=c[0]/255,e=c[1]/255,f=c[2]/255,j=Math.max(d,e,f),k=Math.min(d,e,f),i=(j+k)/2,j===k?g=h=0:(l=j-k,h=i>.5?l/(2-j-k):l/(j+k),g=j===d?(e-f)/l+(f>e?6:0):j===e?(f-d)/l+2:(d-e)/l+4,g*=60),c[0]=g+.5|0,c[1]=100*h+.5|0,c[2]=100*i+.5|0),c},na=function(a,b){var c,d,e,f=a.match(oa)||[],g=0,h=f.length?"":a;for(c=0;c<f.length;c++)d=f[c],e=a.substr(g,a.indexOf(d,g)-g),g+=e.length+d.length,d=ma(d,b),3===d.length&&d.push(1),h+=e+(b?"hsla("+d[0]+","+d[1]+"%,"+d[2]+"%,"+d[3]:"rgba("+d.join(","))+")";return h+a.substr(g)},oa="(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#(?:[0-9a-f]{3}){1,2}\\b";for(j in ka)oa+="|"+j+"\\b";oa=new RegExp(oa+")","gi"),g.colorStringFilter=function(a){var b,c=a[0]+a[1];oa.test(c)&&(b=-1!==c.indexOf("hsl(")||-1!==c.indexOf("hsla("),a[0]=na(a[0],b),a[1]=na(a[1],b)),oa.lastIndex=0},b.defaultStringFilter||(b.defaultStringFilter=g.colorStringFilter);var pa=function(a,b,c,d){if(null==a)return function(a){return a};var e,f=b?(a.match(oa)||[""])[0]:"",g=a.split(f).join("").match(u)||[],h=a.substr(0,a.indexOf(g[0])),i=")"===a.charAt(a.length-1)?")":"",j=-1!==a.indexOf(" ")?" ":",",k=g.length,l=k>0?g[0].replace(s,""):"";return k?e=b?function(a){var b,m,n,o;if("number"==typeof a)a+=l;else if(d&&I.test(a)){for(o=a.replace(I,"|").split("|"),n=0;n<o.length;n++)o[n]=e(o[n]);return o.join(",")}if(b=(a.match(oa)||[f])[0],m=a.split(b).join("").match(u)||[],n=m.length,k>n--)for(;++n<k;)m[n]=c?m[(n-1)/2|0]:g[n];return h+m.join(j)+j+b+i+(-1!==a.indexOf("inset")?" inset":"")}:function(a){var b,f,m;if("number"==typeof a)a+=l;else if(d&&I.test(a)){for(f=a.replace(I,"|").split("|"),m=0;m<f.length;m++)f[m]=e(f[m]);return f.join(",")}if(b=a.match(u)||[],m=b.length,k>m--)for(;++m<k;)b[m]=c?b[(m-1)/2|0]:g[m];return h+b.join(j)+i}:function(a){return a}},qa=function(a){return a=a.split(","),function(b,c,d,e,f,g,h){var i,j=(c+"").split(" ");for(h={},i=0;4>i;i++)h[a[i]]=j[i]=j[i]||j[(i-1)/2>>0];return e.parse(b,h,f,g)}},ra=(R._setPluginRatio=function(a){this.plugin.setRatio(a);for(var b,c,d,e,f,g=this.data,h=g.proxy,i=g.firstMPT,j=1e-6;i;)b=h[i.v],i.r?b=Math.round(b):j>b&&b>-j&&(b=0),i.t[i.p]=b,i=i._next;if(g.autoRotate&&(g.autoRotate.rotation=g.mod?g.mod(h.rotation,this.t):h.rotation),1===a||0===a)for(i=g.firstMPT,f=1===a?"e":"b";i;){if(c=i.t,c.type){if(1===c.type){for(e=c.xs0+c.s+c.xs1,d=1;d<c.l;d++)e+=c["xn"+d]+c["xs"+(d+1)];c[f]=e}}else c[f]=c.s+c.xs0;i=i._next}},function(a,b,c,d,e){this.t=a,this.p=b,this.v=c,this.r=e,d&&(d._prev=this,this._next=d)}),sa=(R._parseToProxy=function(a,b,c,d,e,f){var g,h,i,j,k,l=d,m={},n={},o=c._transform,p=M;for(c._transform=null,M=b,d=k=c.parse(a,b,d,e),M=p,f&&(c._transform=o,l&&(l._prev=null,l._prev&&(l._prev._next=null)));d&&d!==l;){if(d.type<=1&&(h=d.p,n[h]=d.s+d.c,m[h]=d.s,f||(j=new ra(d,"s",h,j,d.r),d.c=0),1===d.type))for(g=d.l;--g>0;)i="xn"+g,h=d.p+"_"+i,n[h]=d.data[i],m[h]=d[i],f||(j=new ra(d,i,h,j,d.rxp[i]));d=d._next}return{proxy:m,end:n,firstMPT:j,pt:k}},R.CSSPropTween=function(a,b,d,e,g,h,i,j,k,l,m){this.t=a,this.p=b,this.s=d,this.c=e,this.n=i||b,a instanceof sa||f.push(this.n),this.r=j,this.type=h||0,k&&(this.pr=k,c=!0),this.b=void 0===l?d:l,this.e=void 0===m?d+e:m,g&&(this._next=g,g._prev=this)}),ta=function(a,b,c,d,e,f){var g=new sa(a,b,c,d-c,e,-1,f);return g.b=c,g.e=g.xs0=d,g},ua=g.parseComplex=function(a,b,c,d,e,f,h,i,j,l){c=c||f||"","function"==typeof d&&(d=d(r,q)),h=new sa(a,b,0,0,h,l?2:1,null,!1,i,c,d),d+="",e&&oa.test(d+c)&&(d=[c,d],g.colorStringFilter(d),c=d[0],d=d[1]);var m,n,o,p,u,v,w,x,y,z,A,B,C,D=c.split(", ").join(",").split(" "),E=d.split(", ").join(",").split(" "),F=D.length,G=k!==!1;for((-1!==d.indexOf(",")||-1!==c.indexOf(","))&&(D=D.join(" ").replace(I,", ").split(" "),E=E.join(" ").replace(I,", ").split(" "),F=D.length),F!==E.length&&(D=(f||"").split(" "),F=D.length),h.plugin=j,h.setRatio=l,oa.lastIndex=0,m=0;F>m;m++)if(p=D[m],u=E[m],x=parseFloat(p),x||0===x)h.appendXtra("",x,ha(u,x),u.replace(t,""),G&&-1!==u.indexOf("px"),!0);else if(e&&oa.test(p))B=u.indexOf(")")+1,B=")"+(B?u.substr(B):""),C=-1!==u.indexOf("hsl")&&T,p=ma(p,C),u=ma(u,C),y=p.length+u.length>6,y&&!T&&0===u[3]?(h["xs"+h.l]+=h.l?" transparent":"transparent",h.e=h.e.split(E[m]).join("transparent")):(T||(y=!1),C?h.appendXtra(y?"hsla(":"hsl(",p[0],ha(u[0],p[0]),",",!1,!0).appendXtra("",p[1],ha(u[1],p[1]),"%,",!1).appendXtra("",p[2],ha(u[2],p[2]),y?"%,":"%"+B,!1):h.appendXtra(y?"rgba(":"rgb(",p[0],u[0]-p[0],",",!0,!0).appendXtra("",p[1],u[1]-p[1],",",!0).appendXtra("",p[2],u[2]-p[2],y?",":B,!0),y&&(p=p.length<4?1:p[3],h.appendXtra("",p,(u.length<4?1:u[3])-p,B,!1))),oa.lastIndex=0;else if(v=p.match(s)){if(w=u.match(t),!w||w.length!==v.length)return h;for(o=0,n=0;n<v.length;n++)A=v[n],z=p.indexOf(A,o),h.appendXtra(p.substr(o,z-o),Number(A),ha(w[n],A),"",G&&"px"===p.substr(z+A.length,2),0===n),o=z+A.length;h["xs"+h.l]+=p.substr(o)}else h["xs"+h.l]+=h.l||h["xs"+h.l]?" "+u:u;if(-1!==d.indexOf("=")&&h.data){for(B=h.xs0+h.data.s,m=1;m<h.l;m++)B+=h["xs"+m]+h.data["xn"+m];h.e=B+h["xs"+m]}return h.l||(h.type=-1,h.xs0=h.e),h.xfirst||h},va=9;for(j=sa.prototype,j.l=j.pr=0;--va>0;)j["xn"+va]=0,j["xs"+va]="";j.xs0="",j._next=j._prev=j.xfirst=j.data=j.plugin=j.setRatio=j.rxp=null,j.appendXtra=function(a,b,c,d,e,f){var g=this,h=g.l;return g["xs"+h]+=f&&(h||g["xs"+h])?" "+a:a||"",c||0===h||g.plugin?(g.l++,g.type=g.setRatio?2:1,g["xs"+g.l]=d||"",h>0?(g.data["xn"+h]=b+c,g.rxp["xn"+h]=e,g["xn"+h]=b,g.plugin||(g.xfirst=new sa(g,"xn"+h,b,c,g.xfirst||g,0,g.n,e,g.pr),g.xfirst.xs0=0),g):(g.data={s:b+c},g.rxp={},g.s=b,g.c=c,g.r=e,g)):(g["xs"+h]+=b+(d||""),g)};var wa=function(a,b){b=b||{},this.p=b.prefix?Y(a)||a:a,i[a]=i[this.p]=this,this.format=b.formatter||pa(b.defaultValue,b.color,b.collapsible,b.multi),b.parser&&(this.parse=b.parser),this.clrs=b.color,this.multi=b.multi,this.keyword=b.keyword,this.dflt=b.defaultValue,this.pr=b.priority||0},xa=R._registerComplexSpecialProp=function(a,b,c){"object"!=typeof b&&(b={parser:c});var d,e,f=a.split(","),g=b.defaultValue;for(c=c||[g],d=0;d<f.length;d++)b.prefix=0===d&&b.prefix,b.defaultValue=c[d]||g,e=new wa(f[d],b)},ya=R._registerPluginProp=function(a){if(!i[a]){var b=a.charAt(0).toUpperCase()+a.substr(1)+"Plugin";xa(a,{parser:function(a,c,d,e,f,g,j){var k=h.com.greensock.plugins[b];return k?(k._cssRegister(),i[d].parse(a,c,d,e,f,g,j)):(V("Error: "+b+" js file not loaded."),f)}})}};j=wa.prototype,j.parseComplex=function(a,b,c,d,e,f){var g,h,i,j,k,l,m=this.keyword;if(this.multi&&(I.test(c)||I.test(b)?(h=b.replace(I,"|").split("|"),i=c.replace(I,"|").split("|")):m&&(h=[b],i=[c])),i){for(j=i.length>h.length?i.length:h.length,g=0;j>g;g++)b=h[g]=h[g]||this.dflt,c=i[g]=i[g]||this.dflt,m&&(k=b.indexOf(m),l=c.indexOf(m),k!==l&&(-1===l?h[g]=h[g].split(m).join(""):-1===k&&(h[g]+=" "+m)));b=h.join(", "),c=i.join(", ")}return ua(a,this.p,b,c,this.clrs,this.dflt,d,this.pr,e,f)},j.parse=function(a,b,c,d,f,g,h){return this.parseComplex(a.style,this.format($(a,this.p,e,!1,this.dflt)),this.format(b),f,g)},g.registerSpecialProp=function(a,b,c){xa(a,{parser:function(a,d,e,f,g,h,i){var j=new sa(a,e,0,0,g,2,e,!1,c);return j.plugin=h,j.setRatio=b(a,d,f._tween,e),j},priority:c})},g.useSVGTransformAttr=m||n;var za,Aa="scaleX,scaleY,scaleZ,x,y,z,skewX,skewY,rotation,rotationX,rotationY,perspective,xPercent,yPercent".split(","),Ba=Y("transform"),Ca=W+"transform",Da=Y("transformOrigin"),Ea=null!==Y("perspective"),Fa=R.Transform=function(){this.perspective=parseFloat(g.defaultTransformPerspective)||0,this.force3D=g.defaultForce3D!==!1&&Ea?g.defaultForce3D||"auto":!1},Ga=window.SVGElement,Ha=function(a,b,c){var d,e=N.createElementNS("http://www.w3.org/2000/svg",a),f=/([a-z])([A-Z])/g;for(d in c)e.setAttributeNS(null,d.replace(f,"$1-$2").toLowerCase(),c[d]);return b.appendChild(e),e},Ia=N.documentElement,Ja=function(){var a,b,c,d=p||/Android/i.test(S)&&!window.chrome;return N.createElementNS&&!d&&(a=Ha("svg",Ia),b=Ha("rect",a,{width:100,height:50,x:100}),c=b.getBoundingClientRect().width,b.style[Da]="50% 50%",b.style[Ba]="scaleX(0.5)",d=c===b.getBoundingClientRect().width&&!(n&&Ea),Ia.removeChild(a)),d}(),Ka=function(a,b,c,d,e,f){var h,i,j,k,l,m,n,o,p,q,r,s,t,u,v=a._gsTransform,w=Oa(a,!0);v&&(t=v.xOrigin,u=v.yOrigin),(!d||(h=d.split(" ")).length<2)&&(n=a.getBBox(),b=ga(b).split(" "),h=[(-1!==b[0].indexOf("%")?parseFloat(b[0])/100*n.width:parseFloat(b[0]))+n.x,(-1!==b[1].indexOf("%")?parseFloat(b[1])/100*n.height:parseFloat(b[1]))+n.y]),c.xOrigin=k=parseFloat(h[0]),c.yOrigin=l=parseFloat(h[1]),d&&w!==Na&&(m=w[0],n=w[1],o=w[2],p=w[3],q=w[4],r=w[5],s=m*p-n*o,i=k*(p/s)+l*(-o/s)+(o*r-p*q)/s,j=k*(-n/s)+l*(m/s)-(m*r-n*q)/s,k=c.xOrigin=h[0]=i,l=c.yOrigin=h[1]=j),v&&(f&&(c.xOffset=v.xOffset,c.yOffset=v.yOffset,v=c),e||e!==!1&&g.defaultSmoothOrigin!==!1?(i=k-t,j=l-u,v.xOffset+=i*w[0]+j*w[2]-i,v.yOffset+=i*w[1]+j*w[3]-j):v.xOffset=v.yOffset=0),f||a.setAttribute("data-svg-origin",h.join(" "))},La=function(a){try{return a.getBBox()}catch(a){}},Ma=function(a){return!!(Ga&&a.getBBox&&a.getCTM&&La(a)&&(!a.parentNode||a.parentNode.getBBox&&a.parentNode.getCTM))},Na=[1,0,0,1,0,0],Oa=function(a,b){var c,d,e,f,g,h,i=a._gsTransform||new Fa,j=1e5,k=a.style;if(Ba?d=$(a,Ca,null,!0):a.currentStyle&&(d=a.currentStyle.filter.match(G),d=d&&4===d.length?[d[0].substr(4),Number(d[2].substr(4)),Number(d[1].substr(4)),d[3].substr(4),i.x||0,i.y||0].join(","):""),c=!d||"none"===d||"matrix(1, 0, 0, 1, 0, 0)"===d,c&&Ba&&((h="none"===Z(a).display)||!a.parentNode)&&(h&&(f=k.display,k.display="block"),a.parentNode||(g=1,Ia.appendChild(a)),d=$(a,Ca,null,!0),c=!d||"none"===d||"matrix(1, 0, 0, 1, 0, 0)"===d,f?k.display=f:h&&Ta(k,"display"),g&&Ia.removeChild(a)),(i.svg||a.getBBox&&Ma(a))&&(c&&-1!==(k[Ba]+"").indexOf("matrix")&&(d=k[Ba],c=0),e=a.getAttribute("transform"),c&&e&&(-1!==e.indexOf("matrix")?(d=e,c=0):-1!==e.indexOf("translate")&&(d="matrix(1,0,0,1,"+e.match(/(?:\-|\b)[\d\-\.e]+\b/gi).join(",")+")",c=0))),c)return Na;for(e=(d||"").match(s)||[],va=e.length;--va>-1;)f=Number(e[va]),e[va]=(g=f-(f|=0))?(g*j+(0>g?-.5:.5)|0)/j+f:f;return b&&e.length>6?[e[0],e[1],e[4],e[5],e[12],e[13]]:e},Pa=R.getTransform=function(a,c,d,e){if(a._gsTransform&&d&&!e)return a._gsTransform;var f,h,i,j,k,l,m=d?a._gsTransform||new Fa:new Fa,n=m.scaleX<0,o=2e-5,p=1e5,q=Ea?parseFloat($(a,Da,c,!1,"0 0 0").split(" ")[2])||m.zOrigin||0:0,r=parseFloat(g.defaultTransformPerspective)||0;if(m.svg=!(!a.getBBox||!Ma(a)),m.svg&&(Ka(a,$(a,Da,c,!1,"50% 50%")+"",m,a.getAttribute("data-svg-origin")),za=g.useSVGTransformAttr||Ja),f=Oa(a),f!==Na){if(16===f.length){var s,t,u,v,w,x=f[0],y=f[1],z=f[2],A=f[3],B=f[4],C=f[5],D=f[6],E=f[7],F=f[8],G=f[9],H=f[10],I=f[12],J=f[13],K=f[14],M=f[11],N=Math.atan2(D,H);m.zOrigin&&(K=-m.zOrigin,I=F*K-f[12],J=G*K-f[13],K=H*K+m.zOrigin-f[14]),m.rotationX=N*L,N&&(v=Math.cos(-N),w=Math.sin(-N),s=B*v+F*w,t=C*v+G*w,u=D*v+H*w,F=B*-w+F*v,G=C*-w+G*v,H=D*-w+H*v,M=E*-w+M*v,B=s,C=t,D=u),N=Math.atan2(-z,H),m.rotationY=N*L,N&&(v=Math.cos(-N),w=Math.sin(-N),s=x*v-F*w,t=y*v-G*w,u=z*v-H*w,G=y*w+G*v,H=z*w+H*v,M=A*w+M*v,x=s,y=t,z=u),N=Math.atan2(y,x),m.rotation=N*L,N&&(v=Math.cos(-N),w=Math.sin(-N),x=x*v+B*w,t=y*v+C*w,C=y*-w+C*v,D=z*-w+D*v,y=t),m.rotationX&&Math.abs(m.rotationX)+Math.abs(m.rotation)>359.9&&(m.rotationX=m.rotation=0,m.rotationY=180-m.rotationY),m.scaleX=(Math.sqrt(x*x+y*y)*p+.5|0)/p,m.scaleY=(Math.sqrt(C*C+G*G)*p+.5|0)/p,m.scaleZ=(Math.sqrt(D*D+H*H)*p+.5|0)/p,m.rotationX||m.rotationY?m.skewX=0:(m.skewX=B||C?Math.atan2(B,C)*L+m.rotation:m.skewX||0,Math.abs(m.skewX)>90&&Math.abs(m.skewX)<270&&(n?(m.scaleX*=-1,m.skewX+=m.rotation<=0?180:-180,m.rotation+=m.rotation<=0?180:-180):(m.scaleY*=-1,m.skewX+=m.skewX<=0?180:-180))),m.perspective=M?1/(0>M?-M:M):0,m.x=I,m.y=J,m.z=K,m.svg&&(m.x-=m.xOrigin-(m.xOrigin*x-m.yOrigin*B),m.y-=m.yOrigin-(m.yOrigin*y-m.xOrigin*C))}else if(!Ea||e||!f.length||m.x!==f[4]||m.y!==f[5]||!m.rotationX&&!m.rotationY){var O=f.length>=6,P=O?f[0]:1,Q=f[1]||0,R=f[2]||0,S=O?f[3]:1;m.x=f[4]||0,m.y=f[5]||0,i=Math.sqrt(P*P+Q*Q),j=Math.sqrt(S*S+R*R),k=P||Q?Math.atan2(Q,P)*L:m.rotation||0,l=R||S?Math.atan2(R,S)*L+k:m.skewX||0,Math.abs(l)>90&&Math.abs(l)<270&&(n?(i*=-1,l+=0>=k?180:-180,k+=0>=k?180:-180):(j*=-1,l+=0>=l?180:-180)),m.scaleX=i,m.scaleY=j,m.rotation=k,m.skewX=l,Ea&&(m.rotationX=m.rotationY=m.z=0,m.perspective=r,m.scaleZ=1),m.svg&&(m.x-=m.xOrigin-(m.xOrigin*P+m.yOrigin*R),m.y-=m.yOrigin-(m.xOrigin*Q+m.yOrigin*S))}m.zOrigin=q;for(h in m)m[h]<o&&m[h]>-o&&(m[h]=0)}return d&&(a._gsTransform=m,m.svg&&(za&&a.style[Ba]?b.delayedCall(.001,function(){Ta(a.style,Ba)}):!za&&a.getAttribute("transform")&&b.delayedCall(.001,function(){a.removeAttribute("transform")}))),m},Qa=function(a){var b,c,d=this.data,e=-d.rotation*K,f=e+d.skewX*K,g=1e5,h=(Math.cos(e)*d.scaleX*g|0)/g,i=(Math.sin(e)*d.scaleX*g|0)/g,j=(Math.sin(f)*-d.scaleY*g|0)/g,k=(Math.cos(f)*d.scaleY*g|0)/g,l=this.t.style,m=this.t.currentStyle;if(m){c=i,i=-j,j=-c,b=m.filter,l.filter="";var n,o,q=this.t.offsetWidth,r=this.t.offsetHeight,s="absolute"!==m.position,t="progid:DXImageTransform.Microsoft.Matrix(M11="+h+", M12="+i+", M21="+j+", M22="+k,u=d.x+q*d.xPercent/100,v=d.y+r*d.yPercent/100;if(null!=d.ox&&(n=(d.oxp?q*d.ox*.01:d.ox)-q/2,o=(d.oyp?r*d.oy*.01:d.oy)-r/2,u+=n-(n*h+o*i),v+=o-(n*j+o*k)),s?(n=q/2,o=r/2,t+=", Dx="+(n-(n*h+o*i)+u)+", Dy="+(o-(n*j+o*k)+v)+")"):t+=", sizingMethod='auto expand')",-1!==b.indexOf("DXImageTransform.Microsoft.Matrix(")?l.filter=b.replace(H,t):l.filter=t+" "+b,(0===a||1===a)&&1===h&&0===i&&0===j&&1===k&&(s&&-1===t.indexOf("Dx=0, Dy=0")||x.test(b)&&100!==parseFloat(RegExp.$1)||-1===b.indexOf(b.indexOf("Alpha"))&&l.removeAttribute("filter")),!s){var y,z,A,B=8>p?1:-1;for(n=d.ieOffsetX||0,o=d.ieOffsetY||0,d.ieOffsetX=Math.round((q-((0>h?-h:h)*q+(0>i?-i:i)*r))/2+u),d.ieOffsetY=Math.round((r-((0>k?-k:k)*r+(0>j?-j:j)*q))/2+v),va=0;4>va;va++)z=ea[va],y=m[z],c=-1!==y.indexOf("px")?parseFloat(y):_(this.t,z,parseFloat(y),y.replace(w,""))||0,A=c!==d[z]?2>va?-d.ieOffsetX:-d.ieOffsetY:2>va?n-d.ieOffsetX:o-d.ieOffsetY,l[z]=(d[z]=Math.round(c-A*(0===va||2===va?1:B)))+"px"}}},Ra=R.set3DTransformRatio=R.setTransformRatio=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,o,p,q,r,s,t,u,v,w,x,y,z=this.data,A=this.t.style,B=z.rotation,C=z.rotationX,D=z.rotationY,E=z.scaleX,F=z.scaleY,G=z.scaleZ,H=z.x,I=z.y,J=z.z,L=z.svg,M=z.perspective,N=z.force3D;if(((1===a||0===a)&&"auto"===N&&(this.tween._totalTime===this.tween._totalDuration||!this.tween._totalTime)||!N)&&!J&&!M&&!D&&!C&&1===G||za&&L||!Ea)return void(B||z.skewX||L?(B*=K,x=z.skewX*K,y=1e5,b=Math.cos(B)*E,e=Math.sin(B)*E,c=Math.sin(B-x)*-F,f=Math.cos(B-x)*F,x&&"simple"===z.skewType&&(s=Math.tan(x-z.skewY*K),s=Math.sqrt(1+s*s),c*=s,f*=s,z.skewY&&(s=Math.tan(z.skewY*K),s=Math.sqrt(1+s*s),b*=s,e*=s)),L&&(H+=z.xOrigin-(z.xOrigin*b+z.yOrigin*c)+z.xOffset,I+=z.yOrigin-(z.xOrigin*e+z.yOrigin*f)+z.yOffset,za&&(z.xPercent||z.yPercent)&&(p=this.t.getBBox(),H+=.01*z.xPercent*p.width,I+=.01*z.yPercent*p.height),p=1e-6,p>H&&H>-p&&(H=0),p>I&&I>-p&&(I=0)),u=(b*y|0)/y+","+(e*y|0)/y+","+(c*y|0)/y+","+(f*y|0)/y+","+H+","+I+")",L&&za?this.t.setAttribute("transform","matrix("+u):A[Ba]=(z.xPercent||z.yPercent?"translate("+z.xPercent+"%,"+z.yPercent+"%) matrix(":"matrix(")+u):A[Ba]=(z.xPercent||z.yPercent?"translate("+z.xPercent+"%,"+z.yPercent+"%) matrix(":"matrix(")+E+",0,0,"+F+","+H+","+I+")");if(n&&(p=1e-4,p>E&&E>-p&&(E=G=2e-5),p>F&&F>-p&&(F=G=2e-5),!M||z.z||z.rotationX||z.rotationY||(M=0)),B||z.skewX)B*=K,q=b=Math.cos(B),r=e=Math.sin(B),z.skewX&&(B-=z.skewX*K,q=Math.cos(B),r=Math.sin(B),"simple"===z.skewType&&(s=Math.tan((z.skewX-z.skewY)*K),s=Math.sqrt(1+s*s),q*=s,r*=s,z.skewY&&(s=Math.tan(z.skewY*K),s=Math.sqrt(1+s*s),b*=s,e*=s))),c=-r,f=q;else{if(!(D||C||1!==G||M||L))return void(A[Ba]=(z.xPercent||z.yPercent?"translate("+z.xPercent+"%,"+z.yPercent+"%) translate3d(":"translate3d(")+H+"px,"+I+"px,"+J+"px)"+(1!==E||1!==F?" scale("+E+","+F+")":""));b=f=1,c=e=0}j=1,d=g=h=i=k=l=0,m=M?-1/M:0,o=z.zOrigin,p=1e-6,v=",",w="0",B=D*K,B&&(q=Math.cos(B),r=Math.sin(B),h=-r,k=m*-r,d=b*r,g=e*r,j=q,m*=q,b*=q,e*=q),B=C*K,B&&(q=Math.cos(B),r=Math.sin(B),s=c*q+d*r,t=f*q+g*r,i=j*r,l=m*r,d=c*-r+d*q,g=f*-r+g*q,j*=q,m*=q,c=s,f=t),1!==G&&(d*=G,g*=G,j*=G,m*=G),1!==F&&(c*=F,f*=F,i*=F,l*=F),1!==E&&(b*=E,e*=E,h*=E,k*=E),(o||L)&&(o&&(H+=d*-o,I+=g*-o,J+=j*-o+o),L&&(H+=z.xOrigin-(z.xOrigin*b+z.yOrigin*c)+z.xOffset,I+=z.yOrigin-(z.xOrigin*e+z.yOrigin*f)+z.yOffset),p>H&&H>-p&&(H=w),p>I&&I>-p&&(I=w),p>J&&J>-p&&(J=0)),u=z.xPercent||z.yPercent?"translate("+z.xPercent+"%,"+z.yPercent+"%) matrix3d(":"matrix3d(",u+=(p>b&&b>-p?w:b)+v+(p>e&&e>-p?w:e)+v+(p>h&&h>-p?w:h),u+=v+(p>k&&k>-p?w:k)+v+(p>c&&c>-p?w:c)+v+(p>f&&f>-p?w:f),C||D||1!==G?(u+=v+(p>i&&i>-p?w:i)+v+(p>l&&l>-p?w:l)+v+(p>d&&d>-p?w:d),u+=v+(p>g&&g>-p?w:g)+v+(p>j&&j>-p?w:j)+v+(p>m&&m>-p?w:m)+v):u+=",0,0,0,0,1,0,",u+=H+v+I+v+J+v+(M?1+-J/M:1)+")",A[Ba]=u};j=Fa.prototype,j.x=j.y=j.z=j.skewX=j.skewY=j.rotation=j.rotationX=j.rotationY=j.zOrigin=j.xPercent=j.yPercent=j.xOffset=j.yOffset=0,j.scaleX=j.scaleY=j.scaleZ=1,xa("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,svgOrigin,transformPerspective,directionalRotation,parseTransform,force3D,skewType,xPercent,yPercent,smoothOrigin",{parser:function(a,b,c,d,f,h,i){if(d._lastParsedTransform===i)return f;d._lastParsedTransform=i;var j;"function"==typeof i[c]&&(j=i[c],i[c]=b);var k,l,m,n,o,p,s,t,u,v=a._gsTransform,w=a.style,x=1e-6,y=Aa.length,z=i,A={},B="transformOrigin",C=Pa(a,e,!0,z.parseTransform),D=z.transform&&("function"==typeof z.transform?z.transform(r,q):z.transform);if(d._transform=C,D&&"string"==typeof D&&Ba)l=P.style,l[Ba]=D,l.display="block",l.position="absolute",N.body.appendChild(P),k=Pa(P,null,!1),C.svg&&(p=C.xOrigin,s=C.yOrigin,k.x-=C.xOffset,k.y-=C.yOffset,(z.transformOrigin||z.svgOrigin)&&(D={},Ka(a,ga(z.transformOrigin),D,z.svgOrigin,z.smoothOrigin,!0),p=D.xOrigin,s=D.yOrigin,k.x-=D.xOffset-C.xOffset,k.y-=D.yOffset-C.yOffset),(p||s)&&(t=Oa(P,!0),k.x-=p-(p*t[0]+s*t[2]),k.y-=s-(p*t[1]+s*t[3]))),N.body.removeChild(P),k.perspective||(k.perspective=C.perspective),null!=z.xPercent&&(k.xPercent=ia(z.xPercent,C.xPercent)),null!=z.yPercent&&(k.yPercent=ia(z.yPercent,C.yPercent));else if("object"==typeof z){if(k={scaleX:ia(null!=z.scaleX?z.scaleX:z.scale,C.scaleX),scaleY:ia(null!=z.scaleY?z.scaleY:z.scale,C.scaleY),scaleZ:ia(z.scaleZ,C.scaleZ),x:ia(z.x,C.x),y:ia(z.y,C.y),z:ia(z.z,C.z),xPercent:ia(z.xPercent,C.xPercent),yPercent:ia(z.yPercent,C.yPercent),perspective:ia(z.transformPerspective,C.perspective)},o=z.directionalRotation,null!=o)if("object"==typeof o)for(l in o)z[l]=o[l];else z.rotation=o;"string"==typeof z.x&&-1!==z.x.indexOf("%")&&(k.x=0,k.xPercent=ia(z.x,C.xPercent)),"string"==typeof z.y&&-1!==z.y.indexOf("%")&&(k.y=0,k.yPercent=ia(z.y,C.yPercent)),k.rotation=ja("rotation"in z?z.rotation:"shortRotation"in z?z.shortRotation+"_short":"rotationZ"in z?z.rotationZ:C.rotation-C.skewY,C.rotation-C.skewY,"rotation",A),Ea&&(k.rotationX=ja("rotationX"in z?z.rotationX:"shortRotationX"in z?z.shortRotationX+"_short":C.rotationX||0,C.rotationX,"rotationX",A),k.rotationY=ja("rotationY"in z?z.rotationY:"shortRotationY"in z?z.shortRotationY+"_short":C.rotationY||0,C.rotationY,"rotationY",A)),k.skewX=ja(z.skewX,C.skewX-C.skewY),(k.skewY=ja(z.skewY,C.skewY))&&(k.skewX+=k.skewY,k.rotation+=k.skewY)}for(Ea&&null!=z.force3D&&(C.force3D=z.force3D,n=!0),C.skewType=z.skewType||C.skewType||g.defaultSkewType,m=C.force3D||C.z||C.rotationX||C.rotationY||k.z||k.rotationX||k.rotationY||k.perspective,m||null==z.scale||(k.scaleZ=1);--y>-1;)u=Aa[y],D=k[u]-C[u],(D>x||-x>D||null!=z[u]||null!=M[u])&&(n=!0,
f=new sa(C,u,C[u],D,f),u in A&&(f.e=A[u]),f.xs0=0,f.plugin=h,d._overwriteProps.push(f.n));return D=z.transformOrigin,C.svg&&(D||z.svgOrigin)&&(p=C.xOffset,s=C.yOffset,Ka(a,ga(D),k,z.svgOrigin,z.smoothOrigin),f=ta(C,"xOrigin",(v?C:k).xOrigin,k.xOrigin,f,B),f=ta(C,"yOrigin",(v?C:k).yOrigin,k.yOrigin,f,B),(p!==C.xOffset||s!==C.yOffset)&&(f=ta(C,"xOffset",v?p:C.xOffset,C.xOffset,f,B),f=ta(C,"yOffset",v?s:C.yOffset,C.yOffset,f,B)),D=za?null:"0px 0px"),(D||Ea&&m&&C.zOrigin)&&(Ba?(n=!0,u=Da,D=(D||$(a,u,e,!1,"50% 50%"))+"",f=new sa(w,u,0,0,f,-1,B),f.b=w[u],f.plugin=h,Ea?(l=C.zOrigin,D=D.split(" "),C.zOrigin=(D.length>2&&(0===l||"0px"!==D[2])?parseFloat(D[2]):l)||0,f.xs0=f.e=D[0]+" "+(D[1]||"50%")+" 0px",f=new sa(C,"zOrigin",0,0,f,-1,f.n),f.b=l,f.xs0=f.e=C.zOrigin):f.xs0=f.e=D):ga(D+"",C)),n&&(d._transformType=C.svg&&za||!m&&3!==this._transformType?2:3),j&&(i[c]=j),f},prefix:!0}),xa("boxShadow",{defaultValue:"0px 0px 0px 0px #999",prefix:!0,color:!0,multi:!0,keyword:"inset"}),xa("borderRadius",{defaultValue:"0px",parser:function(a,b,c,f,g,h){b=this.format(b);var i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y=["borderTopLeftRadius","borderTopRightRadius","borderBottomRightRadius","borderBottomLeftRadius"],z=a.style;for(q=parseFloat(a.offsetWidth),r=parseFloat(a.offsetHeight),i=b.split(" "),j=0;j<y.length;j++)this.p.indexOf("border")&&(y[j]=Y(y[j])),m=l=$(a,y[j],e,!1,"0px"),-1!==m.indexOf(" ")&&(l=m.split(" "),m=l[0],l=l[1]),n=k=i[j],o=parseFloat(m),t=m.substr((o+"").length),u="="===n.charAt(1),u?(p=parseInt(n.charAt(0)+"1",10),n=n.substr(2),p*=parseFloat(n),s=n.substr((p+"").length-(0>p?1:0))||""):(p=parseFloat(n),s=n.substr((p+"").length)),""===s&&(s=d[c]||t),s!==t&&(v=_(a,"borderLeft",o,t),w=_(a,"borderTop",o,t),"%"===s?(m=v/q*100+"%",l=w/r*100+"%"):"em"===s?(x=_(a,"borderLeft",1,"em"),m=v/x+"em",l=w/x+"em"):(m=v+"px",l=w+"px"),u&&(n=parseFloat(m)+p+s,k=parseFloat(l)+p+s)),g=ua(z,y[j],m+" "+l,n+" "+k,!1,"0px",g);return g},prefix:!0,formatter:pa("0px 0px 0px 0px",!1,!0)}),xa("borderBottomLeftRadius,borderBottomRightRadius,borderTopLeftRadius,borderTopRightRadius",{defaultValue:"0px",parser:function(a,b,c,d,f,g){return ua(a.style,c,this.format($(a,c,e,!1,"0px 0px")),this.format(b),!1,"0px",f)},prefix:!0,formatter:pa("0px 0px",!1,!0)}),xa("backgroundPosition",{defaultValue:"0 0",parser:function(a,b,c,d,f,g){var h,i,j,k,l,m,n="background-position",o=e||Z(a,null),q=this.format((o?p?o.getPropertyValue(n+"-x")+" "+o.getPropertyValue(n+"-y"):o.getPropertyValue(n):a.currentStyle.backgroundPositionX+" "+a.currentStyle.backgroundPositionY)||"0 0"),r=this.format(b);if(-1!==q.indexOf("%")!=(-1!==r.indexOf("%"))&&r.split(",").length<2&&(m=$(a,"backgroundImage").replace(D,""),m&&"none"!==m)){for(h=q.split(" "),i=r.split(" "),Q.setAttribute("src",m),j=2;--j>-1;)q=h[j],k=-1!==q.indexOf("%"),k!==(-1!==i[j].indexOf("%"))&&(l=0===j?a.offsetWidth-Q.width:a.offsetHeight-Q.height,h[j]=k?parseFloat(q)/100*l+"px":parseFloat(q)/l*100+"%");q=h.join(" ")}return this.parseComplex(a.style,q,r,f,g)},formatter:ga}),xa("backgroundSize",{defaultValue:"0 0",formatter:function(a){return a+="",ga(-1===a.indexOf(" ")?a+" "+a:a)}}),xa("perspective",{defaultValue:"0px",prefix:!0}),xa("perspectiveOrigin",{defaultValue:"50% 50%",prefix:!0}),xa("transformStyle",{prefix:!0}),xa("backfaceVisibility",{prefix:!0}),xa("userSelect",{prefix:!0}),xa("margin",{parser:qa("marginTop,marginRight,marginBottom,marginLeft")}),xa("padding",{parser:qa("paddingTop,paddingRight,paddingBottom,paddingLeft")}),xa("clip",{defaultValue:"rect(0px,0px,0px,0px)",parser:function(a,b,c,d,f,g){var h,i,j;return 9>p?(i=a.currentStyle,j=8>p?" ":",",h="rect("+i.clipTop+j+i.clipRight+j+i.clipBottom+j+i.clipLeft+")",b=this.format(b).split(",").join(j)):(h=this.format($(a,this.p,e,!1,this.dflt)),b=this.format(b)),this.parseComplex(a.style,h,b,f,g)}}),xa("textShadow",{defaultValue:"0px 0px 0px #999",color:!0,multi:!0}),xa("autoRound,strictUnits",{parser:function(a,b,c,d,e){return e}}),xa("border",{defaultValue:"0px solid #000",parser:function(a,b,c,d,f,g){var h=$(a,"borderTopWidth",e,!1,"0px"),i=this.format(b).split(" "),j=i[0].replace(w,"");return"px"!==j&&(h=parseFloat(h)/_(a,"borderTopWidth",1,j)+j),this.parseComplex(a.style,this.format(h+" "+$(a,"borderTopStyle",e,!1,"solid")+" "+$(a,"borderTopColor",e,!1,"#000")),i.join(" "),f,g)},color:!0,formatter:function(a){var b=a.split(" ");return b[0]+" "+(b[1]||"solid")+" "+(a.match(oa)||["#000"])[0]}}),xa("borderWidth",{parser:qa("borderTopWidth,borderRightWidth,borderBottomWidth,borderLeftWidth")}),xa("float,cssFloat,styleFloat",{parser:function(a,b,c,d,e,f){var g=a.style,h="cssFloat"in g?"cssFloat":"styleFloat";return new sa(g,h,0,0,e,-1,c,!1,0,g[h],b)}});var Sa=function(a){var b,c=this.t,d=c.filter||$(this.data,"filter")||"",e=this.s+this.c*a|0;100===e&&(-1===d.indexOf("atrix(")&&-1===d.indexOf("radient(")&&-1===d.indexOf("oader(")?(c.removeAttribute("filter"),b=!$(this.data,"filter")):(c.filter=d.replace(z,""),b=!0)),b||(this.xn1&&(c.filter=d=d||"alpha(opacity="+e+")"),-1===d.indexOf("pacity")?0===e&&this.xn1||(c.filter=d+" alpha(opacity="+e+")"):c.filter=d.replace(x,"opacity="+e))};xa("opacity,alpha,autoAlpha",{defaultValue:"1",parser:function(a,b,c,d,f,g){var h=parseFloat($(a,"opacity",e,!1,"1")),i=a.style,j="autoAlpha"===c;return"string"==typeof b&&"="===b.charAt(1)&&(b=("-"===b.charAt(0)?-1:1)*parseFloat(b.substr(2))+h),j&&1===h&&"hidden"===$(a,"visibility",e)&&0!==b&&(h=0),T?f=new sa(i,"opacity",h,b-h,f):(f=new sa(i,"opacity",100*h,100*(b-h),f),f.xn1=j?1:0,i.zoom=1,f.type=2,f.b="alpha(opacity="+f.s+")",f.e="alpha(opacity="+(f.s+f.c)+")",f.data=a,f.plugin=g,f.setRatio=Sa),j&&(f=new sa(i,"visibility",0,0,f,-1,null,!1,0,0!==h?"inherit":"hidden",0===b?"hidden":"inherit"),f.xs0="inherit",d._overwriteProps.push(f.n),d._overwriteProps.push(c)),f}});var Ta=function(a,b){b&&(a.removeProperty?(("ms"===b.substr(0,2)||"webkit"===b.substr(0,6))&&(b="-"+b),a.removeProperty(b.replace(B,"-$1").toLowerCase())):a.removeAttribute(b))},Ua=function(a){if(this.t._gsClassPT=this,1===a||0===a){this.t.setAttribute("class",0===a?this.b:this.e);for(var b=this.data,c=this.t.style;b;)b.v?c[b.p]=b.v:Ta(c,b.p),b=b._next;1===a&&this.t._gsClassPT===this&&(this.t._gsClassPT=null)}else this.t.getAttribute("class")!==this.e&&this.t.setAttribute("class",this.e)};xa("className",{parser:function(a,b,d,f,g,h,i){var j,k,l,m,n,o=a.getAttribute("class")||"",p=a.style.cssText;if(g=f._classNamePT=new sa(a,d,0,0,g,2),g.setRatio=Ua,g.pr=-11,c=!0,g.b=o,k=ba(a,e),l=a._gsClassPT){for(m={},n=l.data;n;)m[n.p]=1,n=n._next;l.setRatio(1)}return a._gsClassPT=g,g.e="="!==b.charAt(1)?b:o.replace(new RegExp("(?:\\s|^)"+b.substr(2)+"(?![\\w-])"),"")+("+"===b.charAt(0)?" "+b.substr(2):""),a.setAttribute("class",g.e),j=ca(a,k,ba(a),i,m),a.setAttribute("class",o),g.data=j.firstMPT,a.style.cssText=p,g=g.xfirst=f.parse(a,j.difs,g,h)}});var Va=function(a){if((1===a||0===a)&&this.data._totalTime===this.data._totalDuration&&"isFromStart"!==this.data.data){var b,c,d,e,f,g=this.t.style,h=i.transform.parse;if("all"===this.e)g.cssText="",e=!0;else for(b=this.e.split(" ").join("").split(","),d=b.length;--d>-1;)c=b[d],i[c]&&(i[c].parse===h?e=!0:c="transformOrigin"===c?Da:i[c].p),Ta(g,c);e&&(Ta(g,Ba),f=this.t._gsTransform,f&&(f.svg&&(this.t.removeAttribute("data-svg-origin"),this.t.removeAttribute("transform")),delete this.t._gsTransform))}};for(xa("clearProps",{parser:function(a,b,d,e,f){return f=new sa(a,d,0,0,f,2),f.setRatio=Va,f.e=b,f.pr=-10,f.data=e._tween,c=!0,f}}),j="bezier,throwProps,physicsProps,physics2D".split(","),va=j.length;va--;)ya(j[va]);j=g.prototype,j._firstPT=j._lastParsedTransform=j._transform=null,j._onInitTween=function(a,b,h,j){if(!a.nodeType)return!1;this._target=q=a,this._tween=h,this._vars=b,r=j,k=b.autoRound,c=!1,d=b.suffixMap||g.suffixMap,e=Z(a,""),f=this._overwriteProps;var n,p,s,t,u,v,w,x,z,A=a.style;if(l&&""===A.zIndex&&(n=$(a,"zIndex",e),("auto"===n||""===n)&&this._addLazySet(A,"zIndex",0)),"string"==typeof b&&(t=A.cssText,n=ba(a,e),A.cssText=t+";"+b,n=ca(a,n,ba(a)).difs,!T&&y.test(b)&&(n.opacity=parseFloat(RegExp.$1)),b=n,A.cssText=t),b.className?this._firstPT=p=i.className.parse(a,b.className,"className",this,null,null,b):this._firstPT=p=this.parse(a,b,null),this._transformType){for(z=3===this._transformType,Ba?m&&(l=!0,""===A.zIndex&&(w=$(a,"zIndex",e),("auto"===w||""===w)&&this._addLazySet(A,"zIndex",0)),o&&this._addLazySet(A,"WebkitBackfaceVisibility",this._vars.WebkitBackfaceVisibility||(z?"visible":"hidden"))):A.zoom=1,s=p;s&&s._next;)s=s._next;x=new sa(a,"transform",0,0,null,2),this._linkCSSP(x,null,s),x.setRatio=Ba?Ra:Qa,x.data=this._transform||Pa(a,e,!0),x.tween=h,x.pr=-1,f.pop()}if(c){for(;p;){for(v=p._next,s=t;s&&s.pr>p.pr;)s=s._next;(p._prev=s?s._prev:u)?p._prev._next=p:t=p,(p._next=s)?s._prev=p:u=p,p=v}this._firstPT=t}return!0},j.parse=function(a,b,c,f){var g,h,j,l,m,n,o,p,s,t,u=a.style;for(g in b)n=b[g],"function"==typeof n&&(n=n(r,q)),h=i[g],h?c=h.parse(a,n,g,this,c,f,b):(m=$(a,g,e)+"",s="string"==typeof n,"color"===g||"fill"===g||"stroke"===g||-1!==g.indexOf("Color")||s&&A.test(n)?(s||(n=ma(n),n=(n.length>3?"rgba(":"rgb(")+n.join(",")+")"),c=ua(u,g,m,n,!0,"transparent",c,0,f)):s&&J.test(n)?c=ua(u,g,m,n,!0,null,c,0,f):(j=parseFloat(m),o=j||0===j?m.substr((j+"").length):"",(""===m||"auto"===m)&&("width"===g||"height"===g?(j=fa(a,g,e),o="px"):"left"===g||"top"===g?(j=aa(a,g,e),o="px"):(j="opacity"!==g?0:1,o="")),t=s&&"="===n.charAt(1),t?(l=parseInt(n.charAt(0)+"1",10),n=n.substr(2),l*=parseFloat(n),p=n.replace(w,"")):(l=parseFloat(n),p=s?n.replace(w,""):""),""===p&&(p=g in d?d[g]:o),n=l||0===l?(t?l+j:l)+p:b[g],o!==p&&""!==p&&(l||0===l)&&j&&(j=_(a,g,j,o),"%"===p?(j/=_(a,g,100,"%")/100,b.strictUnits!==!0&&(m=j+"%")):"em"===p||"rem"===p||"vw"===p||"vh"===p?j/=_(a,g,1,p):"px"!==p&&(l=_(a,g,l,p),p="px"),t&&(l||0===l)&&(n=l+j+p)),t&&(l+=j),!j&&0!==j||!l&&0!==l?void 0!==u[g]&&(n||n+""!="NaN"&&null!=n)?(c=new sa(u,g,l||j||0,0,c,-1,g,!1,0,m,n),c.xs0="none"!==n||"display"!==g&&-1===g.indexOf("Style")?n:m):V("invalid "+g+" tween value: "+b[g]):(c=new sa(u,g,j,l-j,c,0,g,k!==!1&&("px"===p||"zIndex"===g),0,m,n),c.xs0=p))),f&&c&&!c.plugin&&(c.plugin=f);return c},j.setRatio=function(a){var b,c,d,e=this._firstPT,f=1e-6;if(1!==a||this._tween._time!==this._tween._duration&&0!==this._tween._time)if(a||this._tween._time!==this._tween._duration&&0!==this._tween._time||this._tween._rawPrevTime===-1e-6)for(;e;){if(b=e.c*a+e.s,e.r?b=Math.round(b):f>b&&b>-f&&(b=0),e.type)if(1===e.type)if(d=e.l,2===d)e.t[e.p]=e.xs0+b+e.xs1+e.xn1+e.xs2;else if(3===d)e.t[e.p]=e.xs0+b+e.xs1+e.xn1+e.xs2+e.xn2+e.xs3;else if(4===d)e.t[e.p]=e.xs0+b+e.xs1+e.xn1+e.xs2+e.xn2+e.xs3+e.xn3+e.xs4;else if(5===d)e.t[e.p]=e.xs0+b+e.xs1+e.xn1+e.xs2+e.xn2+e.xs3+e.xn3+e.xs4+e.xn4+e.xs5;else{for(c=e.xs0+b+e.xs1,d=1;d<e.l;d++)c+=e["xn"+d]+e["xs"+(d+1)];e.t[e.p]=c}else-1===e.type?e.t[e.p]=e.xs0:e.setRatio&&e.setRatio(a);else e.t[e.p]=b+e.xs0;e=e._next}else for(;e;)2!==e.type?e.t[e.p]=e.b:e.setRatio(a),e=e._next;else for(;e;){if(2!==e.type)if(e.r&&-1!==e.type)if(b=Math.round(e.s+e.c),e.type){if(1===e.type){for(d=e.l,c=e.xs0+b+e.xs1,d=1;d<e.l;d++)c+=e["xn"+d]+e["xs"+(d+1)];e.t[e.p]=c}}else e.t[e.p]=b+e.xs0;else e.t[e.p]=e.e;else e.setRatio(a);e=e._next}},j._enableTransforms=function(a){this._transform=this._transform||Pa(this._target,e,!0),this._transformType=this._transform.svg&&za||!a&&3!==this._transformType?2:3};var Wa=function(a){this.t[this.p]=this.e,this.data._linkCSSP(this,this._next,null,!0)};j._addLazySet=function(a,b,c){var d=this._firstPT=new sa(a,b,0,0,this._firstPT,2);d.e=c,d.setRatio=Wa,d.data=this},j._linkCSSP=function(a,b,c,d){return a&&(b&&(b._prev=a),a._next&&(a._next._prev=a._prev),a._prev?a._prev._next=a._next:this._firstPT===a&&(this._firstPT=a._next,d=!0),c?c._next=a:d||null!==this._firstPT||(this._firstPT=a),a._next=b,a._prev=c),a},j._mod=function(a){for(var b=this._firstPT;b;)"function"==typeof a[b.p]&&a[b.p]===Math.round&&(b.r=1),b=b._next},j._kill=function(b){var c,d,e,f=b;if(b.autoAlpha||b.alpha){f={};for(d in b)f[d]=b[d];f.opacity=1,f.autoAlpha&&(f.visibility=1)}for(b.className&&(c=this._classNamePT)&&(e=c.xfirst,e&&e._prev?this._linkCSSP(e._prev,c._next,e._prev._prev):e===this._firstPT&&(this._firstPT=c._next),c._next&&this._linkCSSP(c._next,c._next._next,e._prev),this._classNamePT=null),c=this._firstPT;c;)c.plugin&&c.plugin!==d&&c.plugin._kill&&(c.plugin._kill(b),d=c.plugin),c=c._next;return a.prototype._kill.call(this,f)};var Xa=function(a,b,c){var d,e,f,g;if(a.slice)for(e=a.length;--e>-1;)Xa(a[e],b,c);else for(d=a.childNodes,e=d.length;--e>-1;)f=d[e],g=f.type,f.style&&(b.push(ba(f)),c&&c.push(f)),1!==g&&9!==g&&11!==g||!f.childNodes.length||Xa(f,b,c)};return g.cascadeTo=function(a,c,d){var e,f,g,h,i=b.to(a,c,d),j=[i],k=[],l=[],m=[],n=b._internals.reservedProps;for(a=i._targets||i.target,Xa(a,k,m),i.render(c,!0,!0),Xa(a,l),i.render(0,!0,!0),i._enabled(!0),e=m.length;--e>-1;)if(f=ca(m[e],k[e],l[e]),f.firstMPT){f=f.difs;for(g in d)n[g]&&(f[g]=d[g]);h={};for(g in f)h[g]=k[e][g];j.push(b.fromTo(m[e],c,h,f))}return j},a.activate([g]),g},!0),function(){var a=_gsScope._gsDefine.plugin({propName:"roundProps",version:"1.6.0",priority:-1,API:2,init:function(a,b,c){return this._tween=c,!0}}),b=function(a){for(;a;)a.f||a.blob||(a.m=Math.round),a=a._next},c=a.prototype;c._onInitAllProps=function(){for(var a,c,d,e=this._tween,f=e.vars.roundProps.join?e.vars.roundProps:e.vars.roundProps.split(","),g=f.length,h={},i=e._propLookup.roundProps;--g>-1;)h[f[g]]=Math.round;for(g=f.length;--g>-1;)for(a=f[g],c=e._firstPT;c;)d=c._next,c.pg?c.t._mod(h):c.n===a&&(2===c.f&&c.t?b(c.t._firstPT):(this._add(c.t,a,c.s,c.c),d&&(d._prev=c._prev),c._prev?c._prev._next=d:e._firstPT===c&&(e._firstPT=d),c._next=c._prev=null,e._propLookup[a]=i)),c=d;return!1},c._add=function(a,b,c,d){this._addTween(a,b,c,c+d,b,Math.round),this._overwriteProps.push(b)}}(),function(){_gsScope._gsDefine.plugin({propName:"attr",API:2,version:"0.6.0",init:function(a,b,c,d){var e,f;if("function"!=typeof a.setAttribute)return!1;for(e in b)f=b[e],"function"==typeof f&&(f=f(d,a)),this._addTween(a,"setAttribute",a.getAttribute(e)+"",f+"",e,!1,e),this._overwriteProps.push(e);return!0}})}(),_gsScope._gsDefine.plugin({propName:"directionalRotation",version:"0.3.0",API:2,init:function(a,b,c,d){"object"!=typeof b&&(b={rotation:b}),this.finals={};var e,f,g,h,i,j,k=b.useRadians===!0?2*Math.PI:360,l=1e-6;for(e in b)"useRadians"!==e&&(h=b[e],"function"==typeof h&&(h=h(d,a)),j=(h+"").split("_"),f=j[0],g=parseFloat("function"!=typeof a[e]?a[e]:a[e.indexOf("set")||"function"!=typeof a["get"+e.substr(3)]?e:"get"+e.substr(3)]()),h=this.finals[e]="string"==typeof f&&"="===f.charAt(1)?g+parseInt(f.charAt(0)+"1",10)*Number(f.substr(2)):Number(f)||0,i=h-g,j.length&&(f=j.join("_"),-1!==f.indexOf("short")&&(i%=k,i!==i%(k/2)&&(i=0>i?i+k:i-k)),-1!==f.indexOf("_cw")&&0>i?i=(i+9999999999*k)%k-(i/k|0)*k:-1!==f.indexOf("ccw")&&i>0&&(i=(i-9999999999*k)%k-(i/k|0)*k)),(i>l||-l>i)&&(this._addTween(a,e,g,g+i,e),this._overwriteProps.push(e)));return!0},set:function(a){var b;if(1!==a)this._super.setRatio.call(this,a);else for(b=this._firstPT;b;)b.f?b.t[b.p](this.finals[b.p]):b.t[b.p]=this.finals[b.p],b=b._next}})._autoCSS=!0,_gsScope._gsDefine("easing.Back",["easing.Ease"],function(a){var b,c,d,e=_gsScope.GreenSockGlobals||_gsScope,f=e.com.greensock,g=2*Math.PI,h=Math.PI/2,i=f._class,j=function(b,c){var d=i("easing."+b,function(){},!0),e=d.prototype=new a;return e.constructor=d,e.getRatio=c,d},k=a.register||function(){},l=function(a,b,c,d,e){var f=i("easing."+a,{easeOut:new b,easeIn:new c,easeInOut:new d},!0);return k(f,a),f},m=function(a,b,c){this.t=a,this.v=b,c&&(this.next=c,c.prev=this,this.c=c.v-b,this.gap=c.t-a)},n=function(b,c){var d=i("easing."+b,function(a){this._p1=a||0===a?a:1.70158,this._p2=1.525*this._p1},!0),e=d.prototype=new a;return e.constructor=d,e.getRatio=c,e.config=function(a){return new d(a)},d},o=l("Back",n("BackOut",function(a){return(a-=1)*a*((this._p1+1)*a+this._p1)+1}),n("BackIn",function(a){return a*a*((this._p1+1)*a-this._p1)}),n("BackInOut",function(a){return(a*=2)<1?.5*a*a*((this._p2+1)*a-this._p2):.5*((a-=2)*a*((this._p2+1)*a+this._p2)+2)})),p=i("easing.SlowMo",function(a,b,c){b=b||0===b?b:.7,null==a?a=.7:a>1&&(a=1),this._p=1!==a?b:0,this._p1=(1-a)/2,this._p2=a,this._p3=this._p1+this._p2,this._calcEnd=c===!0},!0),q=p.prototype=new a;return q.constructor=p,q.getRatio=function(a){var b=a+(.5-a)*this._p;return a<this._p1?this._calcEnd?1-(a=1-a/this._p1)*a:b-(a=1-a/this._p1)*a*a*a*b:a>this._p3?this._calcEnd?1-(a=(a-this._p3)/this._p1)*a:b+(a-b)*(a=(a-this._p3)/this._p1)*a*a*a:this._calcEnd?1:b},p.ease=new p(.7,.7),q.config=p.config=function(a,b,c){return new p(a,b,c)},b=i("easing.SteppedEase",function(a){a=a||1,this._p1=1/a,this._p2=a+1},!0),q=b.prototype=new a,q.constructor=b,q.getRatio=function(a){return 0>a?a=0:a>=1&&(a=.999999999),(this._p2*a>>0)*this._p1},q.config=b.config=function(a){return new b(a)},c=i("easing.RoughEase",function(b){b=b||{};for(var c,d,e,f,g,h,i=b.taper||"none",j=[],k=0,l=0|(b.points||20),n=l,o=b.randomize!==!1,p=b.clamp===!0,q=b.template instanceof a?b.template:null,r="number"==typeof b.strength?.4*b.strength:.4;--n>-1;)c=o?Math.random():1/l*n,d=q?q.getRatio(c):c,"none"===i?e=r:"out"===i?(f=1-c,e=f*f*r):"in"===i?e=c*c*r:.5>c?(f=2*c,e=f*f*.5*r):(f=2*(1-c),e=f*f*.5*r),o?d+=Math.random()*e-.5*e:n%2?d+=.5*e:d-=.5*e,p&&(d>1?d=1:0>d&&(d=0)),j[k++]={x:c,y:d};for(j.sort(function(a,b){return a.x-b.x}),h=new m(1,1,null),n=l;--n>-1;)g=j[n],h=new m(g.x,g.y,h);this._prev=new m(0,0,0!==h.t?h:h.next)},!0),q=c.prototype=new a,q.constructor=c,q.getRatio=function(a){var b=this._prev;if(a>b.t){for(;b.next&&a>=b.t;)b=b.next;b=b.prev}else for(;b.prev&&a<=b.t;)b=b.prev;return this._prev=b,b.v+(a-b.t)/b.gap*b.c},q.config=function(a){return new c(a)},c.ease=new c,l("Bounce",j("BounceOut",function(a){return 1/2.75>a?7.5625*a*a:2/2.75>a?7.5625*(a-=1.5/2.75)*a+.75:2.5/2.75>a?7.5625*(a-=2.25/2.75)*a+.9375:7.5625*(a-=2.625/2.75)*a+.984375}),j("BounceIn",function(a){return(a=1-a)<1/2.75?1-7.5625*a*a:2/2.75>a?1-(7.5625*(a-=1.5/2.75)*a+.75):2.5/2.75>a?1-(7.5625*(a-=2.25/2.75)*a+.9375):1-(7.5625*(a-=2.625/2.75)*a+.984375)}),j("BounceInOut",function(a){var b=.5>a;return a=b?1-2*a:2*a-1,a=1/2.75>a?7.5625*a*a:2/2.75>a?7.5625*(a-=1.5/2.75)*a+.75:2.5/2.75>a?7.5625*(a-=2.25/2.75)*a+.9375:7.5625*(a-=2.625/2.75)*a+.984375,b?.5*(1-a):.5*a+.5})),l("Circ",j("CircOut",function(a){return Math.sqrt(1-(a-=1)*a)}),j("CircIn",function(a){return-(Math.sqrt(1-a*a)-1)}),j("CircInOut",function(a){return(a*=2)<1?-.5*(Math.sqrt(1-a*a)-1):.5*(Math.sqrt(1-(a-=2)*a)+1)})),d=function(b,c,d){var e=i("easing."+b,function(a,b){this._p1=a>=1?a:1,this._p2=(b||d)/(1>a?a:1),this._p3=this._p2/g*(Math.asin(1/this._p1)||0),this._p2=g/this._p2},!0),f=e.prototype=new a;return f.constructor=e,f.getRatio=c,f.config=function(a,b){return new e(a,b)},e},l("Elastic",d("ElasticOut",function(a){return this._p1*Math.pow(2,-10*a)*Math.sin((a-this._p3)*this._p2)+1},.3),d("ElasticIn",function(a){return-(this._p1*Math.pow(2,10*(a-=1))*Math.sin((a-this._p3)*this._p2))},.3),d("ElasticInOut",function(a){return(a*=2)<1?-.5*(this._p1*Math.pow(2,10*(a-=1))*Math.sin((a-this._p3)*this._p2)):this._p1*Math.pow(2,-10*(a-=1))*Math.sin((a-this._p3)*this._p2)*.5+1},.45)),l("Expo",j("ExpoOut",function(a){return 1-Math.pow(2,-10*a)}),j("ExpoIn",function(a){return Math.pow(2,10*(a-1))-.001}),j("ExpoInOut",function(a){return(a*=2)<1?.5*Math.pow(2,10*(a-1)):.5*(2-Math.pow(2,-10*(a-1)))})),l("Sine",j("SineOut",function(a){return Math.sin(a*h)}),j("SineIn",function(a){return-Math.cos(a*h)+1}),j("SineInOut",function(a){return-.5*(Math.cos(Math.PI*a)-1)})),i("easing.EaseLookup",{find:function(b){return a.map[b]}},!0),k(e.SlowMo,"SlowMo","ease,"),k(c,"RoughEase","ease,"),k(b,"SteppedEase","ease,"),o},!0)}),_gsScope._gsDefine&&_gsScope._gsQueue.pop()(),function(a,b){"use strict";var c={},d=a.GreenSockGlobals=a.GreenSockGlobals||a;if(!d.TweenLite){var e,f,g,h,i,j=function(a){var b,c=a.split("."),e=d;for(b=0;b<c.length;b++)e[c[b]]=e=e[c[b]]||{};return e},k=j("com.greensock"),l=1e-10,m=function(a){var b,c=[],d=a.length;for(b=0;b!==d;c.push(a[b++]));return c},n=function(){},o=function(){var a=Object.prototype.toString,b=a.call([]);return function(c){return null!=c&&(c instanceof Array||"object"==typeof c&&!!c.push&&a.call(c)===b)}}(),p={},q=function(e,f,g,h){this.sc=p[e]?p[e].sc:[],p[e]=this,this.gsClass=null,this.func=g;var i=[];this.check=function(k){for(var l,m,n,o,r,s=f.length,t=s;--s>-1;)(l=p[f[s]]||new q(f[s],[])).gsClass?(i[s]=l.gsClass,t--):k&&l.sc.push(this);if(0===t&&g){if(m=("com.greensock."+e).split("."),n=m.pop(),o=j(m.join("."))[n]=this.gsClass=g.apply(g,i),h)if(d[n]=c[n]=o,r="undefined"!=typeof module&&module.exports,!r&&"function"==typeof define&&define.amd)define((a.GreenSockAMDPath?a.GreenSockAMDPath+"/":"")+e.split(".").pop(),[],function(){return o});else if(r)if(e===b){module.exports=c[b]=o;for(s in c)o[s]=c[s]}else c[b]&&(c[b][n]=o);for(s=0;s<this.sc.length;s++)this.sc[s].check()}},this.check(!0)},r=a._gsDefine=function(a,b,c,d){return new q(a,b,c,d)},s=k._class=function(a,b,c){return b=b||function(){},r(a,[],function(){return b},c),b};r.globals=d;var t=[0,0,1,1],u=s("easing.Ease",function(a,b,c,d){this._func=a,this._type=c||0,this._power=d||0,this._params=b?t.concat(b):t},!0),v=u.map={},w=u.register=function(a,b,c,d){for(var e,f,g,h,i=b.split(","),j=i.length,l=(c||"easeIn,easeOut,easeInOut").split(",");--j>-1;)for(f=i[j],e=d?s("easing."+f,null,!0):k.easing[f]||{},g=l.length;--g>-1;)h=l[g],v[f+"."+h]=v[h+f]=e[h]=a.getRatio?a:a[h]||new a};for(g=u.prototype,g._calcEnd=!1,g.getRatio=function(a){if(this._func)return this._params[0]=a,this._func.apply(null,this._params);var b=this._type,c=this._power,d=1===b?1-a:2===b?a:.5>a?2*a:2*(1-a);return 1===c?d*=d:2===c?d*=d*d:3===c?d*=d*d*d:4===c&&(d*=d*d*d*d),1===b?1-d:2===b?d:.5>a?d/2:1-d/2},e=["Linear","Quad","Cubic","Quart","Quint,Strong"],f=e.length;--f>-1;)g=e[f]+",Power"+f,w(new u(null,null,1,f),g,"easeOut",!0),w(new u(null,null,2,f),g,"easeIn"+(0===f?",easeNone":"")),w(new u(null,null,3,f),g,"easeInOut");v.linear=k.easing.Linear.easeIn,v.swing=k.easing.Quad.easeInOut;var x=s("events.EventDispatcher",function(a){this._listeners={},this._eventTarget=a||this});g=x.prototype,g.addEventListener=function(a,b,c,d,e){e=e||0;var f,g,j=this._listeners[a],k=0;for(this!==h||i||h.wake(),null==j&&(this._listeners[a]=j=[]),g=j.length;--g>-1;)f=j[g],f.c===b&&f.s===c?j.splice(g,1):0===k&&f.pr<e&&(k=g+1);j.splice(k,0,{c:b,s:c,up:d,pr:e})},g.removeEventListener=function(a,b){var c,d=this._listeners[a];if(d)for(c=d.length;--c>-1;)if(d[c].c===b)return void d.splice(c,1)},g.dispatchEvent=function(a){var b,c,d,e=this._listeners[a];if(e)for(b=e.length,b>1&&(e=e.slice(0)),c=this._eventTarget;--b>-1;)d=e[b],d&&(d.up?d.c.call(d.s||c,{type:a,target:c}):d.c.call(d.s||c))};var y=a.requestAnimationFrame,z=a.cancelAnimationFrame,A=Date.now||function(){return(new Date).getTime()},B=A();for(e=["ms","moz","webkit","o"],f=e.length;--f>-1&&!y;)y=a[e[f]+"RequestAnimationFrame"],z=a[e[f]+"CancelAnimationFrame"]||a[e[f]+"CancelRequestAnimationFrame"];s("Ticker",function(a,b){var c,d,e,f,g,j=this,k=A(),m=b!==!1&&y?"auto":!1,o=500,p=33,q="tick",r=function(a){var b,h,i=A()-B;i>o&&(k+=i-p),B+=i,j.time=(B-k)/1e3,b=j.time-g,(!c||b>0||a===!0)&&(j.frame++,g+=b+(b>=f?.004:f-b),h=!0),a!==!0&&(e=d(r)),h&&j.dispatchEvent(q)};x.call(j),j.time=j.frame=0,j.tick=function(){r(!0)},j.lagSmoothing=function(a,b){o=a||1/l,p=Math.min(b,o,0)},j.sleep=function(){null!=e&&(m&&z?z(e):clearTimeout(e),d=n,e=null,j===h&&(i=!1))},j.wake=function(a){null!==e?j.sleep():a?k+=-B+(B=A()):j.frame>10&&(B=A()-o+5),d=0===c?n:m&&y?y:function(a){return setTimeout(a,1e3*(g-j.time)+1|0)},j===h&&(i=!0),r(2)},j.fps=function(a){return arguments.length?(c=a,f=1/(c||60),g=this.time+f,void j.wake()):c},j.useRAF=function(a){return arguments.length?(j.sleep(),m=a,void j.fps(c)):m},j.fps(a),setTimeout(function(){"auto"===m&&j.frame<5&&"hidden"!==document.visibilityState&&j.useRAF(!1)},1500)}),g=k.Ticker.prototype=new k.events.EventDispatcher,g.constructor=k.Ticker;var C=s("core.Animation",function(a,b){if(this.vars=b=b||{},this._duration=this._totalDuration=a||0,this._delay=Number(b.delay)||0,this._timeScale=1,this._active=b.immediateRender===!0,this.data=b.data,this._reversed=b.reversed===!0,V){i||h.wake();var c=this.vars.useFrames?U:V;c.add(this,c._time),this.vars.paused&&this.paused(!0)}});h=C.ticker=new k.Ticker,g=C.prototype,g._dirty=g._gc=g._initted=g._paused=!1,g._totalTime=g._time=0,g._rawPrevTime=-1,g._next=g._last=g._onUpdate=g._timeline=g.timeline=null,g._paused=!1;var D=function(){i&&A()-B>2e3&&h.wake(),setTimeout(D,2e3)};D(),g.play=function(a,b){return null!=a&&this.seek(a,b),this.reversed(!1).paused(!1)},g.pause=function(a,b){return null!=a&&this.seek(a,b),this.paused(!0)},g.resume=function(a,b){return null!=a&&this.seek(a,b),this.paused(!1)},g.seek=function(a,b){return this.totalTime(Number(a),b!==!1)},g.restart=function(a,b){return this.reversed(!1).paused(!1).totalTime(a?-this._delay:0,b!==!1,!0)},g.reverse=function(a,b){return null!=a&&this.seek(a||this.totalDuration(),b),this.reversed(!0).paused(!1)},g.render=function(a,b,c){},g.invalidate=function(){return this._time=this._totalTime=0,this._initted=this._gc=!1,this._rawPrevTime=-1,(this._gc||!this.timeline)&&this._enabled(!0),this},g.isActive=function(){var a,b=this._timeline,c=this._startTime;return!b||!this._gc&&!this._paused&&b.isActive()&&(a=b.rawTime())>=c&&a<c+this.totalDuration()/this._timeScale},g._enabled=function(a,b){return i||h.wake(),this._gc=!a,this._active=this.isActive(),b!==!0&&(a&&!this.timeline?this._timeline.add(this,this._startTime-this._delay):!a&&this.timeline&&this._timeline._remove(this,!0)),!1},g._kill=function(a,b){return this._enabled(!1,!1)},g.kill=function(a,b){return this._kill(a,b),this},g._uncache=function(a){for(var b=a?this:this.timeline;b;)b._dirty=!0,b=b.timeline;return this},g._swapSelfInParams=function(a){for(var b=a.length,c=a.concat();--b>-1;)"{self}"===a[b]&&(c[b]=this);return c},g._callback=function(a){var b=this.vars,c=b[a],d=b[a+"Params"],e=b[a+"Scope"]||b.callbackScope||this,f=d?d.length:0;switch(f){case 0:c.call(e);break;case 1:c.call(e,d[0]);break;case 2:c.call(e,d[0],d[1]);break;default:c.apply(e,d)}},g.eventCallback=function(a,b,c,d){if("on"===(a||"").substr(0,2)){var e=this.vars;if(1===arguments.length)return e[a];null==b?delete e[a]:(e[a]=b,e[a+"Params"]=o(c)&&-1!==c.join("").indexOf("{self}")?this._swapSelfInParams(c):c,e[a+"Scope"]=d),"onUpdate"===a&&(this._onUpdate=b)}return this},g.delay=function(a){return arguments.length?(this._timeline.smoothChildTiming&&this.startTime(this._startTime+a-this._delay),this._delay=a,this):this._delay},g.duration=function(a){return arguments.length?(this._duration=this._totalDuration=a,this._uncache(!0),this._timeline.smoothChildTiming&&this._time>0&&this._time<this._duration&&0!==a&&this.totalTime(this._totalTime*(a/this._duration),!0),this):(this._dirty=!1,this._duration)},g.totalDuration=function(a){return this._dirty=!1,arguments.length?this.duration(a):this._totalDuration},g.time=function(a,b){return arguments.length?(this._dirty&&this.totalDuration(),this.totalTime(a>this._duration?this._duration:a,b)):this._time},g.totalTime=function(a,b,c){if(i||h.wake(),!arguments.length)return this._totalTime;if(this._timeline){if(0>a&&!c&&(a+=this.totalDuration()),this._timeline.smoothChildTiming){this._dirty&&this.totalDuration();var d=this._totalDuration,e=this._timeline;if(a>d&&!c&&(a=d),this._startTime=(this._paused?this._pauseTime:e._time)-(this._reversed?d-a:a)/this._timeScale,e._dirty||this._uncache(!1),e._timeline)for(;e._timeline;)e._timeline._time!==(e._startTime+e._totalTime)/e._timeScale&&e.totalTime(e._totalTime,!0),e=e._timeline}this._gc&&this._enabled(!0,!1),(this._totalTime!==a||0===this._duration)&&(I.length&&X(),this.render(a,b,!1),I.length&&X())}return this},g.progress=g.totalProgress=function(a,b){var c=this.duration();return arguments.length?this.totalTime(c*a,b):c?this._time/c:this.ratio},g.startTime=function(a){return arguments.length?(a!==this._startTime&&(this._startTime=a,this.timeline&&this.timeline._sortChildren&&this.timeline.add(this,a-this._delay)),this):this._startTime},g.endTime=function(a){return this._startTime+(0!=a?this.totalDuration():this.duration())/this._timeScale},g.timeScale=function(a){if(!arguments.length)return this._timeScale;if(a=a||l,this._timeline&&this._timeline.smoothChildTiming){var b=this._pauseTime,c=b||0===b?b:this._timeline.totalTime();this._startTime=c-(c-this._startTime)*this._timeScale/a}return this._timeScale=a,this._uncache(!1)},g.reversed=function(a){return arguments.length?(a!=this._reversed&&(this._reversed=a,this.totalTime(this._timeline&&!this._timeline.smoothChildTiming?this.totalDuration()-this._totalTime:this._totalTime,!0)),this):this._reversed},g.paused=function(a){if(!arguments.length)return this._paused;var b,c,d=this._timeline;return a!=this._paused&&d&&(i||a||h.wake(),b=d.rawTime(),c=b-this._pauseTime,!a&&d.smoothChildTiming&&(this._startTime+=c,this._uncache(!1)),this._pauseTime=a?b:null,this._paused=a,this._active=this.isActive(),!a&&0!==c&&this._initted&&this.duration()&&(b=d.smoothChildTiming?this._totalTime:(b-this._startTime)/this._timeScale,this.render(b,b===this._totalTime,!0))),this._gc&&!a&&this._enabled(!0,!1),this};var E=s("core.SimpleTimeline",function(a){C.call(this,0,a),this.autoRemoveChildren=this.smoothChildTiming=!0});g=E.prototype=new C,g.constructor=E,g.kill()._gc=!1,g._first=g._last=g._recent=null,g._sortChildren=!1,g.add=g.insert=function(a,b,c,d){var e,f;if(a._startTime=Number(b||0)+a._delay,a._paused&&this!==a._timeline&&(a._pauseTime=a._startTime+(this.rawTime()-a._startTime)/a._timeScale),a.timeline&&a.timeline._remove(a,!0),a.timeline=a._timeline=this,a._gc&&a._enabled(!0,!0),e=this._last,this._sortChildren)for(f=a._startTime;e&&e._startTime>f;)e=e._prev;return e?(a._next=e._next,e._next=a):(a._next=this._first,this._first=a),a._next?a._next._prev=a:this._last=a,a._prev=e,this._recent=a,this._timeline&&this._uncache(!0),this},g._remove=function(a,b){return a.timeline===this&&(b||a._enabled(!1,!0),a._prev?a._prev._next=a._next:this._first===a&&(this._first=a._next),a._next?a._next._prev=a._prev:this._last===a&&(this._last=a._prev),a._next=a._prev=a.timeline=null,a===this._recent&&(this._recent=this._last),this._timeline&&this._uncache(!0)),this},g.render=function(a,b,c){var d,e=this._first;for(this._totalTime=this._time=this._rawPrevTime=a;e;)d=e._next,(e._active||a>=e._startTime&&!e._paused)&&(e._reversed?e.render((e._dirty?e.totalDuration():e._totalDuration)-(a-e._startTime)*e._timeScale,b,c):e.render((a-e._startTime)*e._timeScale,b,c)),e=d},g.rawTime=function(){return i||h.wake(),this._totalTime};var F=s("TweenLite",function(b,c,d){if(C.call(this,c,d),this.render=F.prototype.render,null==b)throw"Cannot tween a null target.";this.target=b="string"!=typeof b?b:F.selector(b)||b;var e,f,g,h=b.jquery||b.length&&b!==a&&b[0]&&(b[0]===a||b[0].nodeType&&b[0].style&&!b.nodeType),i=this.vars.overwrite;if(this._overwrite=i=null==i?T[F.defaultOverwrite]:"number"==typeof i?i>>0:T[i],(h||b instanceof Array||b.push&&o(b))&&"number"!=typeof b[0])for(this._targets=g=m(b),this._propLookup=[],this._siblings=[],e=0;e<g.length;e++)f=g[e],f?"string"!=typeof f?f.length&&f!==a&&f[0]&&(f[0]===a||f[0].nodeType&&f[0].style&&!f.nodeType)?(g.splice(e--,1),this._targets=g=g.concat(m(f))):(this._siblings[e]=Y(f,this,!1),1===i&&this._siblings[e].length>1&&$(f,this,null,1,this._siblings[e])):(f=g[e--]=F.selector(f),"string"==typeof f&&g.splice(e+1,1)):g.splice(e--,1);else this._propLookup={},this._siblings=Y(b,this,!1),1===i&&this._siblings.length>1&&$(b,this,null,1,this._siblings);(this.vars.immediateRender||0===c&&0===this._delay&&this.vars.immediateRender!==!1)&&(this._time=-l,this.render(Math.min(0,-this._delay)))},!0),G=function(b){return b&&b.length&&b!==a&&b[0]&&(b[0]===a||b[0].nodeType&&b[0].style&&!b.nodeType);
},H=function(a,b){var c,d={};for(c in a)S[c]||c in b&&"transform"!==c&&"x"!==c&&"y"!==c&&"width"!==c&&"height"!==c&&"className"!==c&&"border"!==c||!(!P[c]||P[c]&&P[c]._autoCSS)||(d[c]=a[c],delete a[c]);a.css=d};g=F.prototype=new C,g.constructor=F,g.kill()._gc=!1,g.ratio=0,g._firstPT=g._targets=g._overwrittenProps=g._startAt=null,g._notifyPluginsOfEnabled=g._lazy=!1,F.version="1.19.0",F.defaultEase=g._ease=new u(null,null,1,1),F.defaultOverwrite="auto",F.ticker=h,F.autoSleep=120,F.lagSmoothing=function(a,b){h.lagSmoothing(a,b)},F.selector=a.$||a.jQuery||function(b){var c=a.$||a.jQuery;return c?(F.selector=c,c(b)):"undefined"==typeof document?b:document.querySelectorAll?document.querySelectorAll(b):document.getElementById("#"===b.charAt(0)?b.substr(1):b)};var I=[],J={},K=/(?:(-|-=|\+=)?\d*\.?\d*(?:e[\-+]?\d+)?)[0-9]/gi,L=function(a){for(var b,c=this._firstPT,d=1e-6;c;)b=c.blob?a?this.join(""):this.start:c.c*a+c.s,c.m?b=c.m(b,this._target||c.t):d>b&&b>-d&&(b=0),c.f?c.fp?c.t[c.p](c.fp,b):c.t[c.p](b):c.t[c.p]=b,c=c._next},M=function(a,b,c,d){var e,f,g,h,i,j,k,l=[a,b],m=0,n="",o=0;for(l.start=a,c&&(c(l),a=l[0],b=l[1]),l.length=0,e=a.match(K)||[],f=b.match(K)||[],d&&(d._next=null,d.blob=1,l._firstPT=l._applyPT=d),i=f.length,h=0;i>h;h++)k=f[h],j=b.substr(m,b.indexOf(k,m)-m),n+=j||!h?j:",",m+=j.length,o?o=(o+1)%5:"rgba("===j.substr(-5)&&(o=1),k===e[h]||e.length<=h?n+=k:(n&&(l.push(n),n=""),g=parseFloat(e[h]),l.push(g),l._firstPT={_next:l._firstPT,t:l,p:l.length-1,s:g,c:("="===k.charAt(1)?parseInt(k.charAt(0)+"1",10)*parseFloat(k.substr(2)):parseFloat(k)-g)||0,f:0,m:o&&4>o?Math.round:0}),m+=k.length;return n+=b.substr(m),n&&l.push(n),l.setRatio=L,l},N=function(a,b,c,d,e,f,g,h,i){"function"==typeof d&&(d=d(i||0,a));var j,k,l="get"===c?a[b]:c,m=typeof a[b],n="string"==typeof d&&"="===d.charAt(1),o={t:a,p:b,s:l,f:"function"===m,pg:0,n:e||b,m:f?"function"==typeof f?f:Math.round:0,pr:0,c:n?parseInt(d.charAt(0)+"1",10)*parseFloat(d.substr(2)):parseFloat(d)-l||0};return"number"!==m&&("function"===m&&"get"===c&&(k=b.indexOf("set")||"function"!=typeof a["get"+b.substr(3)]?b:"get"+b.substr(3),o.s=l=g?a[k](g):a[k]()),"string"==typeof l&&(g||isNaN(l))?(o.fp=g,j=M(l,d,h||F.defaultStringFilter,o),o={t:j,p:"setRatio",s:0,c:1,f:2,pg:0,n:e||b,pr:0,m:0}):n||(o.s=parseFloat(l),o.c=parseFloat(d)-o.s||0)),o.c?((o._next=this._firstPT)&&(o._next._prev=o),this._firstPT=o,o):void 0},O=F._internals={isArray:o,isSelector:G,lazyTweens:I,blobDif:M},P=F._plugins={},Q=O.tweenLookup={},R=0,S=O.reservedProps={ease:1,delay:1,overwrite:1,onComplete:1,onCompleteParams:1,onCompleteScope:1,useFrames:1,runBackwards:1,startAt:1,onUpdate:1,onUpdateParams:1,onUpdateScope:1,onStart:1,onStartParams:1,onStartScope:1,onReverseComplete:1,onReverseCompleteParams:1,onReverseCompleteScope:1,onRepeat:1,onRepeatParams:1,onRepeatScope:1,easeParams:1,yoyo:1,immediateRender:1,repeat:1,repeatDelay:1,data:1,paused:1,reversed:1,autoCSS:1,lazy:1,onOverwrite:1,callbackScope:1,stringFilter:1,id:1},T={none:0,all:1,auto:2,concurrent:3,allOnStart:4,preexisting:5,"true":1,"false":0},U=C._rootFramesTimeline=new E,V=C._rootTimeline=new E,W=30,X=O.lazyRender=function(){var a,b=I.length;for(J={};--b>-1;)a=I[b],a&&a._lazy!==!1&&(a.render(a._lazy[0],a._lazy[1],!0),a._lazy=!1);I.length=0};V._startTime=h.time,U._startTime=h.frame,V._active=U._active=!0,setTimeout(X,1),C._updateRoot=F.render=function(){var a,b,c;if(I.length&&X(),V.render((h.time-V._startTime)*V._timeScale,!1,!1),U.render((h.frame-U._startTime)*U._timeScale,!1,!1),I.length&&X(),h.frame>=W){W=h.frame+(parseInt(F.autoSleep,10)||120);for(c in Q){for(b=Q[c].tweens,a=b.length;--a>-1;)b[a]._gc&&b.splice(a,1);0===b.length&&delete Q[c]}if(c=V._first,(!c||c._paused)&&F.autoSleep&&!U._first&&1===h._listeners.tick.length){for(;c&&c._paused;)c=c._next;c||h.sleep()}}},h.addEventListener("tick",C._updateRoot);var Y=function(a,b,c){var d,e,f=a._gsTweenID;if(Q[f||(a._gsTweenID=f="t"+R++)]||(Q[f]={target:a,tweens:[]}),b&&(d=Q[f].tweens,d[e=d.length]=b,c))for(;--e>-1;)d[e]===b&&d.splice(e,1);return Q[f].tweens},Z=function(a,b,c,d){var e,f,g=a.vars.onOverwrite;return g&&(e=g(a,b,c,d)),g=F.onOverwrite,g&&(f=g(a,b,c,d)),e!==!1&&f!==!1},$=function(a,b,c,d,e){var f,g,h,i;if(1===d||d>=4){for(i=e.length,f=0;i>f;f++)if((h=e[f])!==b)h._gc||h._kill(null,a,b)&&(g=!0);else if(5===d)break;return g}var j,k=b._startTime+l,m=[],n=0,o=0===b._duration;for(f=e.length;--f>-1;)(h=e[f])===b||h._gc||h._paused||(h._timeline!==b._timeline?(j=j||_(b,0,o),0===_(h,j,o)&&(m[n++]=h)):h._startTime<=k&&h._startTime+h.totalDuration()/h._timeScale>k&&((o||!h._initted)&&k-h._startTime<=2e-10||(m[n++]=h)));for(f=n;--f>-1;)if(h=m[f],2===d&&h._kill(c,a,b)&&(g=!0),2!==d||!h._firstPT&&h._initted){if(2!==d&&!Z(h,b))continue;h._enabled(!1,!1)&&(g=!0)}return g},_=function(a,b,c){for(var d=a._timeline,e=d._timeScale,f=a._startTime;d._timeline;){if(f+=d._startTime,e*=d._timeScale,d._paused)return-100;d=d._timeline}return f/=e,f>b?f-b:c&&f===b||!a._initted&&2*l>f-b?l:(f+=a.totalDuration()/a._timeScale/e)>b+l?0:f-b-l};g._init=function(){var a,b,c,d,e,f,g=this.vars,h=this._overwrittenProps,i=this._duration,j=!!g.immediateRender,k=g.ease;if(g.startAt){this._startAt&&(this._startAt.render(-1,!0),this._startAt.kill()),e={};for(d in g.startAt)e[d]=g.startAt[d];if(e.overwrite=!1,e.immediateRender=!0,e.lazy=j&&g.lazy!==!1,e.startAt=e.delay=null,this._startAt=F.to(this.target,0,e),j)if(this._time>0)this._startAt=null;else if(0!==i)return}else if(g.runBackwards&&0!==i)if(this._startAt)this._startAt.render(-1,!0),this._startAt.kill(),this._startAt=null;else{0!==this._time&&(j=!1),c={};for(d in g)S[d]&&"autoCSS"!==d||(c[d]=g[d]);if(c.overwrite=0,c.data="isFromStart",c.lazy=j&&g.lazy!==!1,c.immediateRender=j,this._startAt=F.to(this.target,0,c),j){if(0===this._time)return}else this._startAt._init(),this._startAt._enabled(!1),this.vars.immediateRender&&(this._startAt=null)}if(this._ease=k=k?k instanceof u?k:"function"==typeof k?new u(k,g.easeParams):v[k]||F.defaultEase:F.defaultEase,g.easeParams instanceof Array&&k.config&&(this._ease=k.config.apply(k,g.easeParams)),this._easeType=this._ease._type,this._easePower=this._ease._power,this._firstPT=null,this._targets)for(f=this._targets.length,a=0;f>a;a++)this._initProps(this._targets[a],this._propLookup[a]={},this._siblings[a],h?h[a]:null,a)&&(b=!0);else b=this._initProps(this.target,this._propLookup,this._siblings,h,0);if(b&&F._onPluginEvent("_onInitAllProps",this),h&&(this._firstPT||"function"!=typeof this.target&&this._enabled(!1,!1)),g.runBackwards)for(c=this._firstPT;c;)c.s+=c.c,c.c=-c.c,c=c._next;this._onUpdate=g.onUpdate,this._initted=!0},g._initProps=function(b,c,d,e,f){var g,h,i,j,k,l;if(null==b)return!1;J[b._gsTweenID]&&X(),this.vars.css||b.style&&b!==a&&b.nodeType&&P.css&&this.vars.autoCSS!==!1&&H(this.vars,b);for(g in this.vars)if(l=this.vars[g],S[g])l&&(l instanceof Array||l.push&&o(l))&&-1!==l.join("").indexOf("{self}")&&(this.vars[g]=l=this._swapSelfInParams(l,this));else if(P[g]&&(j=new P[g])._onInitTween(b,this.vars[g],this,f)){for(this._firstPT=k={_next:this._firstPT,t:j,p:"setRatio",s:0,c:1,f:1,n:g,pg:1,pr:j._priority,m:0},h=j._overwriteProps.length;--h>-1;)c[j._overwriteProps[h]]=this._firstPT;(j._priority||j._onInitAllProps)&&(i=!0),(j._onDisable||j._onEnable)&&(this._notifyPluginsOfEnabled=!0),k._next&&(k._next._prev=k)}else c[g]=N.call(this,b,g,"get",l,g,0,null,this.vars.stringFilter,f);return e&&this._kill(e,b)?this._initProps(b,c,d,e,f):this._overwrite>1&&this._firstPT&&d.length>1&&$(b,this,c,this._overwrite,d)?(this._kill(c,b),this._initProps(b,c,d,e,f)):(this._firstPT&&(this.vars.lazy!==!1&&this._duration||this.vars.lazy&&!this._duration)&&(J[b._gsTweenID]=!0),i)},g.render=function(a,b,c){var d,e,f,g,h=this._time,i=this._duration,j=this._rawPrevTime;if(a>=i-1e-7)this._totalTime=this._time=i,this.ratio=this._ease._calcEnd?this._ease.getRatio(1):1,this._reversed||(d=!0,e="onComplete",c=c||this._timeline.autoRemoveChildren),0===i&&(this._initted||!this.vars.lazy||c)&&(this._startTime===this._timeline._duration&&(a=0),(0>j||0>=a&&a>=-1e-7||j===l&&"isPause"!==this.data)&&j!==a&&(c=!0,j>l&&(e="onReverseComplete")),this._rawPrevTime=g=!b||a||j===a?a:l);else if(1e-7>a)this._totalTime=this._time=0,this.ratio=this._ease._calcEnd?this._ease.getRatio(0):0,(0!==h||0===i&&j>0)&&(e="onReverseComplete",d=this._reversed),0>a&&(this._active=!1,0===i&&(this._initted||!this.vars.lazy||c)&&(j>=0&&(j!==l||"isPause"!==this.data)&&(c=!0),this._rawPrevTime=g=!b||a||j===a?a:l)),this._initted||(c=!0);else if(this._totalTime=this._time=a,this._easeType){var k=a/i,m=this._easeType,n=this._easePower;(1===m||3===m&&k>=.5)&&(k=1-k),3===m&&(k*=2),1===n?k*=k:2===n?k*=k*k:3===n?k*=k*k*k:4===n&&(k*=k*k*k*k),1===m?this.ratio=1-k:2===m?this.ratio=k:.5>a/i?this.ratio=k/2:this.ratio=1-k/2}else this.ratio=this._ease.getRatio(a/i);if(this._time!==h||c){if(!this._initted){if(this._init(),!this._initted||this._gc)return;if(!c&&this._firstPT&&(this.vars.lazy!==!1&&this._duration||this.vars.lazy&&!this._duration))return this._time=this._totalTime=h,this._rawPrevTime=j,I.push(this),void(this._lazy=[a,b]);this._time&&!d?this.ratio=this._ease.getRatio(this._time/i):d&&this._ease._calcEnd&&(this.ratio=this._ease.getRatio(0===this._time?0:1))}for(this._lazy!==!1&&(this._lazy=!1),this._active||!this._paused&&this._time!==h&&a>=0&&(this._active=!0),0===h&&(this._startAt&&(a>=0?this._startAt.render(a,b,c):e||(e="_dummyGS")),this.vars.onStart&&(0!==this._time||0===i)&&(b||this._callback("onStart"))),f=this._firstPT;f;)f.f?f.t[f.p](f.c*this.ratio+f.s):f.t[f.p]=f.c*this.ratio+f.s,f=f._next;this._onUpdate&&(0>a&&this._startAt&&a!==-1e-4&&this._startAt.render(a,b,c),b||(this._time!==h||d||c)&&this._callback("onUpdate")),e&&(!this._gc||c)&&(0>a&&this._startAt&&!this._onUpdate&&a!==-1e-4&&this._startAt.render(a,b,c),d&&(this._timeline.autoRemoveChildren&&this._enabled(!1,!1),this._active=!1),!b&&this.vars[e]&&this._callback(e),0===i&&this._rawPrevTime===l&&g!==l&&(this._rawPrevTime=0))}},g._kill=function(a,b,c){if("all"===a&&(a=null),null==a&&(null==b||b===this.target))return this._lazy=!1,this._enabled(!1,!1);b="string"!=typeof b?b||this._targets||this.target:F.selector(b)||b;var d,e,f,g,h,i,j,k,l,m=c&&this._time&&c._startTime===this._startTime&&this._timeline===c._timeline;if((o(b)||G(b))&&"number"!=typeof b[0])for(d=b.length;--d>-1;)this._kill(a,b[d],c)&&(i=!0);else{if(this._targets){for(d=this._targets.length;--d>-1;)if(b===this._targets[d]){h=this._propLookup[d]||{},this._overwrittenProps=this._overwrittenProps||[],e=this._overwrittenProps[d]=a?this._overwrittenProps[d]||{}:"all";break}}else{if(b!==this.target)return!1;h=this._propLookup,e=this._overwrittenProps=a?this._overwrittenProps||{}:"all"}if(h){if(j=a||h,k=a!==e&&"all"!==e&&a!==h&&("object"!=typeof a||!a._tempKill),c&&(F.onOverwrite||this.vars.onOverwrite)){for(f in j)h[f]&&(l||(l=[]),l.push(f));if((l||!a)&&!Z(this,c,b,l))return!1}for(f in j)(g=h[f])&&(m&&(g.f?g.t[g.p](g.s):g.t[g.p]=g.s,i=!0),g.pg&&g.t._kill(j)&&(i=!0),g.pg&&0!==g.t._overwriteProps.length||(g._prev?g._prev._next=g._next:g===this._firstPT&&(this._firstPT=g._next),g._next&&(g._next._prev=g._prev),g._next=g._prev=null),delete h[f]),k&&(e[f]=1);!this._firstPT&&this._initted&&this._enabled(!1,!1)}}return i},g.invalidate=function(){return this._notifyPluginsOfEnabled&&F._onPluginEvent("_onDisable",this),this._firstPT=this._overwrittenProps=this._startAt=this._onUpdate=null,this._notifyPluginsOfEnabled=this._active=this._lazy=!1,this._propLookup=this._targets?{}:[],C.prototype.invalidate.call(this),this.vars.immediateRender&&(this._time=-l,this.render(Math.min(0,-this._delay))),this},g._enabled=function(a,b){if(i||h.wake(),a&&this._gc){var c,d=this._targets;if(d)for(c=d.length;--c>-1;)this._siblings[c]=Y(d[c],this,!0);else this._siblings=Y(this.target,this,!0)}return C.prototype._enabled.call(this,a,b),this._notifyPluginsOfEnabled&&this._firstPT?F._onPluginEvent(a?"_onEnable":"_onDisable",this):!1},F.to=function(a,b,c){return new F(a,b,c)},F.from=function(a,b,c){return c.runBackwards=!0,c.immediateRender=0!=c.immediateRender,new F(a,b,c)},F.fromTo=function(a,b,c,d){return d.startAt=c,d.immediateRender=0!=d.immediateRender&&0!=c.immediateRender,new F(a,b,d)},F.delayedCall=function(a,b,c,d,e){return new F(b,0,{delay:a,onComplete:b,onCompleteParams:c,callbackScope:d,onReverseComplete:b,onReverseCompleteParams:c,immediateRender:!1,lazy:!1,useFrames:e,overwrite:0})},F.set=function(a,b){return new F(a,0,b)},F.getTweensOf=function(a,b){if(null==a)return[];a="string"!=typeof a?a:F.selector(a)||a;var c,d,e,f;if((o(a)||G(a))&&"number"!=typeof a[0]){for(c=a.length,d=[];--c>-1;)d=d.concat(F.getTweensOf(a[c],b));for(c=d.length;--c>-1;)for(f=d[c],e=c;--e>-1;)f===d[e]&&d.splice(c,1)}else for(d=Y(a).concat(),c=d.length;--c>-1;)(d[c]._gc||b&&!d[c].isActive())&&d.splice(c,1);return d},F.killTweensOf=F.killDelayedCallsTo=function(a,b,c){"object"==typeof b&&(c=b,b=!1);for(var d=F.getTweensOf(a,b),e=d.length;--e>-1;)d[e]._kill(c,a)};var aa=s("plugins.TweenPlugin",function(a,b){this._overwriteProps=(a||"").split(","),this._propName=this._overwriteProps[0],this._priority=b||0,this._super=aa.prototype},!0);if(g=aa.prototype,aa.version="1.19.0",aa.API=2,g._firstPT=null,g._addTween=N,g.setRatio=L,g._kill=function(a){var b,c=this._overwriteProps,d=this._firstPT;if(null!=a[this._propName])this._overwriteProps=[];else for(b=c.length;--b>-1;)null!=a[c[b]]&&c.splice(b,1);for(;d;)null!=a[d.n]&&(d._next&&(d._next._prev=d._prev),d._prev?(d._prev._next=d._next,d._prev=null):this._firstPT===d&&(this._firstPT=d._next)),d=d._next;return!1},g._mod=g._roundProps=function(a){for(var b,c=this._firstPT;c;)b=a[this._propName]||null!=c.n&&a[c.n.split(this._propName+"_").join("")],b&&"function"==typeof b&&(2===c.f?c.t._applyPT.m=b:c.m=b),c=c._next},F._onPluginEvent=function(a,b){var c,d,e,f,g,h=b._firstPT;if("_onInitAllProps"===a){for(;h;){for(g=h._next,d=e;d&&d.pr>h.pr;)d=d._next;(h._prev=d?d._prev:f)?h._prev._next=h:e=h,(h._next=d)?d._prev=h:f=h,h=g}h=b._firstPT=e}for(;h;)h.pg&&"function"==typeof h.t[a]&&h.t[a]()&&(c=!0),h=h._next;return c},aa.activate=function(a){for(var b=a.length;--b>-1;)a[b].API===aa.API&&(P[(new a[b])._propName]=a[b]);return!0},r.plugin=function(a){if(!(a&&a.propName&&a.init&&a.API))throw"illegal plugin definition.";var b,c=a.propName,d=a.priority||0,e=a.overwriteProps,f={init:"_onInitTween",set:"setRatio",kill:"_kill",round:"_mod",mod:"_mod",initAll:"_onInitAllProps"},g=s("plugins."+c.charAt(0).toUpperCase()+c.substr(1)+"Plugin",function(){aa.call(this,c,d),this._overwriteProps=e||[]},a.global===!0),h=g.prototype=new aa(c);h.constructor=g,g.API=a.API;for(b in f)"function"==typeof a[b]&&(h[f[b]]=a[b]);return g.version=a.version,aa.activate([g]),g},e=a._gsQueue){for(f=0;f<e.length;f++)e[f]();for(g in p)p[g].func||a.console.log("GSAP encountered missing dependency: "+g)}i=!1}}("undefined"!=typeof module&&module.exports&&"undefined"!=typeof global?global:this||window,"TweenMax");/*!
 * VERSION: 0.1.12
 * DATE: 2015-08-11
 * UPDATES AND DOCS AT: http://greensock.com/jquery-gsap-plugin/
 *
 * Requires TweenLite version 1.8.0 or higher and CSSPlugin.
 *
 * @license Copyright (c) 2013-2016, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 *
 * @author: Jack Doyle, jack@greensock.com
 */
!function(a){"use strict";var b,c,d,e=a.fn.animate,f=a.fn.stop,g=!0,h=function(a){var b,c={};for(b in a)c[b]=a[b];return c},i={overwrite:1,delay:1,useFrames:1,runBackwards:1,easeParams:1,yoyo:1,immediateRender:1,repeat:1,repeatDelay:1,autoCSS:1},j=",scrollTop,scrollLeft,show,hide,toggle,",k=j,l=function(a,b){for(var c in i)i[c]&&void 0!==a[c]&&(b[c]=a[c])},m=function(a){return function(b){return a.getRatio(b)}},n={},o=function(){var e,f,g,h=window.GreenSockGlobals||window;if(b=h.TweenMax||h.TweenLite,b&&(e=(b.version+".0.0").split("."),f=!(Number(e[0])>0&&Number(e[1])>7),h=h.com.greensock,c=h.plugins.CSSPlugin,n=h.easing.Ease.map||{}),!b||!c||f)return b=null,void(!d&&window.console&&(window.console.log("The jquery.gsap.js plugin requires the TweenMax (or at least TweenLite and CSSPlugin) JavaScript file(s)."+(f?" Version "+e.join(".")+" is too old.":"")),d=!0));if(a.easing){for(g in n)a.easing[g]=m(n[g]);o=!1}};a.fn.animate=function(d,f,i,j){if(d=d||{},o&&(o(),!b||!c))return e.call(this,d,f,i,j);if(!g||d.skipGSAP===!0||"object"==typeof f&&"function"==typeof f.step)return e.call(this,d,f,i,j);var m,p,q,r,s=a.speed(f,i,j),t={ease:n[s.easing]||(s.easing===!1?n.linear:n.swing)},u=this,v="object"==typeof f?f.specialEasing:null;for(p in d){if(m=d[p],m instanceof Array&&n[m[1]]&&(v=v||{},v[p]=m[1],m=m[0]),"show"===m||"hide"===m||"toggle"===m||-1!==k.indexOf(p)&&-1!==k.indexOf(","+p+","))return e.call(this,d,f,i,j);t[-1===p.indexOf("-")?p:a.camelCase(p)]=m}if(v){t=h(t),r=[];for(p in v)m=r[r.length]={},l(t,m),m.ease=n[v[p]]||t.ease,-1!==p.indexOf("-")&&(p=a.camelCase(p)),m[p]=t[p],delete t[p];0===r.length&&(r=null)}return q=function(c){var d,e=h(t);if(r)for(d=r.length;--d>-1;)b.to(this,a.fx.off?0:s.duration/1e3,r[d]);e.onComplete=function(){c?c():s.old&&a(this).each(s.old)},b.to(this,a.fx.off?0:s.duration/1e3,e)},s.queue!==!1?(u.queue(s.queue,q),"function"==typeof s.old&&a(u[u.length-1]).queue(s.queue,function(a){s.old.call(u),a()})):q.call(u),u},a.fn.stop=function(a,c){if(f.call(this,a,c),b){if(c)for(var d,e=b.getTweensOf(this),g=e.length;--g>-1;)d=e[g].totalTime()/e[g].totalDuration(),d>0&&1>d&&e[g].seek(e[g].totalDuration());b.killTweensOf(this)}return this},a.gsap={enabled:function(a){g=a},version:"0.1.12",legacyProps:function(a){k=j+a+","}}}(jQuery);/*!
 * VERSION: 1.19.0
 * DATE: 2016-07-14
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2016, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 *
 * @author: Jack Doyle, jack@greensock.com
 */
var _gsScope="undefined"!=typeof module&&module.exports&&"undefined"!=typeof global?global:this||window;(_gsScope._gsQueue||(_gsScope._gsQueue=[])).push(function(){"use strict";_gsScope._gsDefine("plugins.CSSPlugin",["plugins.TweenPlugin","TweenLite"],function(a,b){var c,d,e,f,g=function(){a.call(this,"css"),this._overwriteProps.length=0,this.setRatio=g.prototype.setRatio},h=_gsScope._gsDefine.globals,i={},j=g.prototype=new a("css");j.constructor=g,g.version="1.19.0",g.API=2,g.defaultTransformPerspective=0,g.defaultSkewType="compensated",g.defaultSmoothOrigin=!0,j="px",g.suffixMap={top:j,right:j,bottom:j,left:j,width:j,height:j,fontSize:j,padding:j,margin:j,perspective:j,lineHeight:""};var k,l,m,n,o,p,q,r,s=/(?:\-|\.|\b)(\d|\.|e\-)+/g,t=/(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g,u=/(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi,v=/(?![+-]?\d*\.?\d+|[+-]|e[+-]\d+)[^0-9]/g,w=/(?:\d|\-|\+|=|#|\.)*/g,x=/opacity *= *([^)]*)/i,y=/opacity:([^;]*)/i,z=/alpha\(opacity *=.+?\)/i,A=/^(rgb|hsl)/,B=/([A-Z])/g,C=/-([a-z])/gi,D=/(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi,E=function(a,b){return b.toUpperCase()},F=/(?:Left|Right|Width)/i,G=/(M11|M12|M21|M22)=[\d\-\.e]+/gi,H=/progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i,I=/,(?=[^\)]*(?:\(|$))/gi,J=/[\s,\(]/i,K=Math.PI/180,L=180/Math.PI,M={},N=document,O=function(a){return N.createElementNS?N.createElementNS("http://www.w3.org/1999/xhtml",a):N.createElement(a)},P=O("div"),Q=O("img"),R=g._internals={_specialProps:i},S=navigator.userAgent,T=function(){var a=S.indexOf("Android"),b=O("a");return m=-1!==S.indexOf("Safari")&&-1===S.indexOf("Chrome")&&(-1===a||Number(S.substr(a+8,1))>3),o=m&&Number(S.substr(S.indexOf("Version/")+8,1))<6,n=-1!==S.indexOf("Firefox"),(/MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(S)||/Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/.exec(S))&&(p=parseFloat(RegExp.$1)),b?(b.style.cssText="top:1px;opacity:.55;",/^0.55/.test(b.style.opacity)):!1}(),U=function(a){return x.test("string"==typeof a?a:(a.currentStyle?a.currentStyle.filter:a.style.filter)||"")?parseFloat(RegExp.$1)/100:1},V=function(a){window.console&&console.log(a)},W="",X="",Y=function(a,b){b=b||P;var c,d,e=b.style;if(void 0!==e[a])return a;for(a=a.charAt(0).toUpperCase()+a.substr(1),c=["O","Moz","ms","Ms","Webkit"],d=5;--d>-1&&void 0===e[c[d]+a];);return d>=0?(X=3===d?"ms":c[d],W="-"+X.toLowerCase()+"-",X+a):null},Z=N.defaultView?N.defaultView.getComputedStyle:function(){},$=g.getStyle=function(a,b,c,d,e){var f;return T||"opacity"!==b?(!d&&a.style[b]?f=a.style[b]:(c=c||Z(a))?f=c[b]||c.getPropertyValue(b)||c.getPropertyValue(b.replace(B,"-$1").toLowerCase()):a.currentStyle&&(f=a.currentStyle[b]),null==e||f&&"none"!==f&&"auto"!==f&&"auto auto"!==f?f:e):U(a)},_=R.convertToPixels=function(a,c,d,e,f){if("px"===e||!e)return d;if("auto"===e||!d)return 0;var h,i,j,k=F.test(c),l=a,m=P.style,n=0>d,o=1===d;if(n&&(d=-d),o&&(d*=100),"%"===e&&-1!==c.indexOf("border"))h=d/100*(k?a.clientWidth:a.clientHeight);else{if(m.cssText="border:0 solid red;position:"+$(a,"position")+";line-height:0;","%"!==e&&l.appendChild&&"v"!==e.charAt(0)&&"rem"!==e)m[k?"borderLeftWidth":"borderTopWidth"]=d+e;else{if(l=a.parentNode||N.body,i=l._gsCache,j=b.ticker.frame,i&&k&&i.time===j)return i.width*d/100;m[k?"width":"height"]=d+e}l.appendChild(P),h=parseFloat(P[k?"offsetWidth":"offsetHeight"]),l.removeChild(P),k&&"%"===e&&g.cacheWidths!==!1&&(i=l._gsCache=l._gsCache||{},i.time=j,i.width=h/d*100),0!==h||f||(h=_(a,c,d,e,!0))}return o&&(h/=100),n?-h:h},aa=R.calculateOffset=function(a,b,c){if("absolute"!==$(a,"position",c))return 0;var d="left"===b?"Left":"Top",e=$(a,"margin"+d,c);return a["offset"+d]-(_(a,b,parseFloat(e),e.replace(w,""))||0)},ba=function(a,b){var c,d,e,f={};if(b=b||Z(a,null))if(c=b.length)for(;--c>-1;)e=b[c],(-1===e.indexOf("-transform")||Ca===e)&&(f[e.replace(C,E)]=b.getPropertyValue(e));else for(c in b)(-1===c.indexOf("Transform")||Ba===c)&&(f[c]=b[c]);else if(b=a.currentStyle||a.style)for(c in b)"string"==typeof c&&void 0===f[c]&&(f[c.replace(C,E)]=b[c]);return T||(f.opacity=U(a)),d=Pa(a,b,!1),f.rotation=d.rotation,f.skewX=d.skewX,f.scaleX=d.scaleX,f.scaleY=d.scaleY,f.x=d.x,f.y=d.y,Ea&&(f.z=d.z,f.rotationX=d.rotationX,f.rotationY=d.rotationY,f.scaleZ=d.scaleZ),f.filters&&delete f.filters,f},ca=function(a,b,c,d,e){var f,g,h,i={},j=a.style;for(g in c)"cssText"!==g&&"length"!==g&&isNaN(g)&&(b[g]!==(f=c[g])||e&&e[g])&&-1===g.indexOf("Origin")&&("number"==typeof f||"string"==typeof f)&&(i[g]="auto"!==f||"left"!==g&&"top"!==g?""!==f&&"auto"!==f&&"none"!==f||"string"!=typeof b[g]||""===b[g].replace(v,"")?f:0:aa(a,g),void 0!==j[g]&&(h=new ra(j,g,j[g],h)));if(d)for(g in d)"className"!==g&&(i[g]=d[g]);return{difs:i,firstMPT:h}},da={width:["Left","Right"],height:["Top","Bottom"]},ea=["marginLeft","marginRight","marginTop","marginBottom"],fa=function(a,b,c){if("svg"===(a.nodeName+"").toLowerCase())return(c||Z(a))[b]||0;if(a.getBBox&&Ma(a))return a.getBBox()[b]||0;var d=parseFloat("width"===b?a.offsetWidth:a.offsetHeight),e=da[b],f=e.length;for(c=c||Z(a,null);--f>-1;)d-=parseFloat($(a,"padding"+e[f],c,!0))||0,d-=parseFloat($(a,"border"+e[f]+"Width",c,!0))||0;return d},ga=function(a,b){if("contain"===a||"auto"===a||"auto auto"===a)return a+" ";(null==a||""===a)&&(a="0 0");var c,d=a.split(" "),e=-1!==a.indexOf("left")?"0%":-1!==a.indexOf("right")?"100%":d[0],f=-1!==a.indexOf("top")?"0%":-1!==a.indexOf("bottom")?"100%":d[1];if(d.length>3&&!b){for(d=a.split(", ").join(",").split(","),a=[],c=0;c<d.length;c++)a.push(ga(d[c]));return a.join(",")}return null==f?f="center"===e?"50%":"0":"center"===f&&(f="50%"),("center"===e||isNaN(parseFloat(e))&&-1===(e+"").indexOf("="))&&(e="50%"),a=e+" "+f+(d.length>2?" "+d[2]:""),b&&(b.oxp=-1!==e.indexOf("%"),b.oyp=-1!==f.indexOf("%"),b.oxr="="===e.charAt(1),b.oyr="="===f.charAt(1),b.ox=parseFloat(e.replace(v,"")),b.oy=parseFloat(f.replace(v,"")),b.v=a),b||a},ha=function(a,b){return"function"==typeof a&&(a=a(r,q)),"string"==typeof a&&"="===a.charAt(1)?parseInt(a.charAt(0)+"1",10)*parseFloat(a.substr(2)):parseFloat(a)-parseFloat(b)||0},ia=function(a,b){return"function"==typeof a&&(a=a(r,q)),null==a?b:"string"==typeof a&&"="===a.charAt(1)?parseInt(a.charAt(0)+"1",10)*parseFloat(a.substr(2))+b:parseFloat(a)||0},ja=function(a,b,c,d){var e,f,g,h,i,j=1e-6;return"function"==typeof a&&(a=a(r,q)),null==a?h=b:"number"==typeof a?h=a:(e=360,f=a.split("_"),i="="===a.charAt(1),g=(i?parseInt(a.charAt(0)+"1",10)*parseFloat(f[0].substr(2)):parseFloat(f[0]))*(-1===a.indexOf("rad")?1:L)-(i?0:b),f.length&&(d&&(d[c]=b+g),-1!==a.indexOf("short")&&(g%=e,g!==g%(e/2)&&(g=0>g?g+e:g-e)),-1!==a.indexOf("_cw")&&0>g?g=(g+9999999999*e)%e-(g/e|0)*e:-1!==a.indexOf("ccw")&&g>0&&(g=(g-9999999999*e)%e-(g/e|0)*e)),h=b+g),j>h&&h>-j&&(h=0),h},ka={aqua:[0,255,255],lime:[0,255,0],silver:[192,192,192],black:[0,0,0],maroon:[128,0,0],teal:[0,128,128],blue:[0,0,255],navy:[0,0,128],white:[255,255,255],fuchsia:[255,0,255],olive:[128,128,0],yellow:[255,255,0],orange:[255,165,0],gray:[128,128,128],purple:[128,0,128],green:[0,128,0],red:[255,0,0],pink:[255,192,203],cyan:[0,255,255],transparent:[255,255,255,0]},la=function(a,b,c){return a=0>a?a+1:a>1?a-1:a,255*(1>6*a?b+(c-b)*a*6:.5>a?c:2>3*a?b+(c-b)*(2/3-a)*6:b)+.5|0},ma=g.parseColor=function(a,b){var c,d,e,f,g,h,i,j,k,l,m;if(a)if("number"==typeof a)c=[a>>16,a>>8&255,255&a];else{if(","===a.charAt(a.length-1)&&(a=a.substr(0,a.length-1)),ka[a])c=ka[a];else if("#"===a.charAt(0))4===a.length&&(d=a.charAt(1),e=a.charAt(2),f=a.charAt(3),a="#"+d+d+e+e+f+f),a=parseInt(a.substr(1),16),c=[a>>16,a>>8&255,255&a];else if("hsl"===a.substr(0,3))if(c=m=a.match(s),b){if(-1!==a.indexOf("="))return a.match(t)}else g=Number(c[0])%360/360,h=Number(c[1])/100,i=Number(c[2])/100,e=.5>=i?i*(h+1):i+h-i*h,d=2*i-e,c.length>3&&(c[3]=Number(a[3])),c[0]=la(g+1/3,d,e),c[1]=la(g,d,e),c[2]=la(g-1/3,d,e);else c=a.match(s)||ka.transparent;c[0]=Number(c[0]),c[1]=Number(c[1]),c[2]=Number(c[2]),c.length>3&&(c[3]=Number(c[3]))}else c=ka.black;return b&&!m&&(d=c[0]/255,e=c[1]/255,f=c[2]/255,j=Math.max(d,e,f),k=Math.min(d,e,f),i=(j+k)/2,j===k?g=h=0:(l=j-k,h=i>.5?l/(2-j-k):l/(j+k),g=j===d?(e-f)/l+(f>e?6:0):j===e?(f-d)/l+2:(d-e)/l+4,g*=60),c[0]=g+.5|0,c[1]=100*h+.5|0,c[2]=100*i+.5|0),c},na=function(a,b){var c,d,e,f=a.match(oa)||[],g=0,h=f.length?"":a;for(c=0;c<f.length;c++)d=f[c],e=a.substr(g,a.indexOf(d,g)-g),g+=e.length+d.length,d=ma(d,b),3===d.length&&d.push(1),h+=e+(b?"hsla("+d[0]+","+d[1]+"%,"+d[2]+"%,"+d[3]:"rgba("+d.join(","))+")";return h+a.substr(g)},oa="(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#(?:[0-9a-f]{3}){1,2}\\b";for(j in ka)oa+="|"+j+"\\b";oa=new RegExp(oa+")","gi"),g.colorStringFilter=function(a){var b,c=a[0]+a[1];oa.test(c)&&(b=-1!==c.indexOf("hsl(")||-1!==c.indexOf("hsla("),a[0]=na(a[0],b),a[1]=na(a[1],b)),oa.lastIndex=0},b.defaultStringFilter||(b.defaultStringFilter=g.colorStringFilter);var pa=function(a,b,c,d){if(null==a)return function(a){return a};var e,f=b?(a.match(oa)||[""])[0]:"",g=a.split(f).join("").match(u)||[],h=a.substr(0,a.indexOf(g[0])),i=")"===a.charAt(a.length-1)?")":"",j=-1!==a.indexOf(" ")?" ":",",k=g.length,l=k>0?g[0].replace(s,""):"";return k?e=b?function(a){var b,m,n,o;if("number"==typeof a)a+=l;else if(d&&I.test(a)){for(o=a.replace(I,"|").split("|"),n=0;n<o.length;n++)o[n]=e(o[n]);return o.join(",")}if(b=(a.match(oa)||[f])[0],m=a.split(b).join("").match(u)||[],n=m.length,k>n--)for(;++n<k;)m[n]=c?m[(n-1)/2|0]:g[n];return h+m.join(j)+j+b+i+(-1!==a.indexOf("inset")?" inset":"")}:function(a){var b,f,m;if("number"==typeof a)a+=l;else if(d&&I.test(a)){for(f=a.replace(I,"|").split("|"),m=0;m<f.length;m++)f[m]=e(f[m]);return f.join(",")}if(b=a.match(u)||[],m=b.length,k>m--)for(;++m<k;)b[m]=c?b[(m-1)/2|0]:g[m];return h+b.join(j)+i}:function(a){return a}},qa=function(a){return a=a.split(","),function(b,c,d,e,f,g,h){var i,j=(c+"").split(" ");for(h={},i=0;4>i;i++)h[a[i]]=j[i]=j[i]||j[(i-1)/2>>0];return e.parse(b,h,f,g)}},ra=(R._setPluginRatio=function(a){this.plugin.setRatio(a);for(var b,c,d,e,f,g=this.data,h=g.proxy,i=g.firstMPT,j=1e-6;i;)b=h[i.v],i.r?b=Math.round(b):j>b&&b>-j&&(b=0),i.t[i.p]=b,i=i._next;if(g.autoRotate&&(g.autoRotate.rotation=g.mod?g.mod(h.rotation,this.t):h.rotation),1===a||0===a)for(i=g.firstMPT,f=1===a?"e":"b";i;){if(c=i.t,c.type){if(1===c.type){for(e=c.xs0+c.s+c.xs1,d=1;d<c.l;d++)e+=c["xn"+d]+c["xs"+(d+1)];c[f]=e}}else c[f]=c.s+c.xs0;i=i._next}},function(a,b,c,d,e){this.t=a,this.p=b,this.v=c,this.r=e,d&&(d._prev=this,this._next=d)}),sa=(R._parseToProxy=function(a,b,c,d,e,f){var g,h,i,j,k,l=d,m={},n={},o=c._transform,p=M;for(c._transform=null,M=b,d=k=c.parse(a,b,d,e),M=p,f&&(c._transform=o,l&&(l._prev=null,l._prev&&(l._prev._next=null)));d&&d!==l;){if(d.type<=1&&(h=d.p,n[h]=d.s+d.c,m[h]=d.s,f||(j=new ra(d,"s",h,j,d.r),d.c=0),1===d.type))for(g=d.l;--g>0;)i="xn"+g,h=d.p+"_"+i,n[h]=d.data[i],m[h]=d[i],f||(j=new ra(d,i,h,j,d.rxp[i]));d=d._next}return{proxy:m,end:n,firstMPT:j,pt:k}},R.CSSPropTween=function(a,b,d,e,g,h,i,j,k,l,m){this.t=a,this.p=b,this.s=d,this.c=e,this.n=i||b,a instanceof sa||f.push(this.n),this.r=j,this.type=h||0,k&&(this.pr=k,c=!0),this.b=void 0===l?d:l,this.e=void 0===m?d+e:m,g&&(this._next=g,g._prev=this)}),ta=function(a,b,c,d,e,f){var g=new sa(a,b,c,d-c,e,-1,f);return g.b=c,g.e=g.xs0=d,g},ua=g.parseComplex=function(a,b,c,d,e,f,h,i,j,l){c=c||f||"","function"==typeof d&&(d=d(r,q)),h=new sa(a,b,0,0,h,l?2:1,null,!1,i,c,d),d+="",e&&oa.test(d+c)&&(d=[c,d],g.colorStringFilter(d),c=d[0],d=d[1]);var m,n,o,p,u,v,w,x,y,z,A,B,C,D=c.split(", ").join(",").split(" "),E=d.split(", ").join(",").split(" "),F=D.length,G=k!==!1;for((-1!==d.indexOf(",")||-1!==c.indexOf(","))&&(D=D.join(" ").replace(I,", ").split(" "),E=E.join(" ").replace(I,", ").split(" "),F=D.length),F!==E.length&&(D=(f||"").split(" "),F=D.length),h.plugin=j,h.setRatio=l,oa.lastIndex=0,m=0;F>m;m++)if(p=D[m],u=E[m],x=parseFloat(p),x||0===x)h.appendXtra("",x,ha(u,x),u.replace(t,""),G&&-1!==u.indexOf("px"),!0);else if(e&&oa.test(p))B=u.indexOf(")")+1,B=")"+(B?u.substr(B):""),C=-1!==u.indexOf("hsl")&&T,p=ma(p,C),u=ma(u,C),y=p.length+u.length>6,y&&!T&&0===u[3]?(h["xs"+h.l]+=h.l?" transparent":"transparent",h.e=h.e.split(E[m]).join("transparent")):(T||(y=!1),C?h.appendXtra(y?"hsla(":"hsl(",p[0],ha(u[0],p[0]),",",!1,!0).appendXtra("",p[1],ha(u[1],p[1]),"%,",!1).appendXtra("",p[2],ha(u[2],p[2]),y?"%,":"%"+B,!1):h.appendXtra(y?"rgba(":"rgb(",p[0],u[0]-p[0],",",!0,!0).appendXtra("",p[1],u[1]-p[1],",",!0).appendXtra("",p[2],u[2]-p[2],y?",":B,!0),y&&(p=p.length<4?1:p[3],h.appendXtra("",p,(u.length<4?1:u[3])-p,B,!1))),oa.lastIndex=0;else if(v=p.match(s)){if(w=u.match(t),!w||w.length!==v.length)return h;for(o=0,n=0;n<v.length;n++)A=v[n],z=p.indexOf(A,o),h.appendXtra(p.substr(o,z-o),Number(A),ha(w[n],A),"",G&&"px"===p.substr(z+A.length,2),0===n),o=z+A.length;h["xs"+h.l]+=p.substr(o)}else h["xs"+h.l]+=h.l||h["xs"+h.l]?" "+u:u;if(-1!==d.indexOf("=")&&h.data){for(B=h.xs0+h.data.s,m=1;m<h.l;m++)B+=h["xs"+m]+h.data["xn"+m];h.e=B+h["xs"+m]}return h.l||(h.type=-1,h.xs0=h.e),h.xfirst||h},va=9;for(j=sa.prototype,j.l=j.pr=0;--va>0;)j["xn"+va]=0,j["xs"+va]="";j.xs0="",j._next=j._prev=j.xfirst=j.data=j.plugin=j.setRatio=j.rxp=null,j.appendXtra=function(a,b,c,d,e,f){var g=this,h=g.l;return g["xs"+h]+=f&&(h||g["xs"+h])?" "+a:a||"",c||0===h||g.plugin?(g.l++,g.type=g.setRatio?2:1,g["xs"+g.l]=d||"",h>0?(g.data["xn"+h]=b+c,g.rxp["xn"+h]=e,g["xn"+h]=b,g.plugin||(g.xfirst=new sa(g,"xn"+h,b,c,g.xfirst||g,0,g.n,e,g.pr),g.xfirst.xs0=0),g):(g.data={s:b+c},g.rxp={},g.s=b,g.c=c,g.r=e,g)):(g["xs"+h]+=b+(d||""),g)};var wa=function(a,b){b=b||{},this.p=b.prefix?Y(a)||a:a,i[a]=i[this.p]=this,this.format=b.formatter||pa(b.defaultValue,b.color,b.collapsible,b.multi),b.parser&&(this.parse=b.parser),this.clrs=b.color,this.multi=b.multi,this.keyword=b.keyword,this.dflt=b.defaultValue,this.pr=b.priority||0},xa=R._registerComplexSpecialProp=function(a,b,c){"object"!=typeof b&&(b={parser:c});var d,e,f=a.split(","),g=b.defaultValue;for(c=c||[g],d=0;d<f.length;d++)b.prefix=0===d&&b.prefix,b.defaultValue=c[d]||g,e=new wa(f[d],b)},ya=R._registerPluginProp=function(a){if(!i[a]){var b=a.charAt(0).toUpperCase()+a.substr(1)+"Plugin";xa(a,{parser:function(a,c,d,e,f,g,j){var k=h.com.greensock.plugins[b];return k?(k._cssRegister(),i[d].parse(a,c,d,e,f,g,j)):(V("Error: "+b+" js file not loaded."),f)}})}};j=wa.prototype,j.parseComplex=function(a,b,c,d,e,f){var g,h,i,j,k,l,m=this.keyword;if(this.multi&&(I.test(c)||I.test(b)?(h=b.replace(I,"|").split("|"),i=c.replace(I,"|").split("|")):m&&(h=[b],i=[c])),i){for(j=i.length>h.length?i.length:h.length,g=0;j>g;g++)b=h[g]=h[g]||this.dflt,c=i[g]=i[g]||this.dflt,m&&(k=b.indexOf(m),l=c.indexOf(m),k!==l&&(-1===l?h[g]=h[g].split(m).join(""):-1===k&&(h[g]+=" "+m)));b=h.join(", "),c=i.join(", ")}return ua(a,this.p,b,c,this.clrs,this.dflt,d,this.pr,e,f)},j.parse=function(a,b,c,d,f,g,h){return this.parseComplex(a.style,this.format($(a,this.p,e,!1,this.dflt)),this.format(b),f,g)},g.registerSpecialProp=function(a,b,c){xa(a,{parser:function(a,d,e,f,g,h,i){var j=new sa(a,e,0,0,g,2,e,!1,c);return j.plugin=h,j.setRatio=b(a,d,f._tween,e),j},priority:c})},g.useSVGTransformAttr=m||n;var za,Aa="scaleX,scaleY,scaleZ,x,y,z,skewX,skewY,rotation,rotationX,rotationY,perspective,xPercent,yPercent".split(","),Ba=Y("transform"),Ca=W+"transform",Da=Y("transformOrigin"),Ea=null!==Y("perspective"),Fa=R.Transform=function(){this.perspective=parseFloat(g.defaultTransformPerspective)||0,this.force3D=g.defaultForce3D!==!1&&Ea?g.defaultForce3D||"auto":!1},Ga=window.SVGElement,Ha=function(a,b,c){var d,e=N.createElementNS("http://www.w3.org/2000/svg",a),f=/([a-z])([A-Z])/g;for(d in c)e.setAttributeNS(null,d.replace(f,"$1-$2").toLowerCase(),c[d]);return b.appendChild(e),e},Ia=N.documentElement,Ja=function(){var a,b,c,d=p||/Android/i.test(S)&&!window.chrome;return N.createElementNS&&!d&&(a=Ha("svg",Ia),b=Ha("rect",a,{width:100,height:50,x:100}),c=b.getBoundingClientRect().width,b.style[Da]="50% 50%",b.style[Ba]="scaleX(0.5)",d=c===b.getBoundingClientRect().width&&!(n&&Ea),Ia.removeChild(a)),d}(),Ka=function(a,b,c,d,e,f){var h,i,j,k,l,m,n,o,p,q,r,s,t,u,v=a._gsTransform,w=Oa(a,!0);v&&(t=v.xOrigin,u=v.yOrigin),(!d||(h=d.split(" ")).length<2)&&(n=a.getBBox(),b=ga(b).split(" "),h=[(-1!==b[0].indexOf("%")?parseFloat(b[0])/100*n.width:parseFloat(b[0]))+n.x,(-1!==b[1].indexOf("%")?parseFloat(b[1])/100*n.height:parseFloat(b[1]))+n.y]),c.xOrigin=k=parseFloat(h[0]),c.yOrigin=l=parseFloat(h[1]),d&&w!==Na&&(m=w[0],n=w[1],o=w[2],p=w[3],q=w[4],r=w[5],s=m*p-n*o,i=k*(p/s)+l*(-o/s)+(o*r-p*q)/s,j=k*(-n/s)+l*(m/s)-(m*r-n*q)/s,k=c.xOrigin=h[0]=i,l=c.yOrigin=h[1]=j),v&&(f&&(c.xOffset=v.xOffset,c.yOffset=v.yOffset,v=c),e||e!==!1&&g.defaultSmoothOrigin!==!1?(i=k-t,j=l-u,v.xOffset+=i*w[0]+j*w[2]-i,v.yOffset+=i*w[1]+j*w[3]-j):v.xOffset=v.yOffset=0),f||a.setAttribute("data-svg-origin",h.join(" "))},La=function(a){try{return a.getBBox()}catch(a){}},Ma=function(a){return!!(Ga&&a.getBBox&&a.getCTM&&La(a)&&(!a.parentNode||a.parentNode.getBBox&&a.parentNode.getCTM))},Na=[1,0,0,1,0,0],Oa=function(a,b){var c,d,e,f,g,h,i=a._gsTransform||new Fa,j=1e5,k=a.style;if(Ba?d=$(a,Ca,null,!0):a.currentStyle&&(d=a.currentStyle.filter.match(G),d=d&&4===d.length?[d[0].substr(4),Number(d[2].substr(4)),Number(d[1].substr(4)),d[3].substr(4),i.x||0,i.y||0].join(","):""),c=!d||"none"===d||"matrix(1, 0, 0, 1, 0, 0)"===d,c&&Ba&&((h="none"===Z(a).display)||!a.parentNode)&&(h&&(f=k.display,k.display="block"),a.parentNode||(g=1,Ia.appendChild(a)),d=$(a,Ca,null,!0),c=!d||"none"===d||"matrix(1, 0, 0, 1, 0, 0)"===d,f?k.display=f:h&&Ta(k,"display"),g&&Ia.removeChild(a)),(i.svg||a.getBBox&&Ma(a))&&(c&&-1!==(k[Ba]+"").indexOf("matrix")&&(d=k[Ba],c=0),e=a.getAttribute("transform"),c&&e&&(-1!==e.indexOf("matrix")?(d=e,c=0):-1!==e.indexOf("translate")&&(d="matrix(1,0,0,1,"+e.match(/(?:\-|\b)[\d\-\.e]+\b/gi).join(",")+")",c=0))),c)return Na;for(e=(d||"").match(s)||[],va=e.length;--va>-1;)f=Number(e[va]),e[va]=(g=f-(f|=0))?(g*j+(0>g?-.5:.5)|0)/j+f:f;return b&&e.length>6?[e[0],e[1],e[4],e[5],e[12],e[13]]:e},Pa=R.getTransform=function(a,c,d,e){if(a._gsTransform&&d&&!e)return a._gsTransform;var f,h,i,j,k,l,m=d?a._gsTransform||new Fa:new Fa,n=m.scaleX<0,o=2e-5,p=1e5,q=Ea?parseFloat($(a,Da,c,!1,"0 0 0").split(" ")[2])||m.zOrigin||0:0,r=parseFloat(g.defaultTransformPerspective)||0;if(m.svg=!(!a.getBBox||!Ma(a)),m.svg&&(Ka(a,$(a,Da,c,!1,"50% 50%")+"",m,a.getAttribute("data-svg-origin")),za=g.useSVGTransformAttr||Ja),f=Oa(a),f!==Na){if(16===f.length){var s,t,u,v,w,x=f[0],y=f[1],z=f[2],A=f[3],B=f[4],C=f[5],D=f[6],E=f[7],F=f[8],G=f[9],H=f[10],I=f[12],J=f[13],K=f[14],M=f[11],N=Math.atan2(D,H);m.zOrigin&&(K=-m.zOrigin,I=F*K-f[12],J=G*K-f[13],K=H*K+m.zOrigin-f[14]),m.rotationX=N*L,N&&(v=Math.cos(-N),w=Math.sin(-N),s=B*v+F*w,t=C*v+G*w,u=D*v+H*w,F=B*-w+F*v,G=C*-w+G*v,H=D*-w+H*v,M=E*-w+M*v,B=s,C=t,D=u),N=Math.atan2(-z,H),m.rotationY=N*L,N&&(v=Math.cos(-N),w=Math.sin(-N),s=x*v-F*w,t=y*v-G*w,u=z*v-H*w,G=y*w+G*v,H=z*w+H*v,M=A*w+M*v,x=s,y=t,z=u),N=Math.atan2(y,x),m.rotation=N*L,N&&(v=Math.cos(-N),w=Math.sin(-N),x=x*v+B*w,t=y*v+C*w,C=y*-w+C*v,D=z*-w+D*v,y=t),m.rotationX&&Math.abs(m.rotationX)+Math.abs(m.rotation)>359.9&&(m.rotationX=m.rotation=0,m.rotationY=180-m.rotationY),m.scaleX=(Math.sqrt(x*x+y*y)*p+.5|0)/p,m.scaleY=(Math.sqrt(C*C+G*G)*p+.5|0)/p,m.scaleZ=(Math.sqrt(D*D+H*H)*p+.5|0)/p,m.rotationX||m.rotationY?m.skewX=0:(m.skewX=B||C?Math.atan2(B,C)*L+m.rotation:m.skewX||0,Math.abs(m.skewX)>90&&Math.abs(m.skewX)<270&&(n?(m.scaleX*=-1,m.skewX+=m.rotation<=0?180:-180,m.rotation+=m.rotation<=0?180:-180):(m.scaleY*=-1,m.skewX+=m.skewX<=0?180:-180))),m.perspective=M?1/(0>M?-M:M):0,m.x=I,m.y=J,m.z=K,m.svg&&(m.x-=m.xOrigin-(m.xOrigin*x-m.yOrigin*B),m.y-=m.yOrigin-(m.yOrigin*y-m.xOrigin*C))}else if(!Ea||e||!f.length||m.x!==f[4]||m.y!==f[5]||!m.rotationX&&!m.rotationY){var O=f.length>=6,P=O?f[0]:1,Q=f[1]||0,R=f[2]||0,S=O?f[3]:1;m.x=f[4]||0,m.y=f[5]||0,i=Math.sqrt(P*P+Q*Q),j=Math.sqrt(S*S+R*R),k=P||Q?Math.atan2(Q,P)*L:m.rotation||0,l=R||S?Math.atan2(R,S)*L+k:m.skewX||0,Math.abs(l)>90&&Math.abs(l)<270&&(n?(i*=-1,l+=0>=k?180:-180,k+=0>=k?180:-180):(j*=-1,l+=0>=l?180:-180)),m.scaleX=i,m.scaleY=j,m.rotation=k,m.skewX=l,Ea&&(m.rotationX=m.rotationY=m.z=0,m.perspective=r,m.scaleZ=1),m.svg&&(m.x-=m.xOrigin-(m.xOrigin*P+m.yOrigin*R),m.y-=m.yOrigin-(m.xOrigin*Q+m.yOrigin*S))}m.zOrigin=q;for(h in m)m[h]<o&&m[h]>-o&&(m[h]=0)}return d&&(a._gsTransform=m,m.svg&&(za&&a.style[Ba]?b.delayedCall(.001,function(){Ta(a.style,Ba)}):!za&&a.getAttribute("transform")&&b.delayedCall(.001,function(){a.removeAttribute("transform")}))),m},Qa=function(a){var b,c,d=this.data,e=-d.rotation*K,f=e+d.skewX*K,g=1e5,h=(Math.cos(e)*d.scaleX*g|0)/g,i=(Math.sin(e)*d.scaleX*g|0)/g,j=(Math.sin(f)*-d.scaleY*g|0)/g,k=(Math.cos(f)*d.scaleY*g|0)/g,l=this.t.style,m=this.t.currentStyle;if(m){c=i,i=-j,j=-c,b=m.filter,l.filter="";var n,o,q=this.t.offsetWidth,r=this.t.offsetHeight,s="absolute"!==m.position,t="progid:DXImageTransform.Microsoft.Matrix(M11="+h+", M12="+i+", M21="+j+", M22="+k,u=d.x+q*d.xPercent/100,v=d.y+r*d.yPercent/100;if(null!=d.ox&&(n=(d.oxp?q*d.ox*.01:d.ox)-q/2,o=(d.oyp?r*d.oy*.01:d.oy)-r/2,u+=n-(n*h+o*i),v+=o-(n*j+o*k)),s?(n=q/2,o=r/2,t+=", Dx="+(n-(n*h+o*i)+u)+", Dy="+(o-(n*j+o*k)+v)+")"):t+=", sizingMethod='auto expand')",-1!==b.indexOf("DXImageTransform.Microsoft.Matrix(")?l.filter=b.replace(H,t):l.filter=t+" "+b,(0===a||1===a)&&1===h&&0===i&&0===j&&1===k&&(s&&-1===t.indexOf("Dx=0, Dy=0")||x.test(b)&&100!==parseFloat(RegExp.$1)||-1===b.indexOf(b.indexOf("Alpha"))&&l.removeAttribute("filter")),!s){var y,z,A,B=8>p?1:-1;for(n=d.ieOffsetX||0,o=d.ieOffsetY||0,d.ieOffsetX=Math.round((q-((0>h?-h:h)*q+(0>i?-i:i)*r))/2+u),d.ieOffsetY=Math.round((r-((0>k?-k:k)*r+(0>j?-j:j)*q))/2+v),va=0;4>va;va++)z=ea[va],y=m[z],c=-1!==y.indexOf("px")?parseFloat(y):_(this.t,z,parseFloat(y),y.replace(w,""))||0,A=c!==d[z]?2>va?-d.ieOffsetX:-d.ieOffsetY:2>va?n-d.ieOffsetX:o-d.ieOffsetY,l[z]=(d[z]=Math.round(c-A*(0===va||2===va?1:B)))+"px"}}},Ra=R.set3DTransformRatio=R.setTransformRatio=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,o,p,q,r,s,t,u,v,w,x,y,z=this.data,A=this.t.style,B=z.rotation,C=z.rotationX,D=z.rotationY,E=z.scaleX,F=z.scaleY,G=z.scaleZ,H=z.x,I=z.y,J=z.z,L=z.svg,M=z.perspective,N=z.force3D;if(((1===a||0===a)&&"auto"===N&&(this.tween._totalTime===this.tween._totalDuration||!this.tween._totalTime)||!N)&&!J&&!M&&!D&&!C&&1===G||za&&L||!Ea)return void(B||z.skewX||L?(B*=K,x=z.skewX*K,y=1e5,b=Math.cos(B)*E,e=Math.sin(B)*E,c=Math.sin(B-x)*-F,f=Math.cos(B-x)*F,x&&"simple"===z.skewType&&(s=Math.tan(x-z.skewY*K),s=Math.sqrt(1+s*s),c*=s,f*=s,z.skewY&&(s=Math.tan(z.skewY*K),s=Math.sqrt(1+s*s),b*=s,e*=s)),L&&(H+=z.xOrigin-(z.xOrigin*b+z.yOrigin*c)+z.xOffset,I+=z.yOrigin-(z.xOrigin*e+z.yOrigin*f)+z.yOffset,za&&(z.xPercent||z.yPercent)&&(p=this.t.getBBox(),H+=.01*z.xPercent*p.width,I+=.01*z.yPercent*p.height),p=1e-6,p>H&&H>-p&&(H=0),p>I&&I>-p&&(I=0)),u=(b*y|0)/y+","+(e*y|0)/y+","+(c*y|0)/y+","+(f*y|0)/y+","+H+","+I+")",L&&za?this.t.setAttribute("transform","matrix("+u):A[Ba]=(z.xPercent||z.yPercent?"translate("+z.xPercent+"%,"+z.yPercent+"%) matrix(":"matrix(")+u):A[Ba]=(z.xPercent||z.yPercent?"translate("+z.xPercent+"%,"+z.yPercent+"%) matrix(":"matrix(")+E+",0,0,"+F+","+H+","+I+")");if(n&&(p=1e-4,p>E&&E>-p&&(E=G=2e-5),p>F&&F>-p&&(F=G=2e-5),!M||z.z||z.rotationX||z.rotationY||(M=0)),B||z.skewX)B*=K,q=b=Math.cos(B),r=e=Math.sin(B),z.skewX&&(B-=z.skewX*K,q=Math.cos(B),r=Math.sin(B),"simple"===z.skewType&&(s=Math.tan((z.skewX-z.skewY)*K),s=Math.sqrt(1+s*s),q*=s,r*=s,z.skewY&&(s=Math.tan(z.skewY*K),s=Math.sqrt(1+s*s),b*=s,e*=s))),c=-r,f=q;else{if(!(D||C||1!==G||M||L))return void(A[Ba]=(z.xPercent||z.yPercent?"translate("+z.xPercent+"%,"+z.yPercent+"%) translate3d(":"translate3d(")+H+"px,"+I+"px,"+J+"px)"+(1!==E||1!==F?" scale("+E+","+F+")":""));b=f=1,c=e=0}j=1,d=g=h=i=k=l=0,m=M?-1/M:0,o=z.zOrigin,p=1e-6,v=",",w="0",B=D*K,B&&(q=Math.cos(B),r=Math.sin(B),h=-r,k=m*-r,d=b*r,g=e*r,j=q,m*=q,b*=q,e*=q),B=C*K,B&&(q=Math.cos(B),r=Math.sin(B),s=c*q+d*r,t=f*q+g*r,i=j*r,l=m*r,d=c*-r+d*q,g=f*-r+g*q,j*=q,m*=q,c=s,f=t),1!==G&&(d*=G,g*=G,j*=G,m*=G),1!==F&&(c*=F,f*=F,i*=F,l*=F),1!==E&&(b*=E,e*=E,h*=E,k*=E),(o||L)&&(o&&(H+=d*-o,I+=g*-o,J+=j*-o+o),L&&(H+=z.xOrigin-(z.xOrigin*b+z.yOrigin*c)+z.xOffset,I+=z.yOrigin-(z.xOrigin*e+z.yOrigin*f)+z.yOffset),p>H&&H>-p&&(H=w),p>I&&I>-p&&(I=w),p>J&&J>-p&&(J=0)),u=z.xPercent||z.yPercent?"translate("+z.xPercent+"%,"+z.yPercent+"%) matrix3d(":"matrix3d(",u+=(p>b&&b>-p?w:b)+v+(p>e&&e>-p?w:e)+v+(p>h&&h>-p?w:h),u+=v+(p>k&&k>-p?w:k)+v+(p>c&&c>-p?w:c)+v+(p>f&&f>-p?w:f),C||D||1!==G?(u+=v+(p>i&&i>-p?w:i)+v+(p>l&&l>-p?w:l)+v+(p>d&&d>-p?w:d),u+=v+(p>g&&g>-p?w:g)+v+(p>j&&j>-p?w:j)+v+(p>m&&m>-p?w:m)+v):u+=",0,0,0,0,1,0,",u+=H+v+I+v+J+v+(M?1+-J/M:1)+")",A[Ba]=u};j=Fa.prototype,j.x=j.y=j.z=j.skewX=j.skewY=j.rotation=j.rotationX=j.rotationY=j.zOrigin=j.xPercent=j.yPercent=j.xOffset=j.yOffset=0,j.scaleX=j.scaleY=j.scaleZ=1,xa("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,svgOrigin,transformPerspective,directionalRotation,parseTransform,force3D,skewType,xPercent,yPercent,smoothOrigin",{parser:function(a,b,c,d,f,h,i){if(d._lastParsedTransform===i)return f;d._lastParsedTransform=i;var j;"function"==typeof i[c]&&(j=i[c],i[c]=b);var k,l,m,n,o,p,s,t,u,v=a._gsTransform,w=a.style,x=1e-6,y=Aa.length,z=i,A={},B="transformOrigin",C=Pa(a,e,!0,z.parseTransform),D=z.transform&&("function"==typeof z.transform?z.transform(r,q):z.transform);if(d._transform=C,D&&"string"==typeof D&&Ba)l=P.style,l[Ba]=D,l.display="block",l.position="absolute",N.body.appendChild(P),k=Pa(P,null,!1),C.svg&&(p=C.xOrigin,s=C.yOrigin,k.x-=C.xOffset,k.y-=C.yOffset,(z.transformOrigin||z.svgOrigin)&&(D={},Ka(a,ga(z.transformOrigin),D,z.svgOrigin,z.smoothOrigin,!0),p=D.xOrigin,s=D.yOrigin,k.x-=D.xOffset-C.xOffset,k.y-=D.yOffset-C.yOffset),(p||s)&&(t=Oa(P,!0),k.x-=p-(p*t[0]+s*t[2]),k.y-=s-(p*t[1]+s*t[3]))),N.body.removeChild(P),k.perspective||(k.perspective=C.perspective),null!=z.xPercent&&(k.xPercent=ia(z.xPercent,C.xPercent)),null!=z.yPercent&&(k.yPercent=ia(z.yPercent,C.yPercent));else if("object"==typeof z){if(k={scaleX:ia(null!=z.scaleX?z.scaleX:z.scale,C.scaleX),scaleY:ia(null!=z.scaleY?z.scaleY:z.scale,C.scaleY),scaleZ:ia(z.scaleZ,C.scaleZ),x:ia(z.x,C.x),y:ia(z.y,C.y),z:ia(z.z,C.z),xPercent:ia(z.xPercent,C.xPercent),yPercent:ia(z.yPercent,C.yPercent),perspective:ia(z.transformPerspective,C.perspective)},o=z.directionalRotation,null!=o)if("object"==typeof o)for(l in o)z[l]=o[l];else z.rotation=o;"string"==typeof z.x&&-1!==z.x.indexOf("%")&&(k.x=0,k.xPercent=ia(z.x,C.xPercent)),"string"==typeof z.y&&-1!==z.y.indexOf("%")&&(k.y=0,k.yPercent=ia(z.y,C.yPercent)),k.rotation=ja("rotation"in z?z.rotation:"shortRotation"in z?z.shortRotation+"_short":"rotationZ"in z?z.rotationZ:C.rotation-C.skewY,C.rotation-C.skewY,"rotation",A),Ea&&(k.rotationX=ja("rotationX"in z?z.rotationX:"shortRotationX"in z?z.shortRotationX+"_short":C.rotationX||0,C.rotationX,"rotationX",A),k.rotationY=ja("rotationY"in z?z.rotationY:"shortRotationY"in z?z.shortRotationY+"_short":C.rotationY||0,C.rotationY,"rotationY",A)),k.skewX=ja(z.skewX,C.skewX-C.skewY),(k.skewY=ja(z.skewY,C.skewY))&&(k.skewX+=k.skewY,k.rotation+=k.skewY)}for(Ea&&null!=z.force3D&&(C.force3D=z.force3D,n=!0),C.skewType=z.skewType||C.skewType||g.defaultSkewType,m=C.force3D||C.z||C.rotationX||C.rotationY||k.z||k.rotationX||k.rotationY||k.perspective,m||null==z.scale||(k.scaleZ=1);--y>-1;)u=Aa[y],D=k[u]-C[u],(D>x||-x>D||null!=z[u]||null!=M[u])&&(n=!0,f=new sa(C,u,C[u],D,f),u in A&&(f.e=A[u]),f.xs0=0,f.plugin=h,d._overwriteProps.push(f.n));return D=z.transformOrigin,C.svg&&(D||z.svgOrigin)&&(p=C.xOffset,s=C.yOffset,Ka(a,ga(D),k,z.svgOrigin,z.smoothOrigin),f=ta(C,"xOrigin",(v?C:k).xOrigin,k.xOrigin,f,B),f=ta(C,"yOrigin",(v?C:k).yOrigin,k.yOrigin,f,B),(p!==C.xOffset||s!==C.yOffset)&&(f=ta(C,"xOffset",v?p:C.xOffset,C.xOffset,f,B),f=ta(C,"yOffset",v?s:C.yOffset,C.yOffset,f,B)),D=za?null:"0px 0px"),(D||Ea&&m&&C.zOrigin)&&(Ba?(n=!0,u=Da,D=(D||$(a,u,e,!1,"50% 50%"))+"",f=new sa(w,u,0,0,f,-1,B),f.b=w[u],f.plugin=h,Ea?(l=C.zOrigin,D=D.split(" "),C.zOrigin=(D.length>2&&(0===l||"0px"!==D[2])?parseFloat(D[2]):l)||0,f.xs0=f.e=D[0]+" "+(D[1]||"50%")+" 0px",f=new sa(C,"zOrigin",0,0,f,-1,f.n),f.b=l,f.xs0=f.e=C.zOrigin):f.xs0=f.e=D):ga(D+"",C)),n&&(d._transformType=C.svg&&za||!m&&3!==this._transformType?2:3),j&&(i[c]=j),f},prefix:!0}),xa("boxShadow",{defaultValue:"0px 0px 0px 0px #999",prefix:!0,color:!0,multi:!0,keyword:"inset"}),xa("borderRadius",{defaultValue:"0px",parser:function(a,b,c,f,g,h){b=this.format(b);var i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y=["borderTopLeftRadius","borderTopRightRadius","borderBottomRightRadius","borderBottomLeftRadius"],z=a.style;for(q=parseFloat(a.offsetWidth),r=parseFloat(a.offsetHeight),i=b.split(" "),j=0;j<y.length;j++)this.p.indexOf("border")&&(y[j]=Y(y[j])),m=l=$(a,y[j],e,!1,"0px"),-1!==m.indexOf(" ")&&(l=m.split(" "),m=l[0],l=l[1]),n=k=i[j],o=parseFloat(m),t=m.substr((o+"").length),u="="===n.charAt(1),u?(p=parseInt(n.charAt(0)+"1",10),n=n.substr(2),p*=parseFloat(n),s=n.substr((p+"").length-(0>p?1:0))||""):(p=parseFloat(n),s=n.substr((p+"").length)),""===s&&(s=d[c]||t),s!==t&&(v=_(a,"borderLeft",o,t),w=_(a,"borderTop",o,t),"%"===s?(m=v/q*100+"%",l=w/r*100+"%"):"em"===s?(x=_(a,"borderLeft",1,"em"),m=v/x+"em",l=w/x+"em"):(m=v+"px",l=w+"px"),u&&(n=parseFloat(m)+p+s,k=parseFloat(l)+p+s)),g=ua(z,y[j],m+" "+l,n+" "+k,!1,"0px",g);return g},prefix:!0,formatter:pa("0px 0px 0px 0px",!1,!0)}),xa("borderBottomLeftRadius,borderBottomRightRadius,borderTopLeftRadius,borderTopRightRadius",{defaultValue:"0px",parser:function(a,b,c,d,f,g){return ua(a.style,c,this.format($(a,c,e,!1,"0px 0px")),this.format(b),!1,"0px",f)},prefix:!0,formatter:pa("0px 0px",!1,!0)}),xa("backgroundPosition",{defaultValue:"0 0",parser:function(a,b,c,d,f,g){var h,i,j,k,l,m,n="background-position",o=e||Z(a,null),q=this.format((o?p?o.getPropertyValue(n+"-x")+" "+o.getPropertyValue(n+"-y"):o.getPropertyValue(n):a.currentStyle.backgroundPositionX+" "+a.currentStyle.backgroundPositionY)||"0 0"),r=this.format(b);if(-1!==q.indexOf("%")!=(-1!==r.indexOf("%"))&&r.split(",").length<2&&(m=$(a,"backgroundImage").replace(D,""),m&&"none"!==m)){for(h=q.split(" "),i=r.split(" "),Q.setAttribute("src",m),j=2;--j>-1;)q=h[j],k=-1!==q.indexOf("%"),k!==(-1!==i[j].indexOf("%"))&&(l=0===j?a.offsetWidth-Q.width:a.offsetHeight-Q.height,h[j]=k?parseFloat(q)/100*l+"px":parseFloat(q)/l*100+"%");q=h.join(" ")}return this.parseComplex(a.style,q,r,f,g)},formatter:ga}),xa("backgroundSize",{defaultValue:"0 0",formatter:function(a){return a+="",ga(-1===a.indexOf(" ")?a+" "+a:a)}}),xa("perspective",{defaultValue:"0px",prefix:!0}),xa("perspectiveOrigin",{defaultValue:"50% 50%",prefix:!0}),xa("transformStyle",{prefix:!0}),xa("backfaceVisibility",{prefix:!0}),xa("userSelect",{prefix:!0}),xa("margin",{parser:qa("marginTop,marginRight,marginBottom,marginLeft")}),xa("padding",{parser:qa("paddingTop,paddingRight,paddingBottom,paddingLeft")}),xa("clip",{defaultValue:"rect(0px,0px,0px,0px)",parser:function(a,b,c,d,f,g){var h,i,j;return 9>p?(i=a.currentStyle,j=8>p?" ":",",h="rect("+i.clipTop+j+i.clipRight+j+i.clipBottom+j+i.clipLeft+")",b=this.format(b).split(",").join(j)):(h=this.format($(a,this.p,e,!1,this.dflt)),b=this.format(b)),this.parseComplex(a.style,h,b,f,g)}}),xa("textShadow",{defaultValue:"0px 0px 0px #999",color:!0,multi:!0}),xa("autoRound,strictUnits",{parser:function(a,b,c,d,e){return e}}),xa("border",{defaultValue:"0px solid #000",parser:function(a,b,c,d,f,g){var h=$(a,"borderTopWidth",e,!1,"0px"),i=this.format(b).split(" "),j=i[0].replace(w,"");return"px"!==j&&(h=parseFloat(h)/_(a,"borderTopWidth",1,j)+j),this.parseComplex(a.style,this.format(h+" "+$(a,"borderTopStyle",e,!1,"solid")+" "+$(a,"borderTopColor",e,!1,"#000")),i.join(" "),f,g)},color:!0,formatter:function(a){var b=a.split(" ");return b[0]+" "+(b[1]||"solid")+" "+(a.match(oa)||["#000"])[0]}}),xa("borderWidth",{
parser:qa("borderTopWidth,borderRightWidth,borderBottomWidth,borderLeftWidth")}),xa("float,cssFloat,styleFloat",{parser:function(a,b,c,d,e,f){var g=a.style,h="cssFloat"in g?"cssFloat":"styleFloat";return new sa(g,h,0,0,e,-1,c,!1,0,g[h],b)}});var Sa=function(a){var b,c=this.t,d=c.filter||$(this.data,"filter")||"",e=this.s+this.c*a|0;100===e&&(-1===d.indexOf("atrix(")&&-1===d.indexOf("radient(")&&-1===d.indexOf("oader(")?(c.removeAttribute("filter"),b=!$(this.data,"filter")):(c.filter=d.replace(z,""),b=!0)),b||(this.xn1&&(c.filter=d=d||"alpha(opacity="+e+")"),-1===d.indexOf("pacity")?0===e&&this.xn1||(c.filter=d+" alpha(opacity="+e+")"):c.filter=d.replace(x,"opacity="+e))};xa("opacity,alpha,autoAlpha",{defaultValue:"1",parser:function(a,b,c,d,f,g){var h=parseFloat($(a,"opacity",e,!1,"1")),i=a.style,j="autoAlpha"===c;return"string"==typeof b&&"="===b.charAt(1)&&(b=("-"===b.charAt(0)?-1:1)*parseFloat(b.substr(2))+h),j&&1===h&&"hidden"===$(a,"visibility",e)&&0!==b&&(h=0),T?f=new sa(i,"opacity",h,b-h,f):(f=new sa(i,"opacity",100*h,100*(b-h),f),f.xn1=j?1:0,i.zoom=1,f.type=2,f.b="alpha(opacity="+f.s+")",f.e="alpha(opacity="+(f.s+f.c)+")",f.data=a,f.plugin=g,f.setRatio=Sa),j&&(f=new sa(i,"visibility",0,0,f,-1,null,!1,0,0!==h?"inherit":"hidden",0===b?"hidden":"inherit"),f.xs0="inherit",d._overwriteProps.push(f.n),d._overwriteProps.push(c)),f}});var Ta=function(a,b){b&&(a.removeProperty?(("ms"===b.substr(0,2)||"webkit"===b.substr(0,6))&&(b="-"+b),a.removeProperty(b.replace(B,"-$1").toLowerCase())):a.removeAttribute(b))},Ua=function(a){if(this.t._gsClassPT=this,1===a||0===a){this.t.setAttribute("class",0===a?this.b:this.e);for(var b=this.data,c=this.t.style;b;)b.v?c[b.p]=b.v:Ta(c,b.p),b=b._next;1===a&&this.t._gsClassPT===this&&(this.t._gsClassPT=null)}else this.t.getAttribute("class")!==this.e&&this.t.setAttribute("class",this.e)};xa("className",{parser:function(a,b,d,f,g,h,i){var j,k,l,m,n,o=a.getAttribute("class")||"",p=a.style.cssText;if(g=f._classNamePT=new sa(a,d,0,0,g,2),g.setRatio=Ua,g.pr=-11,c=!0,g.b=o,k=ba(a,e),l=a._gsClassPT){for(m={},n=l.data;n;)m[n.p]=1,n=n._next;l.setRatio(1)}return a._gsClassPT=g,g.e="="!==b.charAt(1)?b:o.replace(new RegExp("(?:\\s|^)"+b.substr(2)+"(?![\\w-])"),"")+("+"===b.charAt(0)?" "+b.substr(2):""),a.setAttribute("class",g.e),j=ca(a,k,ba(a),i,m),a.setAttribute("class",o),g.data=j.firstMPT,a.style.cssText=p,g=g.xfirst=f.parse(a,j.difs,g,h)}});var Va=function(a){if((1===a||0===a)&&this.data._totalTime===this.data._totalDuration&&"isFromStart"!==this.data.data){var b,c,d,e,f,g=this.t.style,h=i.transform.parse;if("all"===this.e)g.cssText="",e=!0;else for(b=this.e.split(" ").join("").split(","),d=b.length;--d>-1;)c=b[d],i[c]&&(i[c].parse===h?e=!0:c="transformOrigin"===c?Da:i[c].p),Ta(g,c);e&&(Ta(g,Ba),f=this.t._gsTransform,f&&(f.svg&&(this.t.removeAttribute("data-svg-origin"),this.t.removeAttribute("transform")),delete this.t._gsTransform))}};for(xa("clearProps",{parser:function(a,b,d,e,f){return f=new sa(a,d,0,0,f,2),f.setRatio=Va,f.e=b,f.pr=-10,f.data=e._tween,c=!0,f}}),j="bezier,throwProps,physicsProps,physics2D".split(","),va=j.length;va--;)ya(j[va]);j=g.prototype,j._firstPT=j._lastParsedTransform=j._transform=null,j._onInitTween=function(a,b,h,j){if(!a.nodeType)return!1;this._target=q=a,this._tween=h,this._vars=b,r=j,k=b.autoRound,c=!1,d=b.suffixMap||g.suffixMap,e=Z(a,""),f=this._overwriteProps;var n,p,s,t,u,v,w,x,z,A=a.style;if(l&&""===A.zIndex&&(n=$(a,"zIndex",e),("auto"===n||""===n)&&this._addLazySet(A,"zIndex",0)),"string"==typeof b&&(t=A.cssText,n=ba(a,e),A.cssText=t+";"+b,n=ca(a,n,ba(a)).difs,!T&&y.test(b)&&(n.opacity=parseFloat(RegExp.$1)),b=n,A.cssText=t),b.className?this._firstPT=p=i.className.parse(a,b.className,"className",this,null,null,b):this._firstPT=p=this.parse(a,b,null),this._transformType){for(z=3===this._transformType,Ba?m&&(l=!0,""===A.zIndex&&(w=$(a,"zIndex",e),("auto"===w||""===w)&&this._addLazySet(A,"zIndex",0)),o&&this._addLazySet(A,"WebkitBackfaceVisibility",this._vars.WebkitBackfaceVisibility||(z?"visible":"hidden"))):A.zoom=1,s=p;s&&s._next;)s=s._next;x=new sa(a,"transform",0,0,null,2),this._linkCSSP(x,null,s),x.setRatio=Ba?Ra:Qa,x.data=this._transform||Pa(a,e,!0),x.tween=h,x.pr=-1,f.pop()}if(c){for(;p;){for(v=p._next,s=t;s&&s.pr>p.pr;)s=s._next;(p._prev=s?s._prev:u)?p._prev._next=p:t=p,(p._next=s)?s._prev=p:u=p,p=v}this._firstPT=t}return!0},j.parse=function(a,b,c,f){var g,h,j,l,m,n,o,p,s,t,u=a.style;for(g in b)n=b[g],"function"==typeof n&&(n=n(r,q)),h=i[g],h?c=h.parse(a,n,g,this,c,f,b):(m=$(a,g,e)+"",s="string"==typeof n,"color"===g||"fill"===g||"stroke"===g||-1!==g.indexOf("Color")||s&&A.test(n)?(s||(n=ma(n),n=(n.length>3?"rgba(":"rgb(")+n.join(",")+")"),c=ua(u,g,m,n,!0,"transparent",c,0,f)):s&&J.test(n)?c=ua(u,g,m,n,!0,null,c,0,f):(j=parseFloat(m),o=j||0===j?m.substr((j+"").length):"",(""===m||"auto"===m)&&("width"===g||"height"===g?(j=fa(a,g,e),o="px"):"left"===g||"top"===g?(j=aa(a,g,e),o="px"):(j="opacity"!==g?0:1,o="")),t=s&&"="===n.charAt(1),t?(l=parseInt(n.charAt(0)+"1",10),n=n.substr(2),l*=parseFloat(n),p=n.replace(w,"")):(l=parseFloat(n),p=s?n.replace(w,""):""),""===p&&(p=g in d?d[g]:o),n=l||0===l?(t?l+j:l)+p:b[g],o!==p&&""!==p&&(l||0===l)&&j&&(j=_(a,g,j,o),"%"===p?(j/=_(a,g,100,"%")/100,b.strictUnits!==!0&&(m=j+"%")):"em"===p||"rem"===p||"vw"===p||"vh"===p?j/=_(a,g,1,p):"px"!==p&&(l=_(a,g,l,p),p="px"),t&&(l||0===l)&&(n=l+j+p)),t&&(l+=j),!j&&0!==j||!l&&0!==l?void 0!==u[g]&&(n||n+""!="NaN"&&null!=n)?(c=new sa(u,g,l||j||0,0,c,-1,g,!1,0,m,n),c.xs0="none"!==n||"display"!==g&&-1===g.indexOf("Style")?n:m):V("invalid "+g+" tween value: "+b[g]):(c=new sa(u,g,j,l-j,c,0,g,k!==!1&&("px"===p||"zIndex"===g),0,m,n),c.xs0=p))),f&&c&&!c.plugin&&(c.plugin=f);return c},j.setRatio=function(a){var b,c,d,e=this._firstPT,f=1e-6;if(1!==a||this._tween._time!==this._tween._duration&&0!==this._tween._time)if(a||this._tween._time!==this._tween._duration&&0!==this._tween._time||this._tween._rawPrevTime===-1e-6)for(;e;){if(b=e.c*a+e.s,e.r?b=Math.round(b):f>b&&b>-f&&(b=0),e.type)if(1===e.type)if(d=e.l,2===d)e.t[e.p]=e.xs0+b+e.xs1+e.xn1+e.xs2;else if(3===d)e.t[e.p]=e.xs0+b+e.xs1+e.xn1+e.xs2+e.xn2+e.xs3;else if(4===d)e.t[e.p]=e.xs0+b+e.xs1+e.xn1+e.xs2+e.xn2+e.xs3+e.xn3+e.xs4;else if(5===d)e.t[e.p]=e.xs0+b+e.xs1+e.xn1+e.xs2+e.xn2+e.xs3+e.xn3+e.xs4+e.xn4+e.xs5;else{for(c=e.xs0+b+e.xs1,d=1;d<e.l;d++)c+=e["xn"+d]+e["xs"+(d+1)];e.t[e.p]=c}else-1===e.type?e.t[e.p]=e.xs0:e.setRatio&&e.setRatio(a);else e.t[e.p]=b+e.xs0;e=e._next}else for(;e;)2!==e.type?e.t[e.p]=e.b:e.setRatio(a),e=e._next;else for(;e;){if(2!==e.type)if(e.r&&-1!==e.type)if(b=Math.round(e.s+e.c),e.type){if(1===e.type){for(d=e.l,c=e.xs0+b+e.xs1,d=1;d<e.l;d++)c+=e["xn"+d]+e["xs"+(d+1)];e.t[e.p]=c}}else e.t[e.p]=b+e.xs0;else e.t[e.p]=e.e;else e.setRatio(a);e=e._next}},j._enableTransforms=function(a){this._transform=this._transform||Pa(this._target,e,!0),this._transformType=this._transform.svg&&za||!a&&3!==this._transformType?2:3};var Wa=function(a){this.t[this.p]=this.e,this.data._linkCSSP(this,this._next,null,!0)};j._addLazySet=function(a,b,c){var d=this._firstPT=new sa(a,b,0,0,this._firstPT,2);d.e=c,d.setRatio=Wa,d.data=this},j._linkCSSP=function(a,b,c,d){return a&&(b&&(b._prev=a),a._next&&(a._next._prev=a._prev),a._prev?a._prev._next=a._next:this._firstPT===a&&(this._firstPT=a._next,d=!0),c?c._next=a:d||null!==this._firstPT||(this._firstPT=a),a._next=b,a._prev=c),a},j._mod=function(a){for(var b=this._firstPT;b;)"function"==typeof a[b.p]&&a[b.p]===Math.round&&(b.r=1),b=b._next},j._kill=function(b){var c,d,e,f=b;if(b.autoAlpha||b.alpha){f={};for(d in b)f[d]=b[d];f.opacity=1,f.autoAlpha&&(f.visibility=1)}for(b.className&&(c=this._classNamePT)&&(e=c.xfirst,e&&e._prev?this._linkCSSP(e._prev,c._next,e._prev._prev):e===this._firstPT&&(this._firstPT=c._next),c._next&&this._linkCSSP(c._next,c._next._next,e._prev),this._classNamePT=null),c=this._firstPT;c;)c.plugin&&c.plugin!==d&&c.plugin._kill&&(c.plugin._kill(b),d=c.plugin),c=c._next;return a.prototype._kill.call(this,f)};var Xa=function(a,b,c){var d,e,f,g;if(a.slice)for(e=a.length;--e>-1;)Xa(a[e],b,c);else for(d=a.childNodes,e=d.length;--e>-1;)f=d[e],g=f.type,f.style&&(b.push(ba(f)),c&&c.push(f)),1!==g&&9!==g&&11!==g||!f.childNodes.length||Xa(f,b,c)};return g.cascadeTo=function(a,c,d){var e,f,g,h,i=b.to(a,c,d),j=[i],k=[],l=[],m=[],n=b._internals.reservedProps;for(a=i._targets||i.target,Xa(a,k,m),i.render(c,!0,!0),Xa(a,l),i.render(0,!0,!0),i._enabled(!0),e=m.length;--e>-1;)if(f=ca(m[e],k[e],l[e]),f.firstMPT){f=f.difs;for(g in d)n[g]&&(f[g]=d[g]);h={};for(g in f)h[g]=k[e][g];j.push(b.fromTo(m[e],c,h,f))}return j},a.activate([g]),g},!0)}),_gsScope._gsDefine&&_gsScope._gsQueue.pop()(),function(a){"use strict";var b=function(){return(_gsScope.GreenSockGlobals||_gsScope)[a]};"function"==typeof define&&define.amd?define(["TweenLite"],b):"undefined"!=typeof module&&module.exports&&(require("../TweenLite.js"),module.exports=b())}("CSSPlugin");/*!
 * VERSION: 0.0.2
 * DATE: 2016-07-14
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2016, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 *
 * @author: Jack Doyle, jack@greensock.com
 */
var _gsScope="undefined"!=typeof module&&module.exports&&"undefined"!=typeof global?global:this||window;(_gsScope._gsQueue||(_gsScope._gsQueue=[])).push(function(){"use strict";var a=function(a,b,c){var d=a.type,e=a.setRatio,f=b._tween,g=b._target;a.type=2,a.m=c,a.setRatio=function(b){var h,i,j,k=1e-6;if(1!==b||f._time!==f._duration&&0!==f._time)if(b||f._time!==f._duration&&0!==f._time||f._rawPrevTime===-1e-6)if(h=a.c*b+a.s,a.r?h=Math.round(h):k>h&&h>-k&&(h=0),d)if(1===d){for(i=a.xs0+h+a.xs1,j=1;j<a.l;j++)i+=a["xn"+j]+a["xs"+(j+1)];a.t[a.p]=c(i,g)}else-1===d?a.t[a.p]=c(a.xs0,g):e&&e.call(a,b);else a.t[a.p]=c(h+a.xs0,g);else 2!==d?a.t[a.p]=c(a.b,g):e.call(a,b);else if(2!==d)if(a.r&&-1!==d)if(h=Math.round(a.s+a.c),d){if(1===d){for(i=a.xs0+h+a.xs1,j=1;j<a.l;j++)i+=a["xn"+j]+a["xs"+(j+1)];a.t[a.p]=c(i,g)}}else a.t[a.p]=c(h+a.xs0,g);else a.t[a.p]=c(a.e,g);else e.call(a,b)}},b=function(b,c){for(var d=c._firstPT,e=b.rotation&&-1!==c._overwriteProps.join("").indexOf("bezier");d;)"function"==typeof b[d.p]?a(d,c,b[d.p]):e&&"bezier"===d.n&&-1!==d.plugin._overwriteProps.join("").indexOf("rotation")&&(d.data.mod=b.rotation),d=d._next},c=_gsScope._gsDefine.plugin({propName:"modifiers",version:"0.0.2",API:2,init:function(a,b,c){return this._tween=c,this._vars=b,!0},initAll:function(){for(var a,c,d=this._tween,e=this._vars,f=this,g=d._firstPT;g;)c=g._next,a=e[g.n],g.pg?"css"===g.t._propName?b(e,g.t):g.t!==f&&(a=e[g.t._propName],g.t._mod("object"==typeof a?a:e)):"function"==typeof a&&(2===g.f&&g.t?g.t._applyPT.m=a:(this._add(g.t,g.p,g.s,g.c,a),c&&(c._prev=g._prev),g._prev?g._prev._next=c:d._firstPT===g&&(d._firstPT=c),g._next=g._prev=null,d._propLookup[g.n]=f)),g=c;return!1}}),d=c.prototype;d._add=function(a,b,c,d,e){this._addTween(a,b,c,c+d,b,e),this._overwriteProps.push(b)},d=_gsScope._gsDefine.globals.TweenLite.version.split("."),Number(d[0])<=1&&Number(d[1])<19&&window.console&&console.log("ModifiersPlugin requires GSAP 1.19.0 or later.")}),_gsScope._gsDefine&&_gsScope._gsQueue.pop()();/*!
 * verge 1.9.1+201402130803
 * https://github.com/ryanve/verge
 * MIT License 2013 Ryan Van Etten
 */

(function(root, name, make) {
  if (typeof module != 'undefined' && module['exports']) module['exports'] = make();
  else root[name] = make();
}(this, 'verge', function() {

  var xports = {}
    , win = typeof window != 'undefined' && window
    , doc = typeof document != 'undefined' && document
    , docElem = doc && doc.documentElement
    , matchMedia = win['matchMedia'] || win['msMatchMedia']
    , mq = matchMedia ? function(q) {
        return !!matchMedia.call(win, q).matches;
      } : function() {
        return false;
      }
    , viewportW = xports['viewportW'] = function() {
        var a = docElem['clientWidth'], b = win['innerWidth'];
        return a < b ? b : a;
      }
    , viewportH = xports['viewportH'] = function() {
        var a = docElem['clientHeight'], b = win['innerHeight'];
        return a < b ? b : a;
      };

  /**
   * Test if a media query is active. Like Modernizr.mq
   * @since 1.6.0
   * @return {boolean}
   */
  xports['mq'] = mq;

  /**
   * Normalized matchMedia
   * @since 1.6.0
   * @return {MediaQueryList|Object}
   */
  xports['matchMedia'] = matchMedia ? function() {
    // matchMedia must be binded to window
    return matchMedia.apply(win, arguments);
  } : function() {
    // Gracefully degrade to plain object
    return {};
  };

  /**
   * @since 1.8.0
   * @return {{width:number, height:number}}
   */
  function viewport() {
    return {'width':viewportW(), 'height':viewportH()};
  }
  xports['viewport'] = viewport;

  /**
   * Cross-browser window.scrollX
   * @since 1.0.0
   * @return {number}
   */
  xports['scrollX'] = function() {
    return win.pageXOffset || docElem.scrollLeft;
  };

  /**
   * Cross-browser window.scrollY
   * @since 1.0.0
   * @return {number}
   */
  xports['scrollY'] = function() {
    return win.pageYOffset || docElem.scrollTop;
  };

  /**
   * @param {{top:number, right:number, bottom:number, left:number}} coords
   * @param {number=} cushion adjustment
   * @return {Object}
   */
  function calibrate(coords, cushion) {
    var o = {};
    cushion = +cushion || 0;
    o['width'] = (o['right'] = coords['right'] + cushion) - (o['left'] = coords['left'] - cushion);
    o['height'] = (o['bottom'] = coords['bottom'] + cushion) - (o['top'] = coords['top'] - cushion);
    return o;
  }

  /**
   * Cross-browser element.getBoundingClientRect plus optional cushion.
   * Coords are relative to the top-left corner of the viewport.
   * @since 1.0.0
   * @param {Element|Object} el element or stack (uses first item)
   * @param {number=} cushion +/- pixel adjustment amount
   * @return {Object|boolean}
   */
  function rectangle(el, cushion) {
    el = el && !el.nodeType ? el[0] : el;
    if (!el || 1 !== el.nodeType) return false;
    return calibrate(el.getBoundingClientRect(), cushion);
  }
  xports['rectangle'] = rectangle;

  /**
   * Get the viewport aspect ratio (or the aspect ratio of an object or element)
   * @since 1.7.0
   * @param {(Element|Object)=} o optional object with width/height props or methods
   * @return {number}
   * @link http://w3.org/TR/css3-mediaqueries/#orientation
   */
  function aspect(o) {
    o = null == o ? viewport() : 1 === o.nodeType ? rectangle(o) : o;
    var h = o['height'], w = o['width'];
    h = typeof h == 'function' ? h.call(o) : h;
    w = typeof w == 'function' ? w.call(o) : w;
    return w/h;
  }
  xports['aspect'] = aspect;

  /**
   * Test if an element is in the same x-axis section as the viewport.
   * @since 1.0.0
   * @param {Element|Object} el
   * @param {number=} cushion
   * @return {boolean}
   */
  xports['inX'] = function(el, cushion) {
    var r = rectangle(el, cushion);
    return !!r && r.right >= 0 && r.left <= viewportW();
  };

  /**
   * Test if an element is in the same y-axis section as the viewport.
   * @since 1.0.0
   * @param {Element|Object} el
   * @param {number=} cushion
   * @return {boolean}
   */
  xports['inY'] = function(el, cushion) {
    var r = rectangle(el, cushion);
    return !!r && r.bottom >= 0 && r.top <= viewportH();
  };

  /**
   * Test if an element is in the viewport.
   * @since 1.0.0
   * @param {Element|Object} el
   * @param {number=} cushion
   * @return {boolean}
   */
  xports['inViewport'] = function(el, cushion) {
    // Equiv to `inX(el, cushion) && inY(el, cushion)` but just manually do both
    // to avoid calling rectangle() twice. It gzips just as small like this.
    var r = rectangle(el, cushion);
    return !!r && r.bottom >= 0 && r.right >= 0 && r.top <= viewportH() && r.left <= viewportW();
  };

  return xports;
}));/**
 * Update Class
 * @class
 * @param prefix
 * @param selector
 * @param separator
 * @returns {$}
 */
/*

var ev = '',
    out = [];
for (ev in window) {
    if (/^on/.test(ev)) {
        out = ev;
        window.addEventListener(out.substr(2), function (out) {
            if(console.debug !== undefined){
                console.debug(out);
            }
        });

    }
}
*/


$.fn.trigger = function ( type, data ) {
    return this.each(function() {
        console.info("trigger", type, data, this);
        //console.info("trigger", type);
        jQuery.event.trigger( type, data, this );
    });
};


$.fn.updateClass = function ( prefix, selector, separator) {
    var separator = separator || '-';
    var regx = new RegExp(prefix+separator+'\\S+');
    if($(this).attr('class') == undefined){
        $(this).attr('class','')
    }
    var class_attr = $(this).attr('class');
    if(class_attr != undefined ){
        var classFind = (class_attr.match(regx) || []).join(' ');
        $(this).removeClass(classFind).addClass(prefix+separator+selector);

    }
    return $(this);
};


$.getValue = function ( value ) {
    if(isNaN(value)){
        return eval('gusto_obj = '+value);
    }else{
        return value;
    }
};

$.defineSizes = function(){
    $('[data-size-w]').each(function(){

        var params = $(this).data();
        var refer = params.refer;
        var percent = params.percent;

        var res = eval(refer) / 100 * percent
        $(this).width(res);

    });

    $('[data-size-h]').each(function(){

        var params = $(this).data();
        var refer = params.refer;
        var percent = params.percent;

        var res = eval(refer) / 100 * percent
        $(this).height(res);

    });
}

window.is_load = false;


window.onbeforeunload = function(event) {
    event = event || window.event;
    if (event) {
        $(gusto).trigger('loader::show');
    }
};
/*
window.onload = function() {
    window.is_load = true;
    $(gusto).trigger('loader::hide');
};
*/
if ('onpageshow' in window) {
    window.addEventListener('pageshow', function(event) {
        window.is_load = true;
        $(gusto).trigger('loader::hide');
    });
} else {
    window.onload = function() {
        window.is_load = true;
        $(gusto).trigger('loader::hide');
    };
}
window.onresize = function() {
    clearTimeout(window.resizeEnd_Timer);
    window.resizeEnd_Timer = setTimeout(function() {
        $(gusto).trigger('resize');
    }, 250);
};


$(window).on({
    'scroll' : function (){
        $(gusto).trigger('scroll');
    }
});

setTimeout(function(){
    if(!is_load){
        $(gusto).trigger('loader::hide');
    }
},10000);

/**
 * Plugin - Pushstate
 * @pushstatein
 * @description : Pushstate type plugin
 * @version 1.0
 * @author  William Manco
 * @copyright Gusto IDS
 * @param options
 * @returns {Plugin}
 */
"undefined" == typeof Gusto && (Gusto = {}, gusto = {});
"undefined" == typeof Gusto.Frontend && (Gusto.Frontend = {}, gusto.frontend = {});
"undefined" == typeof Gusto.Frontend.Plugin && (Gusto.Frontend.Plugin = {}, gusto.frontend.plugin = {});
"undefined" == typeof Gusto.Frontend.Plugin.Pushstate && (Gusto.Frontend.Plugin.Pushstate = {}, gusto.frontend.plugin.pushstate = {},gusto.frontend.plugin.pushstate.event = {});


Gusto.Frontend.Plugin.Pushstate = function Gusto(){
    this.opt = {
        selector : '[data-module="plugin"][data-type="pushstate"]',
        element : $('[data-module="plugin"][data-type="pushstate"]'),
        active : false,
        pushstate : false
    };

    this.$ = {

    };

    this._init();
    this._events();
    return this;
};

Gusto.Frontend.Plugin.Pushstate.prototype = {
    _init : function(){
        var $this = this;

        $this.opt.element.off();

        if($this.opt.element.length == 0){
            $this.opt.element = $('body');
        }

        $this.on_popstate();

    },
    _events : function(){
        var $this = this;

        $(gusto.frontend.plugin.pushstate.event).on({
            'plugin::pushstate::set_storage' : function (e,args) {
                $this.set_storage(args);
                //$(gusto).off('plugin::pushstate::set_storage');
            },
            'plugin::pushstate::get_storage' : function () {
                return $this.get_storage();
                //$(gusto).off('plugin::pushstate::get_storage');
            },
            'plugin::pushstate::on_click' : function (e,args) {
                $this.on_click(args.targetUrl,args.targetTitle,args);
            },
            'plugin::pushstate::change' : function(){

            }
        });

        $this.opt.element.each(function(){
                $(this).find('a').not('a[data-url]').each(function(){
                    $(this).off('click').on({
                        'click' : function(e){
                            if($this.opt.active) {
                                e.preventDefault();
                            }

                            if( $(this).attr('href') == location.href){
                                return false;
                            }

                            var targetUrl = $(this).attr('href');
                            var targetTitle = '';

                            var args = {};

                            if($(this).data()){
                                args = $(this).data();
                            }

                            $this.on_click(targetUrl,targetTitle,args);

                        }
                    });
                });
                $(this).find('[data-url]').off('click').on({
                    'click' : function(e){
                        if($this.opt.active) {
                            e.preventDefault();
                        }
                        if( $(this).attr('data-url') == location.href){
                            return false;
                        }
                        var targetUrl = $(this).attr('data-url'),
                            targetTitle = $(this).attr('data-title');

                        var args = {};

                        if($(this).data()){
                            args = $(this).data();
                        }

                        $this.on_click(targetUrl,targetTitle,args);
                        if(!$this.opt.active) {
                            location.href = targetUrl;
                        }
                    }
                });
        });
    },

    on_click : function(targetUrl,targetTitle,args){
        var $this = this;
        var args = args;

        args.targetUrl = targetUrl;
        args.targetTitle = targetTitle;
        $this.set_storage(args);
        $this.go_ajax(args);
        if($this.opt.active) {
            window.history.pushState(args, targetTitle, targetUrl);
        }
        $(gusto.frontend.plugin.pushstate.event).trigger('plugin::pushstate::click',args);
        $(gusto.frontend.plugin.pushstate.event).trigger('plugin::pushstate::change', args);

    },

    on_popstate : function(){
        var $this = this;
        if($this.opt.active){
            window.onpopstate = function(e) {
                console.log(e);
                $this.set_storage(e.state);
                $this.go_ajax(e.state);
                $(gusto.frontend.plugin.pushstate.event).trigger('plugin::pushstate::popstate', e);
                $(gusto.frontend.plugin.pushstate.event).trigger('plugin::pushstate::change', e);
            };
        }
    },

    go_ajax : function(){
        var $this = this;

        var args = $this.get_storage();

        var parser_targetUrl = document.createElement('a');
        var parser_targetUrl_prev = document.createElement('a');
        parser_targetUrl.href = args[0].targetUrl;
        parser_targetUrl_prev.href = args[1].targetUrl;

        if(args[0].img){

            $('body').data('plugin::pushstate',args[0]);

            $(gusto.frontend.plugin.pushstate.event).trigger('plugin::pushstate::before-imgload');

            $.imgpreload(args[0].img,function()
            {
                $('body').data('plugin::pushstate',args[0]);
                $(gusto.frontend.plugin.pushstate.event).trigger('plugin::pushstate::success-imgload');
            });

            return;
        }

        if(parser_targetUrl.pathname == parser_targetUrl_prev.pathname){
                return;
        }


        /*
        if(JSON.parse(gusto.storage.getItem('plugin::pushstate::'+parser_targetUrl)) != null){
                var data = JSON.parse(gusto.storage.getItem('plugin::pushstate::'+parser_targetUrl));
                var $current_module = $(data).find('#main [data-module]').first();
                args[0].ajax_state = 'beforeSend';
                $(gusto.frontend.plugin.pushstate.event).trigger('plugin::pushstate::before-ajax');
                args[0].ajax_data_full = data;
                args[0].ajax_data = $(data).find('#main');
                args[0].ajax_state = 'success';
                args[0].module = $current_module.attr('data-module');
                args[0].type = $current_module.attr('data-type');
                args[0].id = $current_module.attr('data-id');
                $('body').data('plugin::pushstate',args[0]);
                $(gusto.frontend.plugin.pushstate.event).trigger('plugin::pushstate::success-ajax');
                return;
            }
*/
        if(!$this.opt.active) {
            return;
        }

        $.ajax({
            type: 'POST',
            url: args[0].targetUrl,
            beforeSend:function(){
                args[0].ajax_state = 'beforeSend';
                $('body').data('plugin::pushstate',args[0]);
                $(gusto.frontend.plugin.pushstate.event).trigger('plugin::pushstate::before-ajax');

            },
            success:function(data){
                var $current_module = $(data).find('#main [data-module]').first();
                args[0].ajax_data_full = data;
                args[0].ajax_data = $(data).find('#main');
                args[0].module = $current_module.attr('data-module');
                args[0].type = $current_module.attr('data-type');
                args[0].id = $current_module.attr('data-id');
                args[0].ajax_state = 'success';
                $('body').data('plugin::pushstate',args[0]);
                $(gusto.frontend.plugin.pushstate.event).trigger('plugin::pushstate::success-ajax');
               // gusto.storage.setItem("plugin::pushstate::"+args[0].targetUrl, JSON.stringify(args[0].ajax_data_full));
            },
            error:function(){
                args[0].ajax_state = 'error';
                $('body').data('plugin::pushstate',args[0]);
                $(gusto.frontend.plugin.pushstate.event).trigger('plugin::pushstate::error-ajax');
            }
        });
    },

    set_storage : function(data){
        var $this = this;
        var prev_storage = $this.get_storage()[0];
        if(!$this.get_storage()[0]){
            prev_storage = data;

      }

        if(data == null){
            return;
        }


        gusto.storage.setItem("plugin::pushstate::prev", JSON.stringify(prev_storage));
        gusto.storage.setItem("plugin::pushstate", JSON.stringify(data));

        $(gusto.frontend.plugin.pushstate.event).trigger('>>plugin::pushstate::set_storage');

        $('body').data('plugin::pushstate',data);

    },

    get_storage : function(){
        var $this = this;
        var args = [];
        if(gusto.storage.getItem('plugin::pushstate') !== undefined) {
            args[0] = JSON.parse(gusto.storage.getItem('plugin::pushstate'));
        }
        if(gusto.storage.getItem('plugin::pushstate::prev') !== undefined) {
            args[1] = JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'));
        }
        return args;
    }
};/**
 * Plugin - Update_dom_ajax
 * @update_dom_ajax
 * @description : Update_dom_ajax type plugin
 * @version 1.0
 * @author  William Manco
 * @copyright Gusto IDS
 * @param options
 * @returns {Plugin}
 */
"undefined" == typeof Gusto && (Gusto = {}, gusto = {});
"undefined" == typeof Gusto.Frontend && (Gusto.Frontend = {}, gusto.frontend = {});
"undefined" == typeof Gusto.Frontend.Plugin && (Gusto.Frontend.Plugin = {}, gusto.frontend.plugin = {});
"undefined" == typeof Gusto.Frontend.Plugin.Update_dom_ajax && (Gusto.Frontend.Plugin.Update_dom_ajax = {}, gusto.frontend.plugin.update_dom_ajax = {});


Gusto.Frontend.Plugin.Update_dom_ajax = function Gusto(){
    this.opt = {
        selector : '[data-module="plugin"][data-type="update_dom_ajax"]',
        element : $('[data-module="plugin"][data-type="update_dom_ajax"]')
    };

    this.$ = {
        main : $('#main')
    };

    this._init();
    this._events();
    return this;
};

Gusto.Frontend.Plugin.Update_dom_ajax.prototype = {
    _init : function(){
        var $this = this;

    },
    _events : function(){
        var $this = this;

        $(gusto).on({
            'plugin::update_dom_ajax::paste' : function(){
                $this.paste();
            },
            'plugin::update_dom_ajax::update' : function(){
                $this.update();
            }
        });

    },

    paste : function () {
        var $this = this;
        var $ajax_data = $('body').data('plugin::pushstate').ajax_data;

        $ajax_data.children().css({
            position : 'absolute',
            top : 0,
            left : 0,
            width : $(window).width(),
            height : $(window).height(),
            visibility : 'hidden'
        });

        $this.$.main.append($ajax_data.html());
    },

    update : function(){
        var $this = this;
        var $second = $this.$.main.children().first().next();
        if($second.length > 0){
            $this.$.main.children().first().remove();
            $this.$.main.css({
                overflow : 'inherit',
                height : 'auto'
            });
            $second.css({
                position : 'static',
                width : 'auto',
                height : 'auto',
                visibility : 'visible'
            });
        }
    }
};
/**
 * Plugin - Utility_module
 * @utility_modulein
 * @description : Utility_module type plugin
 * @version 1.0
 * @author  William Manco
 * @copyright Gusto IDS
 * @param options
 * @returns {Plugin}
 */
"undefined" == typeof Gusto && (Gusto = {}, gusto = {});
"undefined" == typeof Gusto.Frontend && (Gusto.Frontend = {}, gusto.frontend = {});
"undefined" == typeof Gusto.Frontend.Plugin && (Gusto.Frontend.Plugin = {}, gusto.frontend.plugin = {});
"undefined" == typeof Gusto.Frontend.Plugin.Utility_module && (Gusto.Frontend.Plugin.Utility_module = {}, gusto.frontend.plugin.utility_module = {});


Gusto.Frontend.Plugin.Utility_module = function Gusto(){
    this.opt = {
        selector : '[data-module="plugin"][data-type="utility_module"]',
        element : $('[data-module="plugin"][data-type="utility_module"]')
    };

    this.$ = {
        main : $('#main')
    };

    this._init();
    this._events();
    return this;
};

Gusto.Frontend.Plugin.Utility_module.prototype = {
    _init : function(){
        var $this = this;

    },

    _events : function(){
        var $this = this;

        $(gusto).on({
            'plugin::utility_module::reinit_events' : function(){
                return $this.reinit_events();
            },
            'plugin::utility_module::reinit_image_preload' : function(){
                return $this.reinit_image_preload();
            },
            'plugin::utility_module::reinit_pushstate' : function(){
                return $this.reinit_pushstate();
            },
            'plugin::utility_module::get_id' : function(e,args){
                return $this.get_id(args);
            },
            'plugin::utility_module::set_this' : function(e,args){
                return $this.set_this(args);
            },
            'plugin::utility_module::istance_this' : function(e,args){
                return $this.istance_this(args);
            },
            'plugin::utility_module::page_next_init' : function(e,args){
                return $this.page_next_init(args);
            }
        });

    },

    reinit_events : function(){
        $(gusto).trigger('>>reinit_events');
        $(gusto).off('::enter');
        $(gusto).off('::leave');
    },

    reinit_pushstate : function(){
        $(gusto).trigger('>>reinit_pushstate');
        $(gusto.frontend.plugin.pushstate.event).off();
        var id = Object.keys(gusto.frontend.plugin.pushstate)[0];
        try { gusto.frontend.plugin.pushstate[id] = new Gusto.Frontend.Plugin.Pushstate(id);}catch(err) { console.warn(err);}
    },

    reinit_image_preload : function(){
        $(gusto).trigger('>>reinit_image_preload');
        $(gusto.frontend.image_preload.standard.event).off();
        var id = Object.keys(gusto.frontend.image_preload.standard)[0];
        try {
            gusto.frontend.image_preload.standard[id] = null;
            gusto.frontend.image_preload.standard[id] = new Gusto.Frontend.Image_preload.Standard(id);

        }catch(err) { console.warn(err);}
    },

    get_id : function (args) {
        var $this = this;
        var module = eval(args.module_string);
        var $ajax_data = $('body').data('plugin::pushstate').ajax_data;
        var id_next_module = Object.keys(module)[0];
        if(!$.isNumeric(id_next_module)){
            var id = args.id;

            if(id === undefined){
                var d = new Date();
                var n = d.getTime();
                id = Math.floor((Math.random() * 10000) + 1) + Math.floor((Math.random() * 10000) + 1) + n;
            }

            $ajax_data.find('[data-module]').attr('data-id',id+1);
            id_next_module = id+1;
        }else{
            $this.opt.reinit_module = true;
        }

        $('body').data('plugin::utility_module::get_id',id_next_module)
    },

    set_this : function () {
        var $this = this;
        var pushstate = $('body').data('plugin::pushstate');
        var $ajax_data = pushstate.ajax_data;
        var module = pushstate.module;
        var type = pushstate.type;
        var id = pushstate.id;

        /**
         * Replace ID with time
         * @type {Date}
         */
        var d = new Date();
        var millisecond = d.getTime();
        $('body').data('plugin::pushstate').ajax_data = $ajax_data.find('[data-page]').attr('data-id',millisecond).closest('#main');
        $('body').data('plugin::pushstate').id = millisecond;
    },

    reinit_module : function(){

        function capitalize(string) {
            return string.replace(/^./, capitalize.call.bind("".toUpperCase));
        }

        $.each(gusto.frontend, function(module_name, module) {
            $.each(module, function(type_name, type) {

                eval('try { gusto.frontend.'+module_name+'.'+type_name+'[\'0\'] = new Gusto.Frontend.'+capitalize(module_name)+'.'+capitalize(type_name)+'(0);}catch(err) { console.warn(err);}');

            });
        });
    },

    istance_this : function () {
        var $this = this;

        var pushstate = $('body').data('plugin::pushstate');
        var $ajax_data = pushstate.ajax_data;
        var module = pushstate.module;
        var type = pushstate.type;
        var id = pushstate.id;

        function capitalize(string) {
            return string.replace(/^./, capitalize.call.bind("".toUpperCase));
        }

        if($this.opt.reinit_module){
            args.objectName[0] = {};
        }

        eval('try { gusto.frontend.'+module+'.'+type+'["'+id+'"] = new Gusto.Frontend.'+capitalize(module)+'.'+capitalize(type)+'('+id+');}catch(err) { console.warn(err);}');
    },

    page_next_init : function(){
        var $this = this;


    }
};
/**
 * Plugin - Longpress
 * @longpressin
 * @description : Longpress type plugin
 * @version 1.0
 * @author  William Manco
 * @copyright Gusto IDS
 * @param options
 * @returns {Plugin}
 */
"undefined" == typeof Gusto && (Gusto = {}, gusto = {});
"undefined" == typeof Gusto.Frontend && (Gusto.Frontend = {}, gusto.frontend = {});
"undefined" == typeof Gusto.Frontend.Plugin && (Gusto.Frontend.Plugin = {}, gusto.frontend.plugin = {});
"undefined" == typeof Gusto.Frontend.Plugin.Longpress && (Gusto.Frontend.Plugin.Longpress = {}, gusto.frontend.plugin.longpress = {});


Gusto.Frontend.Plugin.Longpress = function Gusto(){
    this.opt = {
        selector : '[data-module="plugin"][data-type="longpress"]',
        element : $('[data-module="plugin"][data-type="longpress"]'),
        pressTimer : {},
        startTime : 200,
        holdTime : 1000
    };

    this.$ = {
        main : $('#main')
    };

    this._init();
    this._events();
    return this;
};

Gusto.Frontend.Plugin.Longpress.prototype = {
    _init : function(){
        var $this = this;

        if($this.element === undefined){
            $this.element = $(window);
        }
        if($this.element.data('hold-time') !== undefined){
            $this.opt.holdTime = $this.element.data('hold-time');
        }

    },
    _events : function(){
        var $this = this;

        $this.element.off('mousedown');
        $this.element.off('mouseup');

        $this.element.on({
            'mouseup' : function(){
                clearTimeout($this.opt.pressTimer);
                clearTimeout($this.opt.pressStartTimer);
                if($this.opt.longpress){
                    $(gusto).trigger('plugin::longpress::clear');
                    $this.opt.longpress = false;
                }
            },
            'mousedown' : function(event){
                if(event.which == 1){
                    $this.opt.pressStartTimer = window.setTimeout(function() {
                        $this.opt.longpress = true;
                        $(gusto).trigger('plugin::longpress::click',event);
                        $(gusto).trigger('plugin::longpress::start');
                    },$this.opt.startTime);

                    $this.opt.pressTimer = window.setTimeout(function() {
                        clearTimeout($this.opt.pressStartTimer);
                        $(gusto).trigger('plugin::longpress::end');
                    },$this.opt.holdTime);
                }
            }
        });

    }

};


/**
 * Plugin - Delay_next_page
 * @delay_next_pagein
 * @description : Delay_next_page type plugin
 * @version 1.0
 * @author  William Manco
 * @copyright Gusto IDS
 * @param options
 * @returns {Plugin}
 */
"undefined" == typeof Gusto && (Gusto = {}, gusto = {});
"undefined" == typeof Gusto.Frontend && (Gusto.Frontend = {}, gusto.frontend = {});
"undefined" == typeof Gusto.Frontend.Plugin && (Gusto.Frontend.Plugin = {}, gusto.frontend.plugin = {});
"undefined" == typeof Gusto.Frontend.Plugin.Delay_next_page && (Gusto.Frontend.Plugin.Delay_next_page = {}, gusto.frontend.plugin.delay_next_page = {});


Gusto.Frontend.Plugin.Delay_next_page = function Gusto(){
    this.opt = {
        selector : '[data-module="plugin"][data-type="delay_next_page"]',
        element : $('[data-module="plugin"][data-type="delay_next_page"]'),
        active : true
    };

    this.$ = {
        main : $('body')
    };


    if(this.opt.active){
        this._init();
        this._events();
    }

    return this;
};

Gusto.Frontend.Plugin.Delay_next_page.prototype = {
    _init : function(){
        var $this = this;

    },
    _events : function(){
        var $this = this;

        if($this.opt.element.find('a').length > 0){
            $base = $this.opt.element;
        }else{
            $base = $this.$.main;
        }


        $base.find('a:not([target="_blank"])').not('a[data-url]').on({
            'click' : function(e){

                console.log(e);

                e.preventDefault();
                $this.opt.click = true;
                $this.opt.currentHref = $(this).attr('href');


                if($this.opt.currentHref == location.href){
                    return false;
                }

                $(gusto).trigger('::leave',{
                    isClick : true
                });

                $('body').find('a').off("click");
                $('body').find('a').on('click', function(){
                    return false;
                });
            }
        });

        $(gusto).on({
            'plugin::set_page::next' : function(e,args){

                if($this.opt.click){
                    window.location.href = $this.opt.currentHref;
                    $this.opt.click = false;
                }
            }
        });

    }
};
/**
 * Nav - Full_page
 * @mixin
 * @description : Fullpage type nav
 * @version 1.0
 * @author  Nicolò Pierotti
 * @copyright Gusto IDS
 * @param options
 * @returns {Nav}
 */
"undefined" == typeof Gusto && (Gusto = {}, gusto = {});
"undefined" == typeof Gusto.Frontend && (Gusto.Frontend = {}, gusto.frontend = {});
"undefined" == typeof Gusto.Frontend.Header && (Gusto.Frontend.Header = {}, gusto.frontend.header = {});
"undefined" == typeof Gusto.Frontend.Header.Standard && (Gusto.Frontend.Header.Standard = {}, gusto.frontend.header.standard = {});

Gusto.Frontend.Header.Standard = function Gusto(){
    this.opt = {
        selector : $('[data-module="header"][data-type="standard"]'),
        element : $('[data-module="header"][data-type="standard"]'),
        scrollable_selector : $('[data-module="header"][data-type="standard"][data-scrollable="true"]'),
        scrolling : $(window),
        inner_scrolling : $(document),
        didScroll:true,
        lastScrollTop:0,
        delta:5,
        navbarHeight:$('[data-module="header"][data-type="standard"]').outerHeight(),
        interval:0,
        timeinterval:250,
        force_all_white : true
    };

    this.$ = {
        logo : this.opt.element.find('.logo'),
        logoItem : this.opt.element.find('.logo div'),
        linkMenu : this.opt.element.find('.link-menu'),
        linkClose : this.opt.element.find('.link-close'),
        linkBack : this.opt.element.find('.link-back')
    };

    this._init();
    this._events();

    return this;
};

Gusto.Frontend.Header.Standard.prototype = {

    _init : function(){
        var $this = this;

        if($this.opt.scrollable_selector.length == 1) {

           /* $(window).scroll(function (event) {
                $(gusto).trigger('header::scroll');
            });

            $this.opt.inteval = setInterval(function () {
                if ($this.opt.didScroll) {
                    $this.hasScrolled();
                    $this.opt.didScroll = false;
                }
            }, $this.opt.timeinterval);
            */
        }

        if($this.$.linkBack.find('.blast').length <= 0){
            $this.$.linkBack.blast({
                delimiter: "character",
                returnGenerated: false,
                aria: false
            });
        }

        if($this.$.linkMenu.find('.blast').length <= 0){
            $this.$.linkMenu.blast({
                delimiter: "character",
                returnGenerated: false,
                aria: false
            });
        }

    },

    _events : function(){
        var $this = this;

        $(gusto).on({
            'page::load' : function(){
                $this.enter_module();
                $this.change_dimension();
            },
            'header::standard::change_dimension': function(event,args){
                $this.change_dimension(args);
            },
            'header::show' : function(){
            },
            'header::hide' : function(){
            },
            'header::scroll' : function(){
                $this.opt.didScroll = true;
            },
            '::leave' : function(e, args){
                if(args !== undefined){
                    if(args.isClick !== undefined){
                        $this.opt.nextPage = true;
                        $this.leave_module();
                    }
                }
            }
        });

        $(gusto.frontend.plugin.pushstate.event).on({
            '>>plugin::pushstate::set_storage': function(){
                var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));
                var pushstatePrevArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'));
/*
                if(pushstateArgs.page != 'home'){
                   // $this.opt.element.updateClass('is','back');
                }else{
                    $this.opt.element.updateClass('is','first');
                }
                */
            }
        });

    },

    hasScrolled : function(){

         var $this = this;
         var st = $this.opt.scrolling.scrollTop();

         // Make sure they scroll more than delta
         if(Math.abs($this.opt.lastScrollTop - st) <= $this.opt.delta)
         return;

         // If they scrolled down and are past the navbar, add class .nav-up.
         // This is necessary so you never see what is "behind" the navbar.
         if (st > $this.opt.lastScrollTop && st > $this.opt.navbarHeight){
         // Scroll Down
             $this.opt.scrollable_selector.removeClass('nav-down').addClass('nav-up');
         } else {
         // Scroll Up
         if(st + $this.opt.scrolling.height() < $this.opt.inner_scrolling.height()) {
             $this.opt.scrollable_selector.removeClass('nav-up').addClass('nav-down');
         }
         }

        $this.opt.lastScrollTop = st;
    },

    enter_module : function(){
        var $this = this;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));

        if(Modernizr.smallscreen || $this.opt.force_all_white){
            $this.tween_enter();
            return;
        }

        switch(pushstateArgs.page){
            default :
                $this.tween_enter();
                break;
        }
    },

    leave_module : function(){
        var $this = this;

        if($this.opt.force_all_white) {
            $this.tween_leave();
        }
    },

    tween_enter : function(){
        var $this = this;

        $this.opt.timelineTweenPageEnter = new TimelineLite();
        $this.opt.timelineTweenPageEnter
            .to($this.$.logoItem,1.3,{ x:'0%', ease:Power4.easeInOut},0)
        ;

        if(!$this.opt.nextPage){
            $this.opt.timelineTweenPageEnter.staggerTo($this.opt.element.find('.blast'),1,{ opacity:1, ease:Power4.easeInOut},.08,0)
        }
    },

    tween_leave : function(){
        var $this = this;

        $this.opt.timelineTweenPageLeave = new TimelineLite();
        $this.opt.timelineTweenPageLeave
            .to($this.$.logoItem,1.3,{ x:'-100%', ease:Power4.easeInOut},0)
            .staggerTo($this.opt.element.find('.blast'),1,{ opacity:0, ease:Power4.easeInOut},-.08,0)

        ;
    },

    change_dimension : function(args){
        var $this = this;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));
        var pushstatePrevArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'));

        if(args !== undefined) {
            if (args.forced !== undefined) {
                $this.tween_change_dimension({
                    dimension: args.dimension,
                    colorLogo: args.colorLogo,
                    isClose: args.isClose,
                    nextPage : args.nextPage
                });
                return false;
            }
        }


        if($('body').hasClass('home')){
            $this.tween_change_dimension({
                dimension : '50%',
                colorLogo : '#000'
            });
        }else{
            $this.tween_change_dimension({
                dimension : '100%',
                colorLogo : '#000'
            });
        };

        /*
        switch(pushstateArgs.page){
            case 'home' :
                $this.tween_change_dimension({
                    dimension : '50%',
                    colorLogo : '#000'
                });
                break;
            default :
                $this.tween_change_dimension({
                    dimension : '100%',
                    colorLogo : '#000'
                });
                break;
        }
        */

    },

    tween_change_dimension : function(args) {
        var $this = this;

        if(args.nextPage){
            return;
        }

        $this.opt.timelineTweenPageEnter = new TimelineLite();

        $this.opt.timelineTweenPageEnter
            .to($this.$.linkMenu,.4,{ opacity:1, ease:Power4.easeInOut},0)
            .to($this.opt.element,0,{ width: args.dimension })
            .to($this.$.logoItem,.5,{ css:{color:  args.colorLogo }, ease:Power4.easeInOut},.5)
        ;




    }

};/**
 * Loader - Standard
 * @mixin
 * @description : Standard type loader
 * @version 1.0
 * @author  William Manco
 * @copyright Gusto IDS
 */
"undefined" == typeof Gusto && (Gusto = {}, gusto = {});
"undefined" == typeof Gusto.Frontend && (Gusto.Frontend = {}, gusto.frontend = {});
"undefined" == typeof Gusto.Frontend.Cookies && (Gusto.Frontend.Cookies = {}, gusto.frontend.cookies = {});
"undefined" == typeof Gusto.Frontend.Cookies.Standard && (Gusto.Frontend.Cookies.Standard = {}, gusto.frontend.cookies.standard = {});

Gusto.Frontend.Cookies.Standard = function Gusto(){
    this.opt = {
        module_selector : '[data-module="cookies"][data-type="standard"]',
        module_target : 'body',
        module_name : window.cookies_adv_name,
        module_valid : '30',
        module_content : 'service accepted'
    };
    this._init();
    this._events();

    this._create();

    return this;
};

Gusto.Frontend.Cookies.Standard.prototype = {

    _init : function(){
        var $this = this;
    },
    _events : function(){
        var $this = this;

       $(gusto).on({
            'cookies::show' : function(){
            },
            'cookies::hide' : function(){
            },
            'cookies::close' : function(){
            },
            'cookies::click' : function(){

                $this._set_cookie($this.opt.module_name, $this.opt.module_content, $this.opt.module_valid);
                console.log([$this.opt.module_name, $this.opt.module_valid, $this.opt.module_content]);
                $($this.opt.module_selector).slideUp(function(){
                    $(this).remove();
                });
            }
        });
    },
    _set_cookie : function(name, value, days){
            var expires;

            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
            } else {
                expires = "";
            }
            document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
    },
    _exist_cookie : function(name) {
        var nameEQ = escape(name) + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
        }
            return false;
    },
    _create : function() {
        var $this = this;
        if(this._exist_cookie($this.opt.module_name)){
            $($this.opt.module_selector).remove();
        }

        $($this.opt.module_selector).slideDown();
        $($this.opt.module_selector).find('#cookies_btn').click(function(e){
            e.preventDefault();
            $(gusto).trigger('cookies::click');
        });
    }

};

/**
 * Loader - Standard
 * @mixin
 * @description : Standard type loader
 * @version 1.0
 * @author  William Manco
 * @copyright Gusto IDS
 */
"undefined" == typeof Gusto && (Gusto = {}, gusto = {});
"undefined" == typeof Gusto.Frontend && (Gusto.Frontend = {}, gusto.frontend = {});
"undefined" == typeof Gusto.Frontend.Browserhappy && (Gusto.Frontend.Browserhappy = {}, gusto.frontend.browserhappy = {});
"undefined" == typeof Gusto.Frontend.Browserhappy.Standard && (Gusto.Frontend.Browserhappy.Standard = {}, gusto.frontend.browserhappy.standard = {});

Gusto.Frontend.Browserhappy.Standard = function Gusto(){
    this.opt = {
        module_selector : '[data-module="browserhappy"][data-type="standard"]',
        module_target : 'body',
        browsers : new Array({name:'chrome', index:35},{name:'safari', index:7},{name:'firefox', index:35},{name:'ie', index:9})
    };
    this._init();


    return this;
};

Gusto.Frontend.Browserhappy.Standard.prototype = {

    _init : function(){
        var $this = this;
        $('[data-module="browserhappy"] .browserhappy_close').click(function(e){ e.preventDefault(); $('[data-module="browserhappy"]').remove(); });


        var browsers = $this.opt.browsers;
        var evalStr = '';
        for(var $i = 0; $i < browsers.length; $i++){
            if($('html').hasClass(browsers[$i].name)){
                var bIndex = browsers[$i].index;
                for(var $c = 0; $c <= bIndex; $c++){
                    if($c < 1){
                        evalStr = evalStr + '$("html.desktop").hasClass("'+browsers[$i].name+$c+'")';
                    }else{
                        evalStr = evalStr + ' || $("html.desktop").hasClass("'+browsers[$i].name+$c+'")';
                    }
                }

            }
        }
        if(eval(evalStr)){
            $('[data-module="browserhappy"]').css({display:'block'});
        }else{
        }
    }

};

/**
 * Matrix - Alternate
 * @alternatein
 * @description : Alternate type matrix
 * @version 1.0
 * @author  William Manco
 * @copyright Gusto IDS
 * @param options
 * @returns {Matrix}
 */
"undefined" == typeof Gusto && (Gusto = {}, gusto = {});
"undefined" == typeof Gusto.Frontend && (Gusto.Frontend = {}, gusto.frontend = {});
"undefined" == typeof Gusto.Frontend.Matrix && (Gusto.Frontend.Matrix = {}, gusto.frontend.matrix = {});
"undefined" == typeof Gusto.Frontend.Matrix.Alternate && (Gusto.Frontend.Matrix.Alternate = {}, gusto.frontend.matrix.alternate = {});


Gusto.Frontend.Matrix.Alternate = function Gusto(id){
    this.opt = {
        selector_id : "#module-slider-swiper-"+id,
        selector: "[data-module='matrix'][data-type='alternate'][data-id='"+id+"']",
        element : $('[data-module="matrix"][data-type="alternate"][data-id="'+id+'"]'),
        module : 'matrix',
        type : 'alternate',
        title_height : parseInt($('[data-module="matrix"][data-type="alternate"][data-id="'+id+'"]').find('.title').height()) + parseInt($('[data-module="matrix"][data-type="alternate"][data-id="'+id+'"]').find('.title').css('padding-top')),
        topbottom_height : parseInt($('[data-module="matrix"][data-type="alternate"][data-id="'+id+'"]').find('.overlay .top').height())+1,
        line_height : parseInt($('[data-module="matrix"][data-type="alternate"][data-id="'+id+'"]').find('.line').height()),
        force_all_white : true
    };

    this.$ = {
        rowLink : this.opt.element.find('.row a'),
        rowLinkSecond : this.opt.element.find('.row a').eq(1),
        containerTitle :  this.opt.element.find('.container-title'),
        containerTitleSecond :  this.opt.element.find('.container-title').eq(1),
        title :  this.opt.element.find('.container-title .title'),
        preTitle :  this.opt.element.find('.container-title .pre-title'),
        containerLine :  this.opt.element.find('.line'),
        overlayChildren :  this.opt.element.find('.overlay').children(),
        overlayTop :  this.opt.element.find('.overlay .top'),
        overlayBottom :  this.opt.element.find('.overlay .bottom')
    };

    this._init();
    this._events();

    return this;
};

Gusto.Frontend.Matrix.Alternate.prototype = {

    _init : function(){
        var $this = this;
        $this.opt.leave_module = true;
        $this.init_module();
    },

    _events : function(){
        var $this = this;

        $(gusto.frontend.image_preload.standard.event).on({
            'image_preload::all' : function(){
                if($this.opt.pageTitleEnterComplete) {
                    $(gusto).trigger('::enter');
                    $(gusto).trigger('page::load');
                }
                $this.opt.preloadImageComplete = true;
            }
        });

        $(gusto.frontend.plugin.pushstate.event).on({
            'plugin::pushstate::before-ajax' : function(){
                $this.leave_module();
            }
        });

        $(gusto).on({
            '::enter' : function(){
                $this.enter_module();
                $('.overlay.fullpage').css('display','none');
            },
            '::leave' : function(){
                $this.leave_module();
            },
            'scroll' : function(){
                $this.tween_enter();
            }
        });

        $($this.$.rowLink).on({
            'mouseenter' : function(){
                if($this.opt.pageTitleEnterComplete){
                    $this.on_hover_link($(this));
                }
            },
            'mouseleave' : function(){
                if($this.opt.pageTitleEnterComplete) {
                    $this.on_leave_link($(this));
                }
            }
        });

        $(document).ready(function(){
            $this.enter_page_title();
        });

    },

    on_hover_link : function($el){
        var $this = this;

        $this.opt.topbottom_height = parseInt($el.find('.overlay .top').height())+1;

        $el.addClass('open');

        var title_height = $this.opt.title_height;
        var topbottom_height = $this.opt.topbottom_height;
        var line_height = $this.opt.line_height;

        $this.opt.timelineTweenHoverEnter = new TimelineLite();
        $this.opt.timelineTweenHoverEnter
            .to($el.find('.overlay .top'),.5,{ y: -topbottom_height, ease:Power3.easeInOut},0)
            .to($el.find('.overlay .bottom'),.5,{ y: topbottom_height,  ease:Power2.easeInOut},0)
            .to($el.find('.pre-title'),.6,{ y: '-50px', ease:Power2.easeInOut},0)
            .to($el.find('.title span'),.4,{ y: -title_height, ease:Power1.easeInOut},0)
            .to($el.find('.line'),.8,{ y: '20px',height: line_height-20, ease:Power2.easeInOut},0)
            .to($el.find('.view'),.8,{ y: '20px', ease:Power2.easeInOut},0)
            .to($el.find('.view-text'),.8,{ y: '0%', ease:Power2.easeInOut},0)
        ;
    },

    on_leave_link : function($el){
        var $this = this;

        var line_height = $this.opt.line_height;

        $el.removeClass('open');

        $this.opt.timelineTweenHoverLeave = new TimelineLite();
        $this.opt.timelineTweenHoverLeave
            .to($el.find('.overlay .top'),.5,{ y: 0, ease:Power3.easeInOut},0)
            .to($el.find('.overlay .bottom'),.5,{ y: 0,  ease:Power3.easeInOut},0)
            .to($el.find('.pre-title'),.7,{ y: '0px', ease:Power3.easeInOut},0)
            .to($el.find('.title span'),.7,{ y: '0px', ease:Power4.easeInOut},0)
            .to($el.find('.line'),.4,{ y: '0px', height: line_height, ease:Power4.easeIn},0)
            .to($el.find('.view'),.4,{ y: '0px', ease:Power4.easeIn},0)
            .to($el.find('.view-text'),.6,{ y: '-100%', ease:Power4.easeInOut},0)
        ;
    },

    init_line : function(){
        var $this = this;

        for(var i = 0; i < 51; i++){
            $this.$.containerLine.append('<div class="line-item line-'+i+'"></div>');
        }

        $this.$.lineItem = $this.$.containerLine.children();
        $this.$.lineItem.height(1);
    },

    shuffle : function(array) {
        return array.sort(function() { return 0.5 - Math.random() });
    },

    /**
     * Enter/Leave
     */

    init_module : function(){
        var $this = this;
        var args  = {};

        $(gusto.frontend.image_preload.standard.event).trigger('image_preload::init');

        if(!JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'))){
            args.targetUrl = $this.opt.element.attr('data-url');
            args.page = $this.opt.element.attr('data-page');
            $(gusto).trigger('plugin::pushstate::set_storage',args);
        }

        if(Modernizr.smallscreen || $this.opt.force_all_white){
            $this.tween_init_transition();
            return;
        }

        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'));
        switch(pushstateArgs.page){
            case 'home' :
                $this.tween_init_transition_from_home();
                break;
            case 'single-product' :
            case 'single-project' :
                $this.tween_init_transition_landing();
                break;
            default :
                $this.tween_init_transition();
                break;
        }
    },

    leave_module : function(){
        var $this = this;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));

        $this.opt.leave_module = true;

        if(Modernizr.smallscreen || $this.opt.force_all_white){
            $this.tween_leave();
            return;
        }

        switch(pushstateArgs.page){
            case 'home' :
                $this.tween_leave_to_home();
                break;
            case 'single-product' :
            case 'single-project' :
                $this.tween_leave_to_single();
                break;
            default :
                $this.tween_leave();
                break;
        }
    },

    enter_module : function() {
        var $this = this;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'));


        if(Modernizr.smallscreen || $this.opt.force_all_white){
            $this.tween_enter();
            return;
        }

        switch (pushstateArgs.page) {
            case 'home' :
                $this.tween_enter_from_home();
                break;
            case 'single-product' :
            case 'single-project' :
                $this.tween_enter_from_landing();
                break;
            default :
                $this.tween_enter();

                break;
        }
    },

    enter_page_title : function(){
        var $this = this;

        $this.opt.timelineTweenPageTitleEnter = new TimelineLite({
            onComplete: function(){
                $this.opt.pageTitleEnterComplete = true;
                if($this.opt.preloadImageComplete){
                    $(gusto).trigger('::enter');
                    $(gusto).trigger('page::load');
                }
                $('.overlay.fullpage').css('display','none');
            }
        });

        $this.opt.timelineTweenPageTitleEnter
            .to($('.page-title .text'),1,{ y: '0%', ease:Expo.easeInOut},0)
        ;

    },

    tween_init_transition : function(){
        var $this = this;
        var rowLink_offset = $this.$.rowLinkSecond.offset().top;
        var window_h = $(window).height() / 2;

        TweenLite.set($this.opt.element,{ css: {visibility: 'visible'}});
        TweenLite.set($this.$.overlayTop,{ y: '0%'});
        TweenLite.set($this.$.overlayBottom,{ y: '0%'});
        TweenLite.set([$this.$.title, $this.$.preTitle],{ opacity: 0,y: '-100px'});
        TweenLite.set($this.$.containerLine,{ y: '100px', opacity: 0});

        if(Modernizr.smallscreen){
            return;
        }

        TweenLite.set($this.opt.element,{ paddingTop: window_h - rowLink_offset});

    },

    tween_enter : function(){
        var $this = this;

        TweenMax.to($('.page-title'),.7,{ height: '0px', ease:Expo.easeInOut});

        $this.opt.timelineTweenPageEnter = new TimelineMax({
            onComplete : function(){
                $this.opt.leave_module = false;
            }
        });

        if(Modernizr.smallscreen){
            $this.opt.timelineTweenPageEnter
                .to($this.$.overlayTop,1,{ y: '-100%', ease:Power4.easeInOut},0)
                .to($this.$.overlayBottom,1,{ y: '100%', ease:Power4.easeInOut},0)
            ;
        }

        var $elements_title = [],
            $elements_preTitle = [],
            $elements_containerLine = [];

        $this.$.containerTitle.each(function(){
            var _this = $(this);
            if(verge.inViewport(_this) && !_this.hasClass('visible')){
                var $title = _this.find('.title');
                $elements_title.push($title);
                $title.addClass('visible');

                var $preTitle = _this.find('.pre-title');
                $elements_preTitle.push($preTitle);
                $preTitle.addClass('visible');

                var $line = _this.find('.line');
                $elements_containerLine.push($line);
                $line.addClass('visible');
            }
        });


        $this.opt.timelineTweenPageEnter
            .staggerTo($elements_preTitle,1,{ y: '0px', opacity:1, ease:Expo.easeOut},.1,.4)
            .staggerTo($elements_title,1.3,{ y: '0px', opacity:1, ease:Expo.easeOut},.1,.4)
            .to($elements_containerLine,.5,{ y:'0px', opacity:1, ease:Expo.easeOut},.4)
        ;


    },

    tween_leave : function(){
        var $this = this;
        $('.overlay.fullpage').css('display','block');

        $this.opt.timelineTweenPageLeave = new TimelineLite({ onComplete : tweenPageLeaveComplete});
        $this.opt.timelineTweenPageLeave
            .to($this.$.containerTitle.children(),.9,{ opacity: 0},0)
            .to($this.$.containerTitle.children(),.9,{ y: '200px',  ease:Power3.easeIn},0)
            .to($this.$.overlayChildren,.8,{ y: '0%', ease:Power4.easeInOut},.7)
        ;

        /**
         * Callback
         */
        function tweenPageLeaveComplete(){
            $(gusto).trigger('page::home::leaveComplete');

            $this.$.title.removeClass('visible');
            $this.$.preTitle.removeClass('visible');
            $this.$.containerLine.removeClass('visible');

            if($('body').data('plugin::pushstate') !== undefined){
                if($('body').data('plugin::pushstate').ajax_state != undefined){
                    if($('body').data('plugin::pushstate').ajax_state != 'success'){
                        $(gusto.frontend.plugin.pushstate.event).on({
                            'plugin::pushstate::success-ajax' : function(){
                                $this.page_next_init();
                            }
                        });
                    }else{
                        $this.page_next_init();
                    }
                }else{
                    $this.page_next_init();
                }
            }else{
                $this.page_next_init();

            }
        }
    },

    page_next_init : function(){
        var $this = this;
        $(gusto).trigger('plugin::set_page::next');

    }

};/**
 * Matrix - Regular
 * @regularin
 * @description : Regular type matrix
 * @version 1.0
 * @author  William Manco
 * @copyright Gusto IDS
 * @param options
 * @returns {Matrix}
 */
"undefined" == typeof Gusto && (Gusto = {}, gusto = {});
"undefined" == typeof Gusto.Frontend && (Gusto.Frontend = {}, gusto.frontend = {});
"undefined" == typeof Gusto.Frontend.Matrix && (Gusto.Frontend.Matrix = {}, gusto.frontend.matrix = {});
"undefined" == typeof Gusto.Frontend.Matrix.Regular && (Gusto.Frontend.Matrix.Regular = {}, gusto.frontend.matrix.regular = {});


Gusto.Frontend.Matrix.Regular = function Gusto(id){
    this.opt = {
        selector_id : "#module-"+id,
        selector: "[data-module='matrix'][data-type='regular'][data-id='"+id+"']",
        element : $('[data-module="matrix"][data-type="regular"]'),
        module : 'matrix',
        type : 'regular',
        force_all_white : true
    };

    this.$ = {
        col : this.opt.element.find('.col'),
        colLink : this.opt.element.find('.col a'),
        colOverlay : this.opt.element.find('.col .overlay'),
        placeholder : this.opt.element.find('.placeholder'),
        img : this.opt.element.find('.center-content-img .img'),
        containerFigure : this.opt.element.find('.figure-container'),
        containerImage : this.opt.element.find('.center-content-img')
    };

    this._init();
    this._events();

    return this;
};

Gusto.Frontend.Matrix.Regular.prototype = {

    _init : function(){
        var $this = this;
        $this.opt.leave_module = true;
        $this.init_position_img();
        $this.init_module();
    },

    _events : function(){
        var $this = this;

        $(gusto).on({
            '::enter' : function(){
                $this.enter_module();
            },
            '::leave' : function(){
                $this.leave_module();
            },
            'resize' : function(){
                $this.reinit_position_img();
            },
            'scroll' : function(){
                if(!$this.opt.animation_close && !$this.opt.slide_is_open){
                    $this.tween_enter();
                    $this.tween_close_all_image();

                }

            }
        });

        $(gusto.frontend.plugin.pushstate.event).on({
            'plugin::pushstate::before-ajax' : function(){
                $this.leave_module();
            },
            'plugin::pushstate::before-imgload' : function(){
                if(!Modernizr.touch){
                    $this.tween_open_image();
                }
            },
            'plugin::pushstate::success-imgload' : function(){
                if(!Modernizr.touch) {
                    $this.tween_load_image();
                }
            }
        });

        $(gusto.frontend.image_preload.standard.event).on({
            'image_preload::all' : function(){
                if($this.opt.pageTitleEnterComplete) {
                    $(gusto).trigger('::enter');
                    $(gusto).trigger('page::load');
                }
                $this.opt.preloadImageComplete = true;
            }
        });

        $this.opt.element.on({
            'mousemove': function (event) {
                if(event.pageY > $('.site-header').height()) {
                    TweenLite.to($this.opt.element.find('.follow-button'), .5, {x: event.pageX, y: event.pageY});
                }
            }
        });

        if(!Modernizr.touch) {
            $this.$.colLink.on({
                'mouseenter' : function(){
                    $this.on_hover_link($(this));
                },
                'mouseleave' : function(){
                    $this.on_leave_link($(this));
                },
                'click' :function(e){
                    e.preventDefault();
                }
            });
        }else{
            $this.$.colLink.on({
                'click' :function(e){
                    e.preventDefault();
                    return false;
                }
            });
        }

        $this.$.placeholder.on({
            'click' : function(){
                $this.tween_close_image();
            }
        });

        $(document).ready(function(){
            $this.enter_page_title();
        });
    },

    shuffle : function(array) {
        return array.sort(function() { return 0.5 - Math.random() });
    },

    on_hover_link : function($el){
        var $this = this;

        if($el.hasClass('zoomed')){
            return;
        }

        $el.addClass('open');

        var $containerImage = $el.find('.center-content-img');
        var $containerFigure = $el.find('.figure-container');
        var $img = $el.find('.center-content-img .img');
        var $line = $el.find('.line');
        var $viewText = $el.find('.view-text');

        $this.opt.timelineTweenHoverEnter = new TimelineLite();
        $this.opt.timelineTweenHoverEnter

           // .to($containerFigure,.5,{ y: '-40%', ease:Power3.easeInOut},0)
            .to($containerImage,.5,{ height: $this.$.containerFigure.attr('data-height')-100, ease:Power3.easeInOut},0)
            .to($containerFigure,.5,{ y: '-30px', ease:Power3.easeInOut},0)
            .to($line,.5,{ y: '-26px', height: '58px', ease:Power3.easeInOut},0)
            .to($viewText,.5,{ y: '0%', ease:Power3.easeInOut},0)
        ;
    },

    on_leave_link : function($el){
        var $this = this;

        $el.removeClass('open');

        var $containerImage = $el.find('.center-content-img');
        var $containerFigure = $el.find('.figure-container');
        var $img = $el.find('.center-content-img .img');
        var $line = $el.find('.line');
        var $viewText = $el.find('.view-text');



        $this.opt.timelineTweenHoverLeave = new TimelineLite();
        $this.opt.timelineTweenHoverLeave
            //.to($containerFigure,.5,{ y: '0%', ease:Power3.easeInOut},0)
            .to($containerImage,.5,{ height: '100%', ease:Power3.easeInOut},0)
            .to($containerFigure,.5,{ y: '0px', ease:Power3.easeInOut},0)
            .to($line,.5,{ y: '0px', height: '0px', ease:Power3.easeInOut},0)
            .to($viewText,.5,{ y: '100%', ease:Power3.easeInOut},0)
        ;
    },

    reinit_position_img : function(){
        var $this = this;

        TweenLite.set($this.$.containerFigure,{ height: $this.$.containerImage.height() });
        $this.$.containerFigure.attr('data-height',$this.$.containerImage.height());
        $this.opt.element.find('.col').each(function() {
            TweenLite.set($(this), { position: 'static'});
        });
        $this.init_position_img();
    },

    init_position_img : function(){
        var $this = this;

        TweenLite.set($this.$.containerFigure,{ height: $this.$.containerImage.height() });
        $this.$.containerFigure.attr('data-height',$this.$.containerImage.height());
        $this.opt.element.find('.col').each(function(){
            var offset = $(this).offset();
            $(this).data('offset',offset)
        });
        $this.opt.element.find('.col').each(function() {
            TweenLite.set($(this), { position: 'absolute', top:$(this).data('offset').top, left:$(this).data('offset').left });
        });
    },

    init_module : function(){
        var $this = this;
        var args  = {};

        $(gusto.frontend.image_preload.standard.event).trigger('image_preload::init');
        if(!JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'))){
            args.targetUrl = $this.opt.element.attr('data-url');
            args.page = $this.opt.element.attr('data-page');
            $(gusto.frontend.plugin.pushstate.event).trigger('plugin::pushstate::set_storage',args);
        }

        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'));
        switch(pushstateArgs.page){
            case 'home' :
                //$this.tween_init_transition_from_home();
                $this.tween_init_transition();

                break;
            default :
                $this.tween_init_transition();
                break;
        }

    },

    leave_module : function(){
        var $this = this;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));

        $this.opt.leave_module = true;

        if(Modernizr.smallscreen || $this.opt.force_all_white){
            $this.tween_leave();
            return;
        }

        switch(pushstateArgs.page){
            case 'home' :
                //$this.tween_leave_to_home();
                $this.tween_leave();

                break;
            case 'single-product' :
                //$this.tween_leave_to_single();
                $this.tween_leave();
                break;
            default :
                $this.tween_leave();
                break;
        }
    },

    enter_module : function(){
        var $this = this;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'));

        if(Modernizr.smallscreen || $this.opt.force_all_white){
            $this.tween_enter();
            return;
        }

        switch(pushstateArgs.page){
            case 'home' :
                $this.tween_enter();

                //$this.tween_enter_from_home();
                break;
            default :
                $this.tween_enter();
                break;
        }
    },

    enter_page_title : function(){
        var $this = this;

        $this.opt.timelineTweenPageTitleEnter = new TimelineLite({
            onComplete: function(){
                $this.opt.pageTitleEnterComplete = true;
                if($this.opt.preloadImageComplete){
                    $(gusto).trigger('::enter');
                    $(gusto).trigger('page::load');
                }
            }
        });

        $this.opt.timelineTweenPageTitleEnter
            .to($('.page-title .text'),1,{ y: '0%', ease:Expo.easeInOut},0)
        ;

    },

    tween_open_image : function(){
        var $this = this;

        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));
        var $currentImg = $this.opt.element.find('a[data-img="'+pushstateArgs.img+'"]');
        var $currentImgBox = $currentImg.closest('.col');
        $this.opt.slide_is_open = true;

        $this.$.placeholder.addClass('active');

        $currentImg.addClass('zoomed');
        $this.on_leave_link($currentImg);

        var $img = $currentImgBox.find('.img');
        var imgSize = calculateAspectRatioFit(
                    parseInt($img.attr('data-image-width')),
                    parseInt($img.attr('data-image-height')),
                    parseInt($currentImgBox.width()),
                    parseInt($currentImgBox.height())
                );

        var heightImgZommed = parseInt($(window).height()) - parseInt($('[data-module="header"][data-type="standard"]').height()*2);
        var scale = (heightImgZommed ) / imgSize.height;

        var scrollTop = $(window).scrollTop();
        var windowH = $(window).height();



        if(!$this.opt.animation_close){
            TweenLite.to($currentImgBox,1,{ left: '50%', top:scrollTop+(windowH/2), x:'-50%', y:'-50%', position:'absolute', zIndex: 100, scale: scale, ease: Power4.easeInOut , onComplete : function(){
                var $figContainer = $currentImgBox.find('.figure-container');
                $figContainer.attr('style','');
                $figContainer.height($figContainer.attr('data-height'));


                if($currentImg.find('.img.clone').length <= 0){

                    $this.$.clone_img = $currentImg.find('.img').clone();
                    $this.$.clone_img
                        .addClass('clone')
                        .css({
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            zIndex : '-1'
                        });

                    $currentImg.find('.figure-container').append($this.$.clone_img);
                    $this.$.image_remove =  $currentImg.find('.img').not('.clone');

                    $.imgpreload($currentImg.attr('data-img'),
                        {
                            all: function()
                            {

                                $currentImg.find('.clone').css('background-image','url('+$(this).attr('src')+')');
                                TweenMax.to($this.$.image_remove,.5,{ opacity : 0, onComplete : function(){
                                    $currentImg.find('.figure-container .img').first().remove();
                                    $this.opt.slide_is_open = false;
                                }});

                            }
                        });
                }else{
                    $this.opt.slide_is_open = false;
                }

            }});

        }else{
            $this.tween_close_all_image();
            $this.opt.slide_is_open = false;

        }





        var elements = [];

        $this.$.col.not($currentImgBox).each(function(){
            var _this = $(this);
            if(verge.inViewport(_this)){
                elements.push(_this.get(0));
            }
        });


        TweenMax.staggerTo($this.shuffle($(elements).find('.overlay')),.7,{ y: '0%', ease:Power4.easeInOut},.05);

        TweenMax.to($this.opt.element.find('.label-close'),.7,{ y : '0%', ease: Power4.easeInOut });

        $(elements).find('.overlay').addClass('is-open');

    },

    tween_close_image : function(){
        var $this = this;

        $this.opt.animation_close = true;

        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));
        var $currentImg = $this.opt.element.find('a[data-img="'+pushstateArgs.img+'"]');
        var $currentImgBox = $currentImg.closest('.col');


        TweenLite.to($currentImgBox,1,{ left: $currentImgBox.data('offset').left, top: $currentImgBox.data('offset').top, x:'0%', y:'0%', position:'absolute', zIndex: 0, scale: 1, ease:Power4.easeInOut, onComplete: function(){
            $this.$.placeholder.removeClass('active');
            $currentImg.removeClass('zoomed');
            $this.opt.animation_close = false
        }});
        TweenMax.to($this.opt.element.find('.label-close'),.7,{ y : '100%', ease: Power4.easeInOut });


        var elements = [];

        $this.$.col.not($currentImgBox).each(function(){
            var _this = $(this);
            if(verge.inViewport(_this)){
                elements.push(_this.get(0));
            }
        });

        TweenMax.staggerTo($this.shuffle($(elements).find('.overlay')),.7,{ y: '-100%', ease:Power4.easeInOut},.05);

    },

    tween_close_all_image : function(){
        var $this = this;



        if($this.$.col.find('.zoomed').length <= 0){
            return;
        }

        $this.opt.animation_close = true;

        $current_img = $this.$.col.find('.zoomed').closest('.col');

        TweenMax.to($current_img,1,{ left: $current_img.data('offset').left, top: $current_img.data('offset').top, x:'0%', y:'0%', position:'absolute', zIndex: 0, scale: 1, ease:Power4.easeInOut, onComplete: function(){
            $this.$.placeholder.removeClass('active');
            $this.$.col.find('a').removeClass('zoomed');
            $this.opt.animation_close = false;
        }});

        TweenMax.to($this.$.col.find('.overlay.is-open'),.7,{ y: '-100%', ease:Power4.easeInOut},.05);
        TweenMax.to($this.opt.element.find('.label-close'),.7,{ y : '100%', ease: Power4.easeInOut });


    },

    tween_load_image : function(){
        var $this = this;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));
        var $currentImg = $this.opt.element.find('a[data-img="'+pushstateArgs.img+'"]');

        $currentImg.find('.img').attr('src',pushstateArgs.img);

    },

    tween_init_transition : function(){
        var $this = this;
    },

    tween_enter : function(){
        var $this = this;

        TweenMax.to($('.page-title'),.7,{ height: '0px', ease:Expo.easeInOut});

        var elements = [];

        $this.$.col.each(function(){
            var _this = $(this);
            if(verge.inViewport(_this) && !_this.hasClass('visible')){
                elements.push(_this.get(0));
                _this.addClass('visible');
            }
        });

        $this.opt.timelineTweenPageEnter = new TimelineMax({
            onComplete : function(){
                $this.opt.leave_module = false;
            }
        });
        $this.opt.timelineTweenPageEnter
            .staggerTo($this.shuffle($(elements).find('.overlay')),1.5,{ y: '100%', ease:Power3.easeInOut},.05,0)
        ;

    },

    tween_leave : function(){
        var $this = this;

        var elements = [];

        $this.$.col.each(function(){
            var _this = $(this);
            if(verge.inViewport(_this)){
                elements.push(_this.get(0));
            }
        });


        $this.opt.timelineTweenPageLeave = new TimelineLite({ onComplete : tweenPageLeaveComplete});
        $this.opt.timelineTweenPageLeave
            .staggerTo($this.shuffle($(elements).find('.overlay')),1.5,{ y: '0%', ease:Power3.easeInOut},.05,0)

        ;

        /**
         * Callback
         */
        function tweenPageLeaveComplete(){
            $(gusto).trigger('page::home::leaveComplete');
            $this.$.col.removeClass('visible');

            if($('body').data('plugin::pushstate') !== undefined){
                if($('body').data('plugin::pushstate').ajax_state != undefined){
                    if($('body').data('plugin::pushstate').ajax_state != 'success'){
                        $(gusto.frontend.plugin.pushstate.event).on({
                            'plugin::pushstate::success-ajax' : function(){
                                $this.page_next_init();
                            }
                        });
                    }else{
                        $this.page_next_init();
                    }
                }else{
                    $this.page_next_init();
                }
            }else{
                $this.page_next_init();

            }
        }
    },

    page_next_init : function(){
        var $this = this;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));

        switch(pushstateArgs.page){
            default:
                $(gusto).trigger('plugin::set_page::next');
                break;
        }
    }

};slider_args =  "{speed : 600}";
/**
 * Slider - Swiper
 * @swiperin
 * @description : Swiper type slider
 * @version 1.0
 * @author  William Manco
 * @copyright Gusto IDS
 * @param options
 * @returns {Slider}
 */
"undefined" == typeof Gusto && (Gusto = {}, gusto = {});
"undefined" == typeof Gusto.Frontend && (Gusto.Frontend = {}, gusto.frontend = {});
"undefined" == typeof Gusto.Frontend.Slider && (Gusto.Frontend.Slider = {}, gusto.frontend.slider = {});
"undefined" == typeof Gusto.Frontend.Slider.Swiper && (Gusto.Frontend.Slider.Swiper = {}, gusto.frontend.slider.swiper = {});


Gusto.Frontend.Slider.Swiper = function Gusto(id){
    this.opt = {
        selector_id : "#module-"+id,
        selector: "[data-module='slider'][data-id='"+id+"']",
        element : $('[data-module="slider"][data-type="swiper"]'),
        module : 'slider',
        type : 'swiper',
        force_all_white : true
    };

    this.$ = {
        swiperContainerH        : this.opt.element.find('.swiper-container-h'),
        swiperContainerV        : this.opt.element.find('.swiper-container-v'),
        arrows                  : this.opt.element.find('.arrows-slider'),
        arrowItem               : this.opt.element.find('.arrows-slider .arrow-item'),
        followButton            : this.opt.element.find('.follow-button')
    };

    this._init();
    this._events({
        reinit : false
    });
    this._events({
        reinit : true
    });

    return this;
};

Gusto.Frontend.Slider.Swiper.prototype = {

    _init : function(){
        var $this = this;

        $this.opt.swiperV = null;
        $this.opt.swiperH = null;

        $this.init_sizes();
        $this.init_slides();
        $this.init_swiper();
        $this.init_module();
        $this.init_area();
        $this.init_arrows();

    },

    _events : function(args){
        var $this = this;

        if(args.reinit){

            $this.opt.element.find('.arrows-slider .arrow-area').off().on({
                'click' : function(){
                    var _this = $(this);
                    $this.on_click_arrow(_this);
                    if(Modernizr.touch){
                        $this.on_touch_click_arrow(_this.find('.arrow-item'));
                    }
                },
                'mouseenter' : function(){
                    var _this = $(this);
                    if(Modernizr.touch){
                        return;
                    }
                    $this.on_hover_arrow(_this.find('.arrow-item'));
                },
                'mouseleave' : function(){
                    var _this = $(this);
                    if(Modernizr.touch){
                        return;
                    }
                    $this.on_leave_arrow(_this.find('.arrow-item'));
                }
            });
            return;
        }


        $(gusto.frontend.image_preload.standard.event).on({
            'image_preload::all': function(){

                if($this.opt.loading_all){
                    return;
                }

                if($this.opt.load_album_first_slide){
                    $this.opt.loading_first = true;
                    $this.$.followButton.removeClass('is-hide');
                    return;
                }

                if($this.opt.load_album){
                    $this.opt.loading_all = true;
                    return;
                }


                if(!$this.opt.enter_module){
                    if($this.opt.pageTitleEnterComplete) {
                        $(gusto).trigger('::enter');
                        $(gusto).trigger('page::load');
                    }
                    $this.opt.preloadImageComplete = true;
                }

            }
        });

        $(gusto.frontend.plugin.pushstate.event).on({

            'plugin::pushstate::before-ajax' : function(){
                $this.leave_module();
            }

        });

        $(gusto).on({
            'plugin::longpress::end' : function(){
                if($this.$.followButton.hasClass('is-hide')){
                    return;
                }
                $this.enter_longpress();
                $this.tween_line_exit();

            },
            'plugin::longpress::clear' : function(){
                $this.exit_longpress();
                $this.tween_line_back();
            },
            'plugin::longpress::click' : function(e,args){
                $this.opt.longpress_click_y = args.pageY;
                $this.tween_line_start();
            },
            '::enter' : function (e,args) {
                if(args !== undefined) {
                    if (args.force) {
                        $this.tween_enter();
                        return;
                    }
                }
               $this.enter_module();
            },
            '::leave' : function(e,args){
                if(args !== undefined){
                    if( args.force){
                        $this.tween_leave(args);
                        return;
                    }
                }
                $this.leave_module();
            },
            'resize' : function(){
                $this.init_sizes();
                $this.opt.swiperH[$this.opt.current_post_slug].update(true);
                TweenMax.set($this.$.swiperContainerV.find('.swiper-slide-active .swiper-wrapper'), { x: $this.opt.swiperH[$this.opt.current_post_slug].translate});

            }
        });

        $('body').on({
            'mousemove' : function(event){

                if($this.opt.enter_longpress){

                    cx = Math.ceil($(window).width() / 2);
                    cy = Math.ceil($(window).height() / 2);
                    dx = (event.pageX - cx);
                    dy = (event.pageY - cy) - $(window).scrollTop();
                    tiltx = (dy / cy);
                    tilty = - (dx / cx);
                    radius = Math.sqrt(Math.pow(tiltx,2) + Math.pow(tilty,2));
                    degree = (radius * 3);

                    $(".swiper-container-v .swiper-slide-active").each(function(){
                        var _this = $(this);
                        TweenLite.to(_this,.5, {y :  (event.pageY - $this.opt.longpress_click_y)/2});
                        TweenLite.to(_this.find('img'),.5, {rotationX: degree*Math.sign(tiltx)});
                    });
                }else{
                    var width_current_slide = $('.swiper-slide-active .swiper-container-h .swiper-slide-active .container-bg').width()/2;
                    if(event.pageY > $('.site-header').height() && event.pageY < ($(window).height() - $('.site-header').height()) ){
                        $this.opt.labelHide = false;

                        TweenLite.to($this.opt.element.find('.follow-button'),.5,{ x: event.pageX, y: event.pageY});

                        if(event.pageX > ($(window).width()/2)+width_current_slide){

                            if(!$this.opt.mouseIsRight){

                                TweenMax.to($this.opt.element.find('.label-prev'),.7,{ y : '100%', ease: Power4.easeInOut });
                                TweenMax.to($this.opt.element.find('.label-center'),.7,{ y : '100%', ease: Power4.easeInOut });
                                TweenMax.to($this.opt.element.find('.label-next'),.7,{ y : '0%', ease: Power4.easeInOut });

                                $this.opt.mouseIsRight = true;
                                $this.opt.mouseIsLeft = false;
                                $this.opt.mouseIsCenter = false;

                            }

                        }else if(event.pageX < ($(window).width()/2)-width_current_slide){

                            if(!$this.opt.mouseIsLeft){

                                TweenMax.to($this.opt.element.find('.label-prev'),.7,{ y : '0%', ease: Power4.easeInOut });
                                TweenMax.to($this.opt.element.find('.label-center'),.7,{ y : '100%', ease: Power4.easeInOut });
                                TweenMax.to($this.opt.element.find('.label-next'),.7,{ y : '100%', ease: Power4.easeInOut });

                                $this.opt.mouseIsLeft = true;
                                $this.opt.mouseIsRight = false;
                                $this.opt.mouseIsCenter = false;
                            }
                        }else{

                            if(!$this.opt.mouseIsCenter) {
                                TweenMax.to($this.opt.element.find('.label-prev'),.7,{ y : '100%', ease: Power4.easeInOut });
                                TweenMax.to($this.opt.element.find('.label-center'),.7,{ y : '0%', ease: Power4.easeInOut });
                                TweenMax.to($this.opt.element.find('.label-next'),.7,{ y : '100%', ease: Power4.easeInOut });

                                $this.opt.mouseIsCenter = true;
                                $this.opt.mouseIsLeft = false;
                                $this.opt.mouseIsRight = false;
                            }
                        }
                    }else {
                        if(!$this.opt.labelHide){
                            TweenMax.to($this.opt.element.find('.follow-button .label'),.7,{ y : '100%', ease: Power4.easeInOut });
                            $this.opt.mouseIsCenter = false;
                            $this.opt.mouseIsLeft = false;
                            $this.opt.mouseIsRight = false;
                            $this.opt.labelHide = true;
                        }


                    }

                }
            },
            'click' : function(event){
                $this.$.swiperContainerH = $this.opt.element.find('.swiper-container-v .swiper-slide-active .swiper-container-h');
                $this.opt.current_post_slug = $this.opt.element.find('.swiper-container-v .swiper-slide-active[data-post-slug]').attr('data-post-slug');

                if(!$this.opt.enter_longpress && !$this.opt.tween_slide){

                    var width_current_slide = $('.swiper-slide-active .swiper-container-h .swiper-slide-active .container-bg').width()/2;
                    if(event.pageX > ($(window).width()/2)+width_current_slide && event.pageY > $('.site-header').height()){
                        if($this.opt.swiperH[$this.opt.current_post_slug].activeIndex != $this.opt.swiperH[$this.opt.current_post_slug].slides.length-1){
                            $this.opt.currentDirectionSwiper = 'right';
                            $this.opt.swiperH[$this.opt.current_post_slug].slideNext();
                            $this.tween_slide($this.opt.swiperH[$this.opt.current_post_slug]);
                        }
                    }else if(event.pageX < ($(window).width()/2)-width_current_slide && event.pageY > $('.site-header').height()){
                        if($this.opt.swiperH[$this.opt.current_post_slug].activeIndex != 0) {
                            $this.opt.currentDirectionSwiper = 'left';
                            $this.opt.swiperH[$this.opt.current_post_slug].slidePrev();
                            $this.tween_slide($this.opt.swiperH[$this.opt.current_post_slug]);
                        }
                    }
                }
            }
        });

        $(document).ready(function(){
            $this.enter_page_title();
        });
    },

    init_arrows : function(){
        var $this = this;

        TweenLite.set($this.opt.element.find('.arrow-dx-item rect'),{ x: 20, y: 0});
        TweenLite.set($this.opt.element.find('.arrow-dx-item polygon'),{ x: 14.5});
        TweenLite.set($this.opt.element.find('.arrow-sx-item rect'),{ x: -20,y: 0});
    },

    init_sizes : function(){
        var $this = this;
        $('.site-main').css({
            overflow : 'hidden',
            height : $(window).height()+'px'
        });
        $this.$.swiperContainerV.height($(window).height()*2);
        TweenMax.set($this.$.swiperContainerV,{ scale : 1, y:-(($(window).height()/2)), transformOrigin:"center center"});
        TweenLite.set($this.opt.element, { css: {overflow: 'hidden',height: $(window).height() ,width : $(window).width()}});

        if($this.opt.swiperV){
            $this.opt.swiperV.update(true);
        }

        if($this.opt.swiperH) {
            $.each( $this.opt.swiperH, function( key, value ) {
                value.update(true);
            });
        }

    },

    init_slides : function(){
        var $this = this;
        $this.opt.slide_append = false;
        $this.opt.template_slide_container = $this.$.swiperContainerH.closest('.swiper-slide').clone()[0];
        $this.opt.template_slide = $('.swiper-container-h .swiper-slide').clone()[0];
        $this.opt.current_post_slug = $('.swiper-slide').attr('data-post-slug');
        $this.$.swiperContainerV.attr('data-swiper-disable',true);
        $this.$.swiperContainerH.attr('data-swiper-disable',false);

        $(gusto.frontend.image_preload.standard.event).trigger('image_preload::restart');
    },

    init_swiper : function(){
        var $this = this;

        $this.opt.swiperH = new Object();
        $this.opt.swiperV = new Swiper('.swiper-container-v', {
            direction: 'vertical',
            slidesPerView: 2,
            touchRatio : 3,
            speed : 800,
            centeredSlides: true,
            disable : true,
            onSlideChangeStart : function(swiper) {
                $this.opt.swiperV.activeIndex = swiper.activeIndex;
                $this.$.currentAlbumSwiper = $this.opt.element.find('.swiper-container-v .swiper-slide-active[data-post-slug]');
                $this.tween_title_album({});
                $this.init_pagination();
                $this.tween_pagination();
            },
            onTouchMove : function(swiper, e){
                var y = (swiper.translate /swiper.virtualSize)*($this.$.titleSwiperContent.height()*$this.$.titleSwiperContent.length);
                $this.tween_title_album({y : y});
            }
        });

        TweenMax.set($this.$.arrows.find('.arrow-sx'),{ opacity : 0});
        TweenMax.set($this.$.followButton.find('.label-prev'),{ opacity : 0});


        $this.opt.args_swiper_h = {
            speed : 1200,
            simulateTouch : false,
            virtualTranslate : true,
            onSlideChangeStart : function(swiper) {
                $this.$.swiperContainerH = $this.opt.element.find('.swiper-container-v .swiper-slide-active .swiper-container-h');
                $this.opt.current_post_slug = $this.opt.element.find('.swiper-container-v .swiper-slide-active').attr('data-post-slug');

                if(swiper.activeIndex >= swiper.slides.length-1){
                    TweenMax.to($this.$.arrows.find('.arrow-dx'),.5,{ opacity : 0});
                    TweenMax.to($this.$.followButton.find('.label-next'),.5,{ opacity : 0});

                }else{
                    TweenMax.to($this.$.arrows.find('.arrow-dx'),.5,{ opacity : 1});
                    TweenMax.to($this.$.followButton.find('.label-next'),.5,{ opacity : 1});
                }

                if(swiper.activeIndex > 0){
                    TweenMax.to($this.$.arrows.find('.arrow-sx'),.5,{ opacity : 1});
                    TweenMax.to($this.$.followButton.find('.label-prev'),.5,{ opacity : 1});
                }else{
                    TweenMax.to($this.$.arrows.find('.arrow-sx'),.5,{ opacity : 0});
                    TweenMax.to($this.$.followButton.find('.label-prev'),.5,{ opacity : 0});

                }
            }
        };

        if(Modernizr.touch){
            $this.opt.args_swiper_h.speed = 300;
            $this.opt.args_swiper_h.simulateTouch = true;
            $this.opt.args_swiper_h.virtualTranslate = false;
            $this.opt.args_swiper_h.onSlideChangeEnd = function (){
                $this.tween_pagination();

            }

        }




    },

    init_area : function(){
        var $this = this;

        TweenLite.set($this.opt.element.find('.title-swiper-container'), { css: {display: 'none'}});
    },

    load_album : function(){
        var $this = this;

        $.ajax({
            url : ajaxurl,
            type : 'post',
            data : {
                action : 'api_get_taxonomy_meta',
                post_type : '_cty_album',
                taxonomy : '_cta_types',
                terms : $this.opt.element.attr('data-current-term')
            },
            success : function( response ) {
                $this.opt.response_taxonomy_meta = JSON.parse(response);
                //$this.opt.load_album = true;
                $this.opt.load_album_first_slide = true;
                $this.create_album();
                $this.create_slides();
                $this.create_title_album();
                $this.tween_title_album({});
                $this.tween_leave_title_album();
            }
        });
    },

    init_pagination : function(){
        var $this = this;

        $this.opt.current_post_slug = $this.opt.element.find('.swiper-container-v .swiper-slide-active[data-post-slug]').attr('data-post-slug');

        var current_post_slug = $this.opt.current_post_slug;
        var slides_length  = $this.opt.swiperH[current_post_slug].slides.length;
        var current_index = $this.opt.swiperH[current_post_slug].activeIndex;
        var index = 0;

        $this.$.currentElementSwiperH = $this.opt.element.find('.swiper-container-v .swiper-slide-active .swiper-container-h');
        $this.$.currentElementSwiperH.find('.pagination').html('<div class="current-index"></div><div class="separator">/</div><div class="slides-length">'+slides_length+'</div>');
        $this.$.currentElementSwiperH.find('.current-index').append('<div class="item-index"><div class="index">'+current_index+'</div></div>');

        TweenMax.to($this.opt.element.find('.pagination'),.2, {opacity:1});
        TweenMax.to($this.opt.element.find('.arrows-slider'),.2, {opacity:1});
    },

    create_album : function(){
        var $this = this;
        var arr_prepend = [];
        var arr_append = [];

        if(Modernizr.touch){
            TweenMax.set($this.$.swiperContainerH.find('.swiper-slide .overlay'), { css: { display: 'none'}});

            for (var i = 0; i < $this.opt.response_taxonomy_meta.length; i++) {
                var item = $this.opt.response_taxonomy_meta[i];

                if(item.post_slug == $this.opt.current_post_slug){
                    $this.opt.slide_append = true;
                    $this.opt.swiperH[$this.opt.current_post_slug] = new Swiper('.swiper-container-v [data-post-slug="'+$this.opt.current_post_slug+'"] .swiper-container-h',$this.opt.args_swiper_h);
                    $this.init_pagination();
                    $this.tween_pagination();
                }
            }
            return;
        }


        for (var i = 0; i < $this.opt.response_taxonomy_meta.length; i++) {

            var item = $this.opt.response_taxonomy_meta[i];
            var args_swiper = $this.opt.args_swiper_h;

            /**
             * Cambia i dati sul template dello slide (album)
             */
            if(Array.isArray($this.opt.response_taxonomy_meta[i]['metadata']) && item.post_slug != $this.opt.current_post_slug) {
                $($this.opt.template_slide_container).find('.title').text($this.opt.response_taxonomy_meta[i]['metadata'][0].title);
                $($this.opt.template_slide_container).find('.container-bg [data-image]').attr('data-image', $this.opt.response_taxonomy_meta[i]['metadata'][0].img_large);
                $($this.opt.template_slide_container).attr('data-post-slug', $this.opt.response_taxonomy_meta[i].post_slug);
                $($this.opt.template_slide_container).attr('data-post-title', $this.opt.response_taxonomy_meta[i].title);
            }

            /**
             * Clona il template dello slider verticale (quindi gli album)
             * rimuove le slide che verranno aggiunte dopo
             */
            var clone = $($this.opt.template_slide_container).clone();
            clone.find('.swiper-slide').remove();

            /**
             * Se l'album da creare è quello della pagina quindi già esistente
             * crea lo slider e vai avanti passando al prossimo turno
             */
            if(item.post_slug == $this.opt.current_post_slug){
                $this.opt.slide_append = true;
                $this.opt.swiperH[$this.opt.current_post_slug] = new Swiper('.swiper-container-v [data-post-slug="'+$this.opt.current_post_slug+'"] .swiper-container-h',$this.opt.args_swiper_h);
                $this.init_pagination();
                $this.tween_pagination();
                if(!arr_prepend.isEmpty()){
                    $this.opt.swiperV.prependSlide(arr_prepend.reverse());
                }
                continue;
            }
            /**
             * Appende gli album
             */
            if($this.opt.slide_append){
                arr_append.push(clone.get(0));
            }else{
                arr_prepend.push(clone.get(0));
            }

            if($this.opt.slide_append && arr_append.length > 0){
                $this.opt.swiperV.appendSlide(arr_append);
            }

        }

        for (var i = 0; i < $this.opt.response_taxonomy_meta.length; i++) {
            var item = $this.opt.response_taxonomy_meta[i];
            var args_swiper = $this.opt.args_swiper_h;
            /**
             * Attiva lo slider su ogni album  (tranne quello corrente)
             */
            $this.opt.swiperH[item.post_slug] = new Swiper('.swiper-container-v [data-post-slug="'+item.post_slug+'"] .swiper-container-h',args_swiper);
        }




    },

    create_slides : function(){
        var $this = this;

        for (var i = 0; i < $this.opt.response_taxonomy_meta.length; i++) {

            var item = $this.opt.response_taxonomy_meta[i];
            var post_slug = item.post_slug;


            if(item.post_slug == $this.opt.current_post_slug){
                $this.opt.slide_append = true;
                continue;
            }

            //$this.opt.response_taxonomy_meta[i]['metadata'].splice(0, 1);
            if(Array.isArray($this.opt.response_taxonomy_meta[i]['metadata'])){
                $this.opt.response_taxonomy_meta[i]['metadata'].forEach(function(item){
                    $($this.opt.template_slide).find('.title').text(item.title);
                    $($this.opt.template_slide).find('.container-bg [src]').attr('src',item.img_thumbnail);
                    $($this.opt.template_slide).find('.container-bg [data-image]').attr('data-image',item.img_large);
                    $($this.opt.template_slide).find('.container-bg [data-image]').addClass('no-load');
                    $this.opt.swiperH[post_slug].appendSlide($($this.opt.template_slide).clone()[0]);
                    $this.opt.only_first_slide = true;
                });
            }

        }

        $this.$.swiperContainerH.find('.swiper-slide').css({
            position : 'relative'
        });


        TweenMax.set($this.opt.element.find('.swiper-container-h .swiper-slide'),{ visibility : 'visible'});

        $this.opt.element.find('.swiper-container-h').each(function(){
            $(this).find('.swiper-slide-active [data-image]').removeClass('no-load');
        });

        $(gusto.frontend.image_preload.standard.event).trigger('image_preload::restart');

    },

    create_title_album : function () {
        var $this = this;

        if($this.opt.element.find('.title-swiper-container').length > 0){
            return;
        }

        $this.opt.element.append('<div class="title-swiper-container"><div class="title-swiper-content"></div></div>');

        $this.$.titleSwiperContainer = $this.opt.element.find('.title-swiper-container');
        $this.$.titleSwiperContent = $this.opt.element.find('.title-swiper-content');

        $('.swiper-slide[data-post-slug]').each(function(){
            var _this = $(this);
            $this.$.titleSwiperContent.append('<div class="title" data-slug="'+_this.attr("data-post-slug")+'">'+_this.attr("data-post-title")+'</div>');
            var text_title_album = $(this).attr('data-post-title');
            $(this).find('.title-album').html(text_title_album);
        });

        TweenMax.to($this.opt.element.find('.title-album'),.7,{ opacity:1, ease:Power2.easeInOut});

        var height_item = $('.title-swiper-container .title').height();
        $this.$.titleSwiperContainer.css({
            height : height_item*3,
            overflow : 'hidden'
        });
        $this.$.titleSwiperContent.css({
            marginTop: height_item
        });


    },

    tween_title_album : function(args){
        var $this = this;

        var album_current = $this.opt.swiperV.activeIndex;

        var step = $('.title-swiper-container .title').height();

        var to_title = 0;
        if(album_current != 0){
            to_title = step*album_current;
        }

        if('y' in args){
            TweenMax.to($this.$.titleSwiperContent,.8,{ y:args.y - step/2 });
        }else{
            TweenMax.to($this.$.titleSwiperContent,.6,{ y:-(to_title), ease: Power3.easeOut});
        }

        var current_title = $('.title-swiper-container .title').eq($this.opt.swiperV.activeIndex);
        var all_title = $('.title-swiper-container .title').not(current_title);

        TweenMax.to(current_title,.6,{ opacity:1, ease: Power1.easeOut});
        TweenMax.to(all_title,.6,{ opacity:.5, ease: Power1.easeOut});

        all_title.removeClass('active');
        current_title.addClass('active');

    },

    tween_enter_title_album : function(){
        var $this = this;

        TweenLite.set($this.opt.element.find('.title-swiper-container'), { css: {display: 'block'}});
        TweenMax.set($this.$.titleSwiperContainer.find('.title'),{ opacity:1});

        TweenMax.to($this.$.titleSwiperContainer,.8,{ opacity:1, ease: Power1.easeOut,delay : .3});
        TweenMax.to($this.$.titleSwiperContainer,1,{ scale: 1, x: 0, ease: Power3.easeInOut});

    },

    tween_leave_title_album : function(){
        var $this = this;
        TweenMax.to($this.$.titleSwiperContainer.find('.title').not('.active'),.8,{ opacity:0, ease: Power2.easeInOut});

        setTimeout(function(){
            TweenMax.to($this.$.titleSwiperContainer,.3,{ opacity:0,scale: 1.1,x: -20,  ease: Power1.easeOut, delay:.5, onComplete: function(){
                TweenLite.set($this.opt.element.find('.title-swiper-container'), { css: {display: 'none', zIndex: 0}});

            }});
        },350);

    },

    tween_slide : function (swiper) {
        var $this = this;

        $this.opt.tween_slide = true;

        $this.$.swiperContainerH = $this.opt.element.find('.swiper-container-v .swiper-slide-active .swiper-container-h');
        $this.opt.current_post_slug = $this.opt.element.find('.swiper-container-v .swiper-slide-active[data-post-slug]').attr('data-post-slug');

        if(Modernizr.smallscreen) {



        }else{
            TweenMax.set($this.$.swiperContainerV.find('.swiper-slide-active .swiper-wrapper'), { x: $this.opt.swiperH[$this.opt.current_post_slug].translate});
            TweenMax.set($this.$.swiperContainerH.find('.swiper-slide-active .overlay'), { x: '0%'});
            TweenMax.to($this.$.swiperContainerH.find('.swiper-slide-active .overlay'),1.5,{  x: '100%', ease: Power3.easeInOut });


            if(!Modernizr.safari){
                TweenMax.set($this.$.swiperContainerH.find('.swiper-slide').not('.swiper-slide-active'),{x: 0});
                TweenMax.set($this.$.swiperContainerH.find('.swiper-slide'), { perspective: '800px'});
                TweenMax.to($this.$.swiperContainerH.find('.swiper-slide-prev img'),1,{ rotationY: "90_counter-clockwise", ease: Power1.easeIn ,onComplete: function(){
                    TweenMax.set($this.$.swiperContainerH.find('.swiper-slide-prev img'), {rotationY: "0"});
                } });
                TweenMax.set($this.$.swiperContainerH.find('.swiper-slide-active img'), {rotationY: "0"});
            }
        }


        if($this.opt.currentDirectionSwiper == 'right'){
            if(Modernizr.safari){
                $this.opt.tween_slide = false;
            }else{
                TweenMax.from($this.$.swiperContainerH.find('.swiper-slide-active'),1,{ x: '50%', ease: Power3.easeOut,
                    onComplete : function(){
                        $this.opt.tween_slide = false;
                    }
                });
            }

        }else{
            if(Modernizr.safari){
                $this.opt.tween_slide = false;
            }else{
                TweenMax.from($this.$.swiperContainerH.find('.swiper-slide-active'),1,{ x: '-50%', ease: Power3.easeOut,
                    onComplete : function(){
                        $this.opt.tween_slide = false;
                    }
                });
            }

        }

        $this.tween_pagination();
    },

    tween_pagination : function () {
        var $this = this;

        var current_post_slug = $this.opt.current_post_slug;
        var current_index = $this.opt.swiperH[current_post_slug].activeIndex;
        var current_index_string = current_index+1;

        var $current_item_index = $this.$.swiperContainerV.find('.swiper-slide-active .pagination .item-index');

        TweenLite.to($current_item_index, .6,{ opacity: 0, ease: Power4.easeIn, onComplete: function(){
            $current_item_index.find('.index').text(current_index_string);
            TweenLite.to($current_item_index, .7,{  opacity: 1, ease: Power4.easeInOut});
        }});
    },

    tween_line_start : function(){
        var $this = this;

        TweenLite.to($this.opt.element.find('.follow-button .line'), 1,{ x: '0%'});

    },

    tween_line_back : function(){
        var $this = this;

        TweenLite.to($this.opt.element.find('.follow-button .line'), 1,{ x: '-100%'});
        TweenLite.to($this.opt.element.find('.follow-button'),.3,{ opacity: 1, delay:.5});


    },

    tween_line_exit : function(){
        var $this = this;

        TweenLite.set($this.opt.element.find('.follow-button .line'),{ x: '-100%'});
        TweenLite.to($this.opt.element.find('.follow-button'),.3,{ opacity: 0});


    },

    enter_longpress : function(){
        var $this = this;

        if(!$this.opt.mouseIsCenter){
           return;
        }
        $this.opt.enter_longpress = true;
        $('body').attr('data-swiper-transform','gsap');
        $this.$.swiperContainerV.attr('data-swiper-disable',false);
        $this.$.swiperContainerH.attr('data-swiper-disable',true);

        TweenMax.to($this.$.swiperContainerV,1,{ scale :.8, y:-(($(window).height()/2)), transformOrigin:"center center", ease: Power3.easeInOut});
        TweenMax.to($this.$.swiperContainerV.find('.swiper-slide-prev[data-post-slug]'),1,{ y: '10%',ease: Power3.easeInOut});
        TweenMax.to($this.$.swiperContainerV.find('.swiper-slide-next[data-post-slug]'),1,{ y: '-10%',ease: Power3.easeInOut});
        TweenMax.to($this.opt.element.find('.title-album'),.2,{ opacity:0});
        TweenMax.to($this.opt.element.find('.pagination'),.2, {opacity: 0});
        TweenMax.to($this.opt.element.find('.arrows-slider'),.2, {opacity: 0});
        TweenMax.to($this.$.swiperContainerV.find('[data-post-slug]'),1,{ opacity:.5,ease: Power3.easeInOut});

        TweenLite.set($this.opt.element.find('.title-swiper-container'), { css: {display: 'block', zIndex: 100}});
        TweenLite.set('.swiper-container-v .swiper-wrapper .container-bg',{ perspective : '500px' });
        TweenLite.set($this.opt.element.find('.swiper-slide .overlay'),{ x : '100%' });
        TweenLite.set($this.opt.element.find('.swiper-slide .overlay'), { css: {display: 'none'} });


        $('body').css({
            'cursor': '-webkit-grab'
        });

        $this.init_arrows();


        /*
        var blurElement = {a:0};//start the blur at 0 pixels
        TweenMax.to(blurElement, 1, {a:3, onUpdate:applyBlur});
        function applyBlur()
        {
            TweenMax.set($('.swiper-container-h'), {webkitFilter:"blur(" + blurElement.a + "px)"});
        };
        */

        $this.tween_enter_title_album();
    },

    exit_longpress : function(){
        var $this = this;
        $this.opt.enter_longpress = false;

        TweenMax.to($this.$.swiperContainerV,1,{ scale : 1, y:-(($(window).height()/2)), transformOrigin:"center center", ease: Power3.easeInOut});
        TweenMax.to($this.$.swiperContainerV.find('.swiper-slide[data-post-slug]'),1,{ y: '0%',ease: Power3.easeInOut, onComplete: function(){
            TweenLite.set($this.opt.element.find('.swiper-slide .overlay'), { css: {display: 'block'} });
            TweenMax.to($this.opt.element.find('.title-album'),.2,{ opacity:1});
            $this._events({
                reinit : true
            });
           setTimeout(function(){
               $this.$.currentAlbumSwiper.find('[data-image]').removeClass('no-load');
               $(gusto.frontend.image_preload.standard.event).trigger('image_preload::restart');
           },500);
        }});

        $('body').attr('data-swiper-transform','css');
        $this.$.swiperContainerV.attr('data-swiper-disable',true);
        $this.$.swiperContainerH.attr('data-swiper-disable',false);
        TweenMax.to($this.$.swiperContainerV.find('.swiper-slide'),1, {y :0, ease: Power4.easeInOut});
        TweenMax.to($this.$.swiperContainerV.find('.swiper-slide img'),1, {rotationX: 0, ease: Power3.easeInOut});
        TweenMax.to($this.opt.element.find(".pagination"),.2, {opacity: 1, ease: Power3.easeInOut});
        TweenMax.to($this.opt.element.find(".arrows-slider"),.2, {opacity: 1, ease: Power3.easeInOut});
        TweenMax.to($this.$.swiperContainerV.find('[data-post-slug]'),1,{ opacity:1,ease: Power3.easeInOut});



        $('body').css({
            'cursor': 'auto'
        });

        /*
        var blurElement = {a:3};
        TweenMax.to(blurElement, 1, {a:0, onUpdate:applyBlur});
        function applyBlur() { TweenMax.set($('.swiper-container-h'), {webkitFilter:"blur(" + blurElement.a + "px)"}); };
        */

        $this.tween_leave_title_album();



    },

    on_click_arrow : function(_this){
        var $this = this;

        $this.$.swiperContainerH = $this.opt.element.find('.swiper-container-v .swiper-slide-active .swiper-container-h');
        $this.opt.current_post_slug = $this.opt.element.find('.swiper-container-v .swiper-slide-active[data-post-slug]').attr('data-post-slug');
        if(!$this.opt.enter_longpress && !$this.opt.tween_slide){
            if(_this.hasClass('arrow-sx')){
                if($this.opt.swiperH[$this.opt.current_post_slug].activeIndex != 0) {
                    $this.opt.currentDirectionSwiper = 'left';
                    $this.opt.swiperH[$this.opt.current_post_slug].slidePrev();
                    $this.tween_slide($this.opt.swiperH[$this.opt.current_post_slug]);
                }
            }else{
                if($this.opt.swiperH[$this.opt.current_post_slug].activeIndex != $this.opt.swiperH[$this.opt.current_post_slug].slides.length-1) {
                    $this.opt.currentDirectionSwiper = 'right';
                    $this.opt.swiperH[$this.opt.current_post_slug].slideNext();
                    $this.tween_slide($this.opt.swiperH[$this.opt.current_post_slug]);
                }
            }
        }

    },

    on_hover_arrow : function(_this){
        var $this = this;

        TweenLite.to(_this.find('rect'),.5, {x : 0,y:0, ease: Power4.easeInOut} );

        if(_this.hasClass('sx')){
            TweenLite.to($this.opt.element.find('.arrow-item.sx'),.5, {x : -8, ease: Power4.easeInOut} );
        }else{
            TweenLite.to($this.opt.element.find('.arrow-item.dx'),.5, {x : 8, ease: Power4.easeInOut} );
        }
    },

    on_leave_arrow : function(){
        var $this = this;

        TweenLite.to($this.opt.element.find('rect.sx'),.5, {x : -30} );
        TweenLite.to($this.opt.element.find('rect.dx'),.5, {x : 30} );
        TweenLite.to($this.opt.element.find('.arrow-item.sx'),.5, {x : 0, ease: Power4.easeInOut} );
        TweenLite.to($this.opt.element.find('.arrow-item.dx'),.5, {x : 0, ease: Power4.easeInOut} );
    },

    on_touch_click_arrow : function(_this){
        var $this = this;
        var delta = -.05;

        TweenLite.to(_this.find('rect'),.5+delta, {x : 0,y:0, ease: Power4.easeInOut} );

        if(_this.hasClass('sx')){
            TweenLite.to($this.$.arrows.find('.arrow-item.sx'),.5+delta, {x : -20, ease: Power4.easeInOut, onComplete:
                function(){
                    TweenLite.to($this.$.arrowItem.find('rect.sx'),.5+delta, {x : 30} );
                    TweenLite.to($this.$.arrows.find('.arrow-item.sx'),.5+delta, {x : 0, ease: Power4.easeInOut} );

                }
            });
        }else{
            TweenLite.to($this.$.arrows.find('.arrow-item.dx'),.5+delta, {x : 20, ease: Power4.easeInOut, onComplete:
                function(){
                    TweenLite.to($this.$.arrowItem.find('rect.dx'),.5+delta, {x : -30} );
                    TweenLite.to($this.$.arrows.find('.arrow-item.dx'),.5+delta, {x : 0, ease: Power4.easeInOut} );
                }
            });
        }
    },

    /**
     * Enter/Leave
     */

    init_module : function(){
        var $this = this;
        var args  = {};



        if(!JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'))){
            args.targetUrl = $this.opt.element.attr('data-url');
            args.page = $this.opt.element.attr('data-page');
            $(gusto.frontend.plugin.pushstate.event).trigger('plugin::pushstate::set_storage',args);
        }

        if(Modernizr.smallscreen || $this.opt.force_all_white){
            $this.tween_init_transition();
            return;
        }

        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'));
        switch(pushstateArgs.page){
            case 'landing-commercial' :
            case 'landing-fashion' :
            case 'landing-projects' :
                $this.tween_init_transition_from_landing();
                break;
            default :
                $this.tween_init_transition();
                break;
        }

    },

    enter_module : function(){
        var $this = this;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'));

        $this.opt.enter_module = true;

        if(Modernizr.smallscreen || $this.opt.force_all_white){
            $this.tween_enter();
            return;
        }

        switch(pushstateArgs.page){
            case 'landing-commercial' :
            case 'landing-fashion' :
            case 'landing-projects' :
                $this.tween_enter_from_landing();
                break;
            default :
                $this.tween_enter();
                break;
        }
    },

    enter_page_title : function(){
        var $this = this;

        $this.opt.timelineTweenPageTitleEnter = new TimelineLite({
            onComplete: function(){
                $this.opt.pageTitleEnterComplete = true;
                if($this.opt.preloadImageComplete){
                    $(gusto).trigger('::enter');
                    $(gusto).trigger('page::load');
                }
            }
        });

        $this.opt.timelineTweenPageTitleEnter
            .to($('.page-title .text'),1,{ y: '0%', ease:Expo.easeInOut},0)
        ;

    },

    leave_module : function(){
        var $this = this;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));

        $this.opt.leave_module = true;


        if(Modernizr.smallscreen || $this.opt.force_all_white){
            $this.tween_leave();
            return;
        }

        switch(pushstateArgs.page){
            case 'landing-fashion' :
            case 'landing-commercial' :
            case 'landing-projects' :
                if($this.opt.element.back_history){
                    $this.tween_leave_to_landing();
                    break;
                }
            default :
                $this.tween_leave();
                break;
        }
    },

    tween_leave : function(args){
        var $this = this;
        TweenMax.set($this.opt.element.find('.title-page-transition .bg'),{ y : '100%' });

        $this.opt.timelineTweenPageLeave = new TimelineLite({ onComplete : tweenPageLeaveComplete});
        $this.opt.timelineTweenPageLeave
            .to($this.opt.element.find('.swiper-slide .overlay'),1.2,{ x: '0%', ease:Power4.easeInOut, onComplete : function(){
                TweenMax.set($this.opt.element.find('.swiper-slide'),{ visibility : 'hidden'});
            }},0)
            .to($this.opt.element.find('.title-page-transition .bg'),.5,{ y : '0%', ease:Power4.easeInOut, onComplete : function(){

            }},0)
        ;

        /**
         * Callback
         */
        function tweenPageLeaveComplete(){

            if(args !== undefined){
                if(args.force){
                    return;
                }
            }
            if($('body').data('plugin::pushstate') !== undefined){
                if($('body').data('plugin::pushstate').ajax_state != undefined){
                    if($('body').data('plugin::pushstate').ajax_state != 'success'){
                        $(gusto.frontend.plugin.pushstate.event).on({
                            'plugin::pushstate::success-ajax' : function(){
                                $this.page_next_init();
                            }
                        });
                    }else{
                        $this.page_next_init();
                    }
                }else{
                    $this.page_next_init();
                }
            }else{
                $this.page_next_init();

            }
        }
    },

    tween_init_transition : function(){
        var $this = this;


    },

    tween_enter : function(){
        var $this = this;

        var padding_title = parseInt($this.opt.element.find('.title-page-transition .title').css('padding-left'));

        TweenMax.set($this.opt.element.find('.swiper-slide-active'),{ visibility : 'visible'});
        TweenMax.set($this.opt.element.find('.swiper-container-h .swiper-slide'),{ visibility : 'visible'});


        TweenMax.to($('.page-title'),.6,{ height: '0px', ease:Expo.easeInOut, onComplete: function(){
            $('.page-title').remove();
        }});

        setTimeout(function(){

            $this.opt.timelineTweenPageEnter = new TimelineLite({ onComplete : tweenPageEnterComplete});
            $this.opt.timelineTweenPageEnter
                .to($this.opt.element.find('.title-page-transition .bg'),1.7,{ x: 0, ease:Expo.easeInOut},0)
                .to($this.opt.element.find('.title-page-transition .title-text'),1.7,{ x: -padding_title, ease:Power4.easeInOut},1.5)
                .to($this.opt.element.find('.swiper-slide-active .overlay'),1.5,{ x: '100%', ease:Power4.easeInOut},.4)
                .to($this.opt.element.find('.swiper-slide-active'),1.7,{ x: '0%', ease:Power2.easeOut},0)
            ;

            function tweenPageEnterComplete(){


            }

            $this.load_album();

        }, 600);



    },

    page_next_init : function(){
        var $this = this;
        $(gusto).trigger('plugin::set_page::next');

    }

};/**
 * Slider - Swiper
 * @swiperin
 * @description : Swiper type slider
 * @version 1.0
 * @author  William Manco
 * @copyright Gusto IDS
 * @param options
 * @returns {Slider}
 */
"undefined" == typeof Gusto && (Gusto = {}, gusto = {});
"undefined" == typeof Gusto.Frontend && (Gusto.Frontend = {}, gusto.frontend = {});
"undefined" == typeof Gusto.Frontend.Slider && (Gusto.Frontend.Slider = {}, gusto.frontend.slider = {});
"undefined" == typeof Gusto.Frontend.Slider.Swiper && (Gusto.Frontend.Slider.Swiper = {}, gusto.frontend.slider.swiper = {});


Gusto.Frontend.Slider.Swiper = function Gusto(id){
    this.opt = {
        selector_id : "#module-"+id,
        selector: "[data-module='slider'][data-id='"+id+"']",
        element : $('[data-module="slider"][data-type="swiper"]'),
        module : 'slider',
        type : 'swiper',
        force_all_white : true
    };

    this.$ = {
        swiperContainerH        : this.opt.element.find('.swiper-container-h'),
        swiperContainerV        : this.opt.element.find('.swiper-container-v'),
        arrows                  : this.opt.element.find('.arrows-slider'),
        arrowItem               : this.opt.element.find('.arrows-slider .arrow-item'),
        followButton            : this.opt.element.find('.follow-button')
    };

    this._init();
    this._events({
        reinit : false
    });
    this._events({
        reinit : true
    });

    return this;
};

Gusto.Frontend.Slider.Swiper.prototype = {

    _init : function(){
        var $this = this;

        $this.opt.swiperV = null;
        $this.opt.swiperH = null;

        $this.init_sizes();
        $this.init_slides();
        $this.init_swiper();
        $this.init_module();
        $this.init_area();
        $this.init_arrows();

    },

    _events : function(args){
        var $this = this;

        if(args.reinit){

            $this.opt.element.find('.arrows-slider .arrow-area').off().on({
                'click' : function(){
                    var _this = $(this);
                    $this.on_click_arrow(_this);
                    if(Modernizr.touch){
                        $this.on_touch_click_arrow(_this.find('.arrow-item'));
                    }
                },
                'mouseenter' : function(){
                    var _this = $(this);
                    if(Modernizr.touch){
                        return;
                    }
                    $this.on_hover_arrow(_this.find('.arrow-item'));
                },
                'mouseleave' : function(){
                    var _this = $(this);
                    if(Modernizr.touch){
                        return;
                    }
                    $this.on_leave_arrow(_this.find('.arrow-item'));
                }
            });
            return;
        }


        $(gusto.frontend.image_preload.standard.event).on({
            'image_preload::all': function(){

                if($this.opt.loading_all){
                    return;
                }

                if($this.opt.load_album_first_slide){
                    $this.opt.loading_first = true;
                    $this.$.followButton.removeClass('is-hide');
                    return;
                }

                if($this.opt.load_album){
                    $this.opt.loading_all = true;
                    return;
                }


                if(!$this.opt.enter_module){
                    if($this.opt.pageTitleEnterComplete) {
                        $(gusto).trigger('::enter');
                        $(gusto).trigger('page::load');
                    }
                    $this.opt.preloadImageComplete = true;
                }

            }
        });

        $(gusto.frontend.plugin.pushstate.event).on({

            'plugin::pushstate::before-ajax' : function(){
                $this.leave_module();
            }

        });

        $(gusto).on({
            'plugin::longpress::end' : function(){
                if($this.$.followButton.hasClass('is-hide')){
                    return;
                }
                $this.enter_longpress();
                $this.tween_line_exit();

            },
            'plugin::longpress::clear' : function(){
                $this.exit_longpress();
                $this.tween_line_back();
            },
            'plugin::longpress::click' : function(e,args){
                $this.opt.longpress_click_y = args.pageY;
                $this.tween_line_start();
            },
            '::enter' : function (e,args) {
                if(args !== undefined) {
                    if (args.force) {
                        $this.tween_enter();
                        return;
                    }
                }
               $this.enter_module();
            },
            '::leave' : function(e,args){
                if(args !== undefined){
                    if( args.force){
                        $this.tween_leave(args);
                        return;
                    }
                }
                $this.leave_module();
            },
            'resize' : function(){
                $this.init_sizes();
                $this.opt.swiperH[$this.opt.current_post_slug].update(true);
                TweenMax.set($this.$.swiperContainerV.find('.swiper-slide-active .swiper-wrapper'), { x: $this.opt.swiperH[$this.opt.current_post_slug].translate});

            }
        });

        $('body').on({
            'mousemove' : function(event){

                if($this.opt.enter_longpress){

                    cx = Math.ceil($(window).width() / 2);
                    cy = Math.ceil($(window).height() / 2);
                    dx = (event.pageX - cx);
                    dy = (event.pageY - cy) - $(window).scrollTop();
                    tiltx = (dy / cy);
                    tilty = - (dx / cx);
                    radius = Math.sqrt(Math.pow(tiltx,2) + Math.pow(tilty,2));
                    degree = (radius * 3);

                    $(".swiper-container-v .swiper-slide-active").each(function(){
                        var _this = $(this);
                        TweenLite.to(_this,.5, {y :  (event.pageY - $this.opt.longpress_click_y)/2});
                        TweenLite.to(_this.find('img'),.5, {rotationX: degree*Math.sign(tiltx)});
                    });
                }else{
                    var width_current_slide = $('.swiper-slide-active .swiper-container-h .swiper-slide-active .container-bg').width()/2;
                    if(event.pageY > $('.site-header').height() && event.pageY < ($(window).height() - $('.site-header').height()) ){
                        $this.opt.labelHide = false;

                        TweenLite.to($this.opt.element.find('.follow-button'),.5,{ x: event.pageX, y: event.pageY});

                        if(event.pageX > ($(window).width()/2)+width_current_slide){

                            if(!$this.opt.mouseIsRight){

                                TweenMax.to($this.opt.element.find('.label-prev'),.7,{ y : '100%', ease: Power4.easeInOut });
                                TweenMax.to($this.opt.element.find('.label-center'),.7,{ y : '100%', ease: Power4.easeInOut });
                                TweenMax.to($this.opt.element.find('.label-next'),.7,{ y : '0%', ease: Power4.easeInOut });

                                $this.opt.mouseIsRight = true;
                                $this.opt.mouseIsLeft = false;
                                $this.opt.mouseIsCenter = false;

                            }

                        }else if(event.pageX < ($(window).width()/2)-width_current_slide){

                            if(!$this.opt.mouseIsLeft){

                                TweenMax.to($this.opt.element.find('.label-prev'),.7,{ y : '0%', ease: Power4.easeInOut });
                                TweenMax.to($this.opt.element.find('.label-center'),.7,{ y : '100%', ease: Power4.easeInOut });
                                TweenMax.to($this.opt.element.find('.label-next'),.7,{ y : '100%', ease: Power4.easeInOut });

                                $this.opt.mouseIsLeft = true;
                                $this.opt.mouseIsRight = false;
                                $this.opt.mouseIsCenter = false;
                            }
                        }else{

                            if(!$this.opt.mouseIsCenter) {
                                TweenMax.to($this.opt.element.find('.label-prev'),.7,{ y : '100%', ease: Power4.easeInOut });
                                TweenMax.to($this.opt.element.find('.label-center'),.7,{ y : '0%', ease: Power4.easeInOut });
                                TweenMax.to($this.opt.element.find('.label-next'),.7,{ y : '100%', ease: Power4.easeInOut });

                                $this.opt.mouseIsCenter = true;
                                $this.opt.mouseIsLeft = false;
                                $this.opt.mouseIsRight = false;
                            }
                        }
                    }else {
                        if(!$this.opt.labelHide){
                            TweenMax.to($this.opt.element.find('.follow-button .label'),.7,{ y : '100%', ease: Power4.easeInOut });
                            $this.opt.mouseIsCenter = false;
                            $this.opt.mouseIsLeft = false;
                            $this.opt.mouseIsRight = false;
                            $this.opt.labelHide = true;
                        }


                    }

                }
            },
            'click' : function(event){
                $this.$.swiperContainerH = $this.opt.element.find('.swiper-container-v .swiper-slide-active .swiper-container-h');
                $this.opt.current_post_slug = $this.opt.element.find('.swiper-container-v .swiper-slide-active[data-post-slug]').attr('data-post-slug');

                if(!$this.opt.enter_longpress && !$this.opt.tween_slide){

                    var width_current_slide = $('.swiper-slide-active .swiper-container-h .swiper-slide-active .container-bg').width()/2;
                    if(event.pageX > ($(window).width()/2)+width_current_slide && event.pageY > $('.site-header').height()){
                        if($this.opt.swiperH[$this.opt.current_post_slug].activeIndex != $this.opt.swiperH[$this.opt.current_post_slug].slides.length-1){
                            $this.opt.currentDirectionSwiper = 'right';
                            $this.opt.swiperH[$this.opt.current_post_slug].slideNext();
                            $this.tween_slide($this.opt.swiperH[$this.opt.current_post_slug]);
                        }
                    }else if(event.pageX < ($(window).width()/2)-width_current_slide && event.pageY > $('.site-header').height()){
                        if($this.opt.swiperH[$this.opt.current_post_slug].activeIndex != 0) {
                            $this.opt.currentDirectionSwiper = 'left';
                            $this.opt.swiperH[$this.opt.current_post_slug].slidePrev();
                            $this.tween_slide($this.opt.swiperH[$this.opt.current_post_slug]);
                        }
                    }
                }
            }
        });

        $(document).ready(function(){
            $this.enter_page_title();
        });
    },

    init_arrows : function(){
        var $this = this;

        TweenLite.set($this.opt.element.find('.arrow-dx-item rect'),{ x: 20, y: 0});
        TweenLite.set($this.opt.element.find('.arrow-dx-item polygon'),{ x: 14.5});
        TweenLite.set($this.opt.element.find('.arrow-sx-item rect'),{ x: -20,y: 0});
    },

    init_sizes : function(){
        var $this = this;
        $('.site-main').css({
            overflow : 'hidden',
            height : $(window).height()+'px'
        });
        $this.$.swiperContainerV.height($(window).height()*2);
        TweenMax.set($this.$.swiperContainerV,{ scale : 1, y:-(($(window).height()/2)), transformOrigin:"center center"});
        TweenLite.set($this.opt.element, { css: {overflow: 'hidden',height: $(window).height() ,width : $(window).width()}});

        if($this.opt.swiperV){
            $this.opt.swiperV.update(true);
        }

        if($this.opt.swiperH) {
            $.each( $this.opt.swiperH, function( key, value ) {
                value.update(true);
            });
        }

    },

    init_slides : function(){
        var $this = this;
        $this.opt.slide_append = false;
        $this.opt.template_slide_container = $this.$.swiperContainerH.closest('.swiper-slide').clone()[0];
        $this.opt.template_slide = $('.swiper-container-h .swiper-slide').clone()[0];
        $this.opt.current_post_slug = $('.swiper-slide').attr('data-post-slug');
        $this.$.swiperContainerV.attr('data-swiper-disable',true);
        $this.$.swiperContainerH.attr('data-swiper-disable',false);

        $(gusto.frontend.image_preload.standard.event).trigger('image_preload::restart');
    },

    init_swiper : function(){
        var $this = this;

        $this.opt.swiperH = new Object();
        $this.opt.swiperV = new Swiper('.swiper-container-v', {
            direction: 'vertical',
            slidesPerView: 2,
            touchRatio : 3,
            speed : 800,
            centeredSlides: true,
            disable : true,
            onSlideChangeStart : function(swiper) {
                $this.opt.swiperV.activeIndex = swiper.activeIndex;
                $this.$.currentAlbumSwiper = $this.opt.element.find('.swiper-container-v .swiper-slide-active[data-post-slug]');
                $this.tween_title_album({});
                $this.init_pagination();
                $this.tween_pagination();
            },
            onTouchMove : function(swiper, e){
                var y = (swiper.translate /swiper.virtualSize)*($this.$.titleSwiperContent.height()*$this.$.titleSwiperContent.length);
                $this.tween_title_album({y : y});
            }
        });

        TweenMax.set($this.$.arrows.find('.arrow-sx'),{ opacity : 0});
        TweenMax.set($this.$.followButton.find('.label-prev'),{ opacity : 0});


        $this.opt.args_swiper_h = {
            speed : 1200,
            simulateTouch : false,
            virtualTranslate : true,
            onSlideChangeStart : function(swiper) {
                $this.$.swiperContainerH = $this.opt.element.find('.swiper-container-v .swiper-slide-active .swiper-container-h');
                $this.opt.current_post_slug = $this.opt.element.find('.swiper-container-v .swiper-slide-active').attr('data-post-slug');

                if(swiper.activeIndex >= swiper.slides.length-1){
                    TweenMax.to($this.$.arrows.find('.arrow-dx'),.5,{ opacity : 0});
                    TweenMax.to($this.$.followButton.find('.label-next'),.5,{ opacity : 0});

                }else{
                    TweenMax.to($this.$.arrows.find('.arrow-dx'),.5,{ opacity : 1});
                    TweenMax.to($this.$.followButton.find('.label-next'),.5,{ opacity : 1});
                }

                if(swiper.activeIndex > 0){
                    TweenMax.to($this.$.arrows.find('.arrow-sx'),.5,{ opacity : 1});
                    TweenMax.to($this.$.followButton.find('.label-prev'),.5,{ opacity : 1});
                }else{
                    TweenMax.to($this.$.arrows.find('.arrow-sx'),.5,{ opacity : 0});
                    TweenMax.to($this.$.followButton.find('.label-prev'),.5,{ opacity : 0});

                }
            }
        };

        if(Modernizr.touch){
            $this.opt.args_swiper_h.speed = 300;
            $this.opt.args_swiper_h.simulateTouch = true;
            $this.opt.args_swiper_h.virtualTranslate = false;
            $this.opt.args_swiper_h.onSlideChangeEnd = function (){
                $this.tween_pagination();

            }

        }




    },

    init_area : function(){
        var $this = this;

        TweenLite.set($this.opt.element.find('.title-swiper-container'), { css: {display: 'none'}});
    },

    load_album : function(){
        var $this = this;

        $.ajax({
            url : ajaxurl,
            type : 'post',
            data : {
                action : 'api_get_taxonomy_meta',
                post_type : '_cty_album',
                taxonomy : '_cta_types',
                terms : $this.opt.element.attr('data-current-term')
            },
            success : function( response ) {
                $this.opt.response_taxonomy_meta = JSON.parse(response);
                //$this.opt.load_album = true;
                $this.opt.load_album_first_slide = true;
                $this.create_album();
                $this.create_slides();
                $this.create_title_album();
                $this.tween_title_album({});
                $this.tween_leave_title_album();
            }
        });
    },

    init_pagination : function(){
        var $this = this;

        $this.opt.current_post_slug = $this.opt.element.find('.swiper-container-v .swiper-slide-active[data-post-slug]').attr('data-post-slug');

        var current_post_slug = $this.opt.current_post_slug;
        var slides_length  = $this.opt.swiperH[current_post_slug].slides.length;
        var current_index = $this.opt.swiperH[current_post_slug].activeIndex;
        var index = 0;

        $this.$.currentElementSwiperH = $this.opt.element.find('.swiper-container-v .swiper-slide-active .swiper-container-h');
        $this.$.currentElementSwiperH.find('.pagination').html('<div class="current-index"></div><div class="separator">/</div><div class="slides-length">'+slides_length+'</div>');
        $this.$.currentElementSwiperH.find('.current-index').append('<div class="item-index"><div class="index">'+current_index+'</div></div>');

        TweenMax.to($this.opt.element.find('.pagination'),.2, {opacity:1});
        TweenMax.to($this.opt.element.find('.arrows-slider'),.2, {opacity:1});
    },

    create_album : function(){
        var $this = this;
        var arr_prepend = [];
        var arr_append = [];

        if(Modernizr.touch){
            TweenMax.set($this.$.swiperContainerH.find('.swiper-slide .overlay'), { css: { display: 'none'}});

            for (var i = 0; i < $this.opt.response_taxonomy_meta.length; i++) {
                var item = $this.opt.response_taxonomy_meta[i];

                if(item.post_slug == $this.opt.current_post_slug){
                    $this.opt.slide_append = true;
                    $this.opt.swiperH[$this.opt.current_post_slug] = new Swiper('.swiper-container-v [data-post-slug="'+$this.opt.current_post_slug+'"] .swiper-container-h',$this.opt.args_swiper_h);
                    $this.init_pagination();
                    $this.tween_pagination();
                }
            }
            return;
        }


        for (var i = 0; i < $this.opt.response_taxonomy_meta.length; i++) {

            var item = $this.opt.response_taxonomy_meta[i];
            var args_swiper = $this.opt.args_swiper_h;

            /**
             * Cambia i dati sul template dello slide (album)
             */
            if(Array.isArray($this.opt.response_taxonomy_meta[i]['metadata']) && item.post_slug != $this.opt.current_post_slug) {
                $($this.opt.template_slide_container).find('.title').text($this.opt.response_taxonomy_meta[i]['metadata'][0].title);
                $($this.opt.template_slide_container).find('.container-bg [data-image]').attr('data-image', $this.opt.response_taxonomy_meta[i]['metadata'][0].img_large);
                $($this.opt.template_slide_container).attr('data-post-slug', $this.opt.response_taxonomy_meta[i].post_slug);
                $($this.opt.template_slide_container).attr('data-post-title', $this.opt.response_taxonomy_meta[i].title);
            }

            /**
             * Clona il template dello slider verticale (quindi gli album)
             * rimuove le slide che verranno aggiunte dopo
             */
            var clone = $($this.opt.template_slide_container).clone();
            clone.find('.swiper-slide').remove();

            /**
             * Se l'album da creare è quello della pagina quindi già esistente
             * crea lo slider e vai avanti passando al prossimo turno
             */
            if(item.post_slug == $this.opt.current_post_slug){
                $this.opt.slide_append = true;
                $this.opt.swiperH[$this.opt.current_post_slug] = new Swiper('.swiper-container-v [data-post-slug="'+$this.opt.current_post_slug+'"] .swiper-container-h',$this.opt.args_swiper_h);
                $this.init_pagination();
                $this.tween_pagination();
                if(!arr_prepend.isEmpty()){
                    $this.opt.swiperV.prependSlide(arr_prepend.reverse());
                }
                continue;
            }
            /**
             * Appende gli album
             */
            if($this.opt.slide_append){
                arr_append.push(clone.get(0));
            }else{
                arr_prepend.push(clone.get(0));
            }

            if($this.opt.slide_append && arr_append.length > 0){
                $this.opt.swiperV.appendSlide(arr_append);
            }

        }

        for (var i = 0; i < $this.opt.response_taxonomy_meta.length; i++) {
            var item = $this.opt.response_taxonomy_meta[i];
            var args_swiper = $this.opt.args_swiper_h;
            /**
             * Attiva lo slider su ogni album  (tranne quello corrente)
             */
            $this.opt.swiperH[item.post_slug] = new Swiper('.swiper-container-v [data-post-slug="'+item.post_slug+'"] .swiper-container-h',args_swiper);
        }




    },

    create_slides : function(){
        var $this = this;

        for (var i = 0; i < $this.opt.response_taxonomy_meta.length; i++) {

            var item = $this.opt.response_taxonomy_meta[i];
            var post_slug = item.post_slug;


            if(item.post_slug == $this.opt.current_post_slug){
                $this.opt.slide_append = true;
                continue;
            }

            //$this.opt.response_taxonomy_meta[i]['metadata'].splice(0, 1);
            if(Array.isArray($this.opt.response_taxonomy_meta[i]['metadata'])){
                $this.opt.response_taxonomy_meta[i]['metadata'].forEach(function(item){
                    $($this.opt.template_slide).find('.title').text(item.title);
                    $($this.opt.template_slide).find('.container-bg [src]').attr('src',item.img_thumbnail);
                    $($this.opt.template_slide).find('.container-bg [data-image]').attr('data-image',item.img_large);
                    $($this.opt.template_slide).find('.container-bg [data-image]').addClass('no-load');
                    $this.opt.swiperH[post_slug].appendSlide($($this.opt.template_slide).clone()[0]);
                    $this.opt.only_first_slide = true;
                });
            }

        }

        $this.$.swiperContainerH.find('.swiper-slide').css({
            position : 'relative'
        });


        TweenMax.set($this.opt.element.find('.swiper-container-h .swiper-slide'),{ visibility : 'visible'});

        $this.opt.element.find('.swiper-container-h').each(function(){
            $(this).find('.swiper-slide-active [data-image]').removeClass('no-load');
        });

        $(gusto.frontend.image_preload.standard.event).trigger('image_preload::restart');

    },

    create_title_album : function () {
        var $this = this;

        if($this.opt.element.find('.title-swiper-container').length > 0){
            return;
        }

        $this.opt.element.append('<div class="title-swiper-container"><div class="title-swiper-content"></div></div>');

        $this.$.titleSwiperContainer = $this.opt.element.find('.title-swiper-container');
        $this.$.titleSwiperContent = $this.opt.element.find('.title-swiper-content');

        $('.swiper-slide[data-post-slug]').each(function(){
            var _this = $(this);
            $this.$.titleSwiperContent.append('<div class="title" data-slug="'+_this.attr("data-post-slug")+'">'+_this.attr("data-post-title")+'</div>');
            var text_title_album = $(this).attr('data-post-title');
            $(this).find('.title-album').html(text_title_album);
        });

        TweenMax.to($this.opt.element.find('.title-album'),.7,{ opacity:1, ease:Power2.easeInOut});

        var height_item = $('.title-swiper-container .title').height();
        $this.$.titleSwiperContainer.css({
            height : height_item*3,
            overflow : 'hidden'
        });
        $this.$.titleSwiperContent.css({
            marginTop: height_item
        });


    },

    tween_title_album : function(args){
        var $this = this;

        var album_current = $this.opt.swiperV.activeIndex;

        var step = $('.title-swiper-container .title').height();

        var to_title = 0;
        if(album_current != 0){
            to_title = step*album_current;
        }

        if('y' in args){
            TweenMax.to($this.$.titleSwiperContent,.8,{ y:args.y - step/2 });
        }else{
            TweenMax.to($this.$.titleSwiperContent,.6,{ y:-(to_title), ease: Power3.easeOut});
        }

        var current_title = $('.title-swiper-container .title').eq($this.opt.swiperV.activeIndex);
        var all_title = $('.title-swiper-container .title').not(current_title);

        TweenMax.to(current_title,.6,{ opacity:1, ease: Power1.easeOut});
        TweenMax.to(all_title,.6,{ opacity:.5, ease: Power1.easeOut});

        all_title.removeClass('active');
        current_title.addClass('active');

    },

    tween_enter_title_album : function(){
        var $this = this;

        TweenLite.set($this.opt.element.find('.title-swiper-container'), { css: {display: 'block'}});
        TweenMax.set($this.$.titleSwiperContainer.find('.title'),{ opacity:1});

        TweenMax.to($this.$.titleSwiperContainer,.8,{ opacity:1, ease: Power1.easeOut,delay : .3});
        TweenMax.to($this.$.titleSwiperContainer,1,{ scale: 1, x: 0, ease: Power3.easeInOut});

    },

    tween_leave_title_album : function(){
        var $this = this;
        TweenMax.to($this.$.titleSwiperContainer.find('.title').not('.active'),.8,{ opacity:0, ease: Power2.easeInOut});

        setTimeout(function(){
            TweenMax.to($this.$.titleSwiperContainer,.3,{ opacity:0,scale: 1.1,x: -20,  ease: Power1.easeOut, delay:.5, onComplete: function(){
                TweenLite.set($this.opt.element.find('.title-swiper-container'), { css: {display: 'none', zIndex: 0}});

            }});
        },350);

    },

    tween_slide : function (swiper) {
        var $this = this;

        $this.opt.tween_slide = true;

        $this.$.swiperContainerH = $this.opt.element.find('.swiper-container-v .swiper-slide-active .swiper-container-h');
        $this.opt.current_post_slug = $this.opt.element.find('.swiper-container-v .swiper-slide-active[data-post-slug]').attr('data-post-slug');

        if(Modernizr.smallscreen) {



        }else{
            TweenMax.set($this.$.swiperContainerV.find('.swiper-slide-active .swiper-wrapper'), { x: $this.opt.swiperH[$this.opt.current_post_slug].translate});
            TweenMax.set($this.$.swiperContainerH.find('.swiper-slide-active .overlay'), { x: '0%'});
            TweenMax.to($this.$.swiperContainerH.find('.swiper-slide-active .overlay'),1.5,{  x: '100%', ease: Power3.easeInOut });


            if(!Modernizr.safari){
                TweenMax.set($this.$.swiperContainerH.find('.swiper-slide').not('.swiper-slide-active'),{x: 0});
                TweenMax.set($this.$.swiperContainerH.find('.swiper-slide'), { perspective: '800px'});
                TweenMax.to($this.$.swiperContainerH.find('.swiper-slide-prev img'),1,{ rotationY: "90_counter-clockwise", ease: Power1.easeIn ,onComplete: function(){
                    TweenMax.set($this.$.swiperContainerH.find('.swiper-slide-prev img'), {rotationY: "0"});
                } });
                TweenMax.set($this.$.swiperContainerH.find('.swiper-slide-active img'), {rotationY: "0"});
            }
        }


        if($this.opt.currentDirectionSwiper == 'right'){
            if(Modernizr.safari){
                $this.opt.tween_slide = false;
            }else{
                TweenMax.from($this.$.swiperContainerH.find('.swiper-slide-active'),1,{ x: '50%', ease: Power3.easeOut,
                    onComplete : function(){
                        $this.opt.tween_slide = false;
                    }
                });
            }

        }else{
            if(Modernizr.safari){
                $this.opt.tween_slide = false;
            }else{
                TweenMax.from($this.$.swiperContainerH.find('.swiper-slide-active'),1,{ x: '-50%', ease: Power3.easeOut,
                    onComplete : function(){
                        $this.opt.tween_slide = false;
                    }
                });
            }

        }

        $this.tween_pagination();
    },

    tween_pagination : function () {
        var $this = this;

        var current_post_slug = $this.opt.current_post_slug;
        var current_index = $this.opt.swiperH[current_post_slug].activeIndex;
        var current_index_string = current_index+1;

        var $current_item_index = $this.$.swiperContainerV.find('.swiper-slide-active .pagination .item-index');

        TweenLite.to($current_item_index, .6,{ opacity: 0, ease: Power4.easeIn, onComplete: function(){
            $current_item_index.find('.index').text(current_index_string);
            TweenLite.to($current_item_index, .7,{  opacity: 1, ease: Power4.easeInOut});
        }});
    },

    tween_line_start : function(){
        var $this = this;

        TweenLite.to($this.opt.element.find('.follow-button .line'), 1,{ x: '0%'});

    },

    tween_line_back : function(){
        var $this = this;

        TweenLite.to($this.opt.element.find('.follow-button .line'), 1,{ x: '-100%'});
        TweenLite.to($this.opt.element.find('.follow-button'),.3,{ opacity: 1, delay:.5});


    },

    tween_line_exit : function(){
        var $this = this;

        TweenLite.set($this.opt.element.find('.follow-button .line'),{ x: '-100%'});
        TweenLite.to($this.opt.element.find('.follow-button'),.3,{ opacity: 0});


    },

    enter_longpress : function(){
        var $this = this;

        if(!$this.opt.mouseIsCenter){
           return;
        }
        $this.opt.enter_longpress = true;
        $('body').attr('data-swiper-transform','gsap');
        $this.$.swiperContainerV.attr('data-swiper-disable',false);
        $this.$.swiperContainerH.attr('data-swiper-disable',true);

        TweenMax.to($this.$.swiperContainerV,1,{ scale :.8, y:-(($(window).height()/2)), transformOrigin:"center center", ease: Power3.easeInOut});
        TweenMax.to($this.$.swiperContainerV.find('.swiper-slide-prev[data-post-slug]'),1,{ y: '10%',ease: Power3.easeInOut});
        TweenMax.to($this.$.swiperContainerV.find('.swiper-slide-next[data-post-slug]'),1,{ y: '-10%',ease: Power3.easeInOut});
        TweenMax.to($this.opt.element.find('.title-album'),.2,{ opacity:0});
        TweenMax.to($this.opt.element.find('.pagination'),.2, {opacity: 0});
        TweenMax.to($this.opt.element.find('.arrows-slider'),.2, {opacity: 0});
        TweenMax.to($this.$.swiperContainerV.find('[data-post-slug]'),1,{ opacity:.5,ease: Power3.easeInOut});

        TweenLite.set($this.opt.element.find('.title-swiper-container'), { css: {display: 'block', zIndex: 100}});
        TweenLite.set('.swiper-container-v .swiper-wrapper .container-bg',{ perspective : '500px' });
        TweenLite.set($this.opt.element.find('.swiper-slide .overlay'),{ x : '100%' });
        TweenLite.set($this.opt.element.find('.swiper-slide .overlay'), { css: {display: 'none'} });


        $('body').css({
            'cursor': '-webkit-grab'
        });

        $this.init_arrows();


        /*
        var blurElement = {a:0};//start the blur at 0 pixels
        TweenMax.to(blurElement, 1, {a:3, onUpdate:applyBlur});
        function applyBlur()
        {
            TweenMax.set($('.swiper-container-h'), {webkitFilter:"blur(" + blurElement.a + "px)"});
        };
        */

        $this.tween_enter_title_album();
    },

    exit_longpress : function(){
        var $this = this;
        $this.opt.enter_longpress = false;

        TweenMax.to($this.$.swiperContainerV,1,{ scale : 1, y:-(($(window).height()/2)), transformOrigin:"center center", ease: Power3.easeInOut});
        TweenMax.to($this.$.swiperContainerV.find('.swiper-slide[data-post-slug]'),1,{ y: '0%',ease: Power3.easeInOut, onComplete: function(){
            TweenLite.set($this.opt.element.find('.swiper-slide .overlay'), { css: {display: 'block'} });
            TweenMax.to($this.opt.element.find('.title-album'),.2,{ opacity:1});
            $this._events({
                reinit : true
            });
           setTimeout(function(){
               $this.$.currentAlbumSwiper.find('[data-image]').removeClass('no-load');
               $(gusto.frontend.image_preload.standard.event).trigger('image_preload::restart');
           },500);
        }});

        $('body').attr('data-swiper-transform','css');
        $this.$.swiperContainerV.attr('data-swiper-disable',true);
        $this.$.swiperContainerH.attr('data-swiper-disable',false);
        TweenMax.to($this.$.swiperContainerV.find('.swiper-slide'),1, {y :0, ease: Power4.easeInOut});
        TweenMax.to($this.$.swiperContainerV.find('.swiper-slide img'),1, {rotationX: 0, ease: Power3.easeInOut});
        TweenMax.to($this.opt.element.find(".pagination"),.2, {opacity: 1, ease: Power3.easeInOut});
        TweenMax.to($this.opt.element.find(".arrows-slider"),.2, {opacity: 1, ease: Power3.easeInOut});
        TweenMax.to($this.$.swiperContainerV.find('[data-post-slug]'),1,{ opacity:1,ease: Power3.easeInOut});



        $('body').css({
            'cursor': 'auto'
        });

        /*
        var blurElement = {a:3};
        TweenMax.to(blurElement, 1, {a:0, onUpdate:applyBlur});
        function applyBlur() { TweenMax.set($('.swiper-container-h'), {webkitFilter:"blur(" + blurElement.a + "px)"}); };
        */

        $this.tween_leave_title_album();



    },

    on_click_arrow : function(_this){
        var $this = this;

        $this.$.swiperContainerH = $this.opt.element.find('.swiper-container-v .swiper-slide-active .swiper-container-h');
        $this.opt.current_post_slug = $this.opt.element.find('.swiper-container-v .swiper-slide-active[data-post-slug]').attr('data-post-slug');
        if(!$this.opt.enter_longpress && !$this.opt.tween_slide){
            if(_this.hasClass('arrow-sx')){
                if($this.opt.swiperH[$this.opt.current_post_slug].activeIndex != 0) {
                    $this.opt.currentDirectionSwiper = 'left';
                    $this.opt.swiperH[$this.opt.current_post_slug].slidePrev();
                    $this.tween_slide($this.opt.swiperH[$this.opt.current_post_slug]);
                }
            }else{
                if($this.opt.swiperH[$this.opt.current_post_slug].activeIndex != $this.opt.swiperH[$this.opt.current_post_slug].slides.length-1) {
                    $this.opt.currentDirectionSwiper = 'right';
                    $this.opt.swiperH[$this.opt.current_post_slug].slideNext();
                    $this.tween_slide($this.opt.swiperH[$this.opt.current_post_slug]);
                }
            }
        }

    },

    on_hover_arrow : function(_this){
        var $this = this;

        TweenLite.to(_this.find('rect'),.5, {x : 0,y:0, ease: Power4.easeInOut} );

        if(_this.hasClass('sx')){
            TweenLite.to($this.opt.element.find('.arrow-item.sx'),.5, {x : -8, ease: Power4.easeInOut} );
        }else{
            TweenLite.to($this.opt.element.find('.arrow-item.dx'),.5, {x : 8, ease: Power4.easeInOut} );
        }
    },

    on_leave_arrow : function(){
        var $this = this;

        TweenLite.to($this.opt.element.find('rect.sx'),.5, {x : -30} );
        TweenLite.to($this.opt.element.find('rect.dx'),.5, {x : 30} );
        TweenLite.to($this.opt.element.find('.arrow-item.sx'),.5, {x : 0, ease: Power4.easeInOut} );
        TweenLite.to($this.opt.element.find('.arrow-item.dx'),.5, {x : 0, ease: Power4.easeInOut} );
    },

    on_touch_click_arrow : function(_this){
        var $this = this;
        var delta = -.05;

        TweenLite.to(_this.find('rect'),.5+delta, {x : 0,y:0, ease: Power4.easeInOut} );

        if(_this.hasClass('sx')){
            TweenLite.to($this.$.arrows.find('.arrow-item.sx'),.5+delta, {x : -20, ease: Power4.easeInOut, onComplete:
                function(){
                    TweenLite.to($this.$.arrowItem.find('rect.sx'),.5+delta, {x : 30} );
                    TweenLite.to($this.$.arrows.find('.arrow-item.sx'),.5+delta, {x : 0, ease: Power4.easeInOut} );

                }
            });
        }else{
            TweenLite.to($this.$.arrows.find('.arrow-item.dx'),.5+delta, {x : 20, ease: Power4.easeInOut, onComplete:
                function(){
                    TweenLite.to($this.$.arrowItem.find('rect.dx'),.5+delta, {x : -30} );
                    TweenLite.to($this.$.arrows.find('.arrow-item.dx'),.5+delta, {x : 0, ease: Power4.easeInOut} );
                }
            });
        }
    },

    /**
     * Enter/Leave
     */

    init_module : function(){
        var $this = this;
        var args  = {};



        if(!JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'))){
            args.targetUrl = $this.opt.element.attr('data-url');
            args.page = $this.opt.element.attr('data-page');
            $(gusto.frontend.plugin.pushstate.event).trigger('plugin::pushstate::set_storage',args);
        }

        if(Modernizr.smallscreen || $this.opt.force_all_white){
            $this.tween_init_transition();
            return;
        }

        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'));
        switch(pushstateArgs.page){
            case 'landing-commercial' :
            case 'landing-fashion' :
            case 'landing-projects' :
                $this.tween_init_transition_from_landing();
                break;
            default :
                $this.tween_init_transition();
                break;
        }

    },

    enter_module : function(){
        var $this = this;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'));

        $this.opt.enter_module = true;

        if(Modernizr.smallscreen || $this.opt.force_all_white){
            $this.tween_enter();
            return;
        }

        switch(pushstateArgs.page){
            case 'landing-commercial' :
            case 'landing-fashion' :
            case 'landing-projects' :
                $this.tween_enter_from_landing();
                break;
            default :
                $this.tween_enter();
                break;
        }
    },

    enter_page_title : function(){
        var $this = this;

        $this.opt.timelineTweenPageTitleEnter = new TimelineLite({
            onComplete: function(){
                $this.opt.pageTitleEnterComplete = true;
                if($this.opt.preloadImageComplete){
                    $(gusto).trigger('::enter');
                    $(gusto).trigger('page::load');
                }
            }
        });

        $this.opt.timelineTweenPageTitleEnter
            .to($('.page-title .text'),1,{ y: '0%', ease:Expo.easeInOut},0)
        ;

    },

    leave_module : function(){
        var $this = this;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));

        $this.opt.leave_module = true;


        if(Modernizr.smallscreen || $this.opt.force_all_white){
            $this.tween_leave();
            return;
        }

        switch(pushstateArgs.page){
            case 'landing-fashion' :
            case 'landing-commercial' :
            case 'landing-projects' :
                if($this.opt.element.back_history){
                    $this.tween_leave_to_landing();
                    break;
                }
            default :
                $this.tween_leave();
                break;
        }
    },

    tween_leave : function(args){
        var $this = this;
        TweenMax.set($this.opt.element.find('.title-page-transition .bg'),{ y : '100%' });

        $this.opt.timelineTweenPageLeave = new TimelineLite({ onComplete : tweenPageLeaveComplete});
        $this.opt.timelineTweenPageLeave
            .to($this.opt.element.find('.swiper-slide .overlay'),1.2,{ x: '0%', ease:Power4.easeInOut, onComplete : function(){
                TweenMax.set($this.opt.element.find('.swiper-slide'),{ visibility : 'hidden'});
            }},0)
            .to($this.opt.element.find('.title-page-transition .bg'),.5,{ y : '0%', ease:Power4.easeInOut, onComplete : function(){

            }},0)
        ;

        /**
         * Callback
         */
        function tweenPageLeaveComplete(){

            if(args !== undefined){
                if(args.force){
                    return;
                }
            }
            if($('body').data('plugin::pushstate') !== undefined){
                if($('body').data('plugin::pushstate').ajax_state != undefined){
                    if($('body').data('plugin::pushstate').ajax_state != 'success'){
                        $(gusto.frontend.plugin.pushstate.event).on({
                            'plugin::pushstate::success-ajax' : function(){
                                $this.page_next_init();
                            }
                        });
                    }else{
                        $this.page_next_init();
                    }
                }else{
                    $this.page_next_init();
                }
            }else{
                $this.page_next_init();

            }
        }
    },

    tween_init_transition : function(){
        var $this = this;


    },

    tween_enter : function(){
        var $this = this;

        var padding_title = parseInt($this.opt.element.find('.title-page-transition .title').css('padding-left'));

        TweenMax.set($this.opt.element.find('.swiper-slide-active'),{ visibility : 'visible'});
        TweenMax.set($this.opt.element.find('.swiper-container-h .swiper-slide'),{ visibility : 'visible'});


        TweenMax.to($('.page-title'),.6,{ height: '0px', ease:Expo.easeInOut, onComplete: function(){
            $('.page-title').remove();
        }});

        setTimeout(function(){

            $this.opt.timelineTweenPageEnter = new TimelineLite({ onComplete : tweenPageEnterComplete});
            $this.opt.timelineTweenPageEnter
                .to($this.opt.element.find('.title-page-transition .bg'),1.7,{ x: 0, ease:Expo.easeInOut},0)
                .to($this.opt.element.find('.title-page-transition .title-text'),1.7,{ x: -padding_title, ease:Power4.easeInOut},1.5)
                .to($this.opt.element.find('.swiper-slide-active .overlay'),1.5,{ x: '100%', ease:Power4.easeInOut},.4)
                .to($this.opt.element.find('.swiper-slide-active'),1.7,{ x: '0%', ease:Power2.easeOut},0)
            ;

            function tweenPageEnterComplete(){


            }

            $this.load_album();

        }, 600);



    },

    page_next_init : function(){
        var $this = this;
        $(gusto).trigger('plugin::set_page::next');

    }

};/**
 * Loader - Standard
 * @mixin
 * @description : Standard type loader
 * @version 1.0
 * @author  William Manco
 * @copyright Gusto IDS
 */


/**
 * jquery.imgpreload 1.6.2 <https://github.com/farinspace/jquery.imgpreload>
 * Copyright 2009-2014 Dimas Begunoff <http://farinspace.com>
 * License MIT <http://opensource.org/licenses/MIT>
 */
if ('undefined' != typeof jQuery) {
    (function($) {
        'use strict';

        // extend jquery (because i love jQuery)
        $.imgpreload = function(imgs, settings) {
            settings = $.extend({}, $.fn.imgpreload.defaults, (settings instanceof Function) ? {
                all: settings
            } : settings);

            // use of typeof required
            // https://developer.mozilla.org/En/Core_JavaScript_1.5_Reference/Operators/Special_Operators/Instanceof_Operator#Description
            if ('string' == typeof imgs) {
                imgs = [imgs];
            }

            var loaded = [];

            $.each(imgs, function(i, elem) {
                var img = new Image();

                var url = elem;

                var img_obj = img;

                if ('string' != typeof elem) {
                    url = $(elem).attr('src') || $(elem).css('background-image').replace(/^url\((?:"|')?(.*)(?:'|")?\)$/mg, "$1");

                    img_obj = elem;
                }

                $(img).bind('load error', function(e) {
                    loaded.push(img_obj);

                    $.data(img_obj, 'loaded', ('error' == e.type) ? false : true);

                    // http://msdn.microsoft.com/en-us/library/ie/tkcsy6fe(v=vs.94).aspx
                    if (settings.each instanceof Function) {
                        settings.each.call(img_obj, loaded.slice(0));
                    }

                    // http://jsperf.com/length-in-a-variable
                    if (loaded.length >= imgs.length && settings.all instanceof Function) {
                        settings.all.call(loaded);
                    }

                    $(this).unbind('load error');
                });

                img.src = url;
            });
        };

        $.fn.imgpreload = function(settings) {
            $.imgpreload(this, settings);

            return this;
        };

        $.fn.imgpreload.defaults = {
            each: null, // callback invoked when each image is loaded
            all: null // callback invoked when all images have loaded
        };

    })(jQuery);
}

"undefined" == typeof Gusto && (Gusto = {}, gusto = {});
"undefined" == typeof Gusto.Frontend && (Gusto.Frontend = {}, gusto.frontend = {});
"undefined" == typeof Gusto.Frontend.Image_preload && (Gusto.Frontend.Image_preload = {}, gusto.frontend.image_preload = {});
"undefined" == typeof Gusto.Frontend.Image_preload.Standard && (Gusto.Frontend.Image_preload.Standard = {}, gusto.frontend.image_preload.standard = {},gusto.frontend.image_preload.standard.event = {});

Gusto.Frontend.Image_preload.Standard = function Gusto(){
    this.opt = {
        module_selector : '[data-image]',
        image_array: [],
        image_array_modes: [],
        image_array_c: 0,
        time_init : new Date()
    };
    this._init();
    this._events();
    this._startLoad();
    return this;
};

Gusto.Frontend.Image_preload.Standard.prototype = {

    _init : function(){
        var $this = this;

        $($this.opt.module_selector).not('.no-load').each(function(){
            var dataImage = $(this).data();
            $this.opt.image_array[$this.opt.image_array_c] = dataImage.image;
            $this.opt.image_array_modes[$this.opt.image_array_c] = dataImage.preload;
            $this.opt.image_array_c++;
        });
    },
    _events : function(){
        var $this = this;

       $(gusto.frontend.image_preload.standard.event).on({
            'image_preload::restart' : function(evt, json){
                $this._init();
                $this._startLoad();
            },
            'image_preload::single' : function(evt, json){ console.info(json);console.info('preloaded single'); },
            'image_preload::all' : function(evt, json){
                console.info(json); console.info('preloaded all');
                gusto.frontend.image_preload.standard.loadAll = true;
            },
            'image_preload::before' : function(){console.info('preload before');},
            'image_preload::after' : function(){console.info('preload after');}
        });
    },
    _startLoad: function(){
        var $this = this;
        $(gusto.frontend.image_preload.standard.event).trigger('image_preload::before');
        $.imgpreload($this.opt.image_array,{
            each:function(i){
                var url = $(this).attr('src');
                var mode = $('[data-image="'+url+'"]').first().data();
                var uniqueID = new Date().valueOf();
                uniqueID = uniqueID + i;
                if(mode){
                    switch (mode.preload){
                        case 1:
                            var htmlInj = '<div style="background-image:url('+$(this).attr('src')+');" class="preload-image-container-bg preload-image-container preload-image-container-hidden"></div>';
                            $('[data-image="'+url+'"]').append(htmlInj);
                            $('[data-image="'+url+'"] .preload-image-container-bg').addClass('preload-image-container-show');
                            break;
                        case 2:
                            var htmlInj = '<div class="preload-image-container preload-image-container-img preload-image-container-hidden"><img src="'+$(this).attr('src')+'" /></div>';
                            $('[data-image="'+url+'"]').append(htmlInj);
                            $('[data-image="'+url+'"] .preload-image-container-bg').addClass('preload-image-container-show');
                            break;
                        case 3:
                            $('[data-image="'+url+'"]').css('background-image','url('+$(this).attr('src')+')').addClass('preload-image-container-show');
                            break;
                        case 4:
                            $('[data-image="'+url+'"]').attr('src',$(this).attr('src')).addClass('preload-image-container-show');
                            break;
                        case 5:
                            uniqueID = parseInt(uniqueID);
                            var htmlInj = '<div data-id="'+uniqueID+'"></div>';
                            $('img[data-image="'+url+'"]').before(htmlInj);
                            var attributes = $('img[data-image="'+url+'"]').prop("attributes");

                            $.each(attributes, function() {
                                if(this.name != 'data-image'){
                                    if(this.name != 'src'){
                                        $('[data-id="'+uniqueID+'"]').attr(this.name, this.value);
                                    }
                                }
                            });

                            $('[data-id="'+uniqueID+'"]')
                                .addClass('preload-image-container-show preload-image-container-bg preload-image-container preload-image-container-hidden')
                                .css('background-image','url('+$(this).attr('src')+')');

                            $('img[data-image="'+url+'"]').remove();

                            /*
                            $('[data-id="'+uniqueID+'"]')
                                .attr("class", classListImage+" preload-image-container-bg preload-image-container preload-image-container-hidden")
                                .addClass('preload-image-container-show');
                                */
                            break;
                        default:
                            var htmlInj = '<div style="background-image:url('+$(this).attr('src')+');" class="preload-image-container-bg preload-image-container preload-image-container-hidden"></div>';
                            $('[data-image="'+url+'"]').append(htmlInj);
                            $('[data-image="'+url+'"] .preload-image-container-bg').addClass('preload-image-container-show');
                            console.warn('data-preload not defined for:'+$(this).attr('src'));
                    }
                    $(gusto.frontend.image_preload.standard.event).trigger('image_preload::single', {"preload":mode.preload, "url":$(this).attr('src')});

                }
                i++;
            },
            all:function(){
            $(gusto.frontend.image_preload.standard.event).trigger('image_preload::all', {"objects":this});
        }
        });
        $(gusto.frontend.image_preload.standard.event).trigger('image_preload::after');
    }

};


/**
 * Page - Home
 * @mixin
 * @description : Home type page
 * @version 1.0
 * @author  William Manco
 * @copyright Gusto IDS
 * @param options
 * @dependency blast, GSAP, scrollmagic
 * @returns {Page}
 */
"undefined" == typeof Gusto && (Gusto = {}, gusto = {});
"undefined" == typeof Gusto.Frontend && (Gusto.Frontend = {}, gusto.frontend = {});
"undefined" == typeof Gusto.Frontend.Page && (Gusto.Frontend.Page = {}, gusto.frontend.page = {});
"undefined" == typeof Gusto.Frontend.Page.Home && (Gusto.Frontend.Page.Home = {}, gusto.frontend.page.home = {});

Gusto.Frontend.Page.Home = function Gusto(id){

    this.opt = {
        id: id,
        selector_id : "#module-page-home-"+id,
        selector: "[data-module='page'][data-type='home'][data-id='"+id+"']",
        element : $('[data-module="page"][data-type="home"][data-id='+id+']'),
        module : 'page',
        type : 'home',
        historyStateCurrent : 1,
        historyStatePrev : 0,
        taxonomyLength : $('.taxonomy-item').length,
        force_all_white : true
    };

    this.$ = {
        photoBigContainer       : this.opt.element.find('.photoBig-container'),
        photoBigContent         : this.opt.element.find('.photoBig-content'),
        photoBigFigure          : this.opt.element.find('.photoBig-figure'),
        photoBigFigureImg       : this.opt.element.find('.photoBig-figure .img'),
        photoSmallContainer     : this.opt.element.find('.photoSmall-container'),
        photoSmallContent       : this.opt.element.find('.photoSmall-content'),
        photoSmallFigure        : this.opt.element.find('.photoSmall-figure'),
        photoSmallFigureImg     : this.opt.element.find('.photoSmall-figure .img'),
        photoMediumContent      : this.opt.element.find('.photoMedium-content'),
        photoMediumContainer    : this.opt.element.find('.photoMedium-container'),
        photoMediumFigure       : this.opt.element.find('.photoMedium-figure'),
        photoMediumFigureImg    : this.opt.element.find('.photoMedium-figure .img'),
        photoMediumOverlay      : this.opt.element.find('.photoMedium-content .overlay'),
        photoMediumOverlayTop   : this.opt.element.find('.photoMedium-content .top'),
        photoMediumOverlayBottom: this.opt.element.find('.photoMedium-content .bottom'),
        photoMediumWrap         : this.opt.element.find('.photoMedium-wrap'),
        navigation              : this.opt.element.find('.navigation'),
        pagination              : this.opt.element.find('.pagination'),
        paginationLineContent   : this.opt.element.find('.pagination .line-content'),
        paginationItem          : this.opt.element.find('.pagination .pag-item'),
        taxonomyContainer       : this.opt.element.find('.taxonomy-container'),
        taxonomyItem            : this.opt.element.find('.taxonomy-container .taxonomy-item'),
        arrows                  : this.opt.element.find('.arrows-slider'),
        arrowItem               : this.opt.element.find('.arrows-slider .arrow-item'),
        leaveBlock              : this.opt.element.find('.leave-block'),
        linkMenuOverlay         : $('[data-module="header"][data-type] .overlay'),
        discover                : this.opt.element.find('.photoBig-container .discover-button'),
        discoverToTrigger       : this.opt.element.find('.photoBig-container .discover-button-to-trigger'),
        colRight                : this.opt.element.find('.col-right')
    };

    this.size = {};

    this._init();
    this._events({
        reinit : false
    });

    return this;
};

Gusto.Frontend.Page.Home.prototype = {

    _init: function () {
        var $this = this;
        $this.init_taxonomy_name();
        $this.init_pagination();
        $this.init_module();
        $this.init_size();
        $this.init_arrows();
    },

    _events: function (args) {
        var $this = this;

        if(args.reinit){
            if(!Modernizr.touch){
                $this.$.photoBigFigure.off().on({
                    'mouseenter' : function(){
                        $this.on_hover_image();
                        $this.on_hover_next();
                        $this.opt.photoBigFigure_isHover = true;
                    },
                    'mouseleave' : function(){
                        $this.on_leave_image();
                        $this.on_leave_next();
                        $this.opt.photoBigFigure_isHover = false;
                    },
                    'click' : function() {

                        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));
                        var _this = $(this);
                        var max_length = _this.attr('data-max');
                        var index = pushstateArgs.index;
                        index++;

                        if(index > max_length){
                            index = 1;
                        }

                        $this.on_click_image({
                            targetUrl : _this.attr('data-url-base'),
                            index : index
                        });

                    }
                });
                $this.$.colRight.off().on({
                    'mouseleave' : function(){
                        $this.on_leave_next();
                    }
                });
            }else{
                $this.$.photoBigFigure.off().on({
                    'click' : function() {
                        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));
                        var _this = $(this);
                        var max_length = _this.attr('data-max');
                        var index = pushstateArgs.index;
                        index++;

                        if(index > max_length){
                            index = 1;
                        }

                        $this.on_click_image({
                            targetUrl : _this.attr('data-url-base'),
                            index : index
                        });
                    }
                });
            }

            return;
        }

        $(gusto.frontend.image_preload.standard.event).on({
            'image_preload::all' : function(){
                override_session_storage();
                    $this.init_duplicate();
                    $this.init_size();
                    $this.init_discover();

                    $this.on_change_pagination({
                        animateSinglePhoto : true,
                        resizing : true,
                        init : true
                    });

                    $(gusto).trigger('::enter');
                    $(gusto).trigger('page::load');
            }
        });

        $(gusto).on({
            '::enter' : function(){
                var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));

                if (!$this.opt.enter_module) {
                    $this.enter_module();
                    $this.opt.enter_module = true;
                }

            },
            '::leave' : function(){
                $this.leave_module();
            }
        });

        $(gusto.frontend.plugin.pushstate.event).on({
            'plugin::pushstate::click' : function(){
                var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));
                if(pushstateArgs.page == 'home'){
                    if (pushstateArgs.targetUrl.length <= 0 || pushstateArgs.targetUrl.indexOf('#') > -1) {
                        $this.on_change_pagination({
                            animateSinglePhoto: false
                        });
                    }
                }
            },
            'plugin::pushstate::popstate' : function(){
                var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));

                if(pushstateArgs.page == 'home'){
                    if(pushstateArgs.targetUrl.length  <= 0 || pushstateArgs.targetUrl.indexOf('#') > -1){
                        $this.on_change_pagination({
                            animateSinglePhoto : false
                        });
                    }
                }

            },
            'plugin::pushstate::before-ajax' : function(){
                $this.leave_module();
            }
        });

        $(window).on({
            'resize' : function(){
                $this.resize();
            }
        });

        if(!Modernizr.touch){
            $this.opt.element.on({
                'mousemove' : function(event){
                    TweenLite.to('.follow-button',.7,{ x: event.pageX+30, y: event.pageY, ease: Power4.easeOut});
                }
            });
        }

        $this.$.arrows.find('.arrow-area').off().on({
            'click' : function(){
                var _this = $(this);
                $this.on_click_arrow(_this);
                if(Modernizr.touch){
                    $this.on_touch_click_arrow(_this.find('.arrow-item'));
                }
            },
            'mouseenter' : function(){
                var _this = $(this);
                if(Modernizr.touch){
                    return;
                }
                $this.on_hover_arrow(_this.find('.arrow-item'));
            },
            'mouseleave' : function(){
                var _this = $(this);
                if(Modernizr.touch){
                    return;
                }
                $this.on_leave_arrow(_this.find('.arrow-item'));
            }
        });

        $this.$.taxonomyItem.off().on({
            'mouseenter' : function(){
                $this.opt.isHoverBigImage = true;
            },
            'mouseleave' : function(){
                $this.opt.isHoverBigImage = false;
            }
        });

        document.addEventListener('visibilitychange', function(){
            console.log('clear:1');
            clearInterval($this.opt.slider_loop_timer);
        });



        window.addEventListener('focus', function() {
            console.log('clear:2');

            clearInterval($this.opt.slider_loop_timer);
        });

        window.addEventListener('blur', function() {
            console.log('clear:3');

            clearInterval($this.opt.slider_loop_timer);
        });

    },

    init_taxonomy_name : function(){
        var $this = this;

        if($this.$.taxonomyItem.find('.container-overflow .blast').length <= 0){
            $this.$.taxonomyItem.find('.container-overflow').blast({
                delimiter: "character",
                returnGenerated: false,
                aria: false
            });
        }
    },

    init_size : function(){
        var $this = this;
        var marginPhoto = 0;
        $this.opt.element.height($(window).height());
        $this.opt.element.width($(window).width());
        $this.$.photoBigFigureImg = $this.opt.element.find('.photoBig-figure .img');
        $this.$.photoMediumFigureImg = $this.opt.element.find('.photoMedium-figure .img');
        $this.$.photoSmallFigureImg = $this.opt.element.find('.photoSmall-figure .img');

        var mediumResult = ($this.$.photoMediumContainer.width() / 100) * ($this.$.photoMediumContent.attr('data-width'));
        var smallResult = ($this.$.photoSmallContainer.width() / 100) * ($this.$.photoSmallContent.attr('data-width'));

        $this.$.photoMediumContent.width(mediumResult);

        $this.$.photoSmallContent.width($this.$.photoSmallContent.attr('data-width')+'%');

        if(Modernizr.smallscreen){
            $this.$.photoSmallContent.width('80px');
        }

        /**
         * Set size of big image
         */
        var $photoBigContent = $this.$.photoBigContent;
        var $photoBigFigure = $this.$.photoBigFigure;
        var $photoBigFigureImg = $this.$.photoBigFigureImg;
        $photoBigFigure.width($photoBigContent.width()*$photoBigFigureImg.length);
        $photoBigFigureImg.width($photoBigContent.width());

        /**
         * Set size of small image
         */

        var $photoSmallContainer = $this.$.photoSmallContainer;
        var $photoSmallContent = $this.$.photoSmallContent;
        var $photoSmallFigure = $this.$.photoSmallFigure;
        var $photoSmallFigureImg = $this.$.photoSmallFigureImg;
        $photoSmallContainer.height($(window).height()/2);
        $photoSmallFigure.width(($photoSmallContent.width()+marginPhoto)*$photoSmallFigureImg.length);
        $photoSmallFigureImg.width($photoSmallContent.width()+marginPhoto);


        /**
         * Set size of medium image + navigation
         */
        var $photoMediumContainer = $this.$.photoMediumContainer;
        var $photoMediumFigure = $this.$.photoMediumFigure;
        var $photoMediumContent = $this.$.photoMediumContent;
        var $photoMediumFigureImg = $this.$.photoMediumFigureImg;
        var $navigation = $this.$.navigation;
        $photoMediumContainer.height(($(window).height()/2)-parseInt($photoMediumContainer.css('padding-bottom')));
        $photoMediumFigure.width($photoMediumContent.width()*$photoMediumFigureImg.length);
        $photoMediumFigureImg.width($photoMediumContent.width());
        $navigation.height(parseInt($(window).height()/2) - (parseInt($photoMediumContent.height())+parseInt($photoMediumContainer.css('padding-bottom'))));

        /**
         * Saving sizes
         */
        $this.size.photoBigFigure = $photoBigFigure.width();
        $this.size.photoBigFigureImg = $photoBigContent.width();
        $this.size.photoMediumFigure = $photoMediumFigure.width();
        $this.size.photoMediumFigureImg = $photoMediumContent.width();
        $this.size.photoSmallFigureImg = $photoSmallContent.width();
        $this.size.photoSmallFigure = $photoSmallFigure.width();

        $this.$.photoMediumContent.width(Math.round(mediumResult));
        $this.$.photoMediumWrap.width(Math.round($this.$.photoMediumContent.width())-2);
        $this.$.photoSmallContent.css('width',$this.$.photoSmallContent.attr('data-width')-1+'%');

        if(Modernizr.smallscreen){
            $this.$.photoSmallContent.width('79px');
        }
    },

    init_arrows : function(){
        var $this = this;

        TweenLite.set($this.opt.element.find('.arrow-dx-item rect'),{ x: 20, y: 0});
        TweenLite.set($this.opt.element.find('.arrow-dx-item polygon'),{ x: 14.5});
        TweenLite.set($this.opt.element.find('.arrow-sx-item rect'),{ x: -20,y: 0});
    },

    init_discover : function(){
        var $this = this;



        $this.$.photoBigFigure.find('a').each(function(){
            var this_page = $(this).attr('data-page');
            $(this).appendTo('.img[data-page="'+this_page+'"]');

        });


    },

    on_hover_discover : function(_this){
        var $this = this;
        var this_page = _this.attr('data-page');
        $this.opt.discover_is_hover = true;
        $this.opt.current_page_hover = this_page;
        TweenMax.to($this.opt.element.find('.label-discover'),.7,{ y : '0%', ease: Power4.easeInOut });

    },

    on_leave_discover : function(){
        var $this = this;
        $this.opt.discover_is_hover = false;
        TweenMax.to($this.opt.element.find('.label-discover'),.7,{ y : '100%', ease: Power4.easeInOut });

    },

    on_hover_next : function(){
        var $this = this;
        if($this.opt.discover_is_hover){
            return;
        }
        TweenMax.to($this.opt.element.find('.label-next'),.7,{ y : '0%', ease: Power4.easeInOut });

    },

    on_leave_next : function(){
        var $this = this;
        TweenMax.to($this.opt.element.find('.label-next'),.7,{ y : '100%', ease: Power4.easeInOut });

    },

    on_click_arrow : function(_this){
        var $this = this;

        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));
        var max_length = $this.$.arrows.attr('data-max');
        var index = pushstateArgs.index;


        if(_this.hasClass('arrow-sx')){
            index--;
        }else{
            index++;
        }

        if(index == 0){
            index = max_length;
        }

        if(index > max_length){
            index = 1;
        }


        $this.go_to({
            targetUrl : $this.$.arrows.attr('data-url-base')+'#'+index,
            targetTitle : index,
            index : parseInt(index),
            page : 'home'
        });
    },

    on_hover_arrow : function(_this){
        var $this = this;

        TweenLite.to(_this.find('rect'),.5, {x : 0,y:0, ease: Power4.easeInOut} );

        if(_this.hasClass('sx')){
            TweenLite.to($this.$.arrows.find('.arrow-item.sx'),.5, {x : -8, ease: Power4.easeInOut} );
        }else{
            TweenLite.to($this.$.arrows.find('.arrow-item.dx'),.5, {x : 8, ease: Power4.easeInOut} );
        }
        console.log('clear:4');

        clearInterval($this.opt.slider_loop_timer);

    },

    on_leave_arrow : function(){
        var $this = this;

        TweenLite.to($this.$.arrowItem.find('rect.sx'),.5, {x : -30} );
        TweenLite.to($this.$.arrowItem.find('rect.dx'),.5, {x : 30} );

        TweenLite.to($this.$.arrows.find('.arrow-item.sx'),.5, {x : 0, ease: Power4.easeInOut} );
        TweenLite.to($this.$.arrows.find('.arrow-item.dx'),.5, {x : 0, ease: Power4.easeInOut} );

    },

    on_touch_click_arrow : function(_this){
        var $this = this;
        var delta = -.05;

        TweenLite.to(_this.find('rect'),.5+delta, {x : 0,y:0, ease: Power4.easeInOut} );

        if(_this.hasClass('sx')){
            TweenLite.to($this.$.arrows.find('.arrow-item.sx'),.5+delta, {x : -20, ease: Power4.easeInOut, onComplete:
                function(){
                    TweenLite.to($this.$.arrowItem.find('rect.sx'),.5+delta, {x : 30} );
                    TweenLite.to($this.$.arrows.find('.arrow-item.sx'),.5+delta, {x : 0, ease: Power4.easeInOut} );

                }
            });
        }else{
            TweenLite.to($this.$.arrows.find('.arrow-item.dx'),.5+delta, {x : 20, ease: Power4.easeInOut, onComplete:
                function(){
                    TweenLite.to($this.$.arrowItem.find('rect.dx'),.5+delta, {x : -30} );
                    TweenLite.to($this.$.arrows.find('.arrow-item.dx'),.5+delta, {x : 0, ease: Power4.easeInOut} );
                }
            });
        }
        console.log('clear:5');

        clearInterval($this.opt.slider_loop_timer);

    },

    init_duplicate : function(){
        var $this = this;

        if($this.opt.init_duplicate){
           return;
        }
        $this.opt.init_duplicate = true;

        $this.$.photoBigFigureImg = $this.opt.element.find('.photoBig-figure .img');
        $this.$.photoMediumFigureImg = $this.opt.element.find('.photoMedium-figure .img');
        $this.$.photoSmallFigureImg = $this.opt.element.find('.photoSmall-figure .img');

        /**
         * Duplicate form big
         */
        var $photoBigFigure = $this.$.photoBigFigure;
        var $photoBigFigureImg = $this.$.photoBigFigureImg;
        var $firstClone = $photoBigFigureImg.first().clone().addClass('clone');
        var $lastClone = $photoBigFigureImg.last().clone().addClass('clone');

        $photoBigFigure.append($firstClone);
        $photoBigFigure.prepend($lastClone);

        /**
         * Duplicate form medium
         */
        var $photoMediumFigure = $this.$.photoMediumFigure;
        var $photoMediumFigureImg = $this.$.photoMediumFigureImg;
        var $firstClone = $photoMediumFigureImg.first().clone().addClass('clone');
        var $lastClone = $photoMediumFigureImg.last().clone().addClass('clone');

        $photoMediumFigure.append($firstClone);
        $photoMediumFigure.prepend($lastClone);

        /**
         * Duplicate form small
         */
        var $photoSmallFigure = $this.$.photoSmallFigure;
        var $photoSmallFigureImg = $this.$.photoSmallFigureImg;
        var $firstClone = $photoSmallFigureImg.first().clone().addClass('clone');
        var $secondClone = $photoSmallFigureImg.first().next().clone().addClass('clone');
        var $lastClone = $photoSmallFigureImg.last().clone().addClass('clone');
        var $prelastClone = $photoSmallFigureImg.last().prev().clone().addClass('clone');

        $photoSmallFigure.prepend($lastClone);
        $photoSmallFigure.prepend($prelastClone);
        $photoSmallFigure.append($firstClone);
        $photoSmallFigure.append($secondClone);

        $this._events({
            reinit : true
        });

    },

    init_tween_slider : function(){
        var $this = this;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));

        if(pushstateArgs == null){
            pushstateArgs = {};
            pushstateArgs.index = 1;
        }

        var toNext = $this.size.photoSmallFigure - (($this.size.photoSmallFigureImg * pushstateArgs.index)+($this.size.photoSmallFigureImg*3));
        var toCurrentBig = $this.size.photoBigFigureImg * (pushstateArgs.index);//$this.size.photoBigFigure - (($this.size.photoBigFigureImg * pushstateArgs.index)+($this.size.photoBigFigureImg));
        var toCurrentMedium = $this.size.photoMediumFigure - (($this.size.photoMediumFigureImg * pushstateArgs.index)+($this.size.photoMediumFigureImg));

        return {
            toNext : toNext,
            toCurrentBig : toCurrentBig,
            toCurrentMedium : toCurrentMedium
        }
    },

    init_slide_link : function(){
        var $this = this;

        $this.$.photoBigFigureImg.append('<a href=""></a>');
    },

    init_loop_slider : function(){
        var $this = this;

        $this.opt.slider_loop_timer = setInterval(function(){

            var delay_goTo = 0;

            if(!Modernizr.touch){
                $this.tween_slider({
                    tween_hover : true
                });
                delay_goTo = 1700;
            }


            var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));
            var index = pushstateArgs.index;
            var max_length = $this.$.arrows.attr('data-max');

            index++;

            if(index > max_length){
                index = 1;
            }


            setTimeout(function(){
                $this.go_to({
                    targetUrl : $this.$.arrows.attr('data-url-base')+'#'+index,
                    targetTitle : index,
                    index : parseInt(index),
                    page : 'home'
                });

            },delay_goTo);


        },4000);
    },

    init_tween_slider_loop_forward : function(){
        var $this = this;

        var toNext = $this.size.photoSmallFigure - (($this.size.photoSmallFigureImg * $this.opt.taxonomyLength)+($this.size.photoSmallFigureImg*(3+1)));
        var toCurrentBig = $this.size.photoBigFigure - (($this.size.photoBigFigureImg * $this.opt.taxonomyLength)+($this.size.photoBigFigureImg*2));
        var toCurrentMedium = $this.size.photoMediumFigure - (($this.size.photoMediumFigureImg * $this.opt.taxonomyLength)+($this.size.photoMediumFigureImg*2));

        return {
            toNext : toNext,
            toCurrentBig : toCurrentBig,
            toCurrentMedium : toCurrentMedium
        }

    },

    init_tween_slider_loop_backward : function(){
        var $this = this;

        var toNext = $this.size.photoSmallFigure - (($this.size.photoSmallFigureImg * $this.opt.taxonomyLength)-($this.size.photoSmallFigureImg*(1)));
        var toCurrentBig = 0;
        var toCurrentMedium = $this.size.photoMediumFigure - (($this.size.photoMediumFigureImg * $this.opt.taxonomyLength)-($this.size.photoMediumFigureImg*3));

        return {
            toNext : toNext,
            toCurrentBig : toCurrentBig,
            toCurrentMedium : toCurrentMedium
        }

    },

    init_pagination : function(){
        var $this = this;

        if($this.$.pagination.find('.blast').length <= 0) {
            $this.$.paginationItem.blast({ delimiter: "character" });
        }

        if($this.$.paginationLineContent.find('.line-item').length <= 0) {
            for(var i = 0; i < 51; i++){
                $this.$.paginationLineContent.append('<div class="line-item line-'+i+'"></div>');
            }
        }

        $this.$.paginationLineItem = $this.$.paginationLineContent.children();

        $this.$.paginationLineItem.height(1);
    },

    on_change_pagination : function (args){
        var $this = this;
        var toFirstBig, toFirstMedium, toFirstSmall = false;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));

        if(pushstateArgs.page != 'home'){
            return;
        }

        var currentTypeTween = 'normal';
        var init_tween = $this.init_tween_slider();
        var toNext = init_tween.toNext;
        var toCurrentBig = init_tween.toCurrentBig;
        var toCurrentMedium = init_tween.toCurrentMedium;



        if(pushstateArgs == null){
            pushstateArgs = {};
            pushstateArgs.index = 1;
        }

        /**
         * Current Index
         */
        $this.opt.historyStateCurrent = pushstateArgs.index;

        if($this.opt.historyStateCurrent == $this.opt.historyStatePrev && !args.resizing){ return; }

        /**
         * If current slide is last and next is first - loop
         */
        if($this.opt.historyStateCurrent == 1 && $this.opt.historyStatePrev == $this.opt.taxonomyLength){
            var loop_forward = $this.init_tween_slider_loop_forward();
            //toCurrentBig = loop_forward.toCurrentBig;
            toCurrentMedium = loop_forward.toCurrentMedium;
            toNext = loop_forward.toNext;
            toFirstBig = toFirstMedium = toFirstSmall = true;
            currentTypeTween = 'forward';
        }
        /**
         * If current slide is first and next is last - loop
         */
        if($this.opt.historyStateCurrent == $this.opt.taxonomyLength && $this.opt.historyStatePrev == 1){
            var loop_backward = $this.init_tween_slider_loop_backward();
            toCurrentBig = loop_backward.toCurrentBig;
            toCurrentMedium = loop_backward.toCurrentMedium;
            toNext = loop_backward.toNext;
            toFirstBig = toFirstMedium = toFirstSmall = true;
            currentTypeTween = 'backward';
        }

        /**
         * Detect the current photo
         */
        var $photoBigFigureImg = $this.$.photoBigFigureImg;
        var $photoMediumFigureImg = $this.$.photoMediumFigureImg;
        var $photoSmallFigureImg = $this.$.photoSmallFigureImg;
        var $currentBigPhoto,
            $currentMediumPhoto,
            $currentSmallPhoto;


        switch(currentTypeTween){
            case 'normal' :
                $currentBigPhoto = $photoBigFigureImg.eq(pushstateArgs.index);
                $currentMediumPhoto = $photoMediumFigureImg.eq($photoMediumFigureImg.length-(pushstateArgs.index+1));
                $currentSmallPhoto = $photoSmallFigureImg.eq($photoSmallFigureImg.length-(pushstateArgs.index+3));
                break;
            case 'forward' :
                $currentBigPhoto = $photoBigFigureImg.eq($photoBigFigureImg.length-2);
                $currentMediumPhoto = $photoMediumFigureImg.eq($photoMediumFigureImg.length-2);
                $currentSmallPhoto = $photoSmallFigureImg.eq($photoSmallFigureImg.length-4);

                break;
            case 'backward' :
                $currentBigPhoto = $photoBigFigureImg.eq(0);
                $currentMediumPhoto = $photoMediumFigureImg.eq(0);
                $currentSmallPhoto = $photoSmallFigureImg.eq(0);
                break;
        }


        $photoBigFigureImg.removeClass('is-current');
        $photoMediumFigureImg.removeClass('is-current');
        $photoSmallFigureImg.removeClass('is-current');
        $photoBigFigureImg.addClass('is-hide');
        $photoMediumFigureImg.addClass('is-hide');
        $photoSmallFigureImg.addClass('is-hide');

        $this.$.currentBigPhoto = $currentBigPhoto;
        $this.$.currentMediumPhoto = $currentMediumPhoto;
        $this.$.currentSmallPhoto = $currentSmallPhoto;


        if(!$this.opt.firstTimePagination){
            $this.$.currentBigPhoto.removeClass('is-hide');
            $this.$.currentMediumPhoto.removeClass('is-hide');
            $this.$.currentSmallPhoto.removeClass('is-hide');
            $this.$.currentBigPhoto.addClass('is-current');
            $this.$.currentMediumPhoto.addClass('is-current');
            $this.$.currentSmallPhoto.addClass('is-current');
            $this.opt.firstTimePagination = true;
        }

        $this.$.discoverToTrigger.off().on({
            'mouseenter' : function(){
                var _this = $(this);
                $this.on_leave_next(_this);
                $this.on_hover_discover(_this);
                $this.opt.isHoverBigImage = true;

            },
            'mouseleave' : function(){
                var _this = $(this);
                $this.on_leave_discover(_this);
                $this.on_hover_next(_this);
                $this.opt.isHoverBigImage = false;

            },
            'click' : function(){
                if(!$this.opt.imageIsHover){
                    console.log($this.$.currentBigPhoto.find('a'));
                    $this.$.currentBigPhoto.find('a').trigger('click');
                }
            }
        });

        /**
         * Previous Index
         */
        $this.opt.historyStatePrev = pushstateArgs.index;

        $this.opt.currentTypeTween = currentTypeTween;

        /*
        console.log({
            toCurrentBig : toCurrentBig,
            toCurrentMedium : toCurrentMedium,
            toNext : toNext,
            toFirstBig : toFirstBig,
            toFirstMedium : toFirstMedium,
            toFirstSmall : toFirstSmall,
            pushstateArgs : pushstateArgs,
            animateSinglePhoto : args.animateSinglePhoto,
            currentTypeTween : currentTypeTween,
            resizing : args.resizing
        });
        */


        $this.tween_slider({
            toCurrentBig : toCurrentBig,
            toCurrentMedium : toCurrentMedium,
            toNext : toNext,
            toFirstBig : toFirstBig,
            toFirstMedium : toFirstMedium,
            toFirstSmall : toFirstSmall,
            pushstateArgs : pushstateArgs,
            animateSinglePhoto : args.animateSinglePhoto,
            currentTypeTween : currentTypeTween,
            resizing : args.resizing
        });

        if(args.resizing && !args.init){
            $this.tween_pagination();

            return;
        }
        $this.tween_taxonomy({
            init : args.init
        });
        $this.tween_pagination();

    },

    on_click_image : function(args){
        var $this = this;

        if($this.opt.discover_is_hover){
            $this.$.photoBigFigure.find('.img[data-page="'+$this.opt.current_page_hover+'"] .overlay').trigger('click');

        }else{
            console.log('clear:6');

            clearInterval($this.opt.slider_loop_timer);
            $this.go_to({
                targetUrl : args.targetUrl+'#'+args.index,
                targetTitle : args.index,
                index : parseInt(args.index),
                page : 'home'
            });
        }

    },

    on_hover_image : function(){
        var $this = this;
        $this.tween_slider({
            tween_hover : true
        });
    },

    on_leave_image : function(){
        var $this = this;

        $this.tween_slider({
            tween_hover : true,
            leave : true
        });
    },

    go_to : function(args){
        var $this = this;

        if(!TweenMax.isTweening($this.$.photoBigFigure)) {
            $(gusto.frontend.plugin.pushstate.event).trigger('plugin::pushstate::on_click', args);
        }

    },

    tween_slider : function (args) {
        var $this = this;

        /**
         * Resizing reset image position
         */
        if(args.resizing){

            TweenLite.set($this.$.photoBigFigure,{ x:-args.toCurrentBig });
            TweenLite.set($this.$.photoMediumFigure,{ x:-args.toCurrentMedium });
            TweenLite.set($this.$.photoSmallFigure,{ x:-args.toNext});
            TweenLite.set($this.$.photoBigFigureImg, {x: 0});
            TweenLite.set($this.$.photoMediumFigureImg, {x: 0});
            TweenLite.set($this.$.photoSmallFigureImg, {x: 0});
            return;
        }

        /**
         * Tween hover/leave photo
         */
        if(args.tween_hover){
            /**
             * Tween hover Big
             */
            var $currentBigPhoto = $this.$.currentBigPhoto;
            var $nextBigPhoto = $this.$.currentBigPhoto.next();

            $this.opt.imageIsHover = false;

            TweenLite.set($currentBigPhoto,{zIndex: 100});
            TweenLite.set($nextBigPhoto,{zIndex: 90});

            $this.opt.tweenHoverBig = true;

            if(args.leave){
                TweenLite.to($currentBigPhoto,.8,{ x: 0, ease: Power4.easeInOut});
                TweenLite.to($currentBigPhoto.find('a'),.8 ,{ left: '-220px', ease: Power3.easeInOut});
                TweenLite.to($nextBigPhoto,.8,{ x: 0, ease: Power4.easeInOut, onComplete : tweenLeaveBigComplete});
            }else{
                TweenLite.to($currentBigPhoto,.8 ,{ x: -220, ease: Power3.easeInOut});
                TweenLite.to($currentBigPhoto.find('a'),.8 ,{ left: '0', ease: Power3.easeInOut});
                TweenLite.to($nextBigPhoto,.8,{ x: -270, ease: Power2.easeInOut, onComplete : tweenHoverBigComplete});
            }


            function tweenLeaveBigComplete(){
                TweenLite.set($this.$.photoBigFigureImg,{zIndex: 0});

                $this.opt.tweenHoverBig = false;
            }

            function tweenHoverBigComplete(){
                $this.$.currentBigPhoto.removeClass('is-hide');
                $this.$.currentMediumPhoto.removeClass('is-hide');
                $this.$.currentSmallPhoto.removeClass('is-hide');
                $this.$.currentBigPhoto.addClass('is-current');
                $this.$.currentMediumPhoto.addClass('is-current');
                $this.$.currentSmallPhoto.addClass('is-current');
            }

            /**
             * Tween hover Small
             */
            var $currentSmallPhoto = $this.$.currentSmallPhoto;
            var $nextSmallPhoto = $this.$.currentSmallPhoto.prev();
            TweenLite.set($currentSmallPhoto,{zIndex: 100});
            TweenLite.set($nextSmallPhoto,{zIndex: 90});

            if(args.leave){
                TweenLite.to($currentSmallPhoto,.8,{ x: 0, ease: Power4.easeInOut});
                TweenLite.to($nextSmallPhoto,.8,{ x: 0, ease: Power4.easeInOut, onComplete : tweenHoverSmallComplete});
            }else{
                TweenLite.to($currentSmallPhoto,1 ,{ x: 50-10, ease: Power3.easeInOut});
                TweenLite.to($nextSmallPhoto,1,{ x: 50, ease: Power2.easeInOut});
            }

            function tweenHoverSmallComplete(){
                TweenLite.set($this.$.photoSmallFigureImg,{zIndex: 0});

            }
            return;
        }

        /**
         * Tween to animate single photo
         */
        if(args.animateSinglePhoto) {
            TweenLite.fromTo($this.$.currentBigPhoto, 1, {x: 200}, {x: 0, ease: Power2.easeInOut});
            TweenLite.fromTo($this.$.currentMediumPhoto, 1.3, {x: 200}, {x: 0, ease: Power4.easeInOut});
            TweenLite.fromTo($this.$.currentSmallPhoto, 1.3, {x: 50}, {x: 0, ease: Power4.easeInOut});
        }

        var toCurrentBig = args.toCurrentBig;

        var photoBigFigure = ($this.size.photoBigFigure - ($this.size.photoBigFigureImg*2));

        if(toCurrentBig == 0 && args.currentTypeTween != 'backward'){
            toCurrentBig = photoBigFigure;
        }

        /**
         * Tween to sliding photo
         */
        $this.opt.slidingComplete = false;
        $this.opt.timelineTweenSlidingPhoto = new TimelineLite({onComplete : timelineSlidingComplete });
        $this.opt.timelineTweenSlidingPhoto
            //.to($this.$.photoBigFigure,1,{ x:-args.toCurrentBig, ease: Power2.easeInOut, onComplete: tweenBigComplete },0)
            .to($this.$.photoBigFigure, 1.1, {x:-toCurrentBig,
                modifiers:{
                    x:function(x){
                        var valueReturn =  x % photoBigFigure;
                        if(x == 0){
                            valueReturn = -(photoBigFigure);
                        }
                        return valueReturn;
                    }
                }, ease:Power2.easeInOut,onComplete:tweenBigComplete
            },0)
            .to($this.$.photoMediumFigure,1.3,{ x:-args.toCurrentMedium, ease: Power4.easeInOut, onComplete: tweenMediumComplete },0)
            .to($this.$.photoSmallFigure,1.3,{ x:-args.toNext, ease: Power4.easeInOut, onComplete: tweenSmallComplete },0)
            .to($this.$.photoBigFigureImg,1, {x: 0, ease: Power2.easeInOut},0)
            .to($this.$.photoMediumFigureImg,1, {x: 0, ease: Power2.easeInOut},0)
            .to($this.$.photoSmallFigureImg,1, {x: 0, ease: Power2.easeInOut},0)
        ;

        /**
         * Callback
         */
        function tweenBigComplete(){
            /*  if(args.toFirstBig == true){
              args.toCurrentBig = $this.size.photoBigFigure - (($this.size.photoBigFigureImg * args.pushstateArgs.index)+($this.size.photoBigFigureImg));
                TweenLite.set($this.$.photoBigFigure,{ x:-args.toCurrentBig});


                args.toFirstBig = false;
            }

             */
            if(toCurrentBig == photoBigFigure){
                $this.$.currentBigPhoto = $this.$.photoBigFigureImg.eq(0);
            }
            if($this.opt.currentTypeTween == 'forward'){
                $this.$.currentBigPhoto = $this.$.photoBigFigureImg.eq(1);
            }


        }
        function tweenMediumComplete(){
            if(args.toFirstMedium == true){
                args.toCurrentMedium = $this.size.photoMediumFigure - (($this.size.photoMediumFigureImg * args.pushstateArgs.index)+($this.size.photoMediumFigureImg));
                TweenLite.set($this.$.photoMediumFigure,{ x:-args.toCurrentMedium});
                args.toFirstMedium = false;
            }
        }
        function tweenSmallComplete(){
            if(args.toFirstSmall == true){
                args.toNext = $this.size.photoSmallFigure - (($this.size.photoSmallFigureImg *  args.pushstateArgs.index)+($this.size.photoSmallFigureImg*3));
                TweenLite.set($this.$.photoSmallFigure,{ x:-args.toNext});
                args.toFirstSmall = false;
            }
        }
        function timelineSlidingComplete(){
            $this.opt.slidingComplete = true;
           // $this.opt.imageIsHover = false;

            if($this.opt.photoBigFigure_isHover){
                $this.on_hover_image();
                $this.on_hover_next();
            }
        }
    },

    tween_taxonomy : function(args){
        var $this = this;

        $this.opt.currentTaxonomy = $this.$.taxonomyItem.eq($this.opt.historyStateCurrent-1);

        var $currentTaxonomy = $this.opt.currentTaxonomy;

        $this.$.taxonomyItem.removeClass('active');
        $this.opt.currentTaxonomy.addClass('active');

        if(args !== undefined){
            if(args.init) {
                $this.$.taxonomyItem.removeClass('active');
                $currentTaxonomy.addClass('active');
                TweenLite.set($this.$.taxonomyItem.find('.container-overflow'), {y: '-100%'});
                TweenLite.set($this.$.taxonomyItem, {'display': 'none'});
                TweenLite.set($currentTaxonomy, {'display': 'block'});
                TweenLite.set($currentTaxonomy.find('.container-overflow'), {y: '0%'});
                return true;
            }
        }

        $this.opt.timelineTweenTaxonomy = new TimelineLite();
        $this.opt.timelineTweenTaxonomy
            .to($this.$.taxonomyItem.find('.container-overflow'),.3,{ y: '-100%', ease:Power3.easeIn, onComplete: function(){
                TweenLite.set($this.$.taxonomyItem,{ 'display' : 'none'});
                TweenLite.set($currentTaxonomy,{ 'display' : 'block'});
                TweenLite.set($currentTaxonomy.find('.container-overflow'),{  y : '100%'});

            }},0)
            .staggerTo($currentTaxonomy.find('.container-overflow'),.8,{ y: '0%', ease:Power3.easeOut},.07)
        ;

    },

    tween_pagination : function(args){
        var $this = this;

        $this.opt.currentPagination = $this.$.paginationItem.eq($this.opt.historyStateCurrent-1);

        $this.$.paginationItem.removeClass('active');
        $this.opt.currentPagination.addClass('active');

        var offset = $this.opt.currentPagination.position();

        $this.opt.timelineTweenPaginationLine = new TimelineLite();
        $this.opt.timelineTweenPaginationLine
            .staggerTo($this.shuffle($this.$.paginationLineItem),1.3,{ x: offset.left, ease:Expo.easeInOut},.005,0)
        ;

    },

    resize: function(){
        var $this = this;
        $this.init_size();
        $this.on_change_pagination({
            resizing : true
        });
    },

    shuffle : function(array) {
        return array.sort(function() { return 0.5 - Math.random() });
    },

    /**
     * Enter/Leave
     */

    init_module : function(){
        var $this = this;
        var args  = {};
        var $thisPaginationItem = $this.$.paginationItem.eq(0);


        args.targetUrl = $thisPaginationItem.attr('data-url');
        args.targetTitle = $thisPaginationItem.attr('data-index');
        args.page = $thisPaginationItem.attr('data-page');
        args.index = 1;

        if(JSON.parse(gusto.storage.getItem('plugin::pushstate::prev')) === null || !window.location.hash) {
            $(gusto.frontend.plugin.pushstate.event).trigger('plugin::pushstate::set_storage', args);
        }

        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'));
        switch(pushstateArgs.page){
            case 'landing-commercial' :
            case 'landing-fashion' :
            case 'landing-projects' :
                //$this.tween_init_transition_from_landing();
                $this.tween_init_transition();

                break;
            default :
                $this.tween_init_transition();
                break;
        }
    },

    leave_module : function(){
        var $this = this;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));

        if(Modernizr.smallscreen || $this.opt.force_all_white){
            $this.tween_leave();
            return;
        }

        switch(pushstateArgs.page){
            case 'landing-commercial' :
            case 'landing-fashion' :
            case 'landing-projects' :
                $this.tween_leave_to_landing();
                break;
            default :
                $this.tween_leave();
                break;
        }
    },

    enter_module : function(){
        var $this = this;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'));

        if(Modernizr.smallscreen || $this.opt.force_all_white){
            $this.tween_enter();
            return;
        }

        switch(pushstateArgs.page){
            case 'landing-commercial' :
            case 'landing-fashion' :
            case 'landing-projects' :
                $this.tween_enter_from_landing();

                break;
            default :
                $this.tween_enter();
                break;
        }
    },

    tween_init_transition : function(){
        var $this = this;

        var arr_leave_el = [];


        $this.$.paginationItem.each(function(){
            $(this).find('span.blast').each(function(){
                arr_leave_el.push($(this));
            });
        });


        TweenLite.set($this.$.leaveBlock,{ x: '0px'});
        TweenLite.set(arr_leave_el,{  y:'-100px',opacity: 0});
        TweenLite.set($this.$.taxonomyItem.find('span.blast'),{opacity: 0});
        TweenLite.set($this.$.paginationLineItem,{ x:'200px',opacity: 0});
        TweenLite.set($this.$.linkMenuOverlay,{ x:'0px'});
        TweenLite.set($this.$.photoMediumOverlay.children(),{ y: '0px'});
        TweenLite.set($this.$.arrowItem,{ opacity: 0});

    },

    tween_enter : function(){
        var $this = this;

        TweenLite.set($this.opt.element,{ visibility : 'visible'});

        var arr_enter_el = [];
        var arr_taxonomy_letter = [];
        var offset = {
            left : 0
        };

        $this.$.paginationItem.each(function(){
            $(this).find('span.blast').each(function(){
                arr_enter_el.push($(this));
            });
        });

        $this.opt.currentTaxonomy.find('span.blast').each(function(){
            arr_taxonomy_letter.push($(this));
        });

        if($this.opt.historyStateCurrent){
            $this.opt.currentPagination = $this.$.paginationItem.eq($this.opt.historyStateCurrent-1);
            offset = $this.opt.currentPagination.position();

        }

        $this.opt.timelineTweenPageEnter = new TimelineLite({ onComplete : onCompleteTweenPageEnter});
        $this.opt.timelineTweenPageEnter
            .staggerTo($this.shuffle($this.$.leaveBlock),1.3,{ x: '-100%', ease:Expo.easeInOut},.1)
            .staggerTo($this.shuffle(arr_taxonomy_letter),.7,{ opacity: 1, ease:Power1.easeIn},.04,.5)
            .staggerTo($this.shuffle(arr_enter_el),.7,{ opacity: 1, ease:Power1.easeOut},.05,0)
            .staggerTo($this.shuffle(arr_enter_el),1.3,{  y:'0px', ease:Power2.easeInOut},.05,0)
            .staggerTo($this.shuffle($this.$.paginationLineItem),1,{ opacity: 1, ease:Expo.easeOut},.01,0)
            .staggerTo($this.shuffle($this.$.paginationLineItem),1.3,{ x:offset.left, ease:Expo.easeOut},.01,0)
            .to($this.$.photoMediumOverlayTop,1.3,{ y: '-100%', ease:Expo.easeInOut},0)
            .to($this.$.photoMediumOverlayBottom,1.3,{ y: '100%', ease:Expo.easeInOut},0)
            .staggerTo($this.$.arrowItem,1,{ opacity: 1, ease:Expo.easeOut},.03,.5)
            .to($this.$.linkMenuOverlay,.5,{ x:'0px', ease:Expo.easeOut},0)
        ;

        function onCompleteTweenPageEnter(){
            TweenMax.set($this.$.taxonomyItem.find('span.blast'),{ opacity:1, ease:Expo.easeOut});
        }

        $this.init_loop_slider();

    },

    tween_leave : function(){
        var $this = this;
        var arr_leave_el = [];
        var arr_taxonomy_letter = [];
        console.log('clear:7');

        clearInterval($this.opt.slider_loop_timer);


        $this.opt.enter_module = false;

        $this.$.paginationItem.each(function(){
            $(this).find('span.blast').each(function(){
                arr_leave_el.push($(this));
            });
        });

        $this.opt.currentTaxonomy.find('span.blast').each(function(){
            arr_taxonomy_letter.push($(this));
        });

        $this.opt.timelineTweenPageLeave = new TimelineLite({ onComplete : tweenPageLeaveComplete});
        $this.opt.timelineTweenPageLeave
            .staggerTo($this.shuffle($this.$.leaveBlock),1,{ x:'1%', ease:Expo.easeInOut},.1)
            .staggerTo($this.shuffle(arr_leave_el),.7,{ opacity: 0, ease:Power1.easeIn},.05,0)
            .staggerTo($this.shuffle(arr_leave_el),1,{  y:'-100px', ease:Power2.easeInOut},.05,0)
            .staggerTo($this.shuffle(arr_taxonomy_letter),.7,{ opacity: 0, ease:Power1.easeIn},.05,0)
            .staggerTo($this.shuffle($this.$.paginationLineItem),.8,{ opacity: 0, ease:Expo.easeIn},.005,0)
            .staggerTo($this.shuffle($this.$.paginationLineItem),1,{ x:'200px', ease:Expo.easeIn},.005,0)
            .to($this.$.photoMediumOverlayTop,.7,{y:'0%', ease:Expo.easeIn},0)
            .to($this.$.photoMediumOverlayBottom,.7,{y:'0%', ease:Expo.easeIn},0)
            .staggerTo($this.$.arrowItem,.5,{ opacity: 0, ease:Expo.easeOut},.03,0)
            .to($this.$.linkMenuOverlay,.5,{ x:'0px', ease:Expo.easeIn},0)
        ;

        /**
         * Callback
         */
        function tweenPageLeaveComplete(){
            var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));
            $this.page_next_init();
            return;
            if(pushstateArgs.targetUrl.indexOf('#') <= -1){

                if($('body').data('plugin::pushstate') !== undefined){

                    if($('body').data('plugin::pushstate').ajax_state != undefined){

                        if($('body').data('plugin::pushstate').ajax_state != 'success'){

                            $(gusto.frontend.plugin.pushstate.event).on({
                                'plugin::pushstate::success-ajax' : function(){

                                    $this.page_next_init();
                                }
                            });
                        }else{
                            $this.page_next_init();
                        }
                    }else{
                        $this.page_next_init();
                    }
                }else{
                    $this.page_next_init();

                }
            }else{

                if(pushstateArgs.index == null){
                    $this.page_next_init();
                }
            }

        }
    },

    page_next_init : function(){
        var $this = this;

        $(gusto).trigger('plugin::set_page::next');

    }


};

/**
 * Page - About
 * @mixin
 * @description : About type page
 * @version 1.0
 * @author  William Manco
 * @copyright Gusto IDS
 * @param options
 * @dependency blast, GSAP, scrollmagic
 * @returns {Page}
 */
"undefined" == typeof Gusto && (Gusto = {}, gusto = {});
"undefined" == typeof Gusto.Frontend && (Gusto.Frontend = {}, gusto.frontend = {});
"undefined" == typeof Gusto.Frontend.Page && (Gusto.Frontend.Page = {}, gusto.frontend.page = {});
"undefined" == typeof Gusto.Frontend.Page.About && (Gusto.Frontend.Page.About = {}, gusto.frontend.page.about = {});

Gusto.Frontend.Page.About = function Gusto(id){

    this.opt = {
        id: id,
        selector_id : "#module-page-about-"+id,
        selector: "[data-module='page'][data-type='about'][data-id='"+id+"']",
        element : $('[data-module="page"][data-type="about"][data-id='+id+']')
    };

    this.$ = {
        aboutChildren : this.opt.element.find('.inout-item'),
        siteContentChildren : $('#content .inout-item'),
        overlayBg : this.opt.element.find('.overlay-bg'),
        containerBg : this.opt.element.find('.container-bg'),
        contentBg : this.opt.element.find('.content-bg'),
        img : this.opt.element.find('.content-bg img'),
        contentBgImg : this.opt.element.find('.content-bg, .content-bg img'),
        title : this.opt.element.find('.title'),
        textContent : this.opt.element.find('.text-content'),
        textContainer : this.opt.element.find('.text-container'),
        linkCategory : this.opt.element.find('.category-album-container a'),
        lineCategory : this.opt.element.find('.category-album-container .line'),
        contentNumber : this.opt.element.find('.category-album-container .content-number'),
        linkMenu : $('.site-header').find('a.link-menu'),
        linkClose : $('.site-header').find('a.link-close'),
        linkBack : $('.site-header').find('a.link-back'),
        social : this.opt.element.find('.social'),
        piva : this.opt.element.find('.piva')
    };

    this._init();
    this._events({
        reinit : false
    });

    return this;
};

Gusto.Frontend.Page.About.prototype = {

    _init: function () {
        var $this = this;
    },

    _events: function (args) {
        var $this = this;

        if(args.reinit){
            $(gusto.frontend.plugin.pushstate.event).on({
                'plugin::pushstate::before-ajax': function(){

                },
                'page::load': function(){
                }
            });
            return;
        }

        $(gusto).on({
            '::leave' : function(e, args){
                if(args !== undefined){
                    if(args.isClick !== undefined){
                        $this.opt.next_page = true;
                    }
                }
            }
        });

        $this.$.linkMenu.off().on({
            'mouseenter' : function(){
                $this.blast_change_word();
            },
            'mouseleave'  : function(){
                //TweenLite.to($this.$.linkMenu.find('.blast'),.3,{ y:  '0px', ease: Power4.easeInOut});
            },
            'click' : function(event){
                event.preventDefault();
                var _this = $(this);
                if(!$(this).hasClass('is_change')){

                    $this._events({
                        reinit : true
                    });
                    $this.change_word();
                    $this.tween_enter();
                    $(gusto).trigger('header::standard::change_dimension',{
                        dimension : '100%',
                        colorLogo : '#fff',
                        isClose : true,
                        forced : true,
                        nextPage : $this.opt.next_page
                    });
                    $(gusto).trigger('::leave',{
                        force : true
                    });
                    setTimeout(function(){
                        _this.addClass('is_change');
                    },700);
                    TweenMax.to($this.$.linkBack,.3,{ opacity:0, ease:Power4.easeInOut},0);

                }else{
                    $this.change_word();
                    $this.tween_leave();
                    var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));

                    $(gusto).trigger('::enter',{
                        force : true
                    });
                    setTimeout(function(){
                        _this.removeClass('is_change');
                        TweenMax.set($this.$.linkBack,{ opacity:1});
                        TweenMax.set($this.$.linkBack.find('.blast'),{ opacity:0});
                        TweenMax.staggerTo($this.$.linkBack.find('.blast'),1,{ opacity:1, ease:Power4.easeInOut},-.08,0);
                    },700);

                    if(pushstateArgs === null){
                        $(gusto).trigger('header::standard::change_dimension', {
                            dimension: '100%',
                            colorLogo: '#000',
                            forced: true,
                            nextPage : $this.opt.next_page
                        });
                        return false;
                    }

                    if(pushstateArgs.page == 'home'){
                        $(gusto).trigger('header::standard::change_dimension', {
                            dimension: '50%',
                            colorLogo: '#000',
                            forced: true,
                            nextPage : $this.opt.next_page
                        });
                    }else{
                        $(gusto).trigger('header::standard::change_dimension', {
                            dimension: '100%',
                            colorLogo: '#000',
                            forced: true,
                            nextPage : $this.opt.next_page
                        });
                    }

                }
                return false;
            }
        });

        $this.$.linkClose.off().on({
            'click' : function(event){
                event.preventDefault();
                $this.change_word();
                $this.tween_leave();
                var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));

                $(gusto).trigger('::enter',{
                    force : true
                });

                if(pushstateArgs.page == 'home'){
                    $(gusto).trigger('header::standard::change_dimension', {
                        dimension: '50%',
                        colorLogo: '#000',
                        forced: true,
                        nextPage : $this.opt.next_page
                    });
                }else{
                    $(gusto).trigger('header::standard::change_dimension', {
                        dimension: '100%',
                        colorLogo: '#000',
                        forced: true,
                        nextPage : $this.opt.next_page
                    });
                }

                return false;
            }
        });

        $this.$.linkCategory.on({
            'mouseenter' : function(){
                var _this = $(this);
                if($this.opt.timelineTweenPageEnterComplete){

                }
                TweenMax.to(_this.find('img'),.3,{ opacity: 1});
                TweenMax.to(_this.find('.title .content-overflow'),.5,{ y: '0%', ease: Power4.easeInOut});
                TweenMax.to(_this.find('.number .content-number'),.4,{ y: '-100%', ease: Power4.easeInOut});
            },
            'mouseleave' : function(){
                var _this = $(this);
                if($this.opt.timelineTweenPageEnterComplete) {

                }
                TweenMax.to(_this.find('img'),.3,{ opacity:.4});
                TweenMax.to(_this.find('.title .content-overflow'),.4,{ y: '100%', ease: Power4.easeInOut});
                TweenMax.to(_this.find('.number .content-number'),.5,{ y: '0%', ease: Power4.easeInOut});

            },
            'click' : function(){
                if($this.opt.tween_enter == true){
                    $this.change_word();
                    setTimeout(function(){
                        $this.$.linkMenu.removeClass('is_change');
                    },700);
                }
                $this.tween_leave();
            }
        });

    },

    tween_enter : function(){
        var $this = this;

        $this.opt.timelineTweenPageLeave = new TimelineLite({ onComplete : function(){
            $this.opt.element.css('display','block');
        }});

        $this.opt.tween_enter = true;

        TweenLite.set($this.$.img,{ y:'-50%'});
        TweenLite.set($this.$.textContent.find('.blast'),{ y:''});
        TweenLite.set($this.$.textContainer,{ opacity:'1'});

        if($this.opt.element.find(".blast").length <= 0){
            $this.$.textContainer.blast({
                delimiter: "sentence",
                tag: "span"
            });
            $this.opt.element.find( ".blast" ).wrapInner( "<div class='container-blast'><div class='content-blast'></div></div>" );

        }


        $this.$.contentBlast = $this.opt.element.find('.content-blast');
        $this.$.containerBlast = $this.opt.element.find('.container-blast');

        $this.opt.timelineTweenPageEnter = new TimelineLite({ onComplete : function(){
            $('html,body').scrollTop(0);
            $this.opt.timelineTweenPageEnterComplete = true;
        }});


        if(Modernizr.smallscreen){
            $this.opt.timelineTweenPageEnter
                .to($this.$.overlayBg,.6,{ y:'0%', ease: Power3.easeInOut},.5)
                .to($this.$.title.find('.content-overflow'),1,{ y: '0%', ease: Power3.easeInOut},.5)
                .staggerTo($this.$.contentBlast,1,{ y:'0%', ease: Power3.easeOut},.05,.7)
                .to($this.$.social,.6,{ opacity:1, ease: Power3.easeInOut},.5)
                .to($this.$.piva,.6,{  opacity:1, ease: Power3.easeInOut},.5)
            ;
            return;
        }

        $this.opt.timelineTweenPageEnter
            .to($this.$.overlayBg,.6,{ y:'0%', ease: Power3.easeInOut},.5)
            .staggerTo($this.$.contentBg,1.5,{ y:'0%', ease: Power3.easeOut},.1,.7)
            .staggerTo($this.$.img,1.5,{ y:'0%', ease: Power3.easeOut},.1,.3)
            .staggerTo($this.$.contentNumber,.9,{ y:'0%', ease: Power3.easeOut},.1,.5)
            .staggerTo($this.$.lineCategory,.9,{ y:'0%', ease: Power3.easeOut},.1,.5)
            .staggerTo($this.$.contentBlast,1,{ y:'0%', ease: Power3.easeOut},.05,.7)
            .to($this.$.social,.6,{ opacity:1, ease: Power3.easeInOut},.5)
            .to($this.$.piva,.6,{  opacity:1, ease: Power3.easeInOut},.5)
        ;

    },

    tween_leave : function(){
        var $this = this;

        $this.opt.timelineTweenPageLeave = new TimelineLite({ onComplete : function(){
            $this.opt.element.css('display','none');
            $this.opt.tween_enter = false;

        }});



        if(Modernizr.smallscreen){
            $this.opt.timelineTweenPageLeave
                .to($this.$.overlayBg,.7,{ y:'100%', ease: Power4.easeInOut},.5)
                .staggerTo($this.$.contentBg,1,{ y:'100%', ease: Power3.easeIn},.1,0)
                .staggerTo($this.$.img,.7,{ y:'100%', ease: Power3.easeIn},.1,0)
                .staggerTo($this.$.contentNumber,.7,{ y:'100%', ease: Power3.easeIn},.1,0)
                .staggerTo($this.$.lineCategory,.7,{ y:'100%', ease: Power3.easeIn},.1,0)
                .staggerTo($this.$.contentBlast,.7,{ y:'100%', ease: Power3.easeIn},.05,0)
                .to($this.$.social,.6,{ opacity:0, ease: Power3.easeIn},.5)
                .to($this.$.piva,.6,{  opacity:0, ease: Power3.easeIn},.5)
                .to($this.$.title.find('.content-overflow'),.4,{ y: '100%', ease: Power4.easeIn},0)
            ;
            return;
        }

        $this.opt.timelineTweenPageLeave
            .to($this.$.overlayBg,.7,{ y:'100%', ease: Power4.easeInOut},.5)
            .staggerTo($this.$.contentBg,1,{ y:'100%', ease: Power3.easeIn},.1,0)
            .staggerTo($this.$.img,.7,{ y:'100%', ease: Power3.easeIn},.1,0)
            .staggerTo($this.$.contentNumber,.7,{ y:'100%', ease: Power3.easeIn},.1,0)
            .staggerTo($this.$.lineCategory,.7,{ y:'100%', ease: Power3.easeIn},.1,0)
            .staggerTo($this.$.contentBlast,.7,{ y:'100%', ease: Power3.easeIn},.05,0)
            .to($this.$.social,.6,{ opacity:0, ease: Power3.easeIn},.5)
            .to($this.$.piva,.6,{  opacity:0, ease: Power3.easeIn},.5)
            .to($this.$.title.find('.content-overflow'),.4,{ y: '100%', ease: Power4.easeIn},0)
        ;

    },

    blast_change_word : function(){
      var $this = this;

        if($this.$.linkMenu.find('.blast').length <= 0){
            $this.$.linkMenu.blast({
                delimiter: "character",
                returnGenerated: false,
                aria: false
            });
        }

    },

    change_word : function(){
        var $this = this;

        var change_to = $this.$.linkMenu.attr('data-change-to');
        var change_from = $this.$.linkMenu.attr('data-change-from');


        $this.blast_change_word();



        TweenMax.staggerTo($this.$.linkMenu.find('.blast'),.3,{ opacity: 0, ease: Power1.easeInOut, onComplete: function(){
            TweenMax.set($this.$.linkMenu,{ opacity: 0});
            $this.$.linkMenu.text(change_to);
            $this.blast_change_word();
            TweenMax.set($this.$.linkMenu.find('.blast'),{ opacity: 0});
            TweenMax.set($this.$.linkMenu,{ opacity: 1});
            $this.$.linkMenu.attr('data-change-to',change_from);
            $this.$.linkMenu.attr('data-change-from',change_to);
            if($this.opt.next_page)
                return;
            setTimeout(function(){
                TweenMax.staggerTo($this.$.linkMenu.find('.blast'),.5,{ opacity: 1, ease: Power1.easeInOut},-.05);
            },700);
        }},.05);

    }
};
/**
 * Page - Project
 * @mixin
 * @description : Project type page
 * @version 1.0
 * @author  William Manco
 * @copyright Gusto IDS
 * @param options
 * @dependency blast, GSAP, scrollmagic
 * @returns {Page}
 */
"undefined" == typeof Gusto && (Gusto = {}, gusto = {});
"undefined" == typeof Gusto.Frontend && (Gusto.Frontend = {}, gusto.frontend = {});
"undefined" == typeof Gusto.Frontend.Page && (Gusto.Frontend.Page = {}, gusto.frontend.page = {});
"undefined" == typeof Gusto.Frontend.Page.Project && (Gusto.Frontend.Page.Project = {}, gusto.frontend.page.project = {});

Gusto.Frontend.Page.Project = function Gusto(id){

    this.opt = {
        id: id,
        selector_id : "#module-page-project-"+id,
        selector: "[data-module='page'][data-type='project'][data-id='"+id+"']",
        element : $('[data-module="page"][data-type="project"][data-id='+id+']'),
        mobile : 0,
        force_all_white : true
    };

    this.$ = {
        title : this.opt.element.find('.title'),
        subTitle : this.opt.element.find('.sub-title'),
        rowTop : this.opt.element.find('.row-top'),
        slideImg : this.opt.element.find('.row-top img'),
        slides : this.opt.element.find('.row-slides .slide'),
        textContent : this.opt.element.find('.text-content'),
        textContentP : this.opt.element.find('.text-content p')
    };

    this.size = {};


    this._init();
    this._events({
        reinit : false
    });

    return this;
};

Gusto.Frontend.Page.Project.prototype = {

    _init: function () {
        var $this = this;



        $this.init_module();
    },

    _events: function (args) {
        var $this = this;

        $(gusto).on({
            '::enter' : function(){
                $this.enter_module();
            },
            '::leave' : function(){
                $this.leave_module();
            }
        });

        $(gusto.frontend.plugin.pushstate.event).on({
            'plugin::pushstate::before-ajax' : function(){
                $this.leave_module();
            }
        });

        $(gusto.frontend.image_preload.standard.event).on({
            'image_preload::all' : function(){
                if($this.opt.pageTitleEnterComplete) {
                    $(gusto).trigger('::enter');
                    $(gusto).trigger('page::load');
                }
                $this.opt.preloadImageComplete = true;
                $this.$.slides.each(function() {
                    var _this = $(this);
                    _this.attr('data-offset-top',_this.find('.container-bg').offset().top);
                });
            }
        });

        $(window).on({
            'scroll' : function() {

                $this.$.textContentP.each(function(){
                    var _this = $(this);

                    if(verge.inViewport(_this) && !_this.hasClass('is-open')) {
                        TweenMax.staggerTo($this.$.contentBlast,1,{ y:'0%', ease: Power3.easeOut},.05,.7);
                        _this.addClass('is-open');
                    }
                });

                if(Modernizr.smallscreen){

                    TweenLite.to($this.$.title.find('span'),.5, {y: 0});
                    TweenLite.to($this.$.subTitle.find('span'),.5, {y: 0});

                    return;
                }

                TweenLite.to($this.$.title.find('span'),.5, {y: $(window).scrollTop() *.2 + 'px'});
                TweenLite.to($this.$.subTitle.find('span'),.5, {y: $(window).scrollTop() *.1 + 'px'});

                $this.$.slides.each(function(){
                    var _this = $(this);
                    var speed = .1;

                    if(verge.inViewport(_this)){
                        $this.tween_open_slide(_this);
                    }

                    if ($this.opt.mobile == 0) {

                        var scrollTop = _this.attr('data-offset-top') - ($(window).scrollTop()+$(window).height());
                        speed = _this.attr('data-parallax-speed');

                        TweenLite.to(_this.find('.container-bg'),.5, {y: scrollTop*speed + 'px'});

                    }
                });

                TweenLite.to($this.$.slideImg,.5, {y: -$(window).scrollTop() *.1 + 'px'});
            }
        });

        $(document).ready(function(){
            $this.enter_page_title();
        });
    },

    tween_open_slide : function(_this){
        var $this = this;

        if(!_this.hasClass('is-open')){
            TweenLite.from(_this.find('img'),2.4,{ x: '-30%', ease:Power3.easeOut});
            TweenLite.to(_this.find('.overlay'),1.8,{ x: '100%', ease:Power4.easeOut});
            _this.addClass('is-open');
        }
    },

    init_module : function(){
        var $this = this;


        TweenLite.set($this.opt.element,{ height: $(window).height()});

        $this.$.textContentP.blast({
            delimiter: "sentence",
            tag: "span"
        });

        $( ".blast" ).wrapInner( "<div class='container-blast'><div class='content-blast'></div></div>" );

        $this.$.contentBlast = $this.opt.element.find('.content-blast');


        if(Modernizr.smallscreen || $this.opt.force_all_white){
            $this.tween_init_transition();
            return;
        }


        if(!JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'))){

            args.targetUrl = $this.opt.element.attr('data-url');
            args.page = $this.opt.element.attr('data-page');
            $(gusto).trigger('plugin::pushstate::set_storage',args);
        }



        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'));
        switch(pushstateArgs.page){
            case 'landing-projects' :
                $this.tween_init_transition_from_landing();
                break;
            default :
                $this.tween_init_transition();
                break;
        }
    },

    leave_module : function(){
        var $this = this;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate'));

        $this.opt.leave_module = true;

        if(Modernizr.smallscreen || $this.opt.force_all_white){
            $this.tween_leave();
            return;
        }

        switch(pushstateArgs.page){
            case 'landing-projects' :
                $this.tween_leave_to_landing();
                break;
            default :
                $this.tween_leave();
                break;
        }
    },

    enter_module : function(){
        var $this = this;
        var pushstateArgs = JSON.parse(gusto.storage.getItem('plugin::pushstate::prev'));

        if(Modernizr.smallscreen || $this.opt.force_all_white){
            $this.tween_enter();
            return;
        }

        switch(pushstateArgs.page){
            case 'landing-projects' :
                $this.tween_enter_from_landing();
                break;
            default :
                $this.tween_enter();
                break;
        }
    },

    enter_page_title : function(){
        var $this = this;

        $this.opt.timelineTweenPageTitleEnter = new TimelineLite({
            onComplete: function(){
                $this.opt.pageTitleEnterComplete = true;
                if($this.opt.preloadImageComplete){
                    $(gusto).trigger('::enter');
                    $(gusto).trigger('page::load');
                }
            }
        });

        $this.opt.timelineTweenPageTitleEnter
            .to($('.page-title .text'),1,{ y: '0%', ease:Expo.easeInOut},0)
            //
        ;

        if(Modernizr.smallscreen) {
            $this.opt.timelineTweenPageTitleEnter.to($('.page-title'),1,{ height: '0px', ease:Expo.easeInOut},1.5)
        }

    },

    tween_init_transition : function(){
        var $this = this;

        TweenLite.set($this.opt.element,{ css: {visibility: 'hidden'}});
        TweenLite.set($this.opt.element.find('.row-top img'),{ x: '-30%'});
        TweenLite.set($this.$.rowTop, {height: $(window).height()});
        if(!Modernizr.smallscreen) {
            TweenLite.set($this.$.title.find('span'), {y: $this.$.title.height()});
            TweenLite.set($this.$.subTitle.find('span'), {y: $this.$.subTitle.height()});
        }

    },

    tween_enter  : function(){
        var $this = this;

        TweenLite.set($this.opt.element,{ css: {visibility: 'visible'}});
        TweenMax.set($this.$.title.find('span'), {y: '0px'});
        if(!Modernizr.smallscreen) {
            TweenMax.set($('.page-title .text'), {display: 'none'});
        }


        $this.opt.timelineTweenPageEnter = new TimelineLite();
        $this.opt.timelineTweenPageEnter
            .to($this.opt.element.find('.row-top img'),1.8,{ x: '0%', ease:Expo.easeInOut}, 0)
            .to($this.opt.element.find('.row-top .overlay'),1.8,{ x: '100%', ease:Expo.easeInOut}, 0)
            .to($this.$.subTitle.find('span'),1.8, {y: '0px', ease:Expo.easeInOut}, 0)
        ;

    },

    tween_leave : function(){
        var $this = this;


        $this.opt.timelineTweenPageLeave = new TimelineLite({ onComplete : tweenPageLeaveComplete});
        $this.opt.timelineTweenPageLeave
            .to($this.opt.element.find('.row-top img'),1.3,{ x: '-30%', ease:Expo.easeInOut},0)
            .to($this.opt.element.find('.row-top .overlay'),1.3,{ x: '0%', ease:Expo.easeInOut},0)
            .to($this.$.subTitle.find('span'),1.3, {y: '25px', ease:Expo.easeInOut},0)
            .to($this.$.title.find('span'),1.3, {y: '85px', ease:Expo.easeInOut},0)
            .to($this.$.contentBlast,.5,{ y:'100%', ease: Power3.easeOut},.7)
            .to($this.$.slides.find('img'),1.3,{ x: '-30%', ease:Power4.easeOut},0)
            .to($this.$.slides.find('.overlay'),1.3,{ x: '0%', ease:Power4.easeOut},0)
        ;

        /**
         * Callback
         */
        function tweenPageLeaveComplete(){

            $this.$.slides.removeClass('is-open');
            $('.blast-root').removeClass('is-open');



            if($('body').data('plugin::pushstate') !== undefined){
                if($('body').data('plugin::pushstate').ajax_state != undefined){
                    if($('body').data('plugin::pushstate').ajax_state != 'success'){
                        $(gusto.frontend.plugin.pushstate.event).on({
                            'plugin::pushstate::success-ajax' : function(){
                                $this.page_next_init();
                            }
                        });
                    }else{
                        $this.page_next_init();
                    }
                }else{
                    $this.page_next_init();
                }
            }else{
                $this.page_next_init();

            }
        }
    },

    page_next_init : function(){
        var $this = this;
        $(gusto).trigger('plugin::set_page::next');

    }

};
/**
 * Swiper 3.3.1
 * Most modern mobile touch slider and framework with hardware accelerated transitions
 *
 * http://www.idangero.us/swiper/
 *
 * Copyright 2016, Vladimir Kharlampidi
 * The iDangero.us
 * http://www.idangero.us/
 *
 * Licensed under MIT
 *
 * Released on: February 7, 2016
 */
(function () {
    'use strict';
    var $;
    /*===========================
    Swiper
    ===========================*/
    var Swiper = function (container, params) {
        if (!(this instanceof Swiper)) return new Swiper(container, params);

        var defaults = {
            direction: 'horizontal',
            touchEventsTarget: 'container',
            initialSlide: 0,
            speed: 300,
            // autoplay
            autoplay: false,
            autoplayDisableOnInteraction: true,
            autoplayStopOnLast: false,
            // To support iOS's swipe-to-go-back gesture (when being used in-app, with UIWebView).
            iOSEdgeSwipeDetection: false,
            iOSEdgeSwipeThreshold: 20,
            // Free mode
            freeMode: false,
            freeModeMomentum: true,
            freeModeMomentumRatio: 1,
            freeModeMomentumBounce: true,
            freeModeMomentumBounceRatio: 1,
            freeModeSticky: false,
            freeModeMinimumVelocity: 0.02,
            // Autoheight
            autoHeight: false,
            // Set wrapper width
            setWrapperSize: false,
            // Virtual Translate
            virtualTranslate: false,
            // Effects
            effect: 'slide', // 'slide' or 'fade' or 'cube' or 'coverflow' or 'flip'
            coverflow: {
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows : true
            },
            flip: {
                slideShadows : true,
                limitRotation: true
            },
            cube: {
                slideShadows: true,
                shadow: true,
                shadowOffset: 20,
                shadowScale: 0.94
            },
            fade: {
                crossFade: false
            },
            // Parallax
            parallax: false,
            // Scrollbar
            scrollbar: null,
            scrollbarHide: true,
            scrollbarDraggable: false,
            scrollbarSnapOnRelease: false,
            // Keyboard Mousewheel
            keyboardControl: false,
            mousewheelControl: false,
            mousewheelReleaseOnEdges: false,
            mousewheelInvert: false,
            mousewheelForceToAxis: false,
            mousewheelSensitivity: 1,
            // Hash Navigation
            hashnav: false,
            // Breakpoints
            breakpoints: undefined,
            // Slides grid
            spaceBetween: 0,
            slidesPerView: 1,
            slidesPerColumn: 1,
            slidesPerColumnFill: 'column',
            slidesPerGroup: 1,
            centeredSlides: false,
            slidesOffsetBefore: 0, // in px
            slidesOffsetAfter: 0, // in px
            // Round length
            roundLengths: false,
            // Touches
            touchRatio: 1,
            touchAngle: 45,
            simulateTouch: true,
            shortSwipes: true,
            longSwipes: true,
            longSwipesRatio: 0.5,
            longSwipesMs: 300,
            followFinger: true,
            onlyExternal: false,
            threshold: 0,
            touchMoveStopPropagation: true,
            // Unique Navigation Elements
            uniqueNavElements: true,
            // Pagination
            pagination: null,
            paginationElement: 'span',
            paginationClickable: false,
            paginationHide: false,
            paginationBulletRender: null,
            paginationProgressRender: null,
            paginationFractionRender: null,
            paginationCustomRender: null,
            paginationType: 'bullets', // 'bullets' or 'progress' or 'fraction' or 'custom'
            // Resistance
            resistance: true,
            resistanceRatio: 0.85,
            // Next/prev buttons
            nextButton: null,
            prevButton: null,
            // Progress
            watchSlidesProgress: false,
            watchSlidesVisibility: false,
            // Cursor
            grabCursor: false,
            // Clicks
            preventClicks: true,
            preventClicksPropagation: true,
            slideToClickedSlide: false,
            // Lazy Loading
            lazyLoading: false,
            lazyLoadingInPrevNext: false,
            lazyLoadingInPrevNextAmount: 1,
            lazyLoadingOnTransitionStart: false,
            // Images
            preloadImages: true,
            updateOnImagesReady: true,
            // loop
            loop: false,
            loopAdditionalSlides: 0,
            loopedSlides: null,
            // Control
            control: undefined,
            controlInverse: false,
            controlBy: 'slide', //or 'container'
            // Swiping/no swiping
            allowSwipeToPrev: true,
            allowSwipeToNext: true,
            swipeHandler: null, //'.swipe-handler',
            noSwiping: true,
            noSwipingClass: 'swiper-no-swiping',
            // NS
            slideClass: 'swiper-slide',
            slideActiveClass: 'swiper-slide-active',
            slideVisibleClass: 'swiper-slide-visible',
            slideDuplicateClass: 'swiper-slide-duplicate',
            slideNextClass: 'swiper-slide-next',
            slidePrevClass: 'swiper-slide-prev',
            wrapperClass: 'swiper-wrapper',
            bulletClass: 'swiper-pagination-bullet',
            bulletActiveClass: 'swiper-pagination-bullet-active',
            buttonDisabledClass: 'swiper-button-disabled',
            paginationCurrentClass: 'swiper-pagination-current',
            paginationTotalClass: 'swiper-pagination-total',
            paginationHiddenClass: 'swiper-pagination-hidden',
            paginationProgressbarClass: 'swiper-pagination-progressbar',
            // Observer
            observer: false,
            observeParents: false,
            // Accessibility
            a11y: false,
            prevSlideMessage: 'Previous slide',
            nextSlideMessage: 'Next slide',
            firstSlideMessage: 'This is the first slide',
            lastSlideMessage: 'This is the last slide',
            paginationBulletMessage: 'Go to slide {{index}}',
            // Callbacks
            runCallbacksOnInit: true
            /*
            Callbacks:
            onInit: function (swiper)
            onDestroy: function (swiper)
            onClick: function (swiper, e)
            onTap: function (swiper, e)
            onDoubleTap: function (swiper, e)
            onSliderMove: function (swiper, e)
            onSlideChangeStart: function (swiper)
            onSlideChangeEnd: function (swiper)
            onTransitionStart: function (swiper)
            onTransitionEnd: function (swiper)
            onImagesReady: function (swiper)
            onProgress: function (swiper, progress)
            onTouchStart: function (swiper, e)
            onTouchMove: function (swiper, e)
            onTouchMoveOpposite: function (swiper, e)
            onTouchEnd: function (swiper, e)
            onReachBeginning: function (swiper)
            onReachEnd: function (swiper)
            onSetTransition: function (swiper, duration)
            onSetTranslate: function (swiper, translate)
            onAutoplayStart: function (swiper)
            onAutoplayStop: function (swiper),
            onLazyImageLoad: function (swiper, slide, image)
            onLazyImageReady: function (swiper, slide, image)
            */

        };
        var initialVirtualTranslate = params && params.virtualTranslate;

        params = params || {};
        var originalParams = {};
        for (var param in params) {
            if (typeof params[param] === 'object' && params[param] !== null && !(params[param].nodeType || params[param] === window || params[param] === document || (typeof Dom7 !== 'undefined' && params[param] instanceof Dom7) || (typeof jQuery !== 'undefined' && params[param] instanceof jQuery))) {
                originalParams[param] = {};
                for (var deepParam in params[param]) {
                    originalParams[param][deepParam] = params[param][deepParam];
                }
            }
            else {
                originalParams[param] = params[param];
            }
        }
        for (var def in defaults) {
            if (typeof params[def] === 'undefined') {
                params[def] = defaults[def];
            }
            else if (typeof params[def] === 'object') {
                for (var deepDef in defaults[def]) {
                    if (typeof params[def][deepDef] === 'undefined') {
                        params[def][deepDef] = defaults[def][deepDef];
                    }
                }
            }
        }

        // Swiper
        var s = this;

        // Params
        s.params = params;
        s.originalParams = originalParams;

        // Classname
        s.classNames = [];
        /*=========================
          Dom Library and plugins
          ===========================*/
        if (typeof $ !== 'undefined' && typeof Dom7 !== 'undefined'){
            $ = Dom7;
        }
        if (typeof $ === 'undefined') {
            if (typeof Dom7 === 'undefined') {
                $ = window.Dom7 || window.Zepto || window.jQuery;
            }
            else {
                $ = Dom7;
            }
            if (!$) return;
        }
        // Export it to Swiper instance
        s.$ = $;

        /*=========================
          Breakpoints
          ===========================*/
        s.currentBreakpoint = undefined;
        s.getActiveBreakpoint = function () {
            //Get breakpoint for window width
            if (!s.params.breakpoints) return false;
            var breakpoint = false;
            var points = [], point;
            for ( point in s.params.breakpoints ) {
                if (s.params.breakpoints.hasOwnProperty(point)) {
                    points.push(point);
                }
            }
            points.sort(function (a, b) {
                return parseInt(a, 10) > parseInt(b, 10);
            });
            for (var i = 0; i < points.length; i++) {
                point = points[i];
                if (point >= window.innerWidth && !breakpoint) {
                    breakpoint = point;
                }
            }
            return breakpoint || 'max';
        };
        s.setBreakpoint = function () {
            //Set breakpoint for window width and update parameters
            var breakpoint = s.getActiveBreakpoint();
            if (breakpoint && s.currentBreakpoint !== breakpoint) {
                var breakPointsParams = breakpoint in s.params.breakpoints ? s.params.breakpoints[breakpoint] : s.originalParams;
                var needsReLoop = s.params.loop && (breakPointsParams.slidesPerView !== s.params.slidesPerView);
                for ( var param in breakPointsParams ) {
                    s.params[param] = breakPointsParams[param];
                }
                s.currentBreakpoint = breakpoint;
                if(needsReLoop && s.destroyLoop) {
                    s.reLoop(true);
                }
            }
        };
        // Set breakpoint on load
        if (s.params.breakpoints) {
            s.setBreakpoint();
        }

        /*=========================
          Preparation - Define Container, Wrapper and Pagination
          ===========================*/
        s.container = $(container);
        if (s.container.length === 0) return;
        if (s.container.length > 1) {
            var swipers = [];
            s.container.each(function () {
                var container = this;
                swipers.push(new Swiper(this, params));
            });
            return swipers;
        }

        // Save instance in container HTML Element and in data
        s.container[0].swiper = s;
        s.container.data('swiper', s);

        s.classNames.push('swiper-container-' + s.params.direction);

        if (s.params.freeMode) {
            s.classNames.push('swiper-container-free-mode');
        }
        if (!s.support.flexbox) {
            s.classNames.push('swiper-container-no-flexbox');
            s.params.slidesPerColumn = 1;
        }
        if (s.params.autoHeight) {
            s.classNames.push('swiper-container-autoheight');
        }
        // Enable slides progress when required
        if (s.params.parallax || s.params.watchSlidesVisibility) {
            s.params.watchSlidesProgress = true;
        }
        // Coverflow / 3D
        if (['cube', 'coverflow', 'flip'].indexOf(s.params.effect) >= 0) {
            if (s.support.transforms3d) {
                s.params.watchSlidesProgress = true;
                s.classNames.push('swiper-container-3d');
            }
            else {
                s.params.effect = 'slide';
            }
        }
        if (s.params.effect !== 'slide') {
            s.classNames.push('swiper-container-' + s.params.effect);
        }
        if (s.params.effect === 'cube') {
            s.params.resistanceRatio = 0;
            s.params.slidesPerView = 1;
            s.params.slidesPerColumn = 1;
            s.params.slidesPerGroup = 1;
            s.params.centeredSlides = false;
            s.params.spaceBetween = 0;
            s.params.virtualTranslate = true;
            s.params.setWrapperSize = false;
        }
        if (s.params.effect === 'fade' || s.params.effect === 'flip') {
            s.params.slidesPerView = 1;
            s.params.slidesPerColumn = 1;
            s.params.slidesPerGroup = 1;
            s.params.watchSlidesProgress = true;
            s.params.spaceBetween = 0;
            s.params.setWrapperSize = false;
            if (typeof initialVirtualTranslate === 'undefined') {
                s.params.virtualTranslate = true;
            }
        }

        // Grab Cursor
        if (s.params.grabCursor && s.support.touch) {
            s.params.grabCursor = false;
        }

        // Wrapper
        s.wrapper = s.container.children('.' + s.params.wrapperClass);

        // Pagination
        if (s.params.pagination) {
            s.paginationContainer = $(s.params.pagination);
            if (s.params.uniqueNavElements && typeof s.params.pagination === 'string' && s.paginationContainer.length > 1 && s.container.find(s.params.pagination).length === 1) {
                s.paginationContainer = s.container.find(s.params.pagination);
            }

            if (s.params.paginationType === 'bullets' && s.params.paginationClickable) {
                s.paginationContainer.addClass('swiper-pagination-clickable');
            }
            else {
                s.params.paginationClickable = false;
            }
            s.paginationContainer.addClass('swiper-pagination-' + s.params.paginationType);
        }
        // Next/Prev Buttons
        if (s.params.nextButton || s.params.prevButton) {
            if (s.params.nextButton) {
                s.nextButton = $(s.params.nextButton);
                if (s.params.uniqueNavElements && typeof s.params.nextButton === 'string' && s.nextButton.length > 1 && s.container.find(s.params.nextButton).length === 1) {
                    s.nextButton = s.container.find(s.params.nextButton);
                }
            }
            if (s.params.prevButton) {
                s.prevButton = $(s.params.prevButton);
                if (s.params.uniqueNavElements && typeof s.params.prevButton === 'string' && s.prevButton.length > 1 && s.container.find(s.params.prevButton).length === 1) {
                    s.prevButton = s.container.find(s.params.prevButton);
                }
            }
        }

        // Is Horizontal
        s.isHorizontal = function () {
            return s.params.direction === 'horizontal';
        };
        // s.isH = isH;

        // RTL
        s.rtl = s.isHorizontal() && (s.container[0].dir.toLowerCase() === 'rtl' || s.container.css('direction') === 'rtl');
        if (s.rtl) {
            s.classNames.push('swiper-container-rtl');
        }

        // Wrong RTL support
        if (s.rtl) {
            s.wrongRTL = s.wrapper.css('display') === '-webkit-box';
        }

        // Columns
        if (s.params.slidesPerColumn > 1) {
            s.classNames.push('swiper-container-multirow');
        }

        // Check for Android
        if (s.device.android) {
            s.classNames.push('swiper-container-android');
        }

        // Add classes
        s.container.addClass(s.classNames.join(' '));

        // Translate
        s.translate = 0;

        // Progress
        s.progress = 0;

        // Velocity
        s.velocity = 0;

        /*=========================
          Locks, unlocks
          ===========================*/
        s.lockSwipeToNext = function () {
            s.params.allowSwipeToNext = false;
        };
        s.lockSwipeToPrev = function () {
            s.params.allowSwipeToPrev = false;
        };
        s.lockSwipes = function () {
            s.params.allowSwipeToNext = s.params.allowSwipeToPrev = false;
        };
        s.unlockSwipeToNext = function () {
            s.params.allowSwipeToNext = true;
        };
        s.unlockSwipeToPrev = function () {
            s.params.allowSwipeToPrev = true;
        };
        s.unlockSwipes = function () {
            s.params.allowSwipeToNext = s.params.allowSwipeToPrev = true;
        };

        /*=========================
          Round helper
          ===========================*/
        function round(a) {
            return Math.floor(a);
        }
        /*=========================
          Set grab cursor
          ===========================*/
        if (s.params.grabCursor) {
            s.container[0].style.cursor = 'move';
            s.container[0].style.cursor = '-webkit-grab';
            s.container[0].style.cursor = '-moz-grab';
            s.container[0].style.cursor = 'grab';
        }
        /*=========================
          Update on Images Ready
          ===========================*/
        s.imagesToLoad = [];
        s.imagesLoaded = 0;

        s.loadImage = function (imgElement, src, srcset, checkForComplete, callback) {
            var image;
            function onReady () {
                if (callback) callback();
            }
            if (!imgElement.complete || !checkForComplete) {
                if (src) {
                    image = new window.Image();
                    image.onload = onReady;
                    image.onerror = onReady;
                    if (srcset) {
                        image.srcset = srcset;
                    }
                    if (src) {
                        image.src = src;
                    }
                } else {
                    onReady();
                }

            } else {//image already loaded...
                onReady();
            }
        };
        s.preloadImages = function () {
            s.imagesToLoad = s.container.find('img');
            function _onReady() {
                if (typeof s === 'undefined' || s === null) return;
                if (s.imagesLoaded !== undefined) s.imagesLoaded++;
                if (s.imagesLoaded === s.imagesToLoad.length) {
                    if (s.params.updateOnImagesReady) s.update();
                    s.emit('onImagesReady', s);
                }
            }
            for (var i = 0; i < s.imagesToLoad.length; i++) {
                s.loadImage(s.imagesToLoad[i], (s.imagesToLoad[i].currentSrc || s.imagesToLoad[i].getAttribute('src')), (s.imagesToLoad[i].srcset || s.imagesToLoad[i].getAttribute('srcset')), true, _onReady);
            }
        };

        /*=========================
          Autoplay
          ===========================*/
        s.autoplayTimeoutId = undefined;
        s.autoplaying = false;
        s.autoplayPaused = false;
        function autoplay() {
            s.autoplayTimeoutId = setTimeout(function () {
                if (s.params.loop) {
                    s.fixLoop();
                    s._slideNext();
                    s.emit('onAutoplay', s);
                }
                else {
                    if (!s.isEnd) {
                        s._slideNext();
                        s.emit('onAutoplay', s);
                    }
                    else {
                        if (!params.autoplayStopOnLast) {
                            s._slideTo(0);
                            s.emit('onAutoplay', s);
                        }
                        else {
                            s.stopAutoplay();
                        }
                    }
                }
            }, s.params.autoplay);
        }
        s.startAutoplay = function () {
            if (typeof s.autoplayTimeoutId !== 'undefined') return false;
            if (!s.params.autoplay) return false;
            if (s.autoplaying) return false;
            s.autoplaying = true;
            s.emit('onAutoplayStart', s);
            autoplay();
        };
        s.stopAutoplay = function (internal) {
            if (!s.autoplayTimeoutId) return;
            if (s.autoplayTimeoutId) clearTimeout(s.autoplayTimeoutId);
            s.autoplaying = false;
            s.autoplayTimeoutId = undefined;
            s.emit('onAutoplayStop', s);
        };
        s.pauseAutoplay = function (speed) {
            if (s.autoplayPaused) return;
            if (s.autoplayTimeoutId) clearTimeout(s.autoplayTimeoutId);
            s.autoplayPaused = true;
            if (speed === 0) {
                s.autoplayPaused = false;
                autoplay();
            }
            else {
                s.wrapper.transitionEnd(function () {
                    if (!s) return;
                    s.autoplayPaused = false;
                    if (!s.autoplaying) {
                        s.stopAutoplay();
                    }
                    else {
                        autoplay();
                    }
                });
            }
        };
        /*=========================
          Min/Max Translate
          ===========================*/
        s.minTranslate = function () {
            return (-s.snapGrid[0]);
        };
        s.maxTranslate = function () {
            return (-s.snapGrid[s.snapGrid.length - 1]);
        };
        /*=========================
          Slider/slides sizes
          ===========================*/
        s.updateAutoHeight = function () {
            // Update Height
            var slide = s.slides.eq(s.activeIndex)[0];
            if (typeof slide !== 'undefined') {
                var newHeight = slide.offsetHeight;
                if (newHeight) s.wrapper.css('height', newHeight + 'px');
            }
        };
        s.updateContainerSize = function () {
            var width, height;
            if (typeof s.params.width !== 'undefined') {
                width = s.params.width;
            }
            else {
                width = s.container[0].clientWidth;
            }
            if (typeof s.params.height !== 'undefined') {
                height = s.params.height;
            }
            else {
                height = s.container[0].clientHeight;
            }
            if (width === 0 && s.isHorizontal() || height === 0 && !s.isHorizontal()) {
                return;
            }

            //Subtract paddings
            width = width - parseInt(s.container.css('padding-left'), 10) - parseInt(s.container.css('padding-right'), 10);
            height = height - parseInt(s.container.css('padding-top'), 10) - parseInt(s.container.css('padding-bottom'), 10);

            // Store values
            s.width = width;
            s.height = height;
            s.size = s.isHorizontal() ? s.width : s.height;
        };

        s.updateSlidesSize = function () {
            s.slides = s.wrapper.children('.' + s.params.slideClass);
            s.snapGrid = [];
            s.slidesGrid = [];
            s.slidesSizesGrid = [];

            var spaceBetween = s.params.spaceBetween,
                slidePosition = -s.params.slidesOffsetBefore,
                i,
                prevSlideSize = 0,
                index = 0;
            if (typeof s.size === 'undefined') return;
            if (typeof spaceBetween === 'string' && spaceBetween.indexOf('%') >= 0) {
                spaceBetween = parseFloat(spaceBetween.replace('%', '')) / 100 * s.size;
            }

            s.virtualSize = -spaceBetween;
            // reset margins
            if (s.rtl) s.slides.css({marginLeft: '', marginTop: ''});
            else s.slides.css({marginRight: '', marginBottom: ''});

            var slidesNumberEvenToRows;
            if (s.params.slidesPerColumn > 1) {
                if (Math.floor(s.slides.length / s.params.slidesPerColumn) === s.slides.length / s.params.slidesPerColumn) {
                    slidesNumberEvenToRows = s.slides.length;
                }
                else {
                    slidesNumberEvenToRows = Math.ceil(s.slides.length / s.params.slidesPerColumn) * s.params.slidesPerColumn;
                }
                if (s.params.slidesPerView !== 'auto' && s.params.slidesPerColumnFill === 'row') {
                    slidesNumberEvenToRows = Math.max(slidesNumberEvenToRows, s.params.slidesPerView * s.params.slidesPerColumn);
                }
            }

            // Calc slides
            var slideSize;
            var slidesPerColumn = s.params.slidesPerColumn;
            var slidesPerRow = slidesNumberEvenToRows / slidesPerColumn;
            var numFullColumns = slidesPerRow - (s.params.slidesPerColumn * slidesPerRow - s.slides.length);
            for (i = 0; i < s.slides.length; i++) {
                slideSize = 0;
                var slide = s.slides.eq(i);
                if (s.params.slidesPerColumn > 1) {
                    // Set slides order
                    var newSlideOrderIndex;
                    var column, row;
                    if (s.params.slidesPerColumnFill === 'column') {
                        column = Math.floor(i / slidesPerColumn);
                        row = i - column * slidesPerColumn;
                        if (column > numFullColumns || (column === numFullColumns && row === slidesPerColumn-1)) {
                            if (++row >= slidesPerColumn) {
                                row = 0;
                                column++;
                            }
                        }
                        newSlideOrderIndex = column + row * slidesNumberEvenToRows / slidesPerColumn;
                        slide
                            .css({
                                '-webkit-box-ordinal-group': newSlideOrderIndex,
                                '-moz-box-ordinal-group': newSlideOrderIndex,
                                '-ms-flex-order': newSlideOrderIndex,
                                '-webkit-order': newSlideOrderIndex,
                                'order': newSlideOrderIndex
                            });
                    }
                    else {
                        row = Math.floor(i / slidesPerRow);
                        column = i - row * slidesPerRow;
                    }
                    slide
                        .css({
                            'margin-top': (row !== 0 && s.params.spaceBetween) && (s.params.spaceBetween + 'px')
                        })
                        .attr('data-swiper-column', column)
                        .attr('data-swiper-row', row);

                }
                if (slide.css('display') === 'none') continue;
                if (s.params.slidesPerView === 'auto') {
                    slideSize = s.isHorizontal() ? slide.outerWidth(true) : slide.outerHeight(true);
                    if (s.params.roundLengths) slideSize = round(slideSize);
                }
                else {
                    slideSize = (s.size - (s.params.slidesPerView - 1) * spaceBetween) / s.params.slidesPerView;
                    if (s.params.roundLengths) slideSize = round(slideSize);

                    if (s.isHorizontal()) {
                        s.slides[i].style.width = slideSize + 'px';
                    }
                    else {
                        s.slides[i].style.height = slideSize + 'px';
                    }
                }
                s.slides[i].swiperSlideSize = slideSize;
                s.slidesSizesGrid.push(slideSize);


                if (s.params.centeredSlides) {
                    slidePosition = slidePosition + slideSize / 2 + prevSlideSize / 2 + spaceBetween;
                    if (i === 0) slidePosition = slidePosition - s.size / 2 - spaceBetween;
                    if (Math.abs(slidePosition) < 1 / 1000) slidePosition = 0;
                    if ((index) % s.params.slidesPerGroup === 0) s.snapGrid.push(slidePosition);
                    s.slidesGrid.push(slidePosition);
                }
                else {
                    if ((index) % s.params.slidesPerGroup === 0) s.snapGrid.push(slidePosition);
                    s.slidesGrid.push(slidePosition);
                    slidePosition = slidePosition + slideSize + spaceBetween;
                }

                s.virtualSize += slideSize + spaceBetween;

                prevSlideSize = slideSize;

                index ++;
            }
            s.virtualSize = Math.max(s.virtualSize, s.size) + s.params.slidesOffsetAfter;
            var newSlidesGrid;

            if (
                s.rtl && s.wrongRTL && (s.params.effect === 'slide' || s.params.effect === 'coverflow')) {
                s.wrapper.css({width: s.virtualSize + s.params.spaceBetween + 'px'});
            }
            if (!s.support.flexbox || s.params.setWrapperSize) {
                if (s.isHorizontal()) s.wrapper.css({width: s.virtualSize + s.params.spaceBetween + 'px'});
                else s.wrapper.css({height: s.virtualSize + s.params.spaceBetween + 'px'});
            }

            if (s.params.slidesPerColumn > 1) {
                s.virtualSize = (slideSize + s.params.spaceBetween) * slidesNumberEvenToRows;
                s.virtualSize = Math.ceil(s.virtualSize / s.params.slidesPerColumn) - s.params.spaceBetween;
                s.wrapper.css({width: s.virtualSize + s.params.spaceBetween + 'px'});
                if (s.params.centeredSlides) {
                    newSlidesGrid = [];
                    for (i = 0; i < s.snapGrid.length; i++) {
                        if (s.snapGrid[i] < s.virtualSize + s.snapGrid[0]) newSlidesGrid.push(s.snapGrid[i]);
                    }
                    s.snapGrid = newSlidesGrid;
                }
            }

            // Remove last grid elements depending on width
            if (!s.params.centeredSlides) {
                newSlidesGrid = [];
                for (i = 0; i < s.snapGrid.length; i++) {
                    if (s.snapGrid[i] <= s.virtualSize - s.size) {
                        newSlidesGrid.push(s.snapGrid[i]);
                    }
                }
                s.snapGrid = newSlidesGrid;
                if (Math.floor(s.virtualSize - s.size) - Math.floor(s.snapGrid[s.snapGrid.length - 1]) > 1) {
                    s.snapGrid.push(s.virtualSize - s.size);
                }
            }
            if (s.snapGrid.length === 0) s.snapGrid = [0];

            if (s.params.spaceBetween !== 0) {
                if (s.isHorizontal()) {
                    if (s.rtl) s.slides.css({marginLeft: spaceBetween + 'px'});
                    else s.slides.css({marginRight: spaceBetween + 'px'});
                }
                else s.slides.css({marginBottom: spaceBetween + 'px'});
            }
            if (s.params.watchSlidesProgress) {
                s.updateSlidesOffset();
            }
        };
        s.updateSlidesOffset = function () {
            for (var i = 0; i < s.slides.length; i++) {
                s.slides[i].swiperSlideOffset = s.isHorizontal() ? s.slides[i].offsetLeft : s.slides[i].offsetTop;
            }
        };

        /*=========================
          Slider/slides progress
          ===========================*/
        s.updateSlidesProgress = function (translate) {
            if (typeof translate === 'undefined') {
                translate = s.translate || 0;
            }
            if (s.slides.length === 0) return;
            if (typeof s.slides[0].swiperSlideOffset === 'undefined') s.updateSlidesOffset();

            var offsetCenter = -translate;
            if (s.rtl) offsetCenter = translate;

            // Visible Slides
            s.slides.removeClass(s.params.slideVisibleClass);
            for (var i = 0; i < s.slides.length; i++) {
                var slide = s.slides[i];
                var slideProgress = (offsetCenter - slide.swiperSlideOffset) / (slide.swiperSlideSize + s.params.spaceBetween);
                if (s.params.watchSlidesVisibility) {
                    var slideBefore = -(offsetCenter - slide.swiperSlideOffset);
                    var slideAfter = slideBefore + s.slidesSizesGrid[i];
                    var isVisible =
                        (slideBefore >= 0 && slideBefore < s.size) ||
                        (slideAfter > 0 && slideAfter <= s.size) ||
                        (slideBefore <= 0 && slideAfter >= s.size);
                    if (isVisible) {
                        s.slides.eq(i).addClass(s.params.slideVisibleClass);
                    }
                }
                slide.progress = s.rtl ? -slideProgress : slideProgress;
            }
        };
        s.updateProgress = function (translate) {
            if (typeof translate === 'undefined') {
                translate = s.translate || 0;
            }
            var translatesDiff = s.maxTranslate() - s.minTranslate();
            var wasBeginning = s.isBeginning;
            var wasEnd = s.isEnd;
            if (translatesDiff === 0) {
                s.progress = 0;
                s.isBeginning = s.isEnd = true;
            }
            else {
                s.progress = (translate - s.minTranslate()) / (translatesDiff);
                s.isBeginning = s.progress <= 0;
                s.isEnd = s.progress >= 1;
            }
            if (s.isBeginning && !wasBeginning) s.emit('onReachBeginning', s);
            if (s.isEnd && !wasEnd) s.emit('onReachEnd', s);

            if (s.params.watchSlidesProgress) s.updateSlidesProgress(translate);
            s.emit('onProgress', s, s.progress);
        };
        s.updateActiveIndex = function () {
            var translate = s.rtl ? s.translate : -s.translate;
            var newActiveIndex, i, snapIndex;
            for (i = 0; i < s.slidesGrid.length; i ++) {
                if (typeof s.slidesGrid[i + 1] !== 'undefined') {
                    if (translate >= s.slidesGrid[i] && translate < s.slidesGrid[i + 1] - (s.slidesGrid[i + 1] - s.slidesGrid[i]) / 2) {
                        newActiveIndex = i;
                    }
                    else if (translate >= s.slidesGrid[i] && translate < s.slidesGrid[i + 1]) {
                        newActiveIndex = i + 1;
                    }
                }
                else {
                    if (translate >= s.slidesGrid[i]) {
                        newActiveIndex = i;
                    }
                }
            }
            // Normalize slideIndex
            if (newActiveIndex < 0 || typeof newActiveIndex === 'undefined') newActiveIndex = 0;
            // for (i = 0; i < s.slidesGrid.length; i++) {
                // if (- translate >= s.slidesGrid[i]) {
                    // newActiveIndex = i;
                // }
            // }
            snapIndex = Math.floor(newActiveIndex / s.params.slidesPerGroup);
            if (snapIndex >= s.snapGrid.length) snapIndex = s.snapGrid.length - 1;

            if (newActiveIndex === s.activeIndex) {
                return;
            }
            s.snapIndex = snapIndex;
            s.previousIndex = s.activeIndex;
            s.activeIndex = newActiveIndex;
            s.updateClasses();
        };

        /*=========================
          Classes
          ===========================*/
        s.updateClasses = function () {
            s.slides.removeClass(s.params.slideActiveClass + ' ' + s.params.slideNextClass + ' ' + s.params.slidePrevClass);
            var activeSlide = s.slides.eq(s.activeIndex);
            // Active classes
            activeSlide.addClass(s.params.slideActiveClass);
            // Next Slide
            var nextSlide = activeSlide.next('.' + s.params.slideClass).addClass(s.params.slideNextClass);
            if (s.params.loop && nextSlide.length === 0) {
                s.slides.eq(0).addClass(s.params.slideNextClass);
            }
            // Prev Slide
            var prevSlide = activeSlide.prev('.' + s.params.slideClass).addClass(s.params.slidePrevClass);
            if (s.params.loop && prevSlide.length === 0) {
                s.slides.eq(-1).addClass(s.params.slidePrevClass);
            }

            // Pagination
            if (s.paginationContainer && s.paginationContainer.length > 0) {
                // Current/Total
                var current,
                    total = s.params.loop ? Math.ceil((s.slides.length - s.loopedSlides * 2) / s.params.slidesPerGroup) : s.snapGrid.length;
                if (s.params.loop) {
                    current = Math.ceil((s.activeIndex - s.loopedSlides)/s.params.slidesPerGroup);
                    if (current > s.slides.length - 1 - s.loopedSlides * 2) {
                        current = current - (s.slides.length - s.loopedSlides * 2);
                    }
                    if (current > total - 1) current = current - total;
                    if (current < 0 && s.params.paginationType !== 'bullets') current = total + current;
                }
                else {
                    if (typeof s.snapIndex !== 'undefined') {
                        current = s.snapIndex;
                    }
                    else {
                        current = s.activeIndex || 0;
                    }
                }
                // Types
                if (s.params.paginationType === 'bullets' && s.bullets && s.bullets.length > 0) {
                    s.bullets.removeClass(s.params.bulletActiveClass);
                    if (s.paginationContainer.length > 1) {
                        s.bullets.each(function () {
                            if ($(this).index() === current) $(this).addClass(s.params.bulletActiveClass);
                        });
                    }
                    else {
                        s.bullets.eq(current).addClass(s.params.bulletActiveClass);
                    }
                }
                if (s.params.paginationType === 'fraction') {
                    s.paginationContainer.find('.' + s.params.paginationCurrentClass).text(current + 1);
                    s.paginationContainer.find('.' + s.params.paginationTotalClass).text(total);
                }
                if (s.params.paginationType === 'progress') {
                    var scale = (current + 1) / total,
                        scaleX = scale,
                        scaleY = 1;
                    if (!s.isHorizontal()) {
                        scaleY = scale;
                        scaleX = 1;
                    }
                    s.paginationContainer.find('.' + s.params.paginationProgressbarClass).transform('translate3d(0,0,0) scaleX(' + scaleX + ') scaleY(' + scaleY + ')').transition(s.params.speed);
                }
                if (s.params.paginationType === 'custom' && s.params.paginationCustomRender) {
                    s.paginationContainer.html(s.params.paginationCustomRender(s, current + 1, total));
                    s.emit('onPaginationRendered', s, s.paginationContainer[0]);
                }
            }

            // Next/active buttons
            if (!s.params.loop) {
                if (s.params.prevButton && s.prevButton && s.prevButton.length > 0) {
                    if (s.isBeginning) {
                        s.prevButton.addClass(s.params.buttonDisabledClass);
                        if (s.params.a11y && s.a11y) s.a11y.disable(s.prevButton);
                    }
                    else {
                        s.prevButton.removeClass(s.params.buttonDisabledClass);
                        if (s.params.a11y && s.a11y) s.a11y.enable(s.prevButton);
                    }
                }
                if (s.params.nextButton && s.nextButton && s.nextButton.length > 0) {
                    if (s.isEnd) {
                        s.nextButton.addClass(s.params.buttonDisabledClass);
                        if (s.params.a11y && s.a11y) s.a11y.disable(s.nextButton);
                    }
                    else {
                        s.nextButton.removeClass(s.params.buttonDisabledClass);
                        if (s.params.a11y && s.a11y) s.a11y.enable(s.nextButton);
                    }
                }
            }
        };

        /*=========================
          Pagination
          ===========================*/
        s.updatePagination = function () {
            if (!s.params.pagination) return;
            if (s.paginationContainer && s.paginationContainer.length > 0) {
                var paginationHTML = '';
                if (s.params.paginationType === 'bullets') {
                    var numberOfBullets = s.params.loop ? Math.ceil((s.slides.length - s.loopedSlides * 2) / s.params.slidesPerGroup) : s.snapGrid.length;
                    for (var i = 0; i < numberOfBullets; i++) {
                        if (s.params.paginationBulletRender) {
                            paginationHTML += s.params.paginationBulletRender(i, s.params.bulletClass);
                        }
                        else {
                            paginationHTML += '<' + s.params.paginationElement+' class="' + s.params.bulletClass + '"></' + s.params.paginationElement + '>';
                        }
                    }
                    s.paginationContainer.html(paginationHTML);
                    s.bullets = s.paginationContainer.find('.' + s.params.bulletClass);
                    if (s.params.paginationClickable && s.params.a11y && s.a11y) {
                        s.a11y.initPagination();
                    }
                }
                if (s.params.paginationType === 'fraction') {
                    if (s.params.paginationFractionRender) {
                        paginationHTML = s.params.paginationFractionRender(s, s.params.paginationCurrentClass, s.params.paginationTotalClass);
                    }
                    else {
                        paginationHTML =
                            '<span class="' + s.params.paginationCurrentClass + '"></span>' +
                            ' / ' +
                            '<span class="' + s.params.paginationTotalClass+'"></span>';
                    }
                    s.paginationContainer.html(paginationHTML);
                }
                if (s.params.paginationType === 'progress') {
                    if (s.params.paginationProgressRender) {
                        paginationHTML = s.params.paginationProgressRender(s, s.params.paginationProgressbarClass);
                    }
                    else {
                        paginationHTML = '<span class="' + s.params.paginationProgressbarClass + '"></span>';
                    }
                    s.paginationContainer.html(paginationHTML);
                }
                if (s.params.paginationType !== 'custom') {
                    s.emit('onPaginationRendered', s, s.paginationContainer[0]);
                }
            }
        };
        /*=========================
          Common update method
          ===========================*/
        s.update = function (updateTranslate) {
            s.updateContainerSize();
            s.updateSlidesSize();
            s.updateProgress();
            s.updatePagination();
            s.updateClasses();
            if (s.params.scrollbar && s.scrollbar) {
                s.scrollbar.set();
            }
            function forceSetTranslate() {
                newTranslate = Math.min(Math.max(s.translate, s.maxTranslate()), s.minTranslate());
                s.setWrapperTranslate(newTranslate);
                s.updateActiveIndex();
                s.updateClasses();
            }
            if (updateTranslate) {
                var translated, newTranslate;
                if (s.controller && s.controller.spline) {
                    s.controller.spline = undefined;
                }
                if (s.params.freeMode) {
                    forceSetTranslate();
                    if (s.params.autoHeight) {
                        s.updateAutoHeight();
                    }
                }
                else {
                    if ((s.params.slidesPerView === 'auto' || s.params.slidesPerView > 1) && s.isEnd && !s.params.centeredSlides) {
                        translated = s.slideTo(s.slides.length - 1, 0, false, true);
                    }
                    else {
                        translated = s.slideTo(s.activeIndex, 0, false, true);
                    }
                    if (!translated) {
                        forceSetTranslate();
                    }
                }
            }
            else if (s.params.autoHeight) {
                s.updateAutoHeight();
            }
        };

        /*=========================
          Resize Handler
          ===========================*/
        s.onResize = function (forceUpdatePagination) {
            //Breakpoints
            if (s.params.breakpoints) {
                s.setBreakpoint();
            }

            // Disable locks on resize
            var allowSwipeToPrev = s.params.allowSwipeToPrev;
            var allowSwipeToNext = s.params.allowSwipeToNext;
            s.params.allowSwipeToPrev = s.params.allowSwipeToNext = true;

            s.updateContainerSize();
            s.updateSlidesSize();
            if (s.params.slidesPerView === 'auto' || s.params.freeMode || forceUpdatePagination) s.updatePagination();
            if (s.params.scrollbar && s.scrollbar) {
                s.scrollbar.set();
            }
            if (s.controller && s.controller.spline) {
                s.controller.spline = undefined;
            }
            var slideChangedBySlideTo = false;
            if (s.params.freeMode) {
                var newTranslate = Math.min(Math.max(s.translate, s.maxTranslate()), s.minTranslate());
                s.setWrapperTranslate(newTranslate);
                s.updateActiveIndex();
                s.updateClasses();

                if (s.params.autoHeight) {
                    s.updateAutoHeight();
                }
            }
            else {
                s.updateClasses();
                if ((s.params.slidesPerView === 'auto' || s.params.slidesPerView > 1) && s.isEnd && !s.params.centeredSlides) {
                    slideChangedBySlideTo = s.slideTo(s.slides.length - 1, 0, false, true);
                }
                else {
                    slideChangedBySlideTo = s.slideTo(s.activeIndex, 0, false, true);
                }
            }
            if (s.params.lazyLoading && !slideChangedBySlideTo && s.lazy) {
                s.lazy.load();
            }
            // Return locks after resize
            s.params.allowSwipeToPrev = allowSwipeToPrev;
            s.params.allowSwipeToNext = allowSwipeToNext;
        };

        /*=========================
          Events
          ===========================*/

        //Define Touch Events
        var desktopEvents = ['mousedown', 'mousemove', 'mouseup'];
        if (window.navigator.pointerEnabled) desktopEvents = ['pointerdown', 'pointermove', 'pointerup'];
        else if (window.navigator.msPointerEnabled) desktopEvents = ['MSPointerDown', 'MSPointerMove', 'MSPointerUp'];
        s.touchEvents = {
            start : s.support.touch || !s.params.simulateTouch  ? 'touchstart' : desktopEvents[0],
            move : s.support.touch || !s.params.simulateTouch ? 'touchmove' : desktopEvents[1],
            end : s.support.touch || !s.params.simulateTouch ? 'touchend' : desktopEvents[2]
        };


        // WP8 Touch Events Fix
        if (window.navigator.pointerEnabled || window.navigator.msPointerEnabled) {
            (s.params.touchEventsTarget === 'container' ? s.container : s.wrapper).addClass('swiper-wp8-' + s.params.direction);
        }

        // Attach/detach events
        s.initEvents = function (detach) {
            var actionDom = detach ? 'off' : 'on';
            var action = detach ? 'removeEventListener' : 'addEventListener';
            var touchEventsTarget = s.params.touchEventsTarget === 'container' ? s.container[0] : s.wrapper[0];
            var target = s.support.touch ? touchEventsTarget : document;

            var moveCapture = s.params.nested ? true : false;

            //Touch Events
            if (s.browser.ie) {
                touchEventsTarget[action](s.touchEvents.start, s.onTouchStart, false);
                target[action](s.touchEvents.move, s.onTouchMove, moveCapture);
                target[action](s.touchEvents.end, s.onTouchEnd, false);
            }
            else {
                if (s.support.touch) {
                    touchEventsTarget[action](s.touchEvents.start, s.onTouchStart, false);
                    touchEventsTarget[action](s.touchEvents.move, s.onTouchMove, moveCapture);
                    touchEventsTarget[action](s.touchEvents.end, s.onTouchEnd, false);
                }
                if (params.simulateTouch && !s.device.ios && !s.device.android) {
                    touchEventsTarget[action]('mousedown', s.onTouchStart, false);
                    document[action]('mousemove', s.onTouchMove, moveCapture);
                    document[action]('mouseup', s.onTouchEnd, false);
                }
            }
            window[action]('resize', s.onResize);

            // Next, Prev, Index
            if (s.params.nextButton && s.nextButton && s.nextButton.length > 0) {
                s.nextButton[actionDom]('click', s.onClickNext);
                if (s.params.a11y && s.a11y) s.nextButton[actionDom]('keydown', s.a11y.onEnterKey);
            }
            if (s.params.prevButton && s.prevButton && s.prevButton.length > 0) {
                s.prevButton[actionDom]('click', s.onClickPrev);
                if (s.params.a11y && s.a11y) s.prevButton[actionDom]('keydown', s.a11y.onEnterKey);
            }
            if (s.params.pagination && s.params.paginationClickable) {
                s.paginationContainer[actionDom]('click', '.' + s.params.bulletClass, s.onClickIndex);
                if (s.params.a11y && s.a11y) s.paginationContainer[actionDom]('keydown', '.' + s.params.bulletClass, s.a11y.onEnterKey);
            }

            // Prevent Links Clicks
            if (s.params.preventClicks || s.params.preventClicksPropagation) touchEventsTarget[action]('click', s.preventClicks, true);
        };
        s.attachEvents = function () {
            s.initEvents();
        };
        s.detachEvents = function () {
            s.initEvents(true);
        };

        /*=========================
          Handle Clicks
          ===========================*/
        // Prevent Clicks
        s.allowClick = true;
        s.preventClicks = function (e) {
            if (!s.allowClick) {
                if (s.params.preventClicks) e.preventDefault();
                if (s.params.preventClicksPropagation && s.animating) {
                    e.stopPropagation();
                    e.stopImmediatePropagation();
                }
            }
        };
        // Clicks
        s.onClickNext = function (e) {
            e.preventDefault();
            if (s.isEnd && !s.params.loop) return;
            s.slideNext();
        };
        s.onClickPrev = function (e) {
            e.preventDefault();
            if (s.isBeginning && !s.params.loop) return;
            s.slidePrev();
        };
        s.onClickIndex = function (e) {
            e.preventDefault();
            var index = $(this).index() * s.params.slidesPerGroup;
            if (s.params.loop) index = index + s.loopedSlides;
            s.slideTo(index);
        };

        /*=========================
          Handle Touches
          ===========================*/
        function findElementInEvent(e, selector) {
            var el = $(e.target);
            if (!el.is(selector)) {
                if (typeof selector === 'string') {
                    el = el.parents(selector);
                }
                else if (selector.nodeType) {
                    var found;
                    el.parents().each(function (index, _el) {
                        if (_el === selector) found = selector;
                    });
                    if (!found) return undefined;
                    else return selector;
                }
            }
            if (el.length === 0) {
                return undefined;
            }
            return el[0];
        }
        s.updateClickedSlide = function (e) {
            var slide = findElementInEvent(e, '.' + s.params.slideClass);
            var slideFound = false;
            if (slide) {
                for (var i = 0; i < s.slides.length; i++) {
                    if (s.slides[i] === slide) slideFound = true;
                }
            }

            if (slide && slideFound) {
                s.clickedSlide = slide;
                s.clickedIndex = $(slide).index();
            }
            else {
                s.clickedSlide = undefined;
                s.clickedIndex = undefined;
                return;
            }
            if (s.params.slideToClickedSlide && s.clickedIndex !== undefined && s.clickedIndex !== s.activeIndex) {
                var slideToIndex = s.clickedIndex,
                    realIndex,
                    duplicatedSlides;
                if (s.params.loop) {
                    if (s.animating) return;
                    realIndex = $(s.clickedSlide).attr('data-swiper-slide-index');
                    if (s.params.centeredSlides) {
                        if ((slideToIndex < s.loopedSlides - s.params.slidesPerView/2) || (slideToIndex > s.slides.length - s.loopedSlides + s.params.slidesPerView/2)) {
                            s.fixLoop();
                            slideToIndex = s.wrapper.children('.' + s.params.slideClass + '[data-swiper-slide-index="' + realIndex + '"]:not(.swiper-slide-duplicate)').eq(0).index();
                            setTimeout(function () {
                                s.slideTo(slideToIndex);
                            }, 0);
                        }
                        else {
                            s.slideTo(slideToIndex);
                        }
                    }
                    else {
                        if (slideToIndex > s.slides.length - s.params.slidesPerView) {
                            s.fixLoop();
                            slideToIndex = s.wrapper.children('.' + s.params.slideClass + '[data-swiper-slide-index="' + realIndex + '"]:not(.swiper-slide-duplicate)').eq(0).index();
                            setTimeout(function () {
                                s.slideTo(slideToIndex);
                            }, 0);
                        }
                        else {
                            s.slideTo(slideToIndex);
                        }
                    }
                }
                else {
                    s.slideTo(slideToIndex);
                }
            }
        };

        var isTouched,
            isMoved,
            allowTouchCallbacks,
            touchStartTime,
            isScrolling,
            currentTranslate,
            startTranslate,
            allowThresholdMove,
            // Form elements to match
            formElements = 'input, select, textarea, button',
            // Last click time
            lastClickTime = Date.now(), clickTimeout,
            //Velocities
            velocities = [],
            allowMomentumBounce;

        // Animating Flag
        s.animating = false;

        // Touches information
        s.touches = {
            startX: 0,
            startY: 0,
            currentX: 0,
            currentY: 0,
            diff: 0
        };

        // Touch handlers
        var isTouchEvent, startMoving;
        s.onTouchStart = function (e) {
            if (e.originalEvent) e = e.originalEvent;
            isTouchEvent = e.type === 'touchstart';
            if (!isTouchEvent && 'which' in e && e.which === 3) return;
            if (s.params.noSwiping && findElementInEvent(e, '.' + s.params.noSwipingClass)) {
                s.allowClick = true;
                return;
            }
            if (s.params.swipeHandler) {
                if (!findElementInEvent(e, s.params.swipeHandler)) return;
            }
            var startX = s.touches.currentX = e.type === 'touchstart' ? e.targetTouches[0].pageX : e.pageX;
            var startY = s.touches.currentY = e.type === 'touchstart' ? e.targetTouches[0].pageY : e.pageY;

            // Do NOT start if iOS edge swipe is detected. Otherwise iOS app (UIWebView) cannot swipe-to-go-back anymore
            if(s.device.ios && s.params.iOSEdgeSwipeDetection && startX <= s.params.iOSEdgeSwipeThreshold) {
                return;
            }

            isTouched = true;
            isMoved = false;
            allowTouchCallbacks = true;
            isScrolling = undefined;
            startMoving = undefined;
            s.touches.startX = startX;
            s.touches.startY = startY;
            touchStartTime = Date.now();
            s.allowClick = true;
            s.updateContainerSize();
            s.swipeDirection = undefined;
            if (s.params.threshold > 0) allowThresholdMove = false;
            if (e.type !== 'touchstart') {
                var preventDefault = true;
                if ($(e.target).is(formElements)) preventDefault = false;
                if (document.activeElement && $(document.activeElement).is(formElements)) {
                    document.activeElement.blur();
                }
                if (preventDefault) {
                    e.preventDefault();
                }
            }
            s.emit('onTouchStart', s, e);
        };

        s.onTouchMove = function (e) {
            /**
             * Gusto MOD
             */
            if($(s.container[0]).attr('data-swiper-disable') == 'true'){return;}

            if (e.originalEvent) e = e.originalEvent;
            if (isTouchEvent && e.type === 'mousemove') return;
            if (e.preventedByNestedSwiper) {
                s.touches.startX = e.type === 'touchmove' ? e.targetTouches[0].pageX : e.pageX;
                s.touches.startY = e.type === 'touchmove' ? e.targetTouches[0].pageY : e.pageY;
                return;
            }
            if (s.params.onlyExternal) {
                // isMoved = true;
                s.allowClick = false;
                if (isTouched) {
                    s.touches.startX = s.touches.currentX = e.type === 'touchmove' ? e.targetTouches[0].pageX : e.pageX;
                    s.touches.startY = s.touches.currentY = e.type === 'touchmove' ? e.targetTouches[0].pageY : e.pageY;
                    touchStartTime = Date.now();
                }
                return;
            }
            if (isTouchEvent && document.activeElement) {
                if (e.target === document.activeElement && $(e.target).is(formElements)) {
                    isMoved = true;
                    s.allowClick = false;
                    return;
                }
            }
            if (allowTouchCallbacks) {
                s.emit('onTouchMove', s, e);
            }
            if (e.targetTouches && e.targetTouches.length > 1) return;

            s.touches.currentX = e.type === 'touchmove' ? e.targetTouches[0].pageX : e.pageX;
            s.touches.currentY = e.type === 'touchmove' ? e.targetTouches[0].pageY : e.pageY;
            /**
             * Gusto MOD
             */
        /*
            if (typeof isScrolling === 'undefined') {
                var touchAngle = Math.atan2(Math.abs(s.touches.currentY - s.touches.startY), Math.abs(s.touches.currentX - s.touches.startX)) * 180 / Math.PI;
                isScrolling = s.isHorizontal() ? touchAngle > s.params.touchAngle : (90 - touchAngle > s.params.touchAngle);
            }
            if (isScrolling) {
                s.emit('onTouchMoveOpposite', s, e);
            }
            */
            if (typeof startMoving === 'undefined' && s.browser.ieTouch) {
               if (s.touches.currentX !== s.touches.startX || s.touches.currentY !== s.touches.startY) {
                    startMoving = true;
               }
            }
            if (!isTouched) return;
            if (isScrolling)  {
                isTouched = false;
                return;
            }
            if (!startMoving && s.browser.ieTouch) {
                return;
            }
            s.allowClick = false;
            s.emit('onSliderMove', s, e);
            e.preventDefault();
            if (s.params.touchMoveStopPropagation && !s.params.nested) {
                e.stopPropagation();
            }

            if (!isMoved) {
                if (params.loop) {
                    s.fixLoop();
                }
                startTranslate = s.getWrapperTranslate();
                s.setWrapperTransition(0);
                if (s.animating) {
                    s.wrapper.trigger('webkitTransitionEnd transitionend oTransitionEnd MSTransitionEnd msTransitionEnd');
                }
                if (s.params.autoplay && s.autoplaying) {
                    if (s.params.autoplayDisableOnInteraction) {
                        s.stopAutoplay();
                    }
                    else {
                        s.pauseAutoplay();
                    }
                }
                allowMomentumBounce = false;
                //Grab Cursor
                if (s.params.grabCursor) {
                    s.container[0].style.cursor = 'move';
                    s.container[0].style.cursor = '-webkit-grabbing';
                    s.container[0].style.cursor = '-moz-grabbin';
                    s.container[0].style.cursor = 'grabbing';
                }
            }
            isMoved = true;

            var diff = s.touches.diff = s.isHorizontal() ? s.touches.currentX - s.touches.startX : s.touches.currentY - s.touches.startY;

            diff = diff * s.params.touchRatio;
            if (s.rtl) diff = -diff;

            s.swipeDirection = diff > 0 ? 'prev' : 'next';
            currentTranslate = diff + startTranslate;

            var disableParentSwiper = true;
            if ((diff > 0 && currentTranslate > s.minTranslate())) {
                disableParentSwiper = false;
                if (s.params.resistance) currentTranslate = s.minTranslate() - 1 + Math.pow(-s.minTranslate() + startTranslate + diff, s.params.resistanceRatio);
            }
            else if (diff < 0 && currentTranslate < s.maxTranslate()) {
                disableParentSwiper = false;
                if (s.params.resistance) currentTranslate = s.maxTranslate() + 1 - Math.pow(s.maxTranslate() - startTranslate - diff, s.params.resistanceRatio);
            }

            if (disableParentSwiper) {
                e.preventedByNestedSwiper = true;
            }

            // Directions locks
            if (!s.params.allowSwipeToNext && s.swipeDirection === 'next' && currentTranslate < startTranslate) {
                currentTranslate = startTranslate;
            }
            if (!s.params.allowSwipeToPrev && s.swipeDirection === 'prev' && currentTranslate > startTranslate) {
                currentTranslate = startTranslate;
            }

            if (!s.params.followFinger) return;

            // Threshold
            if (s.params.threshold > 0) {
                if (Math.abs(diff) > s.params.threshold || allowThresholdMove) {
                    if (!allowThresholdMove) {
                        allowThresholdMove = true;
                        s.touches.startX = s.touches.currentX;
                        s.touches.startY = s.touches.currentY;
                        currentTranslate = startTranslate;
                        s.touches.diff = s.isHorizontal() ? s.touches.currentX - s.touches.startX : s.touches.currentY - s.touches.startY;
                        return;
                    }
                }
                else {
                    currentTranslate = startTranslate;
                    return;
                }
            }
            // Update active index in free mode
            if (s.params.freeMode || s.params.watchSlidesProgress) {
                s.updateActiveIndex();
            }
            if (s.params.freeMode) {
                //Velocity
                if (velocities.length === 0) {
                    velocities.push({
                        position: s.touches[s.isHorizontal() ? 'startX' : 'startY'],
                        time: touchStartTime
                    });
                }
                velocities.push({
                    position: s.touches[s.isHorizontal() ? 'currentX' : 'currentY'],
                    time: (new window.Date()).getTime()
                });
            }
            // Update progress
            s.updateProgress(currentTranslate);
            // Update translate
            s.setWrapperTranslate(currentTranslate);
        };
        s.onTouchEnd = function (e) {
            if (e.originalEvent) e = e.originalEvent;
            if (allowTouchCallbacks) {
                s.emit('onTouchEnd', s, e);
            }
            allowTouchCallbacks = false;
            if (!isTouched) return;
            //Return Grab Cursor
            if (s.params.grabCursor && isMoved && isTouched) {
                s.container[0].style.cursor = 'move';
                s.container[0].style.cursor = '-webkit-grab';
                s.container[0].style.cursor = '-moz-grab';
                s.container[0].style.cursor = 'grab';
            }

            // Time diff
            var touchEndTime = Date.now();
            var timeDiff = touchEndTime - touchStartTime;

            // Tap, doubleTap, Click
            if (s.allowClick) {
                s.updateClickedSlide(e);
                s.emit('onTap', s, e);
                if (timeDiff < 300 && (touchEndTime - lastClickTime) > 300) {
                    if (clickTimeout) clearTimeout(clickTimeout);
                    clickTimeout = setTimeout(function () {
                        if (!s) return;
                        if (s.params.paginationHide && s.paginationContainer.length > 0 && !$(e.target).hasClass(s.params.bulletClass)) {
                            s.paginationContainer.toggleClass(s.params.paginationHiddenClass);
                        }
                        s.emit('onClick', s, e);
                    }, 300);

                }
                if (timeDiff < 300 && (touchEndTime - lastClickTime) < 300) {
                    if (clickTimeout) clearTimeout(clickTimeout);
                    s.emit('onDoubleTap', s, e);
                }
            }

            lastClickTime = Date.now();
            setTimeout(function () {
                if (s) s.allowClick = true;
            }, 0);

            if (!isTouched || !isMoved || !s.swipeDirection || s.touches.diff === 0 || currentTranslate === startTranslate) {
                isTouched = isMoved = false;
                return;
            }
            isTouched = isMoved = false;

            var currentPos;
            if (s.params.followFinger) {
                currentPos = s.rtl ? s.translate : -s.translate;
            }
            else {
                currentPos = -currentTranslate;
            }
            if (s.params.freeMode) {
                if (currentPos < -s.minTranslate()) {
                    s.slideTo(s.activeIndex);
                    return;
                }
                else if (currentPos > -s.maxTranslate()) {
                    if (s.slides.length < s.snapGrid.length) {
                        s.slideTo(s.snapGrid.length - 1);
                    }
                    else {
                        s.slideTo(s.slides.length - 1);
                    }
                    return;
                }

                if (s.params.freeModeMomentum) {
                    if (velocities.length > 1) {
                        var lastMoveEvent = velocities.pop(), velocityEvent = velocities.pop();

                        var distance = lastMoveEvent.position - velocityEvent.position;
                        var time = lastMoveEvent.time - velocityEvent.time;
                        s.velocity = distance / time;
                        s.velocity = s.velocity / 2;
                        if (Math.abs(s.velocity) < s.params.freeModeMinimumVelocity) {
                            s.velocity = 0;
                        }
                        // this implies that the user stopped moving a finger then released.
                        // There would be no events with distance zero, so the last event is stale.
                        if (time > 150 || (new window.Date().getTime() - lastMoveEvent.time) > 300) {
                            s.velocity = 0;
                        }
                    } else {
                        s.velocity = 0;
                    }

                    velocities.length = 0;
                    var momentumDuration = 1000 * s.params.freeModeMomentumRatio;
                    var momentumDistance = s.velocity * momentumDuration;

                    var newPosition = s.translate + momentumDistance;
                    if (s.rtl) newPosition = - newPosition;
                    var doBounce = false;
                    var afterBouncePosition;
                    var bounceAmount = Math.abs(s.velocity) * 20 * s.params.freeModeMomentumBounceRatio;
                    if (newPosition < s.maxTranslate()) {
                        if (s.params.freeModeMomentumBounce) {
                            if (newPosition + s.maxTranslate() < -bounceAmount) {
                                newPosition = s.maxTranslate() - bounceAmount;
                            }
                            afterBouncePosition = s.maxTranslate();
                            doBounce = true;
                            allowMomentumBounce = true;
                        }
                        else {
                            newPosition = s.maxTranslate();
                        }
                    }
                    else if (newPosition > s.minTranslate()) {
                        if (s.params.freeModeMomentumBounce) {
                            if (newPosition - s.minTranslate() > bounceAmount) {
                                newPosition = s.minTranslate() + bounceAmount;
                            }
                            afterBouncePosition = s.minTranslate();
                            doBounce = true;
                            allowMomentumBounce = true;
                        }
                        else {
                            newPosition = s.minTranslate();
                        }
                    }
                    else if (s.params.freeModeSticky) {
                        var j = 0,
                            nextSlide;
                        for (j = 0; j < s.snapGrid.length; j += 1) {
                            if (s.snapGrid[j] > -newPosition) {
                                nextSlide = j;
                                break;
                            }

                        }
                        if (Math.abs(s.snapGrid[nextSlide] - newPosition) < Math.abs(s.snapGrid[nextSlide - 1] - newPosition) || s.swipeDirection === 'next') {
                            newPosition = s.snapGrid[nextSlide];
                        } else {
                            newPosition = s.snapGrid[nextSlide - 1];
                        }
                        if (!s.rtl) newPosition = - newPosition;
                    }
                    //Fix duration
                    if (s.velocity !== 0) {
                        if (s.rtl) {
                            momentumDuration = Math.abs((-newPosition - s.translate) / s.velocity);
                        }
                        else {
                            momentumDuration = Math.abs((newPosition - s.translate) / s.velocity);
                        }
                    }
                    else if (s.params.freeModeSticky) {
                        s.slideReset();
                        return;
                    }

                    if (s.params.freeModeMomentumBounce && doBounce) {
                        s.updateProgress(afterBouncePosition);
                        s.setWrapperTransition(momentumDuration);
                        s.setWrapperTranslate(newPosition);
                        s.onTransitionStart();
                        s.animating = true;
                        s.wrapper.transitionEnd(function () {
                            if (!s || !allowMomentumBounce) return;
                            s.emit('onMomentumBounce', s);

                            s.setWrapperTransition(s.params.speed);
                            s.setWrapperTranslate(afterBouncePosition);
                            s.wrapper.transitionEnd(function () {
                                if (!s) return;
                                s.onTransitionEnd();
                            });
                        });
                    } else if (s.velocity) {
                        s.updateProgress(newPosition);
                        s.setWrapperTransition(momentumDuration);
                        s.setWrapperTranslate(newPosition);
                        s.onTransitionStart();
                        if (!s.animating) {
                            s.animating = true;
                            s.wrapper.transitionEnd(function () {
                                if (!s) return;
                                s.onTransitionEnd();
                            });
                        }

                    } else {
                        s.updateProgress(newPosition);
                    }

                    s.updateActiveIndex();
                }
                if (!s.params.freeModeMomentum || timeDiff >= s.params.longSwipesMs) {
                    s.updateProgress();
                    s.updateActiveIndex();
                }
                return;
            }

            // Find current slide
            var i, stopIndex = 0, groupSize = s.slidesSizesGrid[0];
            for (i = 0; i < s.slidesGrid.length; i += s.params.slidesPerGroup) {
                if (typeof s.slidesGrid[i + s.params.slidesPerGroup] !== 'undefined') {
                    if (currentPos >= s.slidesGrid[i] && currentPos < s.slidesGrid[i + s.params.slidesPerGroup]) {
                        stopIndex = i;
                        groupSize = s.slidesGrid[i + s.params.slidesPerGroup] - s.slidesGrid[i];
                    }
                }
                else {
                    if (currentPos >= s.slidesGrid[i]) {
                        stopIndex = i;
                        groupSize = s.slidesGrid[s.slidesGrid.length - 1] - s.slidesGrid[s.slidesGrid.length - 2];
                    }
                }
            }

            // Find current slide size
            var ratio = (currentPos - s.slidesGrid[stopIndex]) / groupSize;

            if (timeDiff > s.params.longSwipesMs) {
                // Long touches
                if (!s.params.longSwipes) {
                    s.slideTo(s.activeIndex);
                    return;
                }
                if (s.swipeDirection === 'next') {
                    if (ratio >= s.params.longSwipesRatio) s.slideTo(stopIndex + s.params.slidesPerGroup);
                    else s.slideTo(stopIndex);

                }
                if (s.swipeDirection === 'prev') {
                    if (ratio > (1 - s.params.longSwipesRatio)) s.slideTo(stopIndex + s.params.slidesPerGroup);
                    else s.slideTo(stopIndex);
                }
            }
            else {
                // Short swipes
                if (!s.params.shortSwipes) {
                    s.slideTo(s.activeIndex);
                    return;
                }
                if (s.swipeDirection === 'next') {
                    s.slideTo(stopIndex + s.params.slidesPerGroup);

                }
                if (s.swipeDirection === 'prev') {
                    s.slideTo(stopIndex);
                }
            }
        };
        /*=========================
          Transitions
          ===========================*/
        s._slideTo = function (slideIndex, speed) {
            return s.slideTo(slideIndex, speed, true, true);
        };
        s.slideTo = function (slideIndex, speed, runCallbacks, internal) {
            if (typeof runCallbacks === 'undefined') runCallbacks = true;
            if (typeof slideIndex === 'undefined') slideIndex = 0;
            if (slideIndex < 0) slideIndex = 0;
            s.snapIndex = Math.floor(slideIndex / s.params.slidesPerGroup);
            if (s.snapIndex >= s.snapGrid.length) s.snapIndex = s.snapGrid.length - 1;

            var translate = - s.snapGrid[s.snapIndex];
            // Stop autoplay
            if (s.params.autoplay && s.autoplaying) {
                if (internal || !s.params.autoplayDisableOnInteraction) {
                    s.pauseAutoplay(speed);
                }
                else {
                    s.stopAutoplay();
                }
            }
            // Update progress
            s.updateProgress(translate);

            // Normalize slideIndex
            for (var i = 0; i < s.slidesGrid.length; i++) {
                if (- Math.floor(translate * 100) >= Math.floor(s.slidesGrid[i] * 100)) {
                    slideIndex = i;
                }
            }

            // Directions locks
            if (!s.params.allowSwipeToNext && translate < s.translate && translate < s.minTranslate()) {
                return false;
            }
            if (!s.params.allowSwipeToPrev && translate > s.translate && translate > s.maxTranslate()) {
                if ((s.activeIndex || 0) !== slideIndex ) return false;
            }

            // Update Index
            if (typeof speed === 'undefined') speed = s.params.speed;
            s.previousIndex = s.activeIndex || 0;
            s.activeIndex = slideIndex;

            if ((s.rtl && -translate === s.translate) || (!s.rtl && translate === s.translate)) {
                // Update Height
                if (s.params.autoHeight) {
                    s.updateAutoHeight();
                }
                s.updateClasses();
                if (s.params.effect !== 'slide') {
                    s.setWrapperTranslate(translate);
                }
                return false;
            }
            s.updateClasses();
            s.onTransitionStart(runCallbacks);

            if (speed === 0) {
                s.setWrapperTranslate(translate);
                s.setWrapperTransition(0);
                s.onTransitionEnd(runCallbacks);
            }
            else {
                s.setWrapperTranslate(translate);
                s.setWrapperTransition(speed);
                if (!s.animating) {
                    s.animating = true;
                    s.wrapper.transitionEnd(function () {
                        if (!s) return;
                        s.onTransitionEnd(runCallbacks);
                    });
                }

            }

            return true;
        };

        s.onTransitionStart = function (runCallbacks) {
            if (typeof runCallbacks === 'undefined') runCallbacks = true;
            if (s.params.autoHeight) {
                s.updateAutoHeight();
            }
            if (s.lazy) s.lazy.onTransitionStart();
            if (runCallbacks) {
                s.emit('onTransitionStart', s);
                if (s.activeIndex !== s.previousIndex) {
                    s.emit('onSlideChangeStart', s);
                    if (s.activeIndex > s.previousIndex) {
                        s.emit('onSlideNextStart', s);
                    }
                    else {
                        s.emit('onSlidePrevStart', s);
                    }
                }

            }
        };
        s.onTransitionEnd = function (runCallbacks) {
            s.animating = false;
            s.setWrapperTransition(0);
            if (typeof runCallbacks === 'undefined') runCallbacks = true;
            if (s.lazy) s.lazy.onTransitionEnd();
            if (runCallbacks) {
                s.emit('onTransitionEnd', s);
                if (s.activeIndex !== s.previousIndex) {
                    s.emit('onSlideChangeEnd', s);
                    if (s.activeIndex > s.previousIndex) {
                        s.emit('onSlideNextEnd', s);
                    }
                    else {
                        s.emit('onSlidePrevEnd', s);
                    }
                }
            }
            if (s.params.hashnav && s.hashnav) {
                s.hashnav.setHash();
            }

        };
        s.slideNext = function (runCallbacks, speed, internal) {
            if (s.params.loop) {
                if (s.animating) return false;
                s.fixLoop();
                var clientLeft = s.container[0].clientLeft;
                return s.slideTo(s.activeIndex + s.params.slidesPerGroup, speed, runCallbacks, internal);
            }
            else return s.slideTo(s.activeIndex + s.params.slidesPerGroup, speed, runCallbacks, internal);
        };
        s._slideNext = function (speed) {
            return s.slideNext(true, speed, true);
        };
        s.slidePrev = function (runCallbacks, speed, internal) {
            if (s.params.loop) {
                if (s.animating) return false;
                s.fixLoop();
                var clientLeft = s.container[0].clientLeft;
                return s.slideTo(s.activeIndex - 1, speed, runCallbacks, internal);
            }
            else return s.slideTo(s.activeIndex - 1, speed, runCallbacks, internal);
        };
        s._slidePrev = function (speed) {
            return s.slidePrev(true, speed, true);
        };
        s.slideReset = function (runCallbacks, speed, internal) {
            return s.slideTo(s.activeIndex, speed, runCallbacks);
        };

        /*=========================
          Translate/transition helpers
          ===========================*/
        s.setWrapperTransition = function (duration, byController) {
            s.wrapper.transition(duration);
            if (s.params.effect !== 'slide' && s.effects[s.params.effect]) {
                s.effects[s.params.effect].setTransition(duration);
            }
            if (s.params.parallax && s.parallax) {
                s.parallax.setTransition(duration);
            }
            if (s.params.scrollbar && s.scrollbar) {
                s.scrollbar.setTransition(duration);
            }
            if (s.params.control && s.controller) {
                s.controller.setTransition(duration, byController);
            }
            s.emit('onSetTransition', s, duration);
        };
        s.setWrapperTranslate = function (translate, updateActiveIndex, byController) {
            //console.log(translate);

            var x = 0, y = 0, z = 0;
            if (s.isHorizontal()) {
                x = s.rtl ? -translate : translate;
            }
            else {
                y = translate;
            }

            if (s.params.roundLengths) {
                x = round(x);
                y = round(y);
            }

            if (!s.params.virtualTranslate) {
                if (s.support.transforms3d) s.wrapper.transform('translate3d(' + x + 'px, ' + y + 'px, ' + z + 'px)',x,y,z);
                else s.wrapper.transform('translate(' + x + 'px, ' + y + 'px)',x,y,z);
            }

            s.translate = s.isHorizontal() ? x : y;

            // Check if we need to update progress
            var progress;
            var translatesDiff = s.maxTranslate() - s.minTranslate();
            if (translatesDiff === 0) {
                progress = 0;
            }
            else {
                progress = (translate - s.minTranslate()) / (translatesDiff);
            }
            if (progress !== s.progress) {
                s.updateProgress(translate);
            }

            if (updateActiveIndex) s.updateActiveIndex();
            if (s.params.effect !== 'slide' && s.effects[s.params.effect]) {
                s.effects[s.params.effect].setTranslate(s.translate);
            }
            if (s.params.parallax && s.parallax) {
                s.parallax.setTranslate(s.translate);
            }
            if (s.params.scrollbar && s.scrollbar) {
                s.scrollbar.setTranslate(s.translate);
            }
            if (s.params.control && s.controller) {
                s.controller.setTranslate(s.translate, byController);
            }
            s.emit('onSetTranslate', s, s.translate);
        };

        s.getTranslate = function (el, axis) {
            var matrix, curTransform, curStyle, transformMatrix;

            // automatic axis detection
            if (typeof axis === 'undefined') {
                axis = 'x';
            }

            if (s.params.virtualTranslate) {
                return s.rtl ? -s.translate : s.translate;
            }

            curStyle = window.getComputedStyle(el, null);
            if (window.WebKitCSSMatrix) {
                curTransform = curStyle.transform || curStyle.webkitTransform;
                if (curTransform.split(',').length > 6) {
                    curTransform = curTransform.split(', ').map(function(a){
                        return a.replace(',','.');
                    }).join(', ');
                }
                // Some old versions of Webkit choke when 'none' is passed; pass
                // empty string instead in this case
                transformMatrix = new window.WebKitCSSMatrix(curTransform === 'none' ? '' : curTransform);
            }
            else {
                transformMatrix = curStyle.MozTransform || curStyle.OTransform || curStyle.MsTransform || curStyle.msTransform  || curStyle.transform || curStyle.getPropertyValue('transform').replace('translate(', 'matrix(1, 0, 0, 1,');
                matrix = transformMatrix.toString().split(',');
            }

            if (axis === 'x') {
                //Latest Chrome and webkits Fix
                if (window.WebKitCSSMatrix)
                    curTransform = transformMatrix.m41;
                //Crazy IE10 Matrix
                else if (matrix.length === 16)
                    curTransform = parseFloat(matrix[12]);
                //Normal Browsers
                else
                    curTransform = parseFloat(matrix[4]);
            }
            if (axis === 'y') {
                //Latest Chrome and webkits Fix
                if (window.WebKitCSSMatrix)
                    curTransform = transformMatrix.m42;
                //Crazy IE10 Matrix
                else if (matrix.length === 16)
                    curTransform = parseFloat(matrix[13]);
                //Normal Browsers
                else
                    curTransform = parseFloat(matrix[5]);
            }
            if (s.rtl && curTransform) curTransform = -curTransform;
            return curTransform || 0;
        };
        s.getWrapperTranslate = function (axis) {
            if (typeof axis === 'undefined') {
                axis = s.isHorizontal() ? 'x' : 'y';
            }
            return s.getTranslate(s.wrapper[0], axis);
        };

        /*=========================
          Observer
          ===========================*/
        s.observers = [];
        function initObserver(target, options) {
            options = options || {};
            // create an observer instance
            var ObserverFunc = window.MutationObserver || window.WebkitMutationObserver;
            var observer = new ObserverFunc(function (mutations) {
                mutations.forEach(function (mutation) {
                    s.onResize(true);
                    s.emit('onObserverUpdate', s, mutation);
                });
            });

            observer.observe(target, {
                attributes: typeof options.attributes === 'undefined' ? true : options.attributes,
                childList: typeof options.childList === 'undefined' ? true : options.childList,
                characterData: typeof options.characterData === 'undefined' ? true : options.characterData
            });

            s.observers.push(observer);
        }
        s.initObservers = function () {
            if (s.params.observeParents) {
                var containerParents = s.container.parents();
                for (var i = 0; i < containerParents.length; i++) {
                    initObserver(containerParents[i]);
                }
            }

            // Observe container
            initObserver(s.container[0], {childList: false});

            // Observe wrapper
            initObserver(s.wrapper[0], {attributes: false});
        };
        s.disconnectObservers = function () {
            for (var i = 0; i < s.observers.length; i++) {
                s.observers[i].disconnect();
            }
            s.observers = [];
        };
        /*=========================
          Loop
          ===========================*/
        // Create looped slides
        s.createLoop = function () {
            // Remove duplicated slides
            s.wrapper.children('.' + s.params.slideClass + '.' + s.params.slideDuplicateClass).remove();

            var slides = s.wrapper.children('.' + s.params.slideClass);

            if(s.params.slidesPerView === 'auto' && !s.params.loopedSlides) s.params.loopedSlides = slides.length;

            s.loopedSlides = parseInt(s.params.loopedSlides || s.params.slidesPerView, 10);
            s.loopedSlides = s.loopedSlides + s.params.loopAdditionalSlides;
            if (s.loopedSlides > slides.length) {
                s.loopedSlides = slides.length;
            }

            var prependSlides = [], appendSlides = [], i;
            slides.each(function (index, el) {
                var slide = $(this);
                if (index < s.loopedSlides) appendSlides.push(el);
                if (index < slides.length && index >= slides.length - s.loopedSlides) prependSlides.push(el);
                slide.attr('data-swiper-slide-index', index);
            });
            for (i = 0; i < appendSlides.length; i++) {
                s.wrapper.append($(appendSlides[i].cloneNode(true)).addClass(s.params.slideDuplicateClass));
            }
            for (i = prependSlides.length - 1; i >= 0; i--) {
                s.wrapper.prepend($(prependSlides[i].cloneNode(true)).addClass(s.params.slideDuplicateClass));
            }
        };
        s.destroyLoop = function () {
            s.wrapper.children('.' + s.params.slideClass + '.' + s.params.slideDuplicateClass).remove();
            s.slides.removeAttr('data-swiper-slide-index');
        };
        s.reLoop = function (updatePosition) {
            var oldIndex = s.activeIndex - s.loopedSlides;
            s.destroyLoop();
            s.createLoop();
            s.updateSlidesSize();
            if (updatePosition) {
                s.slideTo(oldIndex + s.loopedSlides, 0, false);
            }

        };
        s.fixLoop = function () {
            var newIndex;
            //Fix For Negative Oversliding
            if (s.activeIndex < s.loopedSlides) {
                newIndex = s.slides.length - s.loopedSlides * 3 + s.activeIndex;
                newIndex = newIndex + s.loopedSlides;
                s.slideTo(newIndex, 0, false, true);
            }
            //Fix For Positive Oversliding
            else if ((s.params.slidesPerView === 'auto' && s.activeIndex >= s.loopedSlides * 2) || (s.activeIndex > s.slides.length - s.params.slidesPerView * 2)) {
                newIndex = -s.slides.length + s.activeIndex + s.loopedSlides;
                newIndex = newIndex + s.loopedSlides;
                s.slideTo(newIndex, 0, false, true);
            }
        };
        /*=========================
          Append/Prepend/Remove Slides
          ===========================*/
        s.appendSlide = function (slides) {
            if (s.params.loop) {
                s.destroyLoop();
            }
            if (typeof slides === 'object' && slides.length) {
                for (var i = 0; i < slides.length; i++) {
                    if (slides[i]) s.wrapper.append(slides[i]);
                }
            }
            else {
                s.wrapper.append(slides);
            }
            if (s.params.loop) {
                s.createLoop();
            }
            if (!(s.params.observer && s.support.observer)) {
                s.update(true);
            }
        };
        s.prependSlide = function (slides) {
            if (s.params.loop) {
                s.destroyLoop();
            }
            var newActiveIndex = s.activeIndex + 1;
            if (typeof slides === 'object' && slides.length) {
                for (var i = 0; i < slides.length; i++) {
                    if (slides[i]) s.wrapper.prepend(slides[i]);
                }
                newActiveIndex = s.activeIndex + slides.length;
            }
            else {
                s.wrapper.prepend(slides);
            }
            if (s.params.loop) {
                s.createLoop();
            }
            if (!(s.params.observer && s.support.observer)) {
                s.update(true);
            }
            s.slideTo(newActiveIndex, 0, false);
        };
        s.removeSlide = function (slidesIndexes) {
            if (s.params.loop) {
                s.destroyLoop();
                s.slides = s.wrapper.children('.' + s.params.slideClass);
            }
            var newActiveIndex = s.activeIndex,
                indexToRemove;
            if (typeof slidesIndexes === 'object' && slidesIndexes.length) {
                for (var i = 0; i < slidesIndexes.length; i++) {
                    indexToRemove = slidesIndexes[i];
                    if (s.slides[indexToRemove]) s.slides.eq(indexToRemove).remove();
                    if (indexToRemove < newActiveIndex) newActiveIndex--;
                }
                newActiveIndex = Math.max(newActiveIndex, 0);
            }
            else {
                indexToRemove = slidesIndexes;
                if (s.slides[indexToRemove]) s.slides.eq(indexToRemove).remove();
                if (indexToRemove < newActiveIndex) newActiveIndex--;
                newActiveIndex = Math.max(newActiveIndex, 0);
            }

            if (s.params.loop) {
                s.createLoop();
            }

            if (!(s.params.observer && s.support.observer)) {
                s.update(true);
            }
            if (s.params.loop) {
                s.slideTo(newActiveIndex + s.loopedSlides, 0, false);
            }
            else {
                s.slideTo(newActiveIndex, 0, false);
            }

        };
        s.removeAllSlides = function () {
            var slidesIndexes = [];
            for (var i = 0; i < s.slides.length; i++) {
                slidesIndexes.push(i);
            }
            s.removeSlide(slidesIndexes);
        };


        /*=========================
          Effects
          ===========================*/
        s.effects = {
            fade: {
                setTranslate: function () {
                    for (var i = 0; i < s.slides.length; i++) {
                        var slide = s.slides.eq(i);
                        var offset = slide[0].swiperSlideOffset;
                        var tx = -offset;
                        if (!s.params.virtualTranslate) tx = tx - s.translate;
                        var ty = 0;
                        if (!s.isHorizontal()) {
                            ty = tx;
                            tx = 0;
                        }
                        var slideOpacity = s.params.fade.crossFade ?
                                Math.max(1 - Math.abs(slide[0].progress), 0) :
                                1 + Math.min(Math.max(slide[0].progress, -1), 0);
                        slide
                            .css({
                                opacity: slideOpacity
                            })
                            .transform('translate3d(' + tx + 'px, ' + ty + 'px, 0px)');

                    }

                },
                setTransition: function (duration) {
                    s.slides.transition(duration);
                    if (s.params.virtualTranslate && duration !== 0) {
                        var eventTriggered = false;
                        s.slides.transitionEnd(function () {
                            if (eventTriggered) return;
                            if (!s) return;
                            eventTriggered = true;
                            s.animating = false;
                            var triggerEvents = ['webkitTransitionEnd', 'transitionend', 'oTransitionEnd', 'MSTransitionEnd', 'msTransitionEnd'];
                            for (var i = 0; i < triggerEvents.length; i++) {
                                s.wrapper.trigger(triggerEvents[i]);
                            }
                        });
                    }
                }
            },
            flip: {
                setTranslate: function () {
                    for (var i = 0; i < s.slides.length; i++) {
                        var slide = s.slides.eq(i);
                        var progress = slide[0].progress;
                        if (s.params.flip.limitRotation) {
                            progress = Math.max(Math.min(slide[0].progress, 1), -1);
                        }
                        var offset = slide[0].swiperSlideOffset;
                        var rotate = -180 * progress,
                            rotateY = rotate,
                            rotateX = 0,
                            tx = -offset,
                            ty = 0;
                        if (!s.isHorizontal()) {
                            ty = tx;
                            tx = 0;
                            rotateX = -rotateY;
                            rotateY = 0;
                        }
                        else if (s.rtl) {
                            rotateY = -rotateY;
                        }

                        slide[0].style.zIndex = -Math.abs(Math.round(progress)) + s.slides.length;

                        if (s.params.flip.slideShadows) {
                            //Set shadows
                            var shadowBefore = s.isHorizontal() ? slide.find('.swiper-slide-shadow-left') : slide.find('.swiper-slide-shadow-top');
                            var shadowAfter = s.isHorizontal() ? slide.find('.swiper-slide-shadow-right') : slide.find('.swiper-slide-shadow-bottom');
                            if (shadowBefore.length === 0) {
                                shadowBefore = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? 'left' : 'top') + '"></div>');
                                slide.append(shadowBefore);
                            }
                            if (shadowAfter.length === 0) {
                                shadowAfter = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? 'right' : 'bottom') + '"></div>');
                                slide.append(shadowAfter);
                            }
                            if (shadowBefore.length) shadowBefore[0].style.opacity = Math.max(-progress, 0);
                            if (shadowAfter.length) shadowAfter[0].style.opacity = Math.max(progress, 0);
                        }

                        slide
                            .transform('translate3d(' + tx + 'px, ' + ty + 'px, 0px) rotateX(' + rotateX + 'deg) rotateY(' + rotateY + 'deg)');
                    }
                },
                setTransition: function (duration) {
                    s.slides.transition(duration).find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left').transition(duration);
                    if (s.params.virtualTranslate && duration !== 0) {
                        var eventTriggered = false;
                        s.slides.eq(s.activeIndex).transitionEnd(function () {
                            if (eventTriggered) return;
                            if (!s) return;
                            if (!$(this).hasClass(s.params.slideActiveClass)) return;
                            eventTriggered = true;
                            s.animating = false;
                            var triggerEvents = ['webkitTransitionEnd', 'transitionend', 'oTransitionEnd', 'MSTransitionEnd', 'msTransitionEnd'];
                            for (var i = 0; i < triggerEvents.length; i++) {
                                s.wrapper.trigger(triggerEvents[i]);
                            }
                        });
                    }
                }
            },
            cube: {
                setTranslate: function () {
                    var wrapperRotate = 0, cubeShadow;
                    if (s.params.cube.shadow) {
                        if (s.isHorizontal()) {
                            cubeShadow = s.wrapper.find('.swiper-cube-shadow');
                            if (cubeShadow.length === 0) {
                                cubeShadow = $('<div class="swiper-cube-shadow"></div>');
                                s.wrapper.append(cubeShadow);
                            }
                            cubeShadow.css({height: s.width + 'px'});
                        }
                        else {
                            cubeShadow = s.container.find('.swiper-cube-shadow');
                            if (cubeShadow.length === 0) {
                                cubeShadow = $('<div class="swiper-cube-shadow"></div>');
                                s.container.append(cubeShadow);
                            }
                        }
                    }
                    for (var i = 0; i < s.slides.length; i++) {
                        var slide = s.slides.eq(i);
                        var slideAngle = i * 90;
                        var round = Math.floor(slideAngle / 360);
                        if (s.rtl) {
                            slideAngle = -slideAngle;
                            round = Math.floor(-slideAngle / 360);
                        }
                        var progress = Math.max(Math.min(slide[0].progress, 1), -1);
                        var tx = 0, ty = 0, tz = 0;
                        if (i % 4 === 0) {
                            tx = - round * 4 * s.size;
                            tz = 0;
                        }
                        else if ((i - 1) % 4 === 0) {
                            tx = 0;
                            tz = - round * 4 * s.size;
                        }
                        else if ((i - 2) % 4 === 0) {
                            tx = s.size + round * 4 * s.size;
                            tz = s.size;
                        }
                        else if ((i - 3) % 4 === 0) {
                            tx = - s.size;
                            tz = 3 * s.size + s.size * 4 * round;
                        }
                        if (s.rtl) {
                            tx = -tx;
                        }

                        if (!s.isHorizontal()) {
                            ty = tx;
                            tx = 0;
                        }

                        var transform = 'rotateX(' + (s.isHorizontal() ? 0 : -slideAngle) + 'deg) rotateY(' + (s.isHorizontal() ? slideAngle : 0) + 'deg) translate3d(' + tx + 'px, ' + ty + 'px, ' + tz + 'px)';
                        if (progress <= 1 && progress > -1) {
                            wrapperRotate = i * 90 + progress * 90;
                            if (s.rtl) wrapperRotate = -i * 90 - progress * 90;
                        }
                        slide.transform(transform);
                        if (s.params.cube.slideShadows) {
                            //Set shadows
                            var shadowBefore = s.isHorizontal() ? slide.find('.swiper-slide-shadow-left') : slide.find('.swiper-slide-shadow-top');
                            var shadowAfter = s.isHorizontal() ? slide.find('.swiper-slide-shadow-right') : slide.find('.swiper-slide-shadow-bottom');
                            if (shadowBefore.length === 0) {
                                shadowBefore = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? 'left' : 'top') + '"></div>');
                                slide.append(shadowBefore);
                            }
                            if (shadowAfter.length === 0) {
                                shadowAfter = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? 'right' : 'bottom') + '"></div>');
                                slide.append(shadowAfter);
                            }
                            if (shadowBefore.length) shadowBefore[0].style.opacity = Math.max(-progress, 0);
                            if (shadowAfter.length) shadowAfter[0].style.opacity = Math.max(progress, 0);
                        }
                    }
                    s.wrapper.css({
                        '-webkit-transform-origin': '50% 50% -' + (s.size / 2) + 'px',
                        '-moz-transform-origin': '50% 50% -' + (s.size / 2) + 'px',
                        '-ms-transform-origin': '50% 50% -' + (s.size / 2) + 'px',
                        'transform-origin': '50% 50% -' + (s.size / 2) + 'px'
                    });

                    if (s.params.cube.shadow) {
                        if (s.isHorizontal()) {
                            cubeShadow.transform('translate3d(0px, ' + (s.width / 2 + s.params.cube.shadowOffset) + 'px, ' + (-s.width / 2) + 'px) rotateX(90deg) rotateZ(0deg) scale(' + (s.params.cube.shadowScale) + ')');
                        }
                        else {
                            var shadowAngle = Math.abs(wrapperRotate) - Math.floor(Math.abs(wrapperRotate) / 90) * 90;
                            var multiplier = 1.5 - (Math.sin(shadowAngle * 2 * Math.PI / 360) / 2 + Math.cos(shadowAngle * 2 * Math.PI / 360) / 2);
                            var scale1 = s.params.cube.shadowScale,
                                scale2 = s.params.cube.shadowScale / multiplier,
                                offset = s.params.cube.shadowOffset;
                            cubeShadow.transform('scale3d(' + scale1 + ', 1, ' + scale2 + ') translate3d(0px, ' + (s.height / 2 + offset) + 'px, ' + (-s.height / 2 / scale2) + 'px) rotateX(-90deg)');
                        }
                    }
                    var zFactor = (s.isSafari || s.isUiWebView) ? (-s.size / 2) : 0;
                    s.wrapper.transform('translate3d(0px,0,' + zFactor + 'px) rotateX(' + (s.isHorizontal() ? 0 : wrapperRotate) + 'deg) rotateY(' + (s.isHorizontal() ? -wrapperRotate : 0) + 'deg)');
                },
                setTransition: function (duration) {
                    s.slides.transition(duration).find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left').transition(duration);
                    if (s.params.cube.shadow && !s.isHorizontal()) {
                        s.container.find('.swiper-cube-shadow').transition(duration);
                    }
                }
            },
            coverflow: {
                setTranslate: function () {
                    var transform = s.translate;
                    var center = s.isHorizontal() ? -transform + s.width / 2 : -transform + s.height / 2;
                    var rotate = s.isHorizontal() ? s.params.coverflow.rotate: -s.params.coverflow.rotate;
                    var translate = s.params.coverflow.depth;
                    //Each slide offset from center
                    for (var i = 0, length = s.slides.length; i < length; i++) {
                        var slide = s.slides.eq(i);
                        var slideSize = s.slidesSizesGrid[i];
                        var slideOffset = slide[0].swiperSlideOffset;
                        var offsetMultiplier = (center - slideOffset - slideSize / 2) / slideSize * s.params.coverflow.modifier;

                        var rotateY = s.isHorizontal() ? rotate * offsetMultiplier : 0;
                        var rotateX = s.isHorizontal() ? 0 : rotate * offsetMultiplier;
                        // var rotateZ = 0
                        var translateZ = -translate * Math.abs(offsetMultiplier);

                        var translateY = s.isHorizontal() ? 0 : s.params.coverflow.stretch * (offsetMultiplier);
                        var translateX = s.isHorizontal() ? s.params.coverflow.stretch * (offsetMultiplier) : 0;

                        //Fix for ultra small values
                        if (Math.abs(translateX) < 0.001) translateX = 0;
                        if (Math.abs(translateY) < 0.001) translateY = 0;
                        if (Math.abs(translateZ) < 0.001) translateZ = 0;
                        if (Math.abs(rotateY) < 0.001) rotateY = 0;
                        if (Math.abs(rotateX) < 0.001) rotateX = 0;

                        var slideTransform = 'translate3d(' + translateX + 'px,' + translateY + 'px,' + translateZ + 'px)  rotateX(' + rotateX + 'deg) rotateY(' + rotateY + 'deg)';

                        slide.transform(slideTransform);
                        slide[0].style.zIndex = -Math.abs(Math.round(offsetMultiplier)) + 1;
                        if (s.params.coverflow.slideShadows) {
                            //Set shadows
                            var shadowBefore = s.isHorizontal() ? slide.find('.swiper-slide-shadow-left') : slide.find('.swiper-slide-shadow-top');
                            var shadowAfter = s.isHorizontal() ? slide.find('.swiper-slide-shadow-right') : slide.find('.swiper-slide-shadow-bottom');
                            if (shadowBefore.length === 0) {
                                shadowBefore = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? 'left' : 'top') + '"></div>');
                                slide.append(shadowBefore);
                            }
                            if (shadowAfter.length === 0) {
                                shadowAfter = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? 'right' : 'bottom') + '"></div>');
                                slide.append(shadowAfter);
                            }
                            if (shadowBefore.length) shadowBefore[0].style.opacity = offsetMultiplier > 0 ? offsetMultiplier : 0;
                            if (shadowAfter.length) shadowAfter[0].style.opacity = (-offsetMultiplier) > 0 ? -offsetMultiplier : 0;
                        }
                    }

                    //Set correct perspective for IE10
                    if (s.browser.ie) {
                        var ws = s.wrapper[0].style;
                        ws.perspectiveOrigin = center + 'px 50%';
                    }
                },
                setTransition: function (duration) {
                    s.slides.transition(duration).find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left').transition(duration);
                }
            }
        };

        /*=========================
          Images Lazy Loading
          ===========================*/
        s.lazy = {
            initialImageLoaded: false,
            loadImageInSlide: function (index, loadInDuplicate) {
                if (typeof index === 'undefined') return;
                if (typeof loadInDuplicate === 'undefined') loadInDuplicate = true;
                if (s.slides.length === 0) return;

                var slide = s.slides.eq(index);
                var img = slide.find('.swiper-lazy:not(.swiper-lazy-loaded):not(.swiper-lazy-loading)');
                if (slide.hasClass('swiper-lazy') && !slide.hasClass('swiper-lazy-loaded') && !slide.hasClass('swiper-lazy-loading')) {
                    img = img.add(slide[0]);
                }
                if (img.length === 0) return;

                img.each(function () {
                    var _img = $(this);
                    _img.addClass('swiper-lazy-loading');
                    var background = _img.attr('data-background');
                    var src = _img.attr('data-src'),
                        srcset = _img.attr('data-srcset');
                    s.loadImage(_img[0], (src || background), srcset, false, function () {
                        if (background) {
                            _img.css('background-image', 'url("' + background + '")');
                            _img.removeAttr('data-background');
                        }
                        else {
                            if (srcset) {
                                _img.attr('srcset', srcset);
                                _img.removeAttr('data-srcset');
                            }
                            if (src) {
                                _img.attr('src', src);
                                _img.removeAttr('data-src');
                            }

                        }

                        _img.addClass('swiper-lazy-loaded').removeClass('swiper-lazy-loading');
                        slide.find('.swiper-lazy-preloader, .preloader').remove();
                        if (s.params.loop && loadInDuplicate) {
                            var slideOriginalIndex = slide.attr('data-swiper-slide-index');
                            if (slide.hasClass(s.params.slideDuplicateClass)) {
                                var originalSlide = s.wrapper.children('[data-swiper-slide-index="' + slideOriginalIndex + '"]:not(.' + s.params.slideDuplicateClass + ')');
                                s.lazy.loadImageInSlide(originalSlide.index(), false);
                            }
                            else {
                                var duplicatedSlide = s.wrapper.children('.' + s.params.slideDuplicateClass + '[data-swiper-slide-index="' + slideOriginalIndex + '"]');
                                s.lazy.loadImageInSlide(duplicatedSlide.index(), false);
                            }
                        }
                        s.emit('onLazyImageReady', s, slide[0], _img[0]);
                    });

                    s.emit('onLazyImageLoad', s, slide[0], _img[0]);
                });

            },
            load: function () {
                var i;
                if (s.params.watchSlidesVisibility) {
                    s.wrapper.children('.' + s.params.slideVisibleClass).each(function () {
                        s.lazy.loadImageInSlide($(this).index());
                    });
                }
                else {
                    if (s.params.slidesPerView > 1) {
                        for (i = s.activeIndex; i < s.activeIndex + s.params.slidesPerView ; i++) {
                            if (s.slides[i]) s.lazy.loadImageInSlide(i);
                        }
                    }
                    else {
                        s.lazy.loadImageInSlide(s.activeIndex);
                    }
                }
                if (s.params.lazyLoadingInPrevNext) {
                    if (s.params.slidesPerView > 1 || (s.params.lazyLoadingInPrevNextAmount && s.params.lazyLoadingInPrevNextAmount > 1)) {
                        var amount = s.params.lazyLoadingInPrevNextAmount;
                        var spv = s.params.slidesPerView;
                        var maxIndex = Math.min(s.activeIndex + spv + Math.max(amount, spv), s.slides.length);
                        var minIndex = Math.max(s.activeIndex - Math.max(spv, amount), 0);
                        // Next Slides
                        for (i = s.activeIndex + s.params.slidesPerView; i < maxIndex; i++) {
                            if (s.slides[i]) s.lazy.loadImageInSlide(i);
                        }
                        // Prev Slides
                        for (i = minIndex; i < s.activeIndex ; i++) {
                            if (s.slides[i]) s.lazy.loadImageInSlide(i);
                        }
                    }
                    else {
                        var nextSlide = s.wrapper.children('.' + s.params.slideNextClass);
                        if (nextSlide.length > 0) s.lazy.loadImageInSlide(nextSlide.index());

                        var prevSlide = s.wrapper.children('.' + s.params.slidePrevClass);
                        if (prevSlide.length > 0) s.lazy.loadImageInSlide(prevSlide.index());
                    }
                }
            },
            onTransitionStart: function () {
                if (s.params.lazyLoading) {
                    if (s.params.lazyLoadingOnTransitionStart || (!s.params.lazyLoadingOnTransitionStart && !s.lazy.initialImageLoaded)) {
                        s.lazy.load();
                    }
                }
            },
            onTransitionEnd: function () {
                if (s.params.lazyLoading && !s.params.lazyLoadingOnTransitionStart) {
                    s.lazy.load();
                }
            }
        };


        /*=========================
          Scrollbar
          ===========================*/
        s.scrollbar = {
            isTouched: false,
            setDragPosition: function (e) {
                var sb = s.scrollbar;
                var x = 0, y = 0;
                var translate;
                var pointerPosition = s.isHorizontal() ?
                    ((e.type === 'touchstart' || e.type === 'touchmove') ? e.targetTouches[0].pageX : e.pageX || e.clientX) :
                    ((e.type === 'touchstart' || e.type === 'touchmove') ? e.targetTouches[0].pageY : e.pageY || e.clientY) ;
                var position = (pointerPosition) - sb.track.offset()[s.isHorizontal() ? 'left' : 'top'] - sb.dragSize / 2;
                var positionMin = -s.minTranslate() * sb.moveDivider;
                var positionMax = -s.maxTranslate() * sb.moveDivider;
                if (position < positionMin) {
                    position = positionMin;
                }
                else if (position > positionMax) {
                    position = positionMax;
                }
                position = -position / sb.moveDivider;
                s.updateProgress(position);
                s.setWrapperTranslate(position, true);
            },
            dragStart: function (e) {
                var sb = s.scrollbar;
                sb.isTouched = true;
                e.preventDefault();
                e.stopPropagation();

                sb.setDragPosition(e);
                clearTimeout(sb.dragTimeout);

                sb.track.transition(0);
                if (s.params.scrollbarHide) {
                    sb.track.css('opacity', 1);
                }
                s.wrapper.transition(100);
                sb.drag.transition(100);
                s.emit('onScrollbarDragStart', s);
            },
            dragMove: function (e) {
                var sb = s.scrollbar;
                if (!sb.isTouched) return;
                if (e.preventDefault) e.preventDefault();
                else e.returnValue = false;
                sb.setDragPosition(e);
                s.wrapper.transition(0);
                sb.track.transition(0);
                sb.drag.transition(0);
                s.emit('onScrollbarDragMove', s);
            },
            dragEnd: function (e) {
                var sb = s.scrollbar;
                if (!sb.isTouched) return;
                sb.isTouched = false;
                if (s.params.scrollbarHide) {
                    clearTimeout(sb.dragTimeout);
                    sb.dragTimeout = setTimeout(function () {
                        sb.track.css('opacity', 0);
                        sb.track.transition(400);
                    }, 1000);

                }
                s.emit('onScrollbarDragEnd', s);
                if (s.params.scrollbarSnapOnRelease) {
                    s.slideReset();
                }
            },
            enableDraggable: function () {
                var sb = s.scrollbar;
                var target = s.support.touch ? sb.track : document;
                $(sb.track).on(s.touchEvents.start, sb.dragStart);
                $(target).on(s.touchEvents.move, sb.dragMove);
                $(target).on(s.touchEvents.end, sb.dragEnd);
            },
            disableDraggable: function () {
                var sb = s.scrollbar;
                var target = s.support.touch ? sb.track : document;
                $(sb.track).off(s.touchEvents.start, sb.dragStart);
                $(target).off(s.touchEvents.move, sb.dragMove);
                $(target).off(s.touchEvents.end, sb.dragEnd);
            },
            set: function () {
                if (!s.params.scrollbar) return;
                var sb = s.scrollbar;
                sb.track = $(s.params.scrollbar);
                if (s.params.uniqueNavElements && typeof s.params.scrollbar === 'string' && sb.track.length > 1 && s.container.find(s.params.scrollbar).length === 1) {
                    sb.track = s.container.find(s.params.scrollbar);
                }
                sb.drag = sb.track.find('.swiper-scrollbar-drag');
                if (sb.drag.length === 0) {
                    sb.drag = $('<div class="swiper-scrollbar-drag"></div>');
                    sb.track.append(sb.drag);
                }
                sb.drag[0].style.width = '';
                sb.drag[0].style.height = '';
                sb.trackSize = s.isHorizontal() ? sb.track[0].offsetWidth : sb.track[0].offsetHeight;

                sb.divider = s.size / s.virtualSize;
                sb.moveDivider = sb.divider * (sb.trackSize / s.size);
                sb.dragSize = sb.trackSize * sb.divider;

                if (s.isHorizontal()) {
                    sb.drag[0].style.width = sb.dragSize + 'px';
                }
                else {
                    sb.drag[0].style.height = sb.dragSize + 'px';
                }

                if (sb.divider >= 1) {
                    sb.track[0].style.display = 'none';
                }
                else {
                    sb.track[0].style.display = '';
                }
                if (s.params.scrollbarHide) {
                    sb.track[0].style.opacity = 0;
                }
            },
            setTranslate: function () {
                if (!s.params.scrollbar) return;
                var diff;
                var sb = s.scrollbar;
                var translate = s.translate || 0;
                var newPos;

                var newSize = sb.dragSize;
                newPos = (sb.trackSize - sb.dragSize) * s.progress;
                if (s.rtl && s.isHorizontal()) {
                    newPos = -newPos;
                    if (newPos > 0) {
                        newSize = sb.dragSize - newPos;
                        newPos = 0;
                    }
                    else if (-newPos + sb.dragSize > sb.trackSize) {
                        newSize = sb.trackSize + newPos;
                    }
                }
                else {
                    if (newPos < 0) {
                        newSize = sb.dragSize + newPos;
                        newPos = 0;
                    }
                    else if (newPos + sb.dragSize > sb.trackSize) {
                        newSize = sb.trackSize - newPos;
                    }
                }
                if (s.isHorizontal()) {
                    if (s.support.transforms3d) {
                        sb.drag.transform('translate3d(' + (newPos) + 'px, 0, 0)');
                    }
                    else {
                        sb.drag.transform('translateX(' + (newPos) + 'px)');
                    }
                    sb.drag[0].style.width = newSize + 'px';
                }
                else {
                    if (s.support.transforms3d) {
                        sb.drag.transform('translate3d(0px, ' + (newPos) + 'px, 0)');
                    }
                    else {
                        sb.drag.transform('translateY(' + (newPos) + 'px)');
                    }
                    sb.drag[0].style.height = newSize + 'px';
                }
                if (s.params.scrollbarHide) {
                    clearTimeout(sb.timeout);
                    sb.track[0].style.opacity = 1;
                    sb.timeout = setTimeout(function () {
                        sb.track[0].style.opacity = 0;
                        sb.track.transition(400);
                    }, 1000);
                }
            },
            setTransition: function (duration) {
                if (!s.params.scrollbar) return;
                s.scrollbar.drag.transition(duration);
            }
        };

        /*=========================
          Controller
          ===========================*/
        s.controller = {
            LinearSpline: function (x, y) {
                this.x = x;
                this.y = y;
                this.lastIndex = x.length - 1;
                // Given an x value (x2), return the expected y2 value:
                // (x1,y1) is the known point before given value,
                // (x3,y3) is the known point after given value.
                var i1, i3;
                var l = this.x.length;

                this.interpolate = function (x2) {
                    if (!x2) return 0;

                    // Get the indexes of x1 and x3 (the array indexes before and after given x2):
                    i3 = binarySearch(this.x, x2);
                    i1 = i3 - 1;

                    // We have our indexes i1 & i3, so we can calculate already:
                    // y2 := ((x2−x1) × (y3−y1)) ÷ (x3−x1) + y1
                    return ((x2 - this.x[i1]) * (this.y[i3] - this.y[i1])) / (this.x[i3] - this.x[i1]) + this.y[i1];
                };

                var binarySearch = (function() {
                    var maxIndex, minIndex, guess;
                    return function(array, val) {
                        minIndex = -1;
                        maxIndex = array.length;
                        while (maxIndex - minIndex > 1)
                            if (array[guess = maxIndex + minIndex >> 1] <= val) {
                                minIndex = guess;
                            } else {
                                maxIndex = guess;
                            }
                        return maxIndex;
                    };
                })();
            },
            //xxx: for now i will just save one spline function to to
            getInterpolateFunction: function(c){
                if(!s.controller.spline) s.controller.spline = s.params.loop ?
                    new s.controller.LinearSpline(s.slidesGrid, c.slidesGrid) :
                    new s.controller.LinearSpline(s.snapGrid, c.snapGrid);
            },
            setTranslate: function (translate, byController) {
               var controlled = s.params.control;
               var multiplier, controlledTranslate;
               function setControlledTranslate(c) {
                    // this will create an Interpolate function based on the snapGrids
                    // x is the Grid of the scrolled scroller and y will be the controlled scroller
                    // it makes sense to create this only once and recall it for the interpolation
                    // the function does a lot of value caching for performance
                    translate = c.rtl && c.params.direction === 'horizontal' ? -s.translate : s.translate;
                    if (s.params.controlBy === 'slide') {
                        s.controller.getInterpolateFunction(c);
                        // i am not sure why the values have to be multiplicated this way, tried to invert the snapGrid
                        // but it did not work out
                        controlledTranslate = -s.controller.spline.interpolate(-translate);
                    }

                    if(!controlledTranslate || s.params.controlBy === 'container'){
                        multiplier = (c.maxTranslate() - c.minTranslate()) / (s.maxTranslate() - s.minTranslate());
                        controlledTranslate = (translate - s.minTranslate()) * multiplier + c.minTranslate();
                    }

                    if (s.params.controlInverse) {
                        controlledTranslate = c.maxTranslate() - controlledTranslate;
                    }
                    c.updateProgress(controlledTranslate);
                    c.setWrapperTranslate(controlledTranslate, false, s);
                    c.updateActiveIndex();
               }
               if (s.isArray(controlled)) {
                   for (var i = 0; i < controlled.length; i++) {
                       if (controlled[i] !== byController && controlled[i] instanceof Swiper) {
                           setControlledTranslate(controlled[i]);
                       }
                   }
               }
               else if (controlled instanceof Swiper && byController !== controlled) {

                   setControlledTranslate(controlled);
               }
            },
            setTransition: function (duration, byController) {
                var controlled = s.params.control;
                var i;
                function setControlledTransition(c) {
                    c.setWrapperTransition(duration, s);
                    if (duration !== 0) {
                        c.onTransitionStart();
                        c.wrapper.transitionEnd(function(){
                            if (!controlled) return;
                            if (c.params.loop && s.params.controlBy === 'slide') {
                                c.fixLoop();
                            }
                            c.onTransitionEnd();

                        });
                    }
                }
                if (s.isArray(controlled)) {
                    for (i = 0; i < controlled.length; i++) {
                        if (controlled[i] !== byController && controlled[i] instanceof Swiper) {
                            setControlledTransition(controlled[i]);
                        }
                    }
                }
                else if (controlled instanceof Swiper && byController !== controlled) {
                    setControlledTransition(controlled);
                }
            }
        };

        /*=========================
          Hash Navigation
          ===========================*/
        s.hashnav = {
            init: function () {
                if (!s.params.hashnav) return;
                s.hashnav.initialized = true;
                var hash = document.location.hash.replace('#', '');
                if (!hash) return;
                var speed = 0;
                for (var i = 0, length = s.slides.length; i < length; i++) {
                    var slide = s.slides.eq(i);
                    var slideHash = slide.attr('data-hash');
                    if (slideHash === hash && !slide.hasClass(s.params.slideDuplicateClass)) {
                        var index = slide.index();
                        s.slideTo(index, speed, s.params.runCallbacksOnInit, true);
                    }
                }
            },
            setHash: function () {
                if (!s.hashnav.initialized || !s.params.hashnav) return;
                document.location.hash = s.slides.eq(s.activeIndex).attr('data-hash') || '';
            }
        };

        /*=========================
          Keyboard Control
          ===========================*/
        function handleKeyboard(e) {
            if (e.originalEvent) e = e.originalEvent; //jquery fix
            var kc = e.keyCode || e.charCode;
            // Directions locks
            if (!s.params.allowSwipeToNext && (s.isHorizontal() && kc === 39 || !s.isHorizontal() && kc === 40)) {
                return false;
            }
            if (!s.params.allowSwipeToPrev && (s.isHorizontal() && kc === 37 || !s.isHorizontal() && kc === 38)) {
                return false;
            }
            if (e.shiftKey || e.altKey || e.ctrlKey || e.metaKey) {
                return;
            }
            if (document.activeElement && document.activeElement.nodeName && (document.activeElement.nodeName.toLowerCase() === 'input' || document.activeElement.nodeName.toLowerCase() === 'textarea')) {
                return;
            }
            if (kc === 37 || kc === 39 || kc === 38 || kc === 40) {
                var inView = false;
                //Check that swiper should be inside of visible area of window
                if (s.container.parents('.swiper-slide').length > 0 && s.container.parents('.swiper-slide-active').length === 0) {
                    return;
                }
                var windowScroll = {
                    left: window.pageXOffset,
                    top: window.pageYOffset
                };
                var windowWidth = window.innerWidth;
                var windowHeight = window.innerHeight;
                var swiperOffset = s.container.offset();
                if (s.rtl) swiperOffset.left = swiperOffset.left - s.container[0].scrollLeft;
                var swiperCoord = [
                    [swiperOffset.left, swiperOffset.top],
                    [swiperOffset.left + s.width, swiperOffset.top],
                    [swiperOffset.left, swiperOffset.top + s.height],
                    [swiperOffset.left + s.width, swiperOffset.top + s.height]
                ];
                for (var i = 0; i < swiperCoord.length; i++) {
                    var point = swiperCoord[i];
                    if (
                        point[0] >= windowScroll.left && point[0] <= windowScroll.left + windowWidth &&
                        point[1] >= windowScroll.top && point[1] <= windowScroll.top + windowHeight
                    ) {
                        inView = true;
                    }

                }
                if (!inView) return;
            }
            if (s.isHorizontal()) {
                if (kc === 37 || kc === 39) {
                    if (e.preventDefault) e.preventDefault();
                    else e.returnValue = false;
                }
                if ((kc === 39 && !s.rtl) || (kc === 37 && s.rtl)) s.slideNext();
                if ((kc === 37 && !s.rtl) || (kc === 39 && s.rtl)) s.slidePrev();
            }
            else {
                if (kc === 38 || kc === 40) {
                    if (e.preventDefault) e.preventDefault();
                    else e.returnValue = false;
                }
                if (kc === 40) s.slideNext();
                if (kc === 38) s.slidePrev();
            }
        }
        s.disableKeyboardControl = function () {
            s.params.keyboardControl = false;
            $(document).off('keydown', handleKeyboard);
        };
        s.enableKeyboardControl = function () {
            s.params.keyboardControl = true;
            $(document).on('keydown', handleKeyboard);
        };


        /*=========================
          Mousewheel Control
          ===========================*/
        s.mousewheel = {
            event: false,
            lastScrollTime: (new window.Date()).getTime()
        };
        if (s.params.mousewheelControl) {
            try {
                new window.WheelEvent('wheel');
                s.mousewheel.event = 'wheel';
            } catch (e) {
                if (window.WheelEvent || (s.container[0] && 'wheel' in s.container[0])) {
                    s.mousewheel.event = 'wheel';
                }
            }
            if (!s.mousewheel.event && window.WheelEvent) {

            }
            if (!s.mousewheel.event && document.onmousewheel !== undefined) {
                s.mousewheel.event = 'mousewheel';
            }
            if (!s.mousewheel.event) {
                s.mousewheel.event = 'DOMMouseScroll';
            }
        }
        function handleMousewheel(e) {
            if (e.originalEvent) e = e.originalEvent; //jquery fix
            var we = s.mousewheel.event;
            var delta = 0;
            var rtlFactor = s.rtl ? -1 : 1;

            //WebKits
            if (we === 'mousewheel') {
                if (s.params.mousewheelForceToAxis) {
                    if (s.isHorizontal()) {
                        if (Math.abs(e.wheelDeltaX) > Math.abs(e.wheelDeltaY)) delta = e.wheelDeltaX * rtlFactor;
                        else return;
                    }
                    else {
                        if (Math.abs(e.wheelDeltaY) > Math.abs(e.wheelDeltaX)) delta = e.wheelDeltaY;
                        else return;
                    }
                }
                else {
                    delta = Math.abs(e.wheelDeltaX) > Math.abs(e.wheelDeltaY) ? - e.wheelDeltaX * rtlFactor : - e.wheelDeltaY;
                }
            }
            //Old FireFox
            else if (we === 'DOMMouseScroll') delta = -e.detail;
            //New FireFox
            else if (we === 'wheel') {
                if (s.params.mousewheelForceToAxis) {
                    if (s.isHorizontal()) {
                        if (Math.abs(e.deltaX) > Math.abs(e.deltaY)) delta = -e.deltaX * rtlFactor;
                        else return;
                    }
                    else {
                        if (Math.abs(e.deltaY) > Math.abs(e.deltaX)) delta = -e.deltaY;
                        else return;
                    }
                }
                else {
                    delta = Math.abs(e.deltaX) > Math.abs(e.deltaY) ? - e.deltaX * rtlFactor : - e.deltaY;
                }
            }
            if (delta === 0) return;

            if (s.params.mousewheelInvert) delta = -delta;

            if (!s.params.freeMode) {
                if ((new window.Date()).getTime() - s.mousewheel.lastScrollTime > 60) {
                    if (delta < 0) {
                        if ((!s.isEnd || s.params.loop) && !s.animating) s.slideNext();
                        else if (s.params.mousewheelReleaseOnEdges) return true;
                    }
                    else {
                        if ((!s.isBeginning || s.params.loop) && !s.animating) s.slidePrev();
                        else if (s.params.mousewheelReleaseOnEdges) return true;
                    }
                }
                s.mousewheel.lastScrollTime = (new window.Date()).getTime();

            }
            else {
                //Freemode or scrollContainer:
                var position = s.getWrapperTranslate() + delta * s.params.mousewheelSensitivity;
                var wasBeginning = s.isBeginning,
                    wasEnd = s.isEnd;

                if (position >= s.minTranslate()) position = s.minTranslate();
                if (position <= s.maxTranslate()) position = s.maxTranslate();

                s.setWrapperTransition(0);
                s.setWrapperTranslate(position);
                s.updateProgress();
                s.updateActiveIndex();

                if (!wasBeginning && s.isBeginning || !wasEnd && s.isEnd) {
                    s.updateClasses();
                }

                if (s.params.freeModeSticky) {
                    clearTimeout(s.mousewheel.timeout);
                    s.mousewheel.timeout = setTimeout(function () {
                        s.slideReset();
                    }, 300);
                }
                else {
                    if (s.params.lazyLoading && s.lazy) {
                        s.lazy.load();
                    }
                }

                // Return page scroll on edge positions
                if (position === 0 || position === s.maxTranslate()) return;
            }
            if (s.params.autoplay) s.stopAutoplay();

            if (e.preventDefault) e.preventDefault();
            else e.returnValue = false;
            return false;
        }
        s.disableMousewheelControl = function () {
            if (!s.mousewheel.event) return false;
            s.container.off(s.mousewheel.event, handleMousewheel);
            return true;
        };

        s.enableMousewheelControl = function () {
            if (!s.mousewheel.event) return false;
            s.container.on(s.mousewheel.event, handleMousewheel);
            return true;
        };


        /*=========================
          Parallax
          ===========================*/
        function setParallaxTransform(el, progress) {
            el = $(el);
            var p, pX, pY;
            var rtlFactor = s.rtl ? -1 : 1;

            p = el.attr('data-swiper-parallax') || '0';
            pX = el.attr('data-swiper-parallax-x');
            pY = el.attr('data-swiper-parallax-y');
            if (pX || pY) {
                pX = pX || '0';
                pY = pY || '0';
            }
            else {
                if (s.isHorizontal()) {
                    pX = p;
                    pY = '0';
                }
                else {
                    pY = p;
                    pX = '0';
                }
            }

            if ((pX).indexOf('%') >= 0) {
                pX = parseInt(pX, 10) * progress * rtlFactor + '%';
            }
            else {
                pX = pX * progress * rtlFactor + 'px' ;
            }
            if ((pY).indexOf('%') >= 0) {
                pY = parseInt(pY, 10) * progress + '%';
            }
            else {
                pY = pY * progress + 'px' ;
            }

            el.transform('translate3d(' + pX + ', ' + pY + ',0px)');
        }
        s.parallax = {
            setTranslate: function () {
                s.container.children('[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]').each(function(){
                    setParallaxTransform(this, s.progress);

                });
                s.slides.each(function () {
                    var slide = $(this);
                    slide.find('[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]').each(function () {
                        var progress = Math.min(Math.max(slide[0].progress, -1), 1);
                        setParallaxTransform(this, progress);
                    });
                });
            },
            setTransition: function (duration) {
                if (typeof duration === 'undefined') duration = s.params.speed;
                s.container.find('[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]').each(function(){
                    var el = $(this);
                    var parallaxDuration = parseInt(el.attr('data-swiper-parallax-duration'), 10) || duration;
                    if (duration === 0) parallaxDuration = 0;
                    el.transition(parallaxDuration);
                });
            }
        };


        /*=========================
          Plugins API. Collect all and init all plugins
          ===========================*/
        s._plugins = [];
        for (var plugin in s.plugins) {
            var p = s.plugins[plugin](s, s.params[plugin]);
            if (p) s._plugins.push(p);
        }
        // Method to call all plugins event/method
        s.callPlugins = function (eventName) {
            for (var i = 0; i < s._plugins.length; i++) {
                if (eventName in s._plugins[i]) {
                    s._plugins[i][eventName](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
                }
            }
        };

        /*=========================
          Events/Callbacks/Plugins Emitter
          ===========================*/
        function normalizeEventName (eventName) {
            if (eventName.indexOf('on') !== 0) {
                if (eventName[0] !== eventName[0].toUpperCase()) {
                    eventName = 'on' + eventName[0].toUpperCase() + eventName.substring(1);
                }
                else {
                    eventName = 'on' + eventName;
                }
            }
            return eventName;
        }
        s.emitterEventListeners = {

        };
        s.emit = function (eventName) {
            // Trigger callbacks
            if (s.params[eventName]) {
                s.params[eventName](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
            }
            var i;
            // Trigger events
            if (s.emitterEventListeners[eventName]) {
                for (i = 0; i < s.emitterEventListeners[eventName].length; i++) {
                    s.emitterEventListeners[eventName][i](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
                }
            }
            // Trigger plugins
            if (s.callPlugins) s.callPlugins(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
        };
        s.on = function (eventName, handler) {
            eventName = normalizeEventName(eventName);
            if (!s.emitterEventListeners[eventName]) s.emitterEventListeners[eventName] = [];
            s.emitterEventListeners[eventName].push(handler);
            return s;
        };
        s.off = function (eventName, handler) {
            var i;
            eventName = normalizeEventName(eventName);
            if (typeof handler === 'undefined') {
                // Remove all handlers for such event
                s.emitterEventListeners[eventName] = [];
                return s;
            }
            if (!s.emitterEventListeners[eventName] || s.emitterEventListeners[eventName].length === 0) return;
            for (i = 0; i < s.emitterEventListeners[eventName].length; i++) {
                if(s.emitterEventListeners[eventName][i] === handler) s.emitterEventListeners[eventName].splice(i, 1);
            }
            return s;
        };
        s.once = function (eventName, handler) {
            eventName = normalizeEventName(eventName);
            var _handler = function () {
                handler(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
                s.off(eventName, _handler);
            };
            s.on(eventName, _handler);
            return s;
        };

        // Accessibility tools
        s.a11y = {
            makeFocusable: function ($el) {
                $el.attr('tabIndex', '0');
                return $el;
            },
            addRole: function ($el, role) {
                $el.attr('role', role);
                return $el;
            },

            addLabel: function ($el, label) {
                $el.attr('aria-label', label);
                return $el;
            },

            disable: function ($el) {
                $el.attr('aria-disabled', true);
                return $el;
            },

            enable: function ($el) {
                $el.attr('aria-disabled', false);
                return $el;
            },

            onEnterKey: function (event) {
                if (event.keyCode !== 13) return;
                if ($(event.target).is(s.params.nextButton)) {
                    s.onClickNext(event);
                    if (s.isEnd) {
                        s.a11y.notify(s.params.lastSlideMessage);
                    }
                    else {
                        s.a11y.notify(s.params.nextSlideMessage);
                    }
                }
                else if ($(event.target).is(s.params.prevButton)) {
                    s.onClickPrev(event);
                    if (s.isBeginning) {
                        s.a11y.notify(s.params.firstSlideMessage);
                    }
                    else {
                        s.a11y.notify(s.params.prevSlideMessage);
                    }
                }
                if ($(event.target).is('.' + s.params.bulletClass)) {
                    $(event.target)[0].click();
                }
            },

            liveRegion: $('<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>'),

            notify: function (message) {
                var notification = s.a11y.liveRegion;
                if (notification.length === 0) return;
                notification.html('');
                notification.html(message);
            },
            init: function () {
                // Setup accessibility
                if (s.params.nextButton && s.nextButton && s.nextButton.length > 0) {
                    s.a11y.makeFocusable(s.nextButton);
                    s.a11y.addRole(s.nextButton, 'button');
                    s.a11y.addLabel(s.nextButton, s.params.nextSlideMessage);
                }
                if (s.params.prevButton && s.prevButton && s.prevButton.length > 0) {
                    s.a11y.makeFocusable(s.prevButton);
                    s.a11y.addRole(s.prevButton, 'button');
                    s.a11y.addLabel(s.prevButton, s.params.prevSlideMessage);
                }

                $(s.container).append(s.a11y.liveRegion);
            },
            initPagination: function () {
                if (s.params.pagination && s.params.paginationClickable && s.bullets && s.bullets.length) {
                    s.bullets.each(function () {
                        var bullet = $(this);
                        s.a11y.makeFocusable(bullet);
                        s.a11y.addRole(bullet, 'button');
                        s.a11y.addLabel(bullet, s.params.paginationBulletMessage.replace(/{{index}}/, bullet.index() + 1));
                    });
                }
            },
            destroy: function () {
                if (s.a11y.liveRegion && s.a11y.liveRegion.length > 0) s.a11y.liveRegion.remove();
            }
        };


        /*=========================
          Init/Destroy
          ===========================*/
        s.init = function () {
            if (s.params.loop) s.createLoop();
            s.updateContainerSize();
            s.updateSlidesSize();
            s.updatePagination();
            if (s.params.scrollbar && s.scrollbar) {
                s.scrollbar.set();
                if (s.params.scrollbarDraggable) {
                    s.scrollbar.enableDraggable();
                }
            }
            if (s.params.effect !== 'slide' && s.effects[s.params.effect]) {
                if (!s.params.loop) s.updateProgress();
                s.effects[s.params.effect].setTranslate();
            }
            if (s.params.loop) {
                s.slideTo(s.params.initialSlide + s.loopedSlides, 0, s.params.runCallbacksOnInit);
            }
            else {
                s.slideTo(s.params.initialSlide, 0, s.params.runCallbacksOnInit);
                if (s.params.initialSlide === 0) {
                    if (s.parallax && s.params.parallax) s.parallax.setTranslate();
                    if (s.lazy && s.params.lazyLoading) {
                        s.lazy.load();
                        s.lazy.initialImageLoaded = true;
                    }
                }
            }
            s.attachEvents();
            if (s.params.observer && s.support.observer) {
                s.initObservers();
            }
            if (s.params.preloadImages && !s.params.lazyLoading) {
                s.preloadImages();
            }
            if (s.params.autoplay) {
                s.startAutoplay();
            }
            if (s.params.keyboardControl) {
                if (s.enableKeyboardControl) s.enableKeyboardControl();
            }
            if (s.params.mousewheelControl) {
                if (s.enableMousewheelControl) s.enableMousewheelControl();
            }
            if (s.params.hashnav) {
                if (s.hashnav) s.hashnav.init();
            }
            if (s.params.a11y && s.a11y) s.a11y.init();
            s.emit('onInit', s);
        };

        // Cleanup dynamic styles
        s.cleanupStyles = function () {
            // Container
            s.container.removeClass(s.classNames.join(' ')).removeAttr('style');

            // Wrapper
            s.wrapper.removeAttr('style');

            // Slides
            if (s.slides && s.slides.length) {
                s.slides
                    .removeClass([
                      s.params.slideVisibleClass,
                      s.params.slideActiveClass,
                      s.params.slideNextClass,
                      s.params.slidePrevClass
                    ].join(' '))
                    .removeAttr('style')
                    .removeAttr('data-swiper-column')
                    .removeAttr('data-swiper-row');
            }

            // Pagination/Bullets
            if (s.paginationContainer && s.paginationContainer.length) {
                s.paginationContainer.removeClass(s.params.paginationHiddenClass);
            }
            if (s.bullets && s.bullets.length) {
                s.bullets.removeClass(s.params.bulletActiveClass);
            }

            // Buttons
            if (s.params.prevButton) $(s.params.prevButton).removeClass(s.params.buttonDisabledClass);
            if (s.params.nextButton) $(s.params.nextButton).removeClass(s.params.buttonDisabledClass);

            // Scrollbar
            if (s.params.scrollbar && s.scrollbar) {
                if (s.scrollbar.track && s.scrollbar.track.length) s.scrollbar.track.removeAttr('style');
                if (s.scrollbar.drag && s.scrollbar.drag.length) s.scrollbar.drag.removeAttr('style');
            }
        };

        // Destroy
        s.destroy = function (deleteInstance, cleanupStyles) {
            // Detach evebts
            s.detachEvents();
            // Stop autoplay
            s.stopAutoplay();
            // Disable draggable
            if (s.params.scrollbar && s.scrollbar) {
                if (s.params.scrollbarDraggable) {
                    s.scrollbar.disableDraggable();
                }
            }
            // Destroy loop
            if (s.params.loop) {
                s.destroyLoop();
            }
            // Cleanup styles
            if (cleanupStyles) {
                s.cleanupStyles();
            }
            // Disconnect observer
            s.disconnectObservers();
            // Disable keyboard/mousewheel
            if (s.params.keyboardControl) {
                if (s.disableKeyboardControl) s.disableKeyboardControl();
            }
            if (s.params.mousewheelControl) {
                if (s.disableMousewheelControl) s.disableMousewheelControl();
            }
            // Disable a11y
            if (s.params.a11y && s.a11y) s.a11y.destroy();
            // Destroy callback
            s.emit('onDestroy');
            // Delete instance
            if (deleteInstance !== false) s = null;
        };

        s.init();



        // Return swiper instance
        return s;
    };


    /*==================================================
        Prototype
    ====================================================*/
    Swiper.prototype = {
        isSafari: (function () {
            var ua = navigator.userAgent.toLowerCase();
            return (ua.indexOf('safari') >= 0 && ua.indexOf('chrome') < 0 && ua.indexOf('android') < 0);
        })(),
        isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(navigator.userAgent),
        isArray: function (arr) {
            return Object.prototype.toString.apply(arr) === '[object Array]';
        },
        /*==================================================
        Browser
        ====================================================*/
        browser: {
            ie: window.navigator.pointerEnabled || window.navigator.msPointerEnabled,
            ieTouch: (window.navigator.msPointerEnabled && window.navigator.msMaxTouchPoints > 1) || (window.navigator.pointerEnabled && window.navigator.maxTouchPoints > 1)
        },
        /*==================================================
        Devices
        ====================================================*/
        device: (function () {
            var ua = navigator.userAgent;
            var android = ua.match(/(Android);?[\s\/]+([\d.]+)?/);
            var ipad = ua.match(/(iPad).*OS\s([\d_]+)/);
            var ipod = ua.match(/(iPod)(.*OS\s([\d_]+))?/);
            var iphone = !ipad && ua.match(/(iPhone\sOS)\s([\d_]+)/);
            return {
                ios: ipad || iphone || ipod,
                android: android
            };
        })(),
        /*==================================================
        Feature Detection
        ====================================================*/
        support: {
            touch : (window.Modernizr && Modernizr.touch === true) || (function () {
                return !!(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch);
            })(),

            transforms3d : (window.Modernizr && Modernizr.csstransforms3d === true) || (function () {
                var div = document.createElement('div').style;
                return ('webkitPerspective' in div || 'MozPerspective' in div || 'OPerspective' in div || 'MsPerspective' in div || 'perspective' in div);
            })(),

            flexbox: (function () {
                var div = document.createElement('div').style;
                var styles = ('alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient').split(' ');
                for (var i = 0; i < styles.length; i++) {
                    if (styles[i] in div) return true;
                }
            })(),

            observer: (function () {
                return ('MutationObserver' in window || 'WebkitMutationObserver' in window);
            })()
        },
        /*==================================================
        Plugins
        ====================================================*/
        plugins: {}
    };


    /*===========================
    Dom7 Library
    ===========================*/
    var Dom7 = (function () {
        var Dom7 = function (arr) {
            var _this = this, i = 0;
            // Create array-like object
            for (i = 0; i < arr.length; i++) {
                _this[i] = arr[i];
            }
            _this.length = arr.length;
            // Return collection with methods
            return this;
        };
        var $ = function (selector, context) {
            var arr = [], i = 0;
            if (selector && !context) {
                if (selector instanceof Dom7) {
                    return selector;
                }
            }
            if (selector) {
                // String
                if (typeof selector === 'string') {
                    var els, tempParent, html = selector.trim();
                    if (html.indexOf('<') >= 0 && html.indexOf('>') >= 0) {
                        var toCreate = 'div';
                        if (html.indexOf('<li') === 0) toCreate = 'ul';
                        if (html.indexOf('<tr') === 0) toCreate = 'tbody';
                        if (html.indexOf('<td') === 0 || html.indexOf('<th') === 0) toCreate = 'tr';
                        if (html.indexOf('<tbody') === 0) toCreate = 'table';
                        if (html.indexOf('<option') === 0) toCreate = 'select';
                        tempParent = document.createElement(toCreate);
                        tempParent.innerHTML = selector;
                        for (i = 0; i < tempParent.childNodes.length; i++) {
                            arr.push(tempParent.childNodes[i]);
                        }
                    }
                    else {
                        if (!context && selector[0] === '#' && !selector.match(/[ .<>:~]/)) {
                            // Pure ID selector
                            els = [document.getElementById(selector.split('#')[1])];
                        }
                        else {
                            // Other selectors
                            els = (context || document).querySelectorAll(selector);
                        }
                        for (i = 0; i < els.length; i++) {
                            if (els[i]) arr.push(els[i]);
                        }
                    }
                }
                // Node/element
                else if (selector.nodeType || selector === window || selector === document) {
                    arr.push(selector);
                }
                //Array of elements or instance of Dom
                else if (selector.length > 0 && selector[0].nodeType) {
                    for (i = 0; i < selector.length; i++) {
                        arr.push(selector[i]);
                    }
                }
            }
            return new Dom7(arr);
        };
        Dom7.prototype = {
            // Classes and attriutes
            addClass: function (className) {
                if (typeof className === 'undefined') {
                    return this;
                }
                var classes = className.split(' ');
                for (var i = 0; i < classes.length; i++) {
                    for (var j = 0; j < this.length; j++) {
                        this[j].classList.add(classes[i]);
                    }
                }
                return this;
            },
            removeClass: function (className) {
                var classes = className.split(' ');
                for (var i = 0; i < classes.length; i++) {
                    for (var j = 0; j < this.length; j++) {
                        this[j].classList.remove(classes[i]);
                    }
                }
                return this;
            },
            hasClass: function (className) {
                if (!this[0]) return false;
                else return this[0].classList.contains(className);
            },
            toggleClass: function (className) {
                var classes = className.split(' ');
                for (var i = 0; i < classes.length; i++) {
                    for (var j = 0; j < this.length; j++) {
                        this[j].classList.toggle(classes[i]);
                    }
                }
                return this;
            },
            attr: function (attrs, value) {
                if (arguments.length === 1 && typeof attrs === 'string') {
                    // Get attr
                    if (this[0]) return this[0].getAttribute(attrs);
                    else return undefined;
                }
                else {
                    // Set attrs
                    for (var i = 0; i < this.length; i++) {
                        if (arguments.length === 2) {
                            // String
                            this[i].setAttribute(attrs, value);
                        }
                        else {
                            // Object
                            for (var attrName in attrs) {
                                this[i][attrName] = attrs[attrName];
                                this[i].setAttribute(attrName, attrs[attrName]);
                            }
                        }
                    }
                    return this;
                }
            },
            removeAttr: function (attr) {
                for (var i = 0; i < this.length; i++) {
                    this[i].removeAttribute(attr);
                }
                return this;
            },
            data: function (key, value) {
                if (typeof value === 'undefined') {
                    // Get value
                    if (this[0]) {
                        var dataKey = this[0].getAttribute('data-' + key);
                        if (dataKey) return dataKey;
                        else if (this[0].dom7ElementDataStorage && (key in this[0].dom7ElementDataStorage)) return this[0].dom7ElementDataStorage[key];
                        else return undefined;
                    }
                    else return undefined;
                }
                else {
                    // Set value
                    for (var i = 0; i < this.length; i++) {
                        var el = this[i];
                        if (!el.dom7ElementDataStorage) el.dom7ElementDataStorage = {};
                        el.dom7ElementDataStorage[key] = value;
                    }
                    return this;
                }
            },
            /**
             * Gusto MOD
             * @param transform
             * @param x
             * @param y
             * @param z
             * @returns {Dom7}
             */
            // Transforms
            transform : function (transform,x,y,z) {
                for (var i = 0; i < this.length; i++) {
                    var elStyle = this[i].style;
                    var $this = this;


                    if($('body').attr('data-swiper-transform') == 'gsap'){
                        TweenMax.to($this[i],.6,{ x : x,y : y,z : z});
                    }else{
                        elStyle.webkitTransform = elStyle.MsTransform = elStyle.msTransform = elStyle.MozTransform = elStyle.OTransform = elStyle.transform = transform;
                    }
                }
                return this;
            },
            /**
             * Gusto MOD
             * @param duration
             * @returns {Dom7}
             */
            transition: function (duration) {
                if (typeof duration !== 'string') {
                    duration = duration + 'ms';
                }
                for (var i = 0; i < this.length; i++) {
                    var elStyle = this[i].style;
                    if($('body').data('swiper-transform') != 'gsap'){
                        elStyle.webkitTransitionDuration = elStyle.MsTransitionDuration = elStyle.msTransitionDuration = elStyle.MozTransitionDuration = elStyle.OTransitionDuration = elStyle.transitionDuration = duration;
                    }
                }
                return this;
            },
            //Events
            on: function (eventName, targetSelector, listener, capture) {
                function handleLiveEvent(e) {
                    var target = e.target;
                    if ($(target).is(targetSelector)) listener.call(target, e);
                    else {
                        var parents = $(target).parents();
                        for (var k = 0; k < parents.length; k++) {
                            if ($(parents[k]).is(targetSelector)) listener.call(parents[k], e);
                        }
                    }
                }
                var events = eventName.split(' ');
                var i, j;
                for (i = 0; i < this.length; i++) {
                    if (typeof targetSelector === 'function' || targetSelector === false) {
                        // Usual events
                        if (typeof targetSelector === 'function') {
                            listener = arguments[1];
                            capture = arguments[2] || false;
                        }
                        for (j = 0; j < events.length; j++) {
                            this[i].addEventListener(events[j], listener, capture);
                        }
                    }
                    else {
                        //Live events
                        for (j = 0; j < events.length; j++) {
                            if (!this[i].dom7LiveListeners) this[i].dom7LiveListeners = [];
                            this[i].dom7LiveListeners.push({listener: listener, liveListener: handleLiveEvent});
                            this[i].addEventListener(events[j], handleLiveEvent, capture);
                        }
                    }
                }

                return this;
            },
            off: function (eventName, targetSelector, listener, capture) {
                var events = eventName.split(' ');
                for (var i = 0; i < events.length; i++) {
                    for (var j = 0; j < this.length; j++) {
                        if (typeof targetSelector === 'function' || targetSelector === false) {
                            // Usual events
                            if (typeof targetSelector === 'function') {
                                listener = arguments[1];
                                capture = arguments[2] || false;
                            }
                            this[j].removeEventListener(events[i], listener, capture);
                        }
                        else {
                            // Live event
                            if (this[j].dom7LiveListeners) {
                                for (var k = 0; k < this[j].dom7LiveListeners.length; k++) {
                                    if (this[j].dom7LiveListeners[k].listener === listener) {
                                        this[j].removeEventListener(events[i], this[j].dom7LiveListeners[k].liveListener, capture);
                                    }
                                }
                            }
                        }
                    }
                }
                return this;
            },
            once: function (eventName, targetSelector, listener, capture) {
                var dom = this;
                if (typeof targetSelector === 'function') {
                    targetSelector = false;
                    listener = arguments[1];
                    capture = arguments[2];
                }
                function proxy(e) {
                    listener(e);
                    dom.off(eventName, targetSelector, proxy, capture);
                }
                dom.on(eventName, targetSelector, proxy, capture);
            },
            trigger: function (eventName, eventData) {
                for (var i = 0; i < this.length; i++) {
                    var evt;
                    try {
                        evt = new window.CustomEvent(eventName, {detail: eventData, bubbles: true, cancelable: true});
                    }
                    catch (e) {
                        evt = document.createEvent('Event');
                        evt.initEvent(eventName, true, true);
                        evt.detail = eventData;
                    }
                    this[i].dispatchEvent(evt);
                }
                return this;
            },
            transitionEnd: function (callback) {
                var events = ['webkitTransitionEnd', 'transitionend', 'oTransitionEnd', 'MSTransitionEnd', 'msTransitionEnd'],
                    i, j, dom = this;
                function fireCallBack(e) {
                    /*jshint validthis:true */
                    if (e.target !== this) return;
                    callback.call(this, e);
                    for (i = 0; i < events.length; i++) {
                        dom.off(events[i], fireCallBack);
                    }
                }
                if (callback) {
                    for (i = 0; i < events.length; i++) {
                        dom.on(events[i], fireCallBack);
                    }
                }
                return this;
            },
            // Sizing/Styles
            width: function () {
                if (this[0] === window) {
                    return window.innerWidth;
                }
                else {
                    if (this.length > 0) {
                        return parseFloat(this.css('width'));
                    }
                    else {
                        return null;
                    }
                }
            },
            outerWidth: function (includeMargins) {
                if (this.length > 0) {
                    if (includeMargins)
                        return this[0].offsetWidth + parseFloat(this.css('margin-right')) + parseFloat(this.css('margin-left'));
                    else
                        return this[0].offsetWidth;
                }
                else return null;
            },
            height: function () {
                if (this[0] === window) {
                    return window.innerHeight;
                }
                else {
                    if (this.length > 0) {
                        return parseFloat(this.css('height'));
                    }
                    else {
                        return null;
                    }
                }
            },
            outerHeight: function (includeMargins) {
                if (this.length > 0) {
                    if (includeMargins)
                        return this[0].offsetHeight + parseFloat(this.css('margin-top')) + parseFloat(this.css('margin-bottom'));
                    else
                        return this[0].offsetHeight;
                }
                else return null;
            },
            offset: function () {
                if (this.length > 0) {
                    var el = this[0];
                    var box = el.getBoundingClientRect();
                    var body = document.body;
                    var clientTop  = el.clientTop  || body.clientTop  || 0;
                    var clientLeft = el.clientLeft || body.clientLeft || 0;
                    var scrollTop  = window.pageYOffset || el.scrollTop;
                    var scrollLeft = window.pageXOffset || el.scrollLeft;
                    return {
                        top: box.top  + scrollTop  - clientTop,
                        left: box.left + scrollLeft - clientLeft
                    };
                }
                else {
                    return null;
                }
            },
            css: function (props, value) {
                var i;
                if (arguments.length === 1) {
                    if (typeof props === 'string') {
                        if (this[0]) return window.getComputedStyle(this[0], null).getPropertyValue(props);
                    }
                    else {
                        for (i = 0; i < this.length; i++) {
                            for (var prop in props) {
                                this[i].style[prop] = props[prop];
                            }
                        }
                        return this;
                    }
                }
                if (arguments.length === 2 && typeof props === 'string') {
                    for (i = 0; i < this.length; i++) {
                        this[i].style[props] = value;
                    }
                    return this;
                }
                return this;
            },

            //Dom manipulation
            each: function (callback) {
                for (var i = 0; i < this.length; i++) {
                    callback.call(this[i], i, this[i]);
                }
                return this;
            },
            html: function (html) {
                if (typeof html === 'undefined') {
                    return this[0] ? this[0].innerHTML : undefined;
                }
                else {
                    for (var i = 0; i < this.length; i++) {
                        this[i].innerHTML = html;
                    }
                    return this;
                }
            },
            text: function (text) {
                if (typeof text === 'undefined') {
                    if (this[0]) {
                        return this[0].textContent.trim();
                    }
                    else return null;
                }
                else {
                    for (var i = 0; i < this.length; i++) {
                        this[i].textContent = text;
                    }
                    return this;
                }
            },
            is: function (selector) {
                if (!this[0]) return false;
                var compareWith, i;
                if (typeof selector === 'string') {
                    var el = this[0];
                    if (el === document) return selector === document;
                    if (el === window) return selector === window;

                    if (el.matches) return el.matches(selector);
                    else if (el.webkitMatchesSelector) return el.webkitMatchesSelector(selector);
                    else if (el.mozMatchesSelector) return el.mozMatchesSelector(selector);
                    else if (el.msMatchesSelector) return el.msMatchesSelector(selector);
                    else {
                        compareWith = $(selector);
                        for (i = 0; i < compareWith.length; i++) {
                            if (compareWith[i] === this[0]) return true;
                        }
                        return false;
                    }
                }
                else if (selector === document) return this[0] === document;
                else if (selector === window) return this[0] === window;
                else {
                    if (selector.nodeType || selector instanceof Dom7) {
                        compareWith = selector.nodeType ? [selector] : selector;
                        for (i = 0; i < compareWith.length; i++) {
                            if (compareWith[i] === this[0]) return true;
                        }
                        return false;
                    }
                    return false;
                }

            },
            index: function () {
                if (this[0]) {
                    var child = this[0];
                    var i = 0;
                    while ((child = child.previousSibling) !== null) {
                        if (child.nodeType === 1) i++;
                    }
                    return i;
                }
                else return undefined;
            },
            eq: function (index) {
                if (typeof index === 'undefined') return this;
                var length = this.length;
                var returnIndex;
                if (index > length - 1) {
                    return new Dom7([]);
                }
                if (index < 0) {
                    returnIndex = length + index;
                    if (returnIndex < 0) return new Dom7([]);
                    else return new Dom7([this[returnIndex]]);
                }
                return new Dom7([this[index]]);
            },
            append: function (newChild) {
                var i, j;
                for (i = 0; i < this.length; i++) {
                    if (typeof newChild === 'string') {
                        var tempDiv = document.createElement('div');
                        tempDiv.innerHTML = newChild;
                        while (tempDiv.firstChild) {
                            this[i].appendChild(tempDiv.firstChild);
                        }
                    }
                    else if (newChild instanceof Dom7) {
                        for (j = 0; j < newChild.length; j++) {
                            this[i].appendChild(newChild[j]);
                        }
                    }
                    else {
                        this[i].appendChild(newChild);
                    }
                }
                return this;
            },
            prepend: function (newChild) {
                var i, j;
                for (i = 0; i < this.length; i++) {
                    if (typeof newChild === 'string') {
                        var tempDiv = document.createElement('div');
                        tempDiv.innerHTML = newChild;
                        for (j = tempDiv.childNodes.length - 1; j >= 0; j--) {
                            this[i].insertBefore(tempDiv.childNodes[j], this[i].childNodes[0]);
                        }
                        // this[i].insertAdjacentHTML('afterbegin', newChild);
                    }
                    else if (newChild instanceof Dom7) {
                        for (j = 0; j < newChild.length; j++) {
                            this[i].insertBefore(newChild[j], this[i].childNodes[0]);
                        }
                    }
                    else {
                        this[i].insertBefore(newChild, this[i].childNodes[0]);
                    }
                }
                return this;
            },
            insertBefore: function (selector) {
                var before = $(selector);
                for (var i = 0; i < this.length; i++) {
                    if (before.length === 1) {
                        before[0].parentNode.insertBefore(this[i], before[0]);
                    }
                    else if (before.length > 1) {
                        for (var j = 0; j < before.length; j++) {
                            before[j].parentNode.insertBefore(this[i].cloneNode(true), before[j]);
                        }
                    }
                }
            },
            insertAfter: function (selector) {
                var after = $(selector);
                for (var i = 0; i < this.length; i++) {
                    if (after.length === 1) {
                        after[0].parentNode.insertBefore(this[i], after[0].nextSibling);
                    }
                    else if (after.length > 1) {
                        for (var j = 0; j < after.length; j++) {
                            after[j].parentNode.insertBefore(this[i].cloneNode(true), after[j].nextSibling);
                        }
                    }
                }
            },
            next: function (selector) {
                if (this.length > 0) {
                    if (selector) {
                        if (this[0].nextElementSibling && $(this[0].nextElementSibling).is(selector)) return new Dom7([this[0].nextElementSibling]);
                        else return new Dom7([]);
                    }
                    else {
                        if (this[0].nextElementSibling) return new Dom7([this[0].nextElementSibling]);
                        else return new Dom7([]);
                    }
                }
                else return new Dom7([]);
            },
            nextAll: function (selector) {
                var nextEls = [];
                var el = this[0];
                if (!el) return new Dom7([]);
                while (el.nextElementSibling) {
                    var next = el.nextElementSibling;
                    if (selector) {
                        if($(next).is(selector)) nextEls.push(next);
                    }
                    else nextEls.push(next);
                    el = next;
                }
                return new Dom7(nextEls);
            },
            prev: function (selector) {
                if (this.length > 0) {
                    if (selector) {
                        if (this[0].previousElementSibling && $(this[0].previousElementSibling).is(selector)) return new Dom7([this[0].previousElementSibling]);
                        else return new Dom7([]);
                    }
                    else {
                        if (this[0].previousElementSibling) return new Dom7([this[0].previousElementSibling]);
                        else return new Dom7([]);
                    }
                }
                else return new Dom7([]);
            },
            prevAll: function (selector) {
                var prevEls = [];
                var el = this[0];
                if (!el) return new Dom7([]);
                while (el.previousElementSibling) {
                    var prev = el.previousElementSibling;
                    if (selector) {
                        if($(prev).is(selector)) prevEls.push(prev);
                    }
                    else prevEls.push(prev);
                    el = prev;
                }
                return new Dom7(prevEls);
            },
            parent: function (selector) {
                var parents = [];
                for (var i = 0; i < this.length; i++) {
                    if (selector) {
                        if ($(this[i].parentNode).is(selector)) parents.push(this[i].parentNode);
                    }
                    else {
                        parents.push(this[i].parentNode);
                    }
                }
                return $($.unique(parents));
            },
            parents: function (selector) {
                var parents = [];
                for (var i = 0; i < this.length; i++) {
                    var parent = this[i].parentNode;
                    while (parent) {
                        if (selector) {
                            if ($(parent).is(selector)) parents.push(parent);
                        }
                        else {
                            parents.push(parent);
                        }
                        parent = parent.parentNode;
                    }
                }
                return $($.unique(parents));
            },
            find : function (selector) {
                var foundElements = [];
                for (var i = 0; i < this.length; i++) {
                    var found = this[i].querySelectorAll(selector);
                    for (var j = 0; j < found.length; j++) {
                        foundElements.push(found[j]);
                    }
                }
                return new Dom7(foundElements);
            },
            children: function (selector) {
                var children = [];
                for (var i = 0; i < this.length; i++) {
                    var childNodes = this[i].childNodes;

                    for (var j = 0; j < childNodes.length; j++) {
                        if (!selector) {
                            if (childNodes[j].nodeType === 1) children.push(childNodes[j]);
                        }
                        else {
                            if (childNodes[j].nodeType === 1 && $(childNodes[j]).is(selector)) children.push(childNodes[j]);
                        }
                    }
                }
                return new Dom7($.unique(children));
            },
            remove: function () {
                for (var i = 0; i < this.length; i++) {
                    if (this[i].parentNode) this[i].parentNode.removeChild(this[i]);
                }
                return this;
            },
            add: function () {
                var dom = this;
                var i, j;
                for (i = 0; i < arguments.length; i++) {
                    var toAdd = $(arguments[i]);
                    for (j = 0; j < toAdd.length; j++) {
                        dom[dom.length] = toAdd[j];
                        dom.length++;
                    }
                }
                return dom;
            }
        };
        $.fn = Dom7.prototype;
        $.unique = function (arr) {
            var unique = [];
            for (var i = 0; i < arr.length; i++) {
                if (unique.indexOf(arr[i]) === -1) unique.push(arr[i]);
            }
            return unique;
        };

        return $;
    })();


    /*===========================
     Get Dom libraries
     ===========================*/
    var swiperDomPlugins = ['jQuery', 'Zepto', 'Dom7'];
    for (var i = 0; i < swiperDomPlugins.length; i++) {
    	if (window[swiperDomPlugins[i]]) {
    		addLibraryPlugin(window[swiperDomPlugins[i]]);
    	}
    }
    // Required DOM Plugins
    var domLib;
    if (typeof Dom7 === 'undefined') {
    	domLib = window.Dom7 || window.Zepto || window.jQuery;
    }
    else {
    	domLib = Dom7;
    }

    /*===========================
    Add .swiper plugin from Dom libraries
    ===========================*/
    function addLibraryPlugin(lib) {
        lib.fn.swiper = function (params) {
            var firstInstance;
            lib(this).each(function () {
                var s = new Swiper(this, params);
                if (!firstInstance) firstInstance = s;
            });
            return firstInstance;
        };
    }

    if (domLib) {
        if (!('transitionEnd' in domLib.fn)) {
            domLib.fn.transitionEnd = function (callback) {
                var events = ['webkitTransitionEnd', 'transitionend', 'oTransitionEnd', 'MSTransitionEnd', 'msTransitionEnd'],
                    i, j, dom = this;
                function fireCallBack(e) {
                    /*jshint validthis:true */
                    if (e.target !== this) return;
                    callback.call(this, e);
                    for (i = 0; i < events.length; i++) {
                        dom.off(events[i], fireCallBack);
                    }
                }
                if (callback) {
                    for (i = 0; i < events.length; i++) {
                        dom.on(events[i], fireCallBack);
                    }
                }
                return this;
            };
        }
        if (!('transform' in domLib.fn)) {
            domLib.fn.transform = function (transform) {
                for (var i = 0; i < this.length; i++) {
                    var elStyle = this[i].style;
                    elStyle.webkitTransform = elStyle.MsTransform = elStyle.msTransform = elStyle.MozTransform = elStyle.OTransform = elStyle.transform = transform;
                }
                return this;
            };
        }
        if (!('transition' in domLib.fn)) {
            domLib.fn.transition = function (duration) {
                if (typeof duration !== 'string') {
                    duration = duration + 'ms';
                }
                for (var i = 0; i < this.length; i++) {
                    var elStyle = this[i].style;
                    elStyle.webkitTransitionDuration = elStyle.MsTransitionDuration = elStyle.msTransitionDuration = elStyle.MozTransitionDuration = elStyle.OTransitionDuration = elStyle.transitionDuration = duration;
                }
                return this;
            };
        }
    }

    window.Swiper = Swiper;
})();
/*===========================
Swiper AMD Export
===========================*/
if (typeof(module) !== 'undefined')
{
    module.exports = window.Swiper;
}
else if (typeof define === 'function' && define.amd) {
    define([], function () {
        'use strict';
        return window.Swiper;
    });
}
//# sourceMappingURL=maps/swiper.js.map
/**
 * jquery.imgpreload 1.6.2 <https://github.com/farinspace/jquery.imgpreload>
 * Copyright 2009-2014 Dimas Begunoff <http://farinspace.com>
 * License MIT <http://opensource.org/licenses/MIT>
 */
"undefined"!=typeof jQuery&&!function(a){"use strict";a.imgpreload=function(b,c){c=a.extend({},a.fn.imgpreload.defaults,c instanceof Function?{all:c}:c),"string"==typeof b&&(b=[b]);var d=[];a.each(b,function(e,f){var g=new Image,h=f,i=g;"string"!=typeof f&&(h=a(f).attr("src")||a(f).css("background-image").replace(/^url\((?:"|')?(.*)(?:'|")?\)$/gm,"$1"),i=f),a(g).bind("load error",function(e){d.push(i),a.data(i,"loaded","error"==e.type?!1:!0),c.each instanceof Function&&c.each.call(i,d.slice(0)),d.length>=b.length&&c.all instanceof Function&&c.all.call(d),a(this).unbind("load error")}),g.src=h})},a.fn.imgpreload=function(b){return a.imgpreload(this,b),this},a.fn.imgpreload.defaults={each:null,all:null}}(jQuery);/**
 * Conserve aspect ratio of the orignal region. Useful when shrinking/enlarging
 * images to fit into a certain area.
 *
 * @param {Number} srcWidth Source area width
 * @param {Number} srcHeight Source area height
 * @param {Number} maxWidth Fittable area maximum available width
 * @param {Number} maxHeight Fittable area maximum available height
 * @return {Object} { width, heigth }
 *
 */
function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

    var ratio = [maxWidth / srcWidth, maxHeight / srcHeight ];
    ratio = Math.min(ratio[0], ratio[1]);

    return { width:srcWidth*ratio, height:srcHeight*ratio };
}

Modernizr.addTest('mix-blend-mode', function(){
    return Modernizr.testProp('mixBlendMode');
});



function resize(){
    $('[data-module="matrix"][data-type="alternate"] .row').width($(window).width()).css('overflow','hidden');
}

$( window ).resize(function() {
    resize();
});

$( document ).ready(function() {
    $('html,body').scrollTop(0);
    $(document).scrollTop(0);
    $(window).scrollTop(0);
    resize();
});


if(Modernizr.safari){
    window.onpageshow = function(event) {
        if (event.persisted) {
            window.location.reload()
        }
    };
}

$(window).on('orientationchange', function(e) {

    window.location.reload();
});
